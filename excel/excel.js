 sap.ui.define([
 	'sap/ui/model/json/JSONModel',
 	'sap/ui/export/library',
 	'sap/ui/export/Spreadsheet',
 	'sap/m/MessageToast',
 	'sap/base/Log'
 ], function (JSONModel, exportLibrary, Spreadsheet, MessageToast, Log) {
 	"use strict";
 	var EdmType = exportLibrary.EdmType;
 	return {

 		_downloadExcel: function (name, table, tableData) {
 			var aCols, oSettings, oSheet,
 				filteredData = [],
 				dial = new sap.m.BusyDialog();
 			aCols = this.createColumnConfig(table, tableData);

 			var fileName = "";
 			switch (name) {
 			case "SPO":
 				fileName = "seasonalplantooffer.xlsx";
 				break;
 			case "OLS":
 				fileName = "overviewActualDate.xlsx";
 				break;
 			case "OBU":
 				fileName = "overviewQuoted.xlsx";
 				break;
 			case "OMONTHLY":
 				fileName = "overviewMonthly.xlsx";
 				break;
 			case "SupplySourcing":
 				fileName = "supplySourcing.xlsx";
 				break;
 			case "SupplyActualDate":
 				fileName = "supplyActualDate.xlsx";
 				break;
 			case "LSQuoted":
 				fileName = "KPIs_L_S_Quoted.xlsx";
 				break;
 			case "LSActualDate":
 				fileName = "KPIs_L_SActualDate.xlsx";
 				break;
 			case "QUOTE_MANAGEMENT":
 				fileName = "QuoteManagement.xlsx";
 				break;
 			case "PRODUCTION_LIST":
 				fileName = "ProductionList.xlsx";
 				break;
 			case "SALES_CONTRACTS":
 				fileName = "SalesContracts.xlsx";
 				break;
 			case "PRODUCTION_PLANNING_CURRENT":
 				fileName = "ProductionPlanningCurrent.xlsx";
 				break;
 			case "PRODUCTION_PLANNING_HISTORICAL":
 				fileName = "ProductionPlanningHistorical.xlsx";
 				break;
 			case "PURCHASE_PLANNING_CURRENT":
 				fileName = "ProductionPlanningCurrent.xlsx";
 				break;
 			case "PURCHASE_PLANNING_HISTORICAL":
 				fileName = "ProductionPlanningHistorical.xlsx";
 				break;
 			case "SEASON_SUMMARY":
 				fileName = "SeasonSummary.xlsx";
 				break;
 			case "PURCHASE_CONTRACTS":
 				fileName = "PurchaseContracts.xlsx";
 				break;
 			case "DM":
 				fileName = "DataMaintenance.xlsx";
 				break;
 			default:
 			}

 			filteredData = tableData.filter(function (m) {
 				return m["isSum"] !== "X";
 			});

 			oSettings = {
 				count: 150000,
 				workbook: {
 					columns: aCols.aCols,
 					context: {
 						title: 'Seasonal Plan to Offer'
 					}
 				},
 				dataSource: filteredData,
 				fileName: fileName
 			};

 			oSheet = new Spreadsheet(oSettings);

 			oSheet.onprogress = function (iValue) {};
 			oSheet.build()
 				.then(function () {
 					MessageToast.show('Spreadsheet export has finished');
 				})
 				.finally(oSheet.destroy);

 		},

 		createColumnConfig: function (oTable, tableData) {
 			var that = this,
 				aCols = [],
 				aColumns = oTable.getColumns(),
 				columnText = "",
 				type = "",
 				property = "",
 				desc = "",
 				excelNumberType = "";

 			aColumns.forEach(function (oColumn) {
 				if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
 					if (oColumn.mSkipPropagation.template) {
 						columnText = oColumn.getLabel().getText();
 						if (oColumn.getLabel().getCustomData().length) {

 							type = oColumn.getLabel().data("type");
 							property = oColumn.getLabel().data("path");
 							desc = oColumn.getLabel().data("desc");
 							excelNumberType = oColumn.getLabel().data("excelNumberType");

 							aCols = that._prepareRows(columnText, property, desc, type, aCols, excelNumberType);

 						}
 					}
 				}
 			});

 			return {
 				aCols: aCols
 			};
 		},

 		_prepareRows: function (columnText, property, desc, type, aCols, excelNumberType) {

 			switch (type) {
 			case "textType":
 				if (desc === undefined || desc === null) {
 					aCols.push({
 						property: property,
 						label: columnText,
 						width: 20,
 						type: EdmType.String,
 						wrap: true,
 						textAlign: "Center"
 					});
 				} else {
 					if (property === "ADEXMAT__DSUBCHAR" || property === "DSUBCHAR" || property === "ADEXMAT__RMW11ZP16" || property === "RMW11ZP16" ||
 						property === "DPRODTYPE" || property === "A0MATERIAL__RMW11ZP16" || property === "PRODUCT_TYPE" || property ===
 						"MATERIAL__RMW11ZP16" || property === "ARMW11ZP16" || property === "MATERIAL" || property === "CUSTOMER__DOEBKUNDE"  
 						|| property === "CUSTOMER" || property === "DCOLLNR" || property === "ADEXMAT__DCOLLNR" || property === "ADEXMAT__DCOLLNR"  
 					    || property === "A0CUSTOMER__DOEBKUNDE"  || property === "A0CUSTOMER"  || property === "A0MATERIAL" || property === "MATNR" 
 					    || property === "ARMW21ZH02"  || property === "ADFRTSPC1" || property === "ADFRTSPC2" || property === "ADFRTSPC3"
 					    
 						
 					) {
 						aCols.push({
 							property: property,
 							label: columnText,
 							width: 20,
 							type: EdmType.String,
 							wrap: true,
 							textAlign: "Center"
 						});
 						aCols.push({
 							property: desc,
 							label: columnText + " Desc.",
 							width: 20,
 							type: EdmType.String,
 							wrap: true,
 							textAlign: "Center"
 						});
 					} else
 						aCols.push({
 							label: columnText,
 							property: [property, desc],
 							type: EdmType.String,
 							width: 20,
 							wrap: true,
 							textAlign: "Center",
 							template: '{0} - {1}'
 						});
 				}
 				break;

 			case "numberType":
 				if (excelNumberType === "perKG") {
 					aCols.push({
 						property: property,
 						label: columnText,
 						type: EdmType.Number,
 						width: 18,
 						delimiter: true,
 						scale: 2,
 						groupingSeparator: ".",
 						decimalSeparator: ","
 					});
 				} else {
 					if (excelNumberType === "percent") {
 						aCols.push({
 							property: property,
 							label: columnText,
 							type: EdmType.Number,
 							width: 18,
 							delimiter: true,
 							scale: 1
 						});
 					} else {
 						aCols.push({
 							property: property,
 							label: columnText,
 							type: EdmType.Number,
 							width: 18,
 							delimiter: true,
 							groupingSeparator: ".",
 							decimalSeparator: ","
 						});
 					}
 				}

 				break;

 			case "Date":
 				aCols.push({
 					label: columnText,
 					type: EdmType.Date,
 					property: property,
 					width: 25,
 					wrap: true,
 					textAlign: "Center",
 					inputFormat: 'yyyymmdd'
 				});

 				break;
 			default:
 			}
 			return aCols;
 		}
 	};
 });