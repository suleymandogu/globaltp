sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/doehler/Z_GTP/model/models",
	"sap/ui/core/routing/HashChanger",
		"com/doehler/Z_GTP/controller/BaseController"
], function (UIComponent, Device, models,HashChanger,BaseController) {
	"use strict";

	return UIComponent.extend("com.doehler.Z_GTP.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// reset the routing hash
			HashChanger.getInstance().replaceHash("");
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			var modelItems = ["panelM", "seasonPanelM", "tableM", "supplyTableM", "demandTableM", "globalM", "seasonTableM", "ColumnMonthM",
				"supplyPanelM", "demandPanelM", "quotePanelM", "quoteTableM", "CreateGTPquoteTableM", "createSAPquoteTableM",
				"productionPlanPanelM", "productionPlanTableM", "purchasePlanPanelM", "purchasePlanTableM", "productionListPanelM",
				"productionListTableM", "duplicateQuoteTableM", "seasonSummaryPanelM", "seasonSummaryTableM", "purchaseContractsPanelM",
				"purchaseContractsTableM", "salesContractsPanelM", "salesContractsTableM", "commentTableM","dataMaintenancePanelM","dataMaintenanceTableM"
			];
			this._bindModelsToApp(modelItems);

			var organic = [{
				"ORGANIC_TEXT": "no",
				"ORGANIC": "CUST-X.0000000001563"
			}, {
				"ORGANIC_TEXT": "yes",
				"ORGANIC": "CUST-X.0000000001987"
			}];
			sap.ui.getCore().getModel("globalM").setProperty("/ORGANIC", organic);

			organic.unshift({
				"ORGANIC_TEXT": "",
				"ORGANIC": ""
			});
			sap.ui.getCore().getModel("globalM").setProperty("/ORGANIC_COMMENT", organic);

			var halal = [{
				"HALAL": "CUST-X.0000000001563",
				"HALAL_TEXT": "no"
			}, {
				"HALAL": "CUST-X.0000000001562",
				"HALAL_TEXT": "yes"
			}];
			sap.ui.getCore().getModel("globalM").setProperty("/HALAL", halal);

			halal.unshift({
				"HALAL_TEXT": "",
				"HALAL": ""
			});
			sap.ui.getCore().getModel("globalM").setProperty("/HALAL_COMMENT", halal);

			var kosher = [{
				"KOSHER": "CUST-X.0000000001563",
				"KOSHER_TEXT": "no"
			}, {
				"KOSHER": "CUST-X.0000000001562",
				"KOSHER_TEXT": "yes"
			}];
			sap.ui.getCore().getModel("globalM").setProperty("/KOSHER", kosher);

			kosher.unshift({
				"KOSHER_TEXT": "",
				"KOSHER": ""
			});
			sap.ui.getCore().getModel("globalM").setProperty("/KOSHER_COMMENT", kosher);
			
	

			models.getUser();
			models.getBWPorts();
			models.getFSTexts();
			models.handoverFilters();
			models.setInitialFields();
			models.getAllComments();
		},

		_bindModelsToApp: function (modelItems) {
			var that = this;
			modelItems.forEach(function (item) {
				that.setModel(models.createJSONModel(), item);
				sap.ui.getCore().setModel(that.getModel(item), item);
			});
		}
	});
});