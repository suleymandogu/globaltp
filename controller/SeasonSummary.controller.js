sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/formatter",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	'sap/ui/core/Fragment',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, formatter, dbcontext, dbcontext2, excel, Fragment, JSONModel, MessageToast,
	MessageBox, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.SeasonSummary", {
		formatter: formatter,
		onInit: function () {
			this._setButtonsPressed("seasonalPlanToOfferId");
			this._tableId = this.getView().byId("seasonSummaryTableId");
			this._tableIdText = "seasonSummaryTableId";
			this.bDialog = new sap.m.BusyDialog();
			this.columnfilter = {};
			//panel Model
			this._panelModel = this.getOwnerComponent().getModel("seasonSummaryPanelM");
			this._globalModel = this.getOwnerComponent().getModel("globalM");
			this._globalModel.setProperty("/seasonText", "");
			this._seasonSummaryTableM = this.getOwnerComponent().getModel("seasonSummaryTableM");

			//oDataModel
			this._seasonalODataModel = this.getOwnerComponent().getModel("SEASONAL_MODEL");
			sap.ui.getCore().setModel(this._panelModel, "seasonSummaryPanelM");
			this._initializePanelModel();
			sap.ui.getCore().getModel("globalM").setProperty("/isSeasonSummary", false);
			this.getRouter().getRoute("SeasonSummary").attachPatternMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("seasonalPlanToOfferId");
			if (sap.ui.getCore().getModel("globalM").getProperty("/isSeasonSummary") === true)
				this.createToken();

		},

		onSort: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._seasonSummaryTableM.getProperty("/tableRows");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this.getView().byId("seasonSummaryTableId").getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);

			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._seasonSummaryTableM);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("seasonSummaryTableId").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._seasonSummaryTableM.setProperty("/tableRows", notSumRow);
			// 	that.getView().byId("seasonSummaryTableId").bindRows("seasonSummaryTableM>/tableRows");

			// }, 100)
		},

		onFilterTable: function (oEvent) {
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		onValueHelpInit: function (oEvent) {
			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));

		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/FS1", "");
			this._panelModel.setProperty("/FS2", "");
			this._panelModel.setProperty("/FS3", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/COUNTRYOFPLANT", "");
			this._panelModel.setProperty("/CERTIFICATE", "");
			this._panelModel.setProperty("/SEASON", "");
			this._panelModel.setProperty("/PLANT_CLUSTER", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
			/**************1*******************/
			this._panelModel.setProperty("/VOLUME_KG", 0);
			/******************2************************/
			this._panelModel.setProperty("/PLANNED_KG", 0);
			this._panelModel.setProperty("/QUOTED_KG", 0);
			this._panelModel.setProperty("/PENDING_QUOTE", 0);
			/******************3********************/
			this._panelModel.setProperty("/TARGET_KG", 0);
			this._panelModel.setProperty("/REALIZED_KG", 0);
			this._panelModel.setProperty("/AVERAGE_PRICE_EUR_KG", 0);

		},

		onGo: function () {
			var that = this,
				data = [],
				headerKpis = [];
			var seasonInput = this._panelModel.getData()["SEASON"];
			var filterCurrent = this._panelModel.getData();
			var seasonCheckMsg = this._globalModel.getProperty("/seasonCheckMsg");
			if (seasonCheckMsg !== undefined && seasonCheckMsg !== "") {
				MessageBox.warning(seasonCheckMsg);
				return;
			}
			if (seasonInput === "") {
				MessageBox.warning("Season can not be empty!");
				return;
			}
			var SFULLYEAR = this._panelModel.getData().SFULLYEAR;
			var EFULLYEAR = this._panelModel.getData().EFULLYEAR;
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				if (seasonInput.split("[").length > 1) {
					if (seasonInput.split("[")[1].split(" - ").length > 1) {
						SFULLYEAR = seasonInput.split("[")[1].split(" - ")[0];
						var sFormmat = SFULLYEAR.split(".")[2] + SFULLYEAR.split(".")[1] + SFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
						EFULLYEAR = seasonInput.split("[")[1].split(" - ")[1];
						var eFormmat = EFULLYEAR.split(".")[2] + EFULLYEAR.split(".")[1] + EFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormmat);
					}
				}
			}
			var dateFrom = SFULLYEAR.split(".")[1] + "." + SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[2];
			dateFrom = new Date(
				dateFrom);
			var yearFrom = dateFrom.getFullYear() - 1;
			var prevDateFrom = SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[1] + "." + yearFrom.toString();
			var dateTo = new Date(dateFrom - 24 * 60 * 60 * 1000);
			var prevDateTo = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(new Date(dateTo));

			this._panelModel.setProperty("/PREV_FROM", prevDateFrom);
			this._panelModel.setProperty("/PREV_TO", prevDateTo);
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_SUMMARY"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			// that._globalModel.setProperty("/seasonText", filterData["SEASON_TEXT"] === undefined ? "" : filterData["SEASON_TEXT"]);

			dbcontext.callServer(params, function (oModel) {
				that._globalModel.setProperty("/isSaved", "");
				data = oModel.getData().RESULTS;
				data.forEach(function (row) {
					row["ZCOUNTRY_F"] = row["ZCOUNTRY"] + row["ZCOUNTRY___T"];
				});
				headerKpis = oModel.getData().HEADER_KPIS;
				that._panelModel.setProperty("/VOLUME_KG", headerKpis.VOLUME_KG);
				that._panelModel.setProperty("/PLANNED_KG", headerKpis.PLANNED_KG);
				that._panelModel.setProperty("/QUOTED_KG", headerKpis.QUOTED_KG);
				that._panelModel.setProperty("/PENDING_QUOTE", headerKpis.PENDING_QUOTE);
				that._panelModel.setProperty("/TARGET_KG", headerKpis.TARGET_KG);
				that._panelModel.setProperty("/REALIZED_KG", headerKpis.REALIZED_KG);
				that._panelModel.setProperty("/AVERAGE_PRICE_EUR_KG", headerKpis.AVERAGE_PRICE_EUR_KG);
				that._seasonSummaryTableM.setProperty("/tableRows", data);
				debugger;
				that.initallData = data;
				that._findTableKeys();
			}, that);
		},

		onPressAggregation: function () {
			this.convertGroup(this.mainTableKeys);
			this.collectData();
		},

		convertGroup: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData, groupCols);
			this.groupData = groupData;
		},

		collectData: function () {
			var newCollectedData = [];
			var that = this;
			$.each(this.groupData, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.PLANT_CLUSTER = k.PLANT_CLUSTER;
					newEntry.PLANT_CLUSTER_T = k.PLANT_CLUSTER_T;
					newEntry.ZCOUNTRY = k.ZCOUNTRY;
					newEntry.ZCOUNTRY___T = k.ZCOUNTRY___T;
					newEntry.PLANT = k.PLANT;
					newEntry.PLANT___T = k.PLANT___T;
					newEntry.RMW21ZH02 = k.RMW21ZH02;
					newEntry.RMW21ZH02___T = k.RMW21ZH02___T;

					newEntry.RMW11ZP16 = k.RMW11ZP16;
					newEntry.RMW11ZP16___T = k.RMW11ZP16___T;
					newEntry.RMW11ZE03 = k.RMW11ZE03;
					newEntry.RMW11ZE03___T = k.RMW11ZE03___T;
					newEntry.RMW21ZE03 = k.RMW21ZE03;
					newEntry.RMW21ZE03___T = k.RMW21ZE03___T;
					newEntry.RMW31ZE03 = k.RMW31ZE03;
					newEntry.RMW31ZE03___T = k.RMW31ZE03___T;
					newEntry.DFRTSPC1 = k.DFRTSPC1;
					newEntry.DFRTSPC1___T = k.DFRTSPC1___T;
					newEntry.DFRTSPC2 = k.DFRTSPC2;
					newEntry.DFRTSPC2___T = k.DFRTSPC2___T;
					newEntry.DFRTSPC3 = k.DFRTSPC3;
					newEntry.DFRTSPC3___T = k.DFRTSPC3___T;

					newEntry.SALES_TARGET_KG = newEntry.SALES_TARGET_KG === undefined ? 0 : newEntry.SALES_TARGET_KG;
					newEntry.SALES_TARGET_KG = (k.SALES_TARGET_KG) + newEntry.SALES_TARGET_KG;
					
					newEntry.SALES_REALIZED_KG = newEntry.SALES_REALIZED_KG === undefined ? 0 : newEntry.SALES_REALIZED_KG;
					newEntry.SALES_REALIZED_KG = (k.SALES_REALIZED_KG) + newEntry.SALES_REALIZED_KG;
					
					newEntry.QUOTES_ACCEPTED_KG = newEntry.QUOTES_ACCEPTED_KG === undefined ? 0 : newEntry.QUOTES_ACCEPTED_KG;
					newEntry.QUOTES_ACCEPTED_KG = (k.QUOTES_ACCEPTED_KG) + newEntry.QUOTES_ACCEPTED_KG;

					newEntry.SALES_REALIZED_KG_PRE = newEntry.SALES_REALIZED_KG_PRE === undefined ? 0 : newEntry.SALES_REALIZED_KG_PRE;
					newEntry.SALES_REALIZED_KG_PRE = (k.SALES_REALIZED_KG_PRE) + newEntry.SALES_REALIZED_KG_PRE;
					newEntry.SALES_REALIZED_EUR = newEntry.SALES_REALIZED_EUR === undefined ? 0 : newEntry.SALES_REALIZED_EUR;
					newEntry.SALES_REALIZED_EUR = (k.SALES_REALIZED_EUR) + newEntry.SALES_REALIZED_EUR;
					newEntry.SALES_TARGET_REALIZED_IN_PERC = newEntry.SALES_TARGET_REALIZED_IN_PERC === undefined ? 0 : newEntry.SALES_TARGET_REALIZED_IN_PERC;
					// if (k.SALES_TARGET_KG !== 0) {
					// 	newEntry.SALES_TARGET_REALIZED_IN_PERC += ((k.SALES_REALIZED_KG) / (k.SALES_TARGET_KG) * 100);
					// }
					newEntry.LS_1 = newEntry.LS_1 === undefined ? 0 : newEntry.LS_1;
					newEntry.LS_1 = (k.LS_1) + newEntry.LS_1;
					newEntry.OPEN_QUOTES = newEntry.OPEN_QUOTES === undefined ? 0 : newEntry.OPEN_QUOTES;
					newEntry.OPEN_QUOTES = (k.OPEN_QUOTES) + newEntry.OPEN_QUOTES;
					newEntry.LS_ON_QUOTES = newEntry.LS_ON_QUOTES === undefined ? 0 : newEntry.LS_ON_QUOTES;
					newEntry.LS_ON_QUOTES = (k.LS_ON_QUOTES) + newEntry.LS_ON_QUOTES;
					newEntry.PROD_TARGET_KG = newEntry.PROD_TARGET_KG === undefined ? 0 : newEntry.PROD_TARGET_KG;
					newEntry.PROD_TARGET_KG = (k.PROD_TARGET_KG) + newEntry.PROD_TARGET_KG;
					newEntry.PROD_REALIZED_KG = newEntry.PROD_REALIZED_KG === undefined ? 0 : newEntry.PROD_REALIZED_KG;
					newEntry.PROD_REALIZED_KG = (k.PROD_REALIZED_KG) + newEntry.PROD_REALIZED_KG;

					newEntry.PROD_REALIZED_KG_PRE = newEntry.PROD_REALIZED_KG_PRE === undefined ? 0 : newEntry.PROD_REALIZED_KG_PRE;
					newEntry.PROD_REALIZED_KG_PRE = (k.PROD_REALIZED_KG_PRE) + newEntry.PROD_REALIZED_KG_PRE;
					newEntry.PROD_TARGET_REALIZED_IN_PERC = newEntry.PROD_TARGET_REALIZED_IN_PERC === undefined ? 0 : newEntry.PROD_TARGET_REALIZED_IN_PERC;
					// if (k.PROD_TARGET_KG !== 0) {
					// 	newEntry.PROD_TARGET_REALIZED_IN_PERC += ((k.PROD_REALIZED_KG) / (k.PROD_TARGET_KG) * 100);
					// }

					newEntry.LS_ON_PLANNED_PRODUCTION = newEntry.LS_ON_PLANNED_PRODUCTION === undefined ? 0 : newEntry.LS_ON_PLANNED_PRODUCTION;
					newEntry.LS_ON_PLANNED_PRODUCTION = (k.LS_ON_PLANNED_PRODUCTION) + newEntry.LS_ON_PLANNED_PRODUCTION;
					newEntry.PURCH_TARGET_KG = newEntry.PURCH_TARGET_KG === undefined ? 0 : newEntry.PURCH_TARGET_KG;
					newEntry.PURCH_TARGET_KG = (k.PURCH_TARGET_KG) + newEntry.PURCH_TARGET_KG;
					newEntry.PURCH_REALIZED_KG = newEntry.PURCH_REALIZED_KG === undefined ? 0 : newEntry.PURCH_REALIZED_KG;
					newEntry.PURCH_REALIZED_KG = (k.PURCH_REALIZED_KG) + newEntry.PURCH_REALIZED_KG;

					newEntry.PURCH_REALIZED_KG_PRE = newEntry.PURCH_REALIZED_KG_PRE === undefined ? 0 : newEntry.PURCH_REALIZED_KG_PRE;
					newEntry.PURCH_REALIZED_KG_PRE = (k.PURCH_REALIZED_KG_PRE) + newEntry.PURCH_REALIZED_KG_PRE;
					newEntry.PURCH_TARGET_REALIZED_IN_PERC = newEntry.PURCH_TARGET_REALIZED_IN_PERC === undefined ? 0 : newEntry.PURCH_TARGET_REALIZED_IN_PERC;

					newEntry.OPEN_PLANNED_PURCHASE = newEntry.OPEN_PLANNED_PURCHASE === undefined ? 0 : newEntry.OPEN_PLANNED_PURCHASE;
					newEntry.OPEN_PLANNED_PURCHASE = (k.OPEN_PLANNED_PURCHASE) + newEntry.OPEN_PLANNED_PURCHASE;
					newEntry.LS_ON_PLANNED_PURCHASE = newEntry.LS_ON_PLANNED_PURCHASE === undefined ? 0 : newEntry.LS_ON_PLANNED_PURCHASE;
					newEntry.LS_ON_PLANNED_PURCHASE = (k.LS_ON_PLANNED_PURCHASE) + newEntry.LS_ON_PLANNED_PURCHASE;
				});

				newEntry.LS_ON_PLANNED_PURCHASE = newEntry.LS_ON_PLANNED_PRODUCTION + newEntry.OPEN_PLANNED_PURCHASE;

				if (newEntry.SALES_TARGET_KG !== 0) {
					newEntry.SALES_TARGET_REALIZED_IN_PERC = (( (newEntry.SALES_REALIZED_KG) + (newEntry.QUOTES_ACCEPTED_KG) ) / (newEntry.SALES_TARGET_KG)) * 100;
				}

				if (newEntry.PROD_TARGET_KG !== 0) {
					newEntry.PROD_TARGET_REALIZED_IN_PERC = ((newEntry.PROD_REALIZED_KG) / (newEntry.PROD_TARGET_KG)) * 100;
				}

				if (newEntry.PURCH_TARGET_KG !== 0) {
					newEntry.PURCH_TARGET_REALIZED_IN_PERC = ((newEntry.PURCH_REALIZED_KG) / (newEntry.PURCH_TARGET_KG)) * 100;
				}

				newEntry.OPEN_PLANNED_PRODUCTION = newEntry.OPEN_PLANNED_PRODUCTION === undefined ? 0 : newEntry.OPEN_PLANNED_PRODUCTION;
				newEntry.OPEN_PLANNED_PRODUCTION = (newEntry.PROD_TARGET_KG - newEntry.PROD_REALIZED_KG);
				if (newEntry.OPEN_PLANNED_PRODUCTION < 0) newEntry.OPEN_PLANNED_PRODUCTION = 0;

				newEntry = that._hasComment(newEntry, 1);
				newCollectedData.push(newEntry);
			});
			var totalRow = this.setTotalRow(newCollectedData, "", this.columnfilter);

			totalRow.SALES_TARGET_REALIZED_IN_PERC = ( totalRow.SALES_REALIZED_KG + totalRow.QUOTES_ACCEPTED_KG ) / totalRow.SALES_TARGET_KG * 100;
			totalRow.PROD_TARGET_REALIZED_IN_PERC = totalRow.PROD_REALIZED_KG / totalRow.PROD_TARGET_KG * 100;
			totalRow.PURCH_TARGET_REALIZED_IN_PERC = totalRow.PURCH_REALIZED_KG / totalRow.PURCH_TARGET_KG * 100;
			totalRow.OPEN_PLANNED_PRODUCTION = totalRow.PROD_TARGET_KG - totalRow.PROD_REALIZED_KG;
			if (totalRow.OPEN_PLANNED_PRODUCTION < 0) totalRow.OPEN_PLANNED_PRODUCTION = 0;
			totalRow.LS_ON_PLANNED_PRODUCTION = totalRow.LS_ON_QUOTES + totalRow.OPEN_PLANNED_PRODUCTION;
			totalRow.OPEN_PLANNED_PURCHASE = totalRow.PURCH_TARGET_KG - totalRow.PURCH_REALIZED_KG;
			if (totalRow.OPEN_PLANNED_PURCHASE < 0) totalRow.OPEN_PLANNED_PURCHASE = 0;
			totalRow.LS_ON_PLANNED_PURCHASE = totalRow.LS_ON_PLANNED_PRODUCTION + totalRow.OPEN_PLANNED_PURCHASE;

			if (isNaN(totalRow.SALES_TARGET_REALIZED_IN_PERC) || totalRow.SALES_TARGET_REALIZED_IN_PERC === Infinity || totalRow.SALES_TARGET_REALIZED_IN_PERC ===
				-Infinity) totalRow.SALES_TARGET_REALIZED_IN_PERC =
				0;
			if (isNaN(totalRow.PROD_TARGET_REALIZED_IN_PERC) || totalRow.PROD_TARGET_REALIZED_IN_PERC === Infinity || totalRow.PROD_TARGET_REALIZED_IN_PERC ===
				-Infinity) totalRow.PROD_TARGET_REALIZED_IN_PERC =
				0;
			if (isNaN(totalRow.PURCH_TARGET_REALIZED_IN_PERC) || totalRow.PURCH_TARGET_REALIZED_IN_PERC === Infinity || totalRow.PURCH_TARGET_REALIZED_IN_PERC ===
				-Infinity) totalRow.PURCH_TARGET_REALIZED_IN_PERC =
				0;
			if (isNaN(totalRow.OPEN_PLANNED_PRODUCTION) || totalRow.OPEN_PLANNED_PRODUCTION === Infinity || totalRow.OPEN_PLANNED_PRODUCTION ===
				-Infinity) totalRow.OPEN_PLANNED_PRODUCTION =
				0;
			if (isNaN(totalRow.LS_ON_PLANNED_PRODUCTION) || totalRow.LS_ON_PLANNED_PRODUCTION === Infinity || totalRow.LS_ON_PLANNED_PRODUCTION ===
				-Infinity) totalRow.LS_ON_PLANNED_PRODUCTION =
				0;
			if (isNaN(totalRow.OPEN_PLANNED_PURCHASE) || totalRow.OPEN_PLANNED_PURCHASE === Infinity || totalRow.OPEN_PLANNED_PURCHASE === -
				Infinity) totalRow.OPEN_PLANNED_PURCHASE =
				0;
			if (isNaN(totalRow.LS_ON_PLANNED_PURCHASE) || totalRow.LS_ON_PLANNED_PURCHASE === Infinity || totalRow.LS_ON_PLANNED_PURCHASE ===
				-
				Infinity) totalRow.LS_ON_PLANNED_PURCHASE =
				0;

			var avg = 0;
			if (totalRow.SALES_REALIZED_KG !== 0)
				avg = totalRow.SALES_REALIZED_EUR / totalRow.SALES_REALIZED_KG;

			this._panelModel.setProperty("/AVERAGE_PRICE_EUR_KG", avg);

			newCollectedData.unshift(totalRow);
			newCollectedData.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this._seasonSummaryTableM.setProperty("/tableRows", newCollectedData);
		},

		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();

				}
				if (controls[i].getMetadata().getName() === "sap.m.Input") {
					controls[i].setValue("");
				}
			}
		},

		onExport: function () {
			var data = [];
			var tableData = this._seasonSummaryTableM.getData();
			var copiedData = tableData.tableRows;
			var aIndices = this._tableId.getBinding("rows").aIndices;

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			excel._downloadExcel("SEASON_SUMMARY", this._tableId, data);
		},

		//************************************************ Season search help**************************************

		checkSeason: function () {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_DATA"
				},
				"INPUTPARAMS": [{
					"FRUIT": this._panelModel.getProperty("/FRUIT"),
					"PLANT": this._panelModel.getProperty("/PLANT"),
					"PLANT_CLUSTER" : this._panelModel.getProperty("/PLANT_CLUSTER"),
					"COUNTRYOFPLANT" : this._panelModel.getProperty("/COUNTRYOFPLANT")
				}]
			};

			return params;

		},

		handleValueHelp: function (oEvent) {
			var params = "";
			var fruit = this._panelModel.getProperty("/FRUIT");
			if (fruit === "") {
				MessageToast.show("Fruit field can not be empty!");
				return;
			} else {
				params = this.checkSeason();

			}

			var oView = this.getView();
			this._sInputId = oEvent.getSource().getId();

			// create value help dialog
			if (!this._pValueHelpDialog) {
				this._pValueHelpDialog = Fragment.load({
					id: oView.getId(),
					name: "com.doehler.Z_GTP.fragments.SeasonSearchHelp",
					controller: this
				}).then(function (oValueHelpDialog) {
					oView.addDependent(oValueHelpDialog);
					return oValueHelpDialog;
				});
			}
			this._prepareSeason(params, this._globalModel, this._pValueHelpDialog, undefined, "X");
		},

		_handleValueHelpSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Name",
				FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

		_handleValueHelpClose: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var productInput = this.byId(this._sInputId);
				productInput.setValue(oSelectedItem.getTitle());
			}
			oEvent.getSource().getBinding("items").filter([]);

			var eFormat = "",
				sFormmat = "";
			if (this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath()) === undefined)
				return;

			var line = this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath());
			if (line.efullYear.split(".").length === 3) {
				eFormat = line.efullYear.split(".")[2] + line.efullYear.split(".")[1] + line.efullYear.split(".")[0];
			}
			if (line.sfullYear.split(".").length === 3) {
				sFormmat = line.sfullYear.split(".")[2] + line.sfullYear.split(".")[1] + line.sfullYear.split(".")[0];
			}
			this._panelModel.setProperty("/SFULLYEAR", line.sfullYear);
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
			this._panelModel
				.setProperty("/EFULLYEAR", line.efullYear);
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormat);
			this._panelModel.setProperty(
				"/SEASON_TEXT", line.text);
		},

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		currTableData: null,
		oTPC: null,
		oCC2: null,
		currTableData2: null,
		oTPC2: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId;
			var oVM = this.getView().byId("seasonSummarytableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() !== "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
					// Find Table Key
					thiz._findTableKeys();
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this._tableIdText;
			var oTable = this._tableId1;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys();
		},

		_findTableKeys: function () {
			var thiz = this;
			var oTable = this._tableId;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys.length)
				thiz.onPressAggregation();
		},

		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},

		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},

		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey === item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},

		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		//************************************************table variant codes**************************************

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdSS");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("seasonSummaryPanelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//	that.addSearchFilter();
			}, function () {
				//	that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isSeasonSummary", true);
			sap.ui.getCore().getModel("globalM").setProperty("/seasonalSummaryInit", true);
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("seasonSummaryPanelM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantSS: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("seasonSummaryPanelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantSS: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("seasonSummaryPanelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMSS: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1) {
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined)
				this._updatePanelData(fromData, sap.ui.getCore().getModel("seasonSummaryPanelM"));
			var aControls = this.getView().byId("searchGridId").getContent();
			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));
					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {
									if (value != "") {
										if (item.getFieldName() === "FRUIT") {
											const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
											item.addToken(new sap.m.Token({
												key: value,
												text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
											}));
										} else {
											item.addToken(new sap.m.Token({
												key: value,
												text: value
											}));
										}
									}
								});

							}
						}
					});
				}, 1000);
		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			if (type === "added" || type === "removed" || type === "removedAll") {
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "WERKS"
				    || oEvent.getSource().getFieldName() === "PLANT_CLUSTER" || oEvent.getSource().getFieldName() === "COUNTRY_PLANT") {

					this._panelModel.setProperty("/EFULLYEAR", "");
					this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/PREV_FROM", "");
					this._panelModel.setProperty("/PREV_TO", "");
					this._panelModel.setProperty("/SEASON", "");

					this._prefillSeason(oEvent);
				}
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "PRODUCT_TYPE") {
					this.getView().byId("FS1").removeAllTokens();
					this.getView().byId("FS2").removeAllTokens();
					this.getView().byId("FS3").removeAllTokens();
				}
			}

		},
		
		onSelectToken: function(oEvent){
			debugger;
		},

		_prefillSeason: function (oEvent) {
			var fruit = this.getView().byId("fruitId");
			var params = this.checkSeason();
			if (fruit.getTokens().length === 1)
				this._prepareSeason(params, this._globalModel, null, this._panelModel, "");
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			// this.clearSelectionFields();
			this.initVariant();
			this.initSearchVariant();
		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();

			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					that._findTableKeys();
					// that._findTableKeys2();
				},
				function (error) { /* code if some error */ }
			);
		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);
		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

		onGoSales: function (oEvent) {
			var Source = oEvent.getSource();
			var ViewName = "";
			var srow = Source.getBindingContext('seasonSummaryTableM').getObject();

			switch (oEvent.getSource().getBindingPath("text")) {
			case "SALES_REALIZED_KG":
				ViewName = "SalesContracts";
				srow["salesHandover"] = "X";
				srow["SFULLYEAR"] = this._panelModel.getData().SFULLYEAR;
				srow["EFULLYEAR"] = this._panelModel.getData().EFULLYEAR;;
				srow["EFULLYEAR_FORMAT"] = this._panelModel.getData().EFULLYEAR_FORMAT;
				srow["SFULLYEAR_FORMAT"] = this._panelModel.getData().SFULLYEAR_FORMAT;
				srow["SEASON"] = this._panelModel.getData().SEASON;
				break;
			case "PURCH_REALIZED_KG":
				ViewName = "PurchaseContracts";
				// srow["salesHandover"] = "X";
				srow["SFULLYEAR"] = this._panelModel.getData().SFULLYEAR;
				srow["EFULLYEAR"] = this._panelModel.getData().EFULLYEAR;;
				srow["EFULLYEAR_FORMAT"] = this._panelModel.getData().EFULLYEAR_FORMAT;
				srow["SFULLYEAR_FORMAT"] = this._panelModel.getData().SFULLYEAR_FORMAT;
				srow["SEASON"] = this._panelModel.getData().SEASON;
				break;
			case "SALES_REALIZED_KG_PRE":
			case "PURCH_REALIZED_KG_PRE":
				ViewName = oEvent.getSource().getBindingPath("text") === "SALES_REALIZED_KG_PRE" ? "SalesContracts" : "PurchaseContracts"; 
				srow["salesHandover"] = "X";
				if (this._panelModel.getData().SFULLYEAR === this._globalModel.getData().seasonDates[1].sfullYear)
					var lv_index = 0;
				else if (this._panelModel.getData().SFULLYEAR === this._globalModel.getData().seasonDates[2].sfullYear)
					var lv_index = 1;
				else { return; }
				srow["SFULLYEAR"] = this._globalModel.getData().seasonDates[lv_index].sfullYear;
				srow["EFULLYEAR"] = this._globalModel.getData().seasonDates[lv_index].efullYear;
				srow["EFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[lv_index].efullYear.split(".")[2] + this._globalModel.getData().seasonDates[lv_index].efullYear.split(".")[1] + this._globalModel.getData().seasonDates[lv_index].efullYear.split(".")[0];
				srow["SFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[lv_index].sfullYear.split(".")[2] + this._globalModel.getData().seasonDates[lv_index].sfullYear.split(".")[1] + this._globalModel.getData().seasonDates[lv_index].sfullYear.split(".")[0];
			var	lastseason = this._globalModel.getData().seasonDates[lv_index];
				srow["SEASON"] = lastseason.text + "[" + lastseason.sfullYear + " - " + lastseason.efullYear + "]";
				break;
			default:
				// ViewName = "SalesContracts";
				// srow["SFULLYEAR"] = this._globalModel.getData().seasonDates[1].sfullYear;
				// srow["EFULLYEAR"] = this._globalModel.getData().seasonDates[1].efullYear;
				// srow["EFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[1].efullYear.split(".")[2] + this._globalModel.getData().seasonDates[
				// 	1].efullYear.split(".")[1] + this._globalModel.getData().seasonDates[1].efullYear.split(".")[0];
				// srow["SFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[1].sfullYear.split(".")[2] + this._globalModel.getData().seasonDates[
				// 	1].sfullYear.split(".")[1] + this._globalModel.getData().seasonDates[1].sfullYear.split(".")[0];
				// lastseason = this._globalModel.getData().seasonDates[1];
				// srow["SEASON"] = lastseason.text + "[" + lastseason.sfullYear + " - " + lastseason.efullYear + "]";
			}

			Object.keys(srow).forEach(function (key) {
				var splitkey = key.split("_")[1];
				var splitkey_2x = key.split("__");
				if (splitkey_2x.length === 2 && splitkey === "") {
					splitkey = splitkey_2x[1].split("_")[1];
				}
				if (splitkey === "T")
					delete srow[key];
			});

			this.getRouter().navTo(ViewName, {
				row: JSON.stringify(srow),
				viewid: this._tableId.getId()
			}, false);

		}

	});

});