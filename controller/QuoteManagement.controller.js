sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/formatter",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	'sap/ui/core/Fragment',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, formatter, dbcontext, dbcontext2, excel, Fragment, JSONModel, MessageToast,
	MessageBox, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.QuoteManagement", {
		formatter: formatter,
		onInit: function () {
			this._setButtonsPressed("seasonalPlanToOfferId");
			this._tableId = this.getView().byId("quoteManagementTableId");

			this._tableIdText = "quoteManagementTableId";
			this._tableIdText1 = "DuplicateQuoteTableId";
			this.bDialog = new sap.m.BusyDialog();
			this.columnfilter = {};
			//panel Model
			this._panelModel = this.getOwnerComponent().getModel("quotePanelM");
			this._panelModel.setProperty("/totalQuote", 0);
			this._panelModel.setProperty("/totalQuoteQuoan", 0);
			this._globalModel = this.getOwnerComponent().getModel("globalM");
			this._globalModel.setProperty("/seasonText", "");
			this._quoteTableM = this.getOwnerComponent().getModel("quoteTableM");
			this.CreateGTPquoteTableM = this.getOwnerComponent().getModel("CreateGTPquoteTableM");
			this.createSAPquoteTableM = this.getOwnerComponent().getModel("createSAPquoteTableM");
			this._duplicateQuoteTableM = this.getOwnerComponent().getModel("duplicateQuoteTableM");
			var that = this;

			this._globalModel.setProperty("/GTPFlagButton", false);

			var Promise1 = new Promise(function (myResolve, myReject) {
				that.getSearchHelpData("INDICATOR", myResolve, myReject);

			});

			var Promise2 = new Promise(function (myResolve, myReject) {
				that.getSearchHelpData("ABGRU", myResolve, myReject);

			});

			Promise.all([Promise1, Promise2])
				.then(values => {
					that._globalModel.setProperty("/INDICATOR", values[0]);
					that._globalModel.setProperty("/ABGRU", values[1]);
					// that._globalModel.setProperty("/QUOTE_ITEM_SATATUS", values[2]);
					debugger;
				})

			// myPromise.then(
			// 	function (value) {
			// 		that._globalModel.setProperty("/INDICATOR", value);
			// 		debugger;
			// 	},
			// 	function (error) { /* code if some error */ }
			// );

			this._seasonalODataModel = this.getOwnerComponent().getModel("QUOTE_MANAGEMENT_MODEL");
			sap.ui.getCore().setModel(this._panelModel, "quotePanelM");
			this._initializePanelModel();
			sap.ui.getCore().getModel("globalM").setProperty("/isQuoteManagement", false);
			sap.ui.getCore().getModel("globalM").setProperty("/QuoteManagementNavto", undefined);
			this.getRouter().getRoute("QuoteManagement").attachPatternMatched(this._onRouteMatched, this);

			var priceQuantityUnit = [{
				"KMEIN": ""
			}, {
				"KMEIN": "KG"
			}, {
				"KMEIN": "TO"
			}, {
				"KMEIN": "GAL"
			}];
			this._globalModel.setProperty("/PriceQuantityUnit", priceQuantityUnit);

			var priceUnit = [{
				"KPEIN": ""
			}, {
				"KPEIN": "1"
			}, {
				"KPEIN": "10"
			}, {
				"KPEIN": "100"
			}, {
				"KPEIN": "1000"
			}];
			this._globalModel.setProperty("/PriceUnit", priceUnit);

			var organic = [{
				"ORGANIC_TEXT": "no",
				"ORGANIC": "CUST-X.0000000001563"
			}, {
				"ORGANIC_TEXT": "yes",
				"ORGANIC": "CUST-X.0000000001987"
			}];
			this._globalModel.setProperty("/ORGANIC", organic);

			var halal = [{
				"HALAL": "CUST-X.0000000001563",
				"HALAL_TEXT": "no"
			}, {
				"HALAL": "CUST-X.0000000001562",
				"HALAL_TEXT": "yes"
			}];
			this._globalModel.setProperty("/HALAL", halal);

			var kosher = [{
				"KOSHER": "CUST-X.0000000001563",
				"KOSHER_TEXT": "no"
			}, {
				"KOSHER": "CUST-X.0000000001562",
				"KOSHER_TEXT": "yes"
			}];
			this._globalModel.setProperty("/KOSHER", kosher);
		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("seasonalPlanToOfferId");
			this._tableId.setVisible(false);
			this._tableId.setVisible(true);
			var argum = oEvent.getParameter("arguments");
			try {
				var row = JSON.parse(argum.row);
				var viewid = argum.viewid;
				this.clearSelectionFields();
				this.createToken(undefined, row, viewid);
				sap.ui.getCore().getModel("globalM").setProperty("/QuoteManagementNavto", argum);
				this.getView().byId("custMIdPSI").addToken(new sap.m.Token({
					key: "02",
					text: "open"
				}));
				this._panelModel.setProperty("/QUOTE_ITEM_SATATUS", "02");
				// this._panelModel.setProperty("/EFULLYEAR", row["EFULLYEAR"]);
				// this._panelModel.setProperty("/SFULLYEAR", row["SFULLYEAR"]);
				this.onGo();
			} catch (err) {
				sap.ui.getCore().getModel("globalM").setProperty("/QuoteManagementNavto", undefined);
				if (sap.ui.getCore().getModel("globalM").getProperty("/isQuoteManagement") === true)
					this.createToken();
			}

		},

		onSortQM: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._quoteTableM.getProperty("/tableRows");
			var sortProperty = oEvent.getParameter("column").getSortProperty();
			var oBinding = this.getView().byId("quoteManagementTableId").getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);
			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._quoteTableM);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("quoteManagementTableId").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._quoteTableM.setProperty("/tableRows", notSumRow);
			// 	that.getView().byId("quoteManagementTableId").bindRows("quoteTableM>/tableRows");

			// }, 100)
		},
		onFilterQM: function (oEvent) {
			var data = this._quoteTableM.getProperty("/tableRows");
			var dataIndices = [];
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				// this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				// this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}
			var aIndices = oEvent.getSource().getBindingInfo("rows").binding.aIndices;

			aIndices.forEach(function (ind) {
				if (data[ind].isSum !== "X")
					dataIndices.push(data[ind]);
			});

			var notSumRow = _.filter(_.cloneDeep(data), function (item) {
				return item.isSum !== "X";
			});

			this._filterTableData(notSumRow, dataIndices);

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		onValueHelpInit: function (oEvent) {
			if (oEvent.getSource().getRelFieldModel() === "CreateGTPquoteTableM") {
				var pressed = sap.ui.getCore().getModel("globalM").getData()["pressTableValueControl"];
				var createGTPquoteTableModel = this.CreateGTPquoteTableM;
				if (pressed !== undefined) {
					if (this._checkValidationOfGTPQuote(oEvent.getSource().getFieldName(), pressed.getBindingContext("CreateGTPquoteTableM").getPath(),
							createGTPquoteTableModel)) {
						oEvent.getSource().setController(this);
						oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
					}
				}
			} else if (oEvent.getSource().getRelFieldModel() === "createSAPquoteTableM") {
				pressed = sap.ui.getCore().getModel("globalM").getData()["pressTableValueControl"];
				if (pressed !== undefined) {
					if (this._checkValidationOfSAPQuote(oEvent.getSource().getFieldName(), pressed.getBindingContext("createSAPquoteTableM").getPath())) {
						oEvent.getSource().setController(this);
						oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
					}
				}
			} else {
				oEvent.getSource().setController(this);
				oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
			}

		},

		onChangeSeason: function (oEvent) {
			if (oEvent.getParameter("newValue") === "" || oEvent.getParameter("newValue") === undefined) {
				this._panelModel.setProperty("/SFULLYEAR", "");
				this._panelModel.setProperty("/EFULLYEAR", "");
				this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/PREV_FROM", "");
				this._panelModel.setProperty("/PREV_TO", "");
				this._panelModel.setProperty("/SEASON", "");
			} else {
				oEvent.getSource().setValue("");
				MessageToast.show("You cannot enter a value manually!");
			}
		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/SALESORG", "");
			this._panelModel.setProperty("/REGION", "");
			this._panelModel.setProperty("/AREA", "");
			this._panelModel.setProperty("/COUNTRY", "");
			this._panelModel.setProperty("/SEASON", "");
			this._panelModel.setProperty("/CUSTOMERL1", "");
			this._panelModel.setProperty("/CUSTOMERL2", "");
			this._panelModel.setProperty("/SFULLYEAR", "");
			this._panelModel.setProperty("/EFULLYEAR", "");
			this._panelModel.setProperty("/AZIND", "");
			this._panelModel.setProperty("/QUOTE_NUMBER", "");
			this._panelModel.setProperty("/QUOTE_ITEM_SATATUS", "");
			this._panelModel.setProperty("/WORKFLOW_STATUS", "");
			this._panelModel.setProperty("/VSBED", "");
			this._panelModel.setProperty("/GTP_QUOTE_NO", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
			this._panelModel.setProperty("/VAL_STATUS", "");
			var that = this;
			setTimeout(
				function () {
					if (that.getView().byId("custMIdPSI") !== undefined) {
						that.getView().byId("custMIdPSI").removeAllTokens();
						that.getView().byId("custMIdPSI").destroyTokens();
						// if (sap.ui.getCore().getModel("globalM").getProperty("/QuoteManagementNavto") === undefined) {
						that.getView().byId("custMIdPSI").addToken(new sap.m.Token({
							key: "02",
							text: "open"
						}));
						that._panelModel.setProperty("/QUOTE_ITEM_SATATUS", "02");
						// }
					}

					if (that.getView().byId("valstatID") !== undefined) {
						that.getView().byId("valstatID").removeAllTokens();
						that.getView().byId("valstatID").destroyTokens();
						// if (sap.ui.getCore().getModel("globalM").getProperty("/QuoteManagementNavto") === undefined) {
						that.getView().byId("valstatID").addToken(new sap.m.Token({
							key: "1",
							text: "Valid"
						}));
						that._panelModel.setProperty("/VAL_STATUS", "1");
						// }
					}

				}, 1000);

		},

		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();

				}
				if (controls[i].getMetadata().getName() === "sap.m.Input") {
					controls[i].setValue("");
				}
			}
		},

		onExport: function () {
			var data = [];
			var tableData = this._quoteTableM.getData();
			var copiedData = tableData.tableRows;
			var aIndices = this._tableId.getBinding("rows").aIndices;

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			excel._downloadExcel("QUOTE_MANAGEMENT", this._tableId, data);

		},

		onGo: function (oEvent) {
			var that = this;
			var isSaved = that._globalModel.getProperty("/isSaved");
			var seasonInput = this._panelModel.getData()["SEASON"];
			var filterCurrent = this._panelModel.getData();
			var tableModel = sap.ui.getCore().getModel("quoteTableM");

			if (seasonInput === "") {
				this._panelModel.setProperty("/EFULLYEAR", "");
				this._panelModel.setProperty("/SFULLYEAR", "");
				this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/PREV_FROM", "");
				this._panelModel.setProperty("/PREV_TO", "");
				this._panelModel.setProperty("/SEASON", "");
			}

			// if (seasonInput === undefined || seasonInput === "") {
			// 	MessageBox.warning("Season input can not be empty");
			// 	return;
			// }
			var SFULLYEAR = this._panelModel.getData().SFULLYEAR;
			var EFULLYEAR = this._panelModel.getData().EFULLYEAR;
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				if (seasonInput.split("[").length > 1) {
					if (seasonInput.split("[")[1].split(" - ").length > 1) {
						SFULLYEAR = seasonInput.split("[")[1].split(" - ")[0];
						var sFormmat = SFULLYEAR.split(".")[2] + SFULLYEAR.split(".")[1] + SFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
						EFULLYEAR = seasonInput.split("[")[1].split(" - ")[1];
						var eFormmat = EFULLYEAR.split(".")[2] + EFULLYEAR.split(".")[1] + EFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormmat);
					}
				}
			}
			var dateFrom = SFULLYEAR.split(".")[1] + "." + SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[2];
			dateFrom = new Date(dateFrom);
			var yearFrom = dateFrom.getFullYear() - 1;
			var prevDateFrom = SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[1] + "." + yearFrom.toString();
			var dateTo = new Date(dateFrom - 24 * 60 * 60 * 1000);
			var prevDateTo = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(new Date(dateTo));

			this._panelModel.setProperty("/PREV_FROM", prevDateFrom);
			this._panelModel.setProperty("/PREV_TO", prevDateTo);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_QUOTE_MANAGEMENT_LIST"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			dbcontext.callServer(params, function (oModel) {
				that._globalModel.setProperty("/isSaved", "");
				var data = oModel.getData().RESULTS;
				data.forEach(function (m) {
					m["A0CUSTOMER__DOEBKUNDE_F"] = m["A0CUSTOMER__DOEBKUNDE"] + m["A0CUSTOMER__DOEBKUNDE_T"];
					m["A0CUSTOMER_F"] = m["A0CUSTOMER"] + m["A0CUSTOMER_T"];
					m["GTP"] = m["A0DOC_NUMBER"].indexOf("GTP") > -1;

					m["SALES_GROSS_KG"] = 0;
					if (m["QUOTATION_QUANTITY"] !== 0) m["SALES_GROSS_KG"] = m["SALES_GROSS"] / m["QUOTATION_QUANTITY"];
					m["A0MATERIAL__RMW11ZE03_T"] = that.getDetermineOrganic(m["A0MATERIAL__RMW11ZE03"]);
					m["A0MATERIAL__RMW81ZE03_T"] = that.getDetermineHalal(m["A0MATERIAL__RMW81ZE03"]);
					m["A0MATERIAL__RMW71ZE03_T"] = that.getDetermineKosher(m["A0MATERIAL__RMW71ZE03"]);
					m["A0MATERIAL__DFRTSPC1"] = m["A0MATERIAL__DFRTSPC1"] === "#" ? "" : m["A0MATERIAL__DFRTSPC1"];
					m["A0MATERIAL__DFRTSPC2"] = m["A0MATERIAL__DFRTSPC2"] === "#" ? "" : m["A0MATERIAL__DFRTSPC2"];
					m["A0MATERIAL__DFRTSPC3"] = m["A0MATERIAL__DFRTSPC3"] === "#" ? "" : m["A0MATERIAL__DFRTSPC3"];
					m["AZIND"] = m["AZIND"] === "#" || m["AZIND"] === "" ? "1" : m["AZIND"];
					m["isSum"] = "";
				});
				that._filterTableData(data);
				that._findTableKeys();
			}, that);

		},

		_findTableKeys: function () {
			var thiz = this;
			var oTable = this._tableId;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});

			this._quoteTableM.getProperty("/tableRows").forEach(function (row) {
				row = thiz._hasComment(row, 1);
			});

		},

		_filterTableData: function (data, dataIndices) {

			var totalRow = this.setTotalRow(dataIndices === undefined ? data : dataIndices, "QUOTE", this.columnfilter);
			var numQuote = data.length;
			data.unshift(totalRow);
			data.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						if (key === "GROSS_PRICE_CURR" || key === "SALES_GROSS_KG" || key === "SALES_GROSS_KG_EUR")
							row[key] = parseFloat(row[key] /*.toFixed(2)*/ );
						else
							row[key] = parseFloat(row[key] /*.toFixed(0)*/ );
					}
				});

			});

			this._panelModel.setProperty("/totalQuote", numQuote);
			this._panelModel.setProperty("/totalQuoteQuoan", totalRow.QUOTATION_QUANTITY);

			this._quoteTableM.setProperty("/tableRows", data);
		},

		onChange: function (oEvent) {
			this._panelModel.setProperty("/EFULLYEAR", "");
			this._panelModel.setProperty("/SFULLYEAR", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
			this._panelModel.setProperty("/SEASON", "");
		},

		onLiveChange: function (oEvent) {
			this._panelModel.setProperty("/EFULLYEAR", "");
			this._panelModel.setProperty("/SFULLYEAR", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
			this._panelModel.setProperty("/SEASON", "");
		},
		//************************************************ Season search help**************************************

		checkSeason: function () {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_DATA"
				},
				"INPUTPARAMS": [{
					"FRUIT": this._panelModel.getProperty("/FRUIT"),
					"PLANT": this._panelModel.getProperty("/PLANT")
				}]
			};

			return params;

		},

		handleValueHelp: function (oEvent) {
			var params = "";
			var fruit = this._panelModel.getProperty("/FRUIT");
			if (fruit === "") {
				MessageToast.show("Fruit field can not be empty!");
				return;
			} else {
				params = this.checkSeason();

			}

			var oView = this.getView();
			this._sInputId = oEvent.getSource().getId();

			// create value help dialog
			if (!this._pValueHelpDialog) {
				this._pValueHelpDialog = Fragment.load({
					id: oView.getId(),
					name: "com.doehler.Z_GTP.fragments.SeasonSearchHelp",
					controller: this
				}).then(function (oValueHelpDialog) {
					oView.addDependent(oValueHelpDialog);
					return oValueHelpDialog;
				});
			}
			this._prepareSeason(params, this._globalModel, this._pValueHelpDialog, undefined, "X");
		},

		_handleValueHelpSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Name",
				FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

		_handleValueHelpClose: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var productInput = this.byId(this._sInputId);
				productInput.setValue(oSelectedItem.getTitle());
			}
			oEvent.getSource().getBinding("items").filter([]);

			var eFormat = "",
				sFormmat = "";
			if (this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath()) === undefined)
				return;

			var line = this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath());
			if (line.efullYear.split(".").length === 3) {
				eFormat = line.efullYear.split(".")[2] + line.efullYear.split(".")[1] + line.efullYear.split(".")[0];
			}
			if (line.sfullYear.split(".").length === 3) {
				sFormmat = line.sfullYear.split(".")[2] + line.sfullYear.split(".")[1] + line.sfullYear.split(".")[0];
			}
			this._panelModel.setProperty("/SFULLYEAR", line.sfullYear);
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
			this._panelModel
				.setProperty("/EFULLYEAR", line.efullYear);
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormat);
			this._panelModel.setProperty(
				"/SEASON_TEXT", line.text);
		},

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		currTableData: null,
		oTPC: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		oCC1: null,
		currTableData1: null,
		oTPC1: null,
		onSettings1: function () {
			this.oTPC1.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC1 = oCC;
				oCC1.delItem(thiz._tableIdText1);
				oCC1.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId;
			var oVM = this.getView().byId("quoteTableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() !== "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}

					// Find Table Key
					thiz._findTableKeys();
				});
			});
		},

		initVariant1: function () {
			var thiz = this;
			var oTable = this._tableId1;
			var oVM = Fragment.byId("fr1", "DuplicateQuoteTableVMId");
			var Coll = Fragment.byId("fr1", "colDup_1");
			Fragment.byId("fr1", "DuplicateQuoteTableId").setGroupBy(Coll);
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariantF(this._tableIdText1, 'fr1', function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC1 = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText1);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData1 = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC1 = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData1 = data; // store data temp
					if (oVM.getSelectionKey() !== "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys();
		},

		/* on select variant */
		onSelectVariant1: function (oEvent) {
			var tableId = this._tableIdText1;
			var oTable = Fragment.byId("fr1", tableId);
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC1.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},

		onSaveVariant1: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC1;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText1;
			var oTable = Fragment.byId("fr1", tableId);
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData1) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData1,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},

		onManageVM1: function (oEvent) {
			var oCC = this.oCC1;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		//************************************************table variant codes**************************************

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdQuote");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("quotePanelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//	that.addSearchFilter();
			}, function () {
				//	that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isQuoteManagement", true);
			sap.ui.getCore().getModel("globalM").setProperty("/quoteInit", true);
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("quotePanelM").setData(ovar[ovar.defaultKey]);
			var argum = sap.ui.getCore().getModel("globalM").getProperty("/QuoteManagementNavto");
			try {
				var row = JSON.parse(argum.row);
				var viewid = argum.viewid;
				this.createToken(undefined, row, viewid);
			} catch (err) {
				this.createToken();

			}

		},

		/* on select variant */
		onSelectVariantQuote: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("quotePanelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantQuote: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("quotePanelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMQuote: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1, param2, param3) {
			var that = this;
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined) {
				this._updatePanelData(fromData, sap.ui.getCore().getModel("quotePanelM"), param2, param3);
			}
			var aControls = this.getView().byId("searchGridId").getContent();
			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));
					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if (item.getFieldName() === "QUOTE_ITEM_SATATUS") {
								var result = "";
								var oTokens = that.getView().byId("custMIdPSI").getTokens();

								for (var i = 0; i < oTokens.length; i++) {
									result = result + ',' + oTokens[i].getKey();
								}

								if (result !== "")
									result = result.substr(1, result.length);

								that.getView().byId("custMIdPSI").setSelectedValues(result);
							}

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {

								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {

									if (value != "") {
										if (item.getFieldName() === "FRUIT") {
											const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
											item.addToken(new sap.m.Token({
												key: value,
												text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
											}));
										} else {
											item.addToken(new sap.m.Token({
												key: value,
												text: value
											}));
										}

									}
								});

							}
						}
					});
				}, 1000);
		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			if (type === "added" || type === "removed" || type === "removedAll") {
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "WERKS") {
					var argum = sap.ui.getCore().getModel("globalM").getProperty("/QuoteManagementNavto");
					if (argum === undefined) {
						this._panelModel.setProperty("/EFULLYEAR", "");
						this._panelModel.setProperty("/SFULLYEAR", "");
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
						this._panelModel.setProperty("/PREV_FROM", "");
						this._panelModel.setProperty("/PREV_TO", "");
						this._panelModel.setProperty("/SEASON", "");
						this._prefillSeason(oEvent);
					}
				}
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "PRODUCT_TYPE") {
					// this.getView().byId("FS1").removeAllTokens();
					// this.getView().byId("FS2").removeAllTokens();
					// this.getView().byId("FS3").removeAllTokens();
				}
			}

		},

		_prefillSeason: function (oEvent) {
			var fruit = this.getView().byId("fruitId");
			var params = this.checkSeason();
			if (fruit.getTokens().length === 1)
				this._prepareSeason(params, this._globalModel, null, this._panelModel, "", "Q");
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			//	this.clearSelectionFields();
			this.initVariant();
			this.initSearchVariant();
		},

		onChangeGtp: function (oObject) {
			this._globalModel.setProperty("/GTPFlag", "U");
			this._globalModel.setProperty("/GTPFlagButton", false);
			var selectedRow = {};
			var table = this._tableId,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				aIndices = table.getBinding().aIndices,
				gtp_quoteData = this._quoteTableM.getProperty("/tableRows");

			if (aSelIndex.length === 1) {
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});
				aSelIndex = newSelIndex;
				selectedRow = gtp_quoteData[aSelIndex[0]];
				if (selectedRow["A0DOC_NUMBER"].substring(0, 3) != "GTP") {
					MessageBox.warning("Please select a GTP quote row before you start quote change!");
					return;
				}
			} else {
				MessageBox.warning("Please only select a GTP quote row before you start quote change!");
				return;
			}

			// if (aSelIndex.length === 1) {
			// 	if (selectedRow["A0DOC_NUMBER"].substring(0, 3) != "GTP") {
			// 		MessageBox.warning("Please select a GTP quote row before you start quote change!");
			// 		return;
			// 	}
			// } else {
			// 	if (aSelIndex.length === 2) {
			// 		if (gtp_quoteData[aSelIndex[0]]["isSum"] === "X") {
			// 			selectedRow = gtp_quoteData[aSelIndex[1]];
			// 			if (selectedRow["A0DOC_NUMBER"].substring(0, 3) != "GTP") {
			// 				MessageBox.warning("Please select a GTP quote row before you start quote change!");
			// 				return;
			// 			}
			// 		} else {
			// 			if (gtp_quoteData[aSelIndex[1]]["isSum"] === "X") {
			// 				if (selectedRow["A0DOC_NUMBER"].substring(0, 3) != "GTP") {
			// 					MessageBox.warning("Please select a GTP quote row before you start quote change!");
			// 					return;
			// 				}
			// 			} else {
			// 				MessageBox.warning("Please only select a GTP quote row before you start quote change!");
			// 				return;
			// 			}
			// 		}
			// 	} else {

			// 	}
			// }

			var data = [{
				QUOTE_NO: selectedRow["A0DOC_NUMBER"],
				QUOTE_ITEM: selectedRow["A0S_ORD_ITEM"],
				MATNR: selectedRow["A0MATERIAL"],
				FRUIT: selectedRow["A0MATERIAL__RMW21ZH02"],
				FRUIT_T: selectedRow["A0MATERIAL__RMW21ZH02_T"],
				FRUIT_EDITABLE: selectedRow["A0MATERIAL"] === "",
				PRODUCT_TYPE_EDITABLE: selectedRow["A0MATERIAL"] === "",
				PRODUCT_TYPE: selectedRow["A0MATERIAL__RMW11ZP16"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZP16"],
				WERKS: selectedRow["A0PLANT"] === "#" ? "" : selectedRow["A0PLANT"],
				VKORG: selectedRow["A0SALESORG"] === "#" ? "" : selectedRow["A0SALESORG"],
				VTWEG: selectedRow["A0DISTR_CHAN"] === "#" ? "" : selectedRow["A0DISTR_CHAN"],
				CUSTL2: selectedRow["A0CUSTOMER"] === "#" ? "" : selectedRow["A0CUSTOMER"],
				SHIP_TO_PARTY: selectedRow["A0SHIP_TO"] === "#" ? "" : selectedRow["A0SHIP_TO"],
				PAYER: selectedRow["A0CUSTOMER"] === "#" ? "" : selectedRow["A0CUSTOMER"],
				INCO1: selectedRow["A0INCOTERMS"] === "#" ? "" : selectedRow["A0INCOTERMS"],
				ZTERM: selectedRow["A0PMNTTRMS"] === "#" ? "" : selectedRow["A0PMNTTRMS"],
				VOLUME: selectedRow["QUOTATION_QUANTITY"] === "#" ? "" : selectedRow["QUOTATION_QUANTITY"],
				VOLUME_MEINS: selectedRow["ADOEMEINS"],
				ZZMBG: selectedRow["MIN_ORDER_QTY"],
				ZZMBG_MEINS: selectedRow["A0UNIT"],
				PRICE: selectedRow.GROSS_PRICE_CURR / selectedRow.PRICE_UNIT,
				PRICE_WAERS: selectedRow["A0DOC_CURRCY"],
				KPEIN: selectedRow["PRICE_UNIT"],
				KMEIN: selectedRow["PRICE_UNIT_U"],
				ANGDT: selectedRow["A0QUOT_FROM"],
				BNDDT: selectedRow["A0QUOT_TO"],
				GUEBG: selectedRow["ADGUEBG"],
				GUEEN: selectedRow["ADGUEEN"],
				QUOTE_COMMENT: selectedRow["ADGTPCMNT"] === "#" ? "" : selectedRow["ADGTPCMNT"],
				PACKAGING_CLASS: selectedRow["ADPACKCLAS"] === "#" ? "" : selectedRow["ADPACKCLAS"],
				VSBED: selectedRow["ADOEVSBED"] === "#" ? "" : selectedRow["ADOEVSBED"],
				SEASON_START: this._panelModel.getData().SFULLYEAR_FORMAT,
				SEASON_END: this._panelModel.getData().EFULLYEAR_FORMAT,
				ARMW11ZE03: selectedRow["A0MATERIAL__RMW11ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001987",
				ARMW21ZE03: selectedRow["A0MATERIAL__RMW71ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001562",
				ARMW31ZE03: selectedRow["A0MATERIAL__RMW81ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001562",
				ARMW11ZE03_T: this.getDetermineOrganic(selectedRow["A0MATERIAL__RMW11ZE03_T"] === "no" ? "CUST-X.0000000001563" :
					"0000000001987"),
				ARMW21ZE03_T: this.getDetermineHalal(selectedRow["A0MATERIAL__RMW71ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001562"),
				ARMW31ZE03_T: this.getDetermineKosher(selectedRow["A0MATERIAL__RMW81ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001562"),
				FRUIT_SPCF_1_ITEMS: [],
				FRUIT_SPCF_2_ITEMS: [],
				FRUIT_SPCF_3_ITEMS: [],
				ADFRTSPC1: selectedRow["A0MATERIAL__DFRTSPC1"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC1"],
				ADFRTSPC2: selectedRow["A0MATERIAL__DFRTSPC2"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC2"],
				ADFRTSPC3: selectedRow["A0MATERIAL__DFRTSPC3"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC3"]
			}];

			this.CreateGTPquoteTableM.setProperty("/tableRows", data);
			this._updateFS1(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);
			this._updateFS2(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);
			this._updateFS3(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);

			var oControl = oObject.getSource();
			if (!this._oPopoverCreateGTP) {
				Fragment.load({
					id: this.getView().getId(),
					name: "com.doehler.Z_GTP.fragments.CreateGTPQuoteTable",
					controller: this
				}).then(function (oPopover) {
					this._oPopoverCreateGTP = oPopover;
					this.getView().addDependent(this._oPopoverCreateGTP);
					this._oPopoverCreateGTP.openBy(oControl);
				}.bind(this));
			} else {
				this._oPopoverCreateGTP.openBy(oControl);
			}

		},

		onCreateGtp: function (oObject) {
			this._globalModel.setProperty("/GTPFlag", "I");
			this._globalModel.setProperty("/GTPFlagButton", true);
			var that = this;
			var isSelected = false;
			var selectedRow = {};
			var filteredData = sap.ui.getCore().getModel("quotePanelM").getData();
			var gtp_quoteData = this._quoteTableM.getProperty("/tableRows");
			if (filteredData.FRUIT === "") {
				MessageBox.warning("Please select a fruit before you start quote creation.");
				return;
			}
			if (filteredData.FRUIT !== "") {
				try {
					var fruitPanelText = this.getView().byId("fruitId").getTokens()[0].getText();
				} catch (err) {}

			}
			var currentDay = new Date();
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyyMMdd"
			});

			var table = this._tableId,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				aIndices = table.getBinding().aIndices;

			if (aSelIndex.length === 1) {
				isSelected = true;
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});
				aSelIndex = newSelIndex;
				selectedRow = gtp_quoteData[aSelIndex[0]];
			}

			var PlantFilterValue = filteredData.PLANT;
			if (PlantFilterValue.indexOf(",") !== -1) PlantFilterValue = "";

			var data = [{
				MATNR: isSelected ? selectedRow["A0MATERIAL"] === "#" ? "" : selectedRow["A0MATERIAL"] : "",
				FRUIT: isSelected ? selectedRow["A0MATERIAL"] !== "" && selectedRow["A0MATERIAL"] !== "#" ? "" : selectedRow[
					"A0MATERIAL__RMW21ZH02"] === "#" ? "" : selectedRow[
					"A0MATERIAL__RMW21ZH02"] : filteredData.FRUIT,
				FRUIT_T: isSelected ? selectedRow["A0MATERIAL"] !== "" && selectedRow["A0MATERIAL"] !== "#" ? "" : selectedRow[
						"A0MATERIAL__RMW21ZH02_T"] === "#" ? "" :
					selectedRow["A0MATERIAL__RMW21ZH02_T"] : fruitPanelText,
				FRUIT_EDITABLE: (isSelected ? selectedRow["A0MATERIAL"] === "#" ? "" : selectedRow["A0MATERIAL"] : "") === "",
				PRODUCT_TYPE: isSelected ? selectedRow["A0MATERIAL"] !== "" && selectedRow["A0MATERIAL"] !== "#" ? "" : selectedRow[
						"A0MATERIAL__RMW11ZP16"] === "#" ? "" :
					selectedRow["A0MATERIAL__RMW11ZP16"] : filteredData
					.PRODUCT_TYPE,
				PRODUCT_TYPE_EDITABLE: (isSelected ? selectedRow["A0MATERIAL"] === "#" ? "" : selectedRow["A0MATERIAL"] : "") === "",
				WERKS: isSelected ? selectedRow["A0PLANT"] === "#" ? "" : selectedRow["A0PLANT"] : PlantFilterValue,
				VKORG: isSelected ? selectedRow["A0SALESORG"] === "#" ? "" : selectedRow["A0SALESORG"] : filteredData.SALESORG,
				VTWEG: isSelected ? selectedRow["A0DISTR_CHAN"] === "#" ? "10" : selectedRow["A0DISTR_CHAN"] : "10",
				CUSTL2: isSelected ? selectedRow["A0CUSTOMER"] === "#" ? "" : selectedRow["A0CUSTOMER"] : filteredData.CUSTOMERL2,
				SHIP_TO_PARTY: isSelected ? selectedRow["A0SHIP_TO"] === "#" ? "" : selectedRow["A0SHIP_TO"] : filteredData.CUSTOMERL2,
				PAYER: isSelected ? selectedRow["A0CUSTOMER"] === "#" ? "" : selectedRow["A0CUSTOMER"] : filteredData.CUSTOMERL2,
				INCO1: isSelected ? selectedRow["A0INCOTERMS"] === "#" ? "FCA" : selectedRow["A0INCOTERMS"] : "FCA",
				ZTERM: isSelected ? selectedRow["A0PMNTTRMS"] === "#" ? "00" : selectedRow["A0PMNTTRMS"] : "00",
				VOLUME: 0,
				VOLUME_MEINS: "KG",
				ZZMBG: 0,
				ZZMBG_MEINS: "KG",
				PRICE: isSelected ? selectedRow.GROSS_PRICE_CURR / selectedRow.PRICE_UNIT : 0,
				KPEIN: "1",
				KMEIN: "KG",
				ANGDT: dateFormat.format(currentDay),
				BNDDT: "",
				GUEBG: filteredData.GUEBG === undefined ? "" : filteredData.GUEBG,
				GUEEN: filteredData.GUEEN === undefined ? "" : filteredData.GUEEN,
				SEASON_START: this._panelModel.getData().SFULLYEAR_FORMAT,
				SEASON_END: this._panelModel.getData().EFULLYEAR_FORMAT,
				QUOTE_COMMENT: "",
				PACKAGING_CLASS: isSelected ? selectedRow.ADPACKCLAS === "#" ? "" : selectedRow.ADPACKCLAS : "",
				PRICE_WAERS: isSelected ? selectedRow.A0DOC_CURRCY === "#" ? "" : selectedRow.A0DOC_CURRCY : "",
				VSBED: "05",
				ARMW11ZE03: isSelected ? selectedRow["A0MATERIAL__RMW11ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001987" : "CUST-X.0000000001563",
				ARMW21ZE03: isSelected ? selectedRow["A0MATERIAL__RMW71ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001562" : "CUST-X.0000000001563",
				ARMW31ZE03: isSelected ? selectedRow["A0MATERIAL__RMW81ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001562" : "CUST-X.0000000001563",
				ARMW11ZE03_T: that.getDetermineOrganic(isSelected ? selectedRow["A0MATERIAL__RMW11ZE03_T"] === "no" ? "CUST-X.0000000001563" :
					"0000000001987" : "CUST-X.0000000001563"),
				ARMW21ZE03_T: that.getDetermineHalal(isSelected ? selectedRow["A0MATERIAL__RMW71ZE03_T"] === "no" ? "CUST-X.0000000001563" :
					"0000000001562" : "CUST-X.0000000001563"),
				ARMW31ZE03_T: that.getDetermineKosher(isSelected ? selectedRow["A0MATERIAL__RMW81ZE03_T"] === "no" ? "CUST-X.0000000001563" :
					"0000000001562" : "CUST-X.0000000001563"),
				FRUIT_SPCF_1_ITEMS: [],
				FRUIT_SPCF_2_ITEMS: [],
				FRUIT_SPCF_3_ITEMS: [],
				ADFRTSPC1: isSelected ? selectedRow["A0MATERIAL__DFRTSPC1"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC1"] : "",
				ADFRTSPC2: isSelected ? selectedRow["A0MATERIAL__DFRTSPC2"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC2"] : "",
				ADFRTSPC3: isSelected ? selectedRow["A0MATERIAL__DFRTSPC3"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC3"] : ""
			}];

			this.CreateGTPquoteTableM.setProperty("/tableRows", data);
			this._updateFS1(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);
			this._updateFS2(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);
			this._updateFS3(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);

			var oControl = oObject.getSource();
			if (!this._oPopoverCreateGTP) {
				Fragment.load({
					id: this.getView().getId(),
					name: "com.doehler.Z_GTP.fragments.CreateGTPQuoteTable",
					controller: this
				}).then(function (oPopover) {
					this._oPopoverCreateGTP = oPopover;
					this.getView().addDependent(this._oPopoverCreateGTP);
					this._oPopoverCreateGTP.openBy(oControl);
				}.bind(this));
			} else {
				this._oPopoverCreateGTP.openBy(oControl);
			}

		},
		handleCloseButtonCreateGtp: function (oEvent) {
			this._oPopoverCreateGTP.close();
		},

		onseasonTableMSelect: function (oEvent) {},

		onTableRowChangeGTPQuote: function (oEvent) {
			var that = this;
			var createGTPquoteTableModel = this.CreateGTPquoteTableM;
			var customer,
				salesOrg,
				distChannel,
				matnr;
			var fieldName = oEvent.getSource().getFieldName();
			var UserInput = oEvent.getParameter("value") === "" || oEvent.getParameter("value") === undefined ? oEvent.getSource().getProperty(
				"desc") : oEvent.getParameter("value");
			var bindingContext = oEvent.getSource().getBindingContext("CreateGTPquoteTableM") !== undefined ? oEvent.getSource().getBindingContext(
					"CreateGTPquoteTableM") :
				sap
				.ui.getCore().getModel("globalM").getProperty("/pressTableValueControl").getBindingContext("CreateGTPquoteTableM");
			var path = bindingContext.getPath();
			if (fieldName === "CUSTOMERL2") {
				customer = createGTPquoteTableModel.getProperty(path + "/CUSTL2");
				createGTPquoteTableModel.setProperty(path + "/SHIP_TO_PARTY", customer);
				createGTPquoteTableModel.setProperty(path + "/PAYER", customer);

			}

			if (fieldName === "PRICE") {
				if (UserInput === "")
					createGTPquoteTableModel.setProperty(path + "/PRICE", 0);
			}

			if (fieldName === "PLANT") {
				if (UserInput === "" || UserInput === undefined) {
					this.getView().byId("plantfilterid").removeAllTokens();
					this.getView().byId("plantfilterid").destroyTokens();
				} else {
					this._panelModel.setProperty("/PLANT", UserInput);
					this.getView().byId("plantfilterid").addToken(new sap.m.Token({
						key: UserInput,
						text: UserInput
					}));
				}

			}

			if (fieldName === "MATERIAL_GTP_QUOTE") {
				matnr = createGTPquoteTableModel.getProperty(path + "/MATNR");
				if (matnr !== "") {

					var Promise1 = new Promise(function (myResolve, myReject) {
						that._checkFruitMantr(UserInput, that._panelModel.getProperty("/FRUIT"), myResolve);
					});

					Promise1.then(
						function (value) {
							if (value === "E") {
								createGTPquoteTableModel.setProperty(path + "/MATNR", "");
							} else {
								createGTPquoteTableModel.setProperty(path + "/FRUIT", "");
								createGTPquoteTableModel.setProperty(path + "/FRUIT_T", "");
								createGTPquoteTableModel.setProperty(path + "/ADFRTSPC1", "");
								createGTPquoteTableModel.setProperty(path + "/ADFRTSPC2", "");
								createGTPquoteTableModel.setProperty(path + "/ADFRTSPC3", "");
								createGTPquoteTableModel.setProperty(path + "/ARMW11ZE03", "CUST-X.0000000001563");
								createGTPquoteTableModel.setProperty(path + "/ARMW21ZE03", "CUST-X.0000000001563");
								createGTPquoteTableModel.setProperty(path + "/ARMW31ZE03", "CUST-X.0000000001563");
								createGTPquoteTableModel.setProperty(path + "/PRODUCT_TYPE", "");
								that._getQuoteValidTo(path);
								createGTPquoteTableModel.setProperty(path + "/FRUIT_EDITABLE", false);
								createGTPquoteTableModel.setProperty(path + "/PRODUCT_TYPE_EDITABLE", false);
							}

						}
					);

				} else {
					createGTPquoteTableModel.setProperty(path + "/FRUIT_EDITABLE", true);
					createGTPquoteTableModel.setProperty(path + "/PRODUCT_TYPE_EDITABLE", true);
				}

			}

			if (fieldName === "FRUIT") {
				createGTPquoteTableModel.setProperty(path + "/FRUIT", createGTPquoteTableModel.getProperty(path + "/FRUIT_T"));
			}

			if (fieldName === "PRODUCT_TYPE") {
				var row = createGTPquoteTableModel.getProperty(path);
				this._updateFS1(this.CreateGTPquoteTableM, path, row);
				this._updateFS2(this.CreateGTPquoteTableM, path, row);
				this._updateFS3(this.CreateGTPquoteTableM, path, row);
			}

			if (fieldName === "MATERIAL_GTP_QUOTE" || fieldName === "PLANT") {
				var plant = createGTPquoteTableModel.getProperty(path + "/WERKS");
				matnr = createGTPquoteTableModel.getProperty(path + "/MATNR");
				if (plant !== "" && matnr !== "") {
					var Promise2 = new Promise(function (myResolve, myReject) {
						that._fillPackingClass(matnr, plant, myResolve);
					});

					Promise2.then(
						function (value) {
							if (value !== "") {
								createGTPquoteTableModel.setProperty(path + "/PACKAGING_CLASS", value);
								createGTPquoteTableModel.setProperty(path + "/PACKAGING_CLASS_EDITABLE", false);

							} else {
								createGTPquoteTableModel.setProperty(path + "/PACKAGING_CLASS_EDITABLE", true);
							}
						})

				}
			}

			this._checkValidationOfGTPQuote(fieldName, path, createGTPquoteTableModel);

		},

		_getQuoteValidTo: function (path) {
			var that = this,
				sendData = {},
				weekNumber = 0,
				today = new Date(),
				line = this.CreateGTPquoteTableM.getProperty(path);
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyyMMdd"
			});
			if ((line.MATNR === "" || line.MATNR === undefined) || (line.BNDDT !== "" && line.BNDDT !== undefined)) return;
			sendData.MATERIAL = line.MATNR;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "DETERMINE_QUOTE_VALID_TO"
				},
				"INPUTPARAMS": [sendData]
			};

			dbcontext.callServer(params, function (oModel) {
				weekNumber = oModel.getData().LV_WEEKS;
				if (weekNumber !== 0) {
					today.setDate(today.getDate() + weekNumber * 7);
					that.CreateGTPquoteTableM.setProperty(path + "/BNDDT", dateFormat.format(today));
				}
			}, that);

		},

		handleCreateGTPQuote: function () {
			var that = this;
			var flag = this._globalModel.getProperty("/GTPFlag");
			//var sFullYear=this._panelModel.getData()["SFULLYEAR_FORMAT"];

			var gtp_quoteData = this.CreateGTPquoteTableM.getProperty("/tableRows");
			var sendData = {
				QUOTES: []
			};
			var isFieldsFull = this._checkGTPItems(gtp_quoteData);
			if (isFieldsFull === false) {
				MessageBox.warning(
					"Please fill all fields."
				);
				return;
			}

			//for Fruit Specific checks
			var msg = this._checkFruitSpecificsIsReady(gtp_quoteData, "ADFRTSPC1", "ADFRTSPC2", "ADFRTSPC3");
			if (msg !== "") {
				MessageBox.warning(msg);
				return;
			}

			if (gtp_quoteData.length > 0 && flag !== "U") {
				if (gtp_quoteData[0]["SEASON_START"] === "" && gtp_quoteData[0]["SEASON_END"] === "") {
					if (this._globalModel.getData()["seasonDates"] !== undefined) {
						if (this._globalModel.getData()["seasonDates"].length > 0) {
							var sYear = this._globalModel.getData()["seasonDates"][1].sfullYear;
							var eYear = this._globalModel.getData()["seasonDates"][1].efullYear;
							if (sYear.split(".").length === 3) {
								gtp_quoteData[0]["SEASON_START"] = sYear.split(".")[2] + sYear.split(".")[1] + sYear.split(".")[0];
							}
							if (eYear.split(".").length === 3) {
								gtp_quoteData[0]["SEASON_END"] = eYear.split(".")[2] + eYear.split(".")[1] + eYear.split(".")[0];
							}

						}
					}
					if (gtp_quoteData[0]["SEASON_START"] === "" && gtp_quoteData[0]["SEASON_END"] === "") {
						MessageBox.warning("Season could not be determined!");
						return;
					}
				}
			}

			sendData.QUOTES = sendData.QUOTES.concat(gtp_quoteData);

			switch (flag) {
			case "I":
				var func = "CREATE_QUOTE_GTP";
				break;
			default:
				var func = "CHANGE_QUOTE_GTP";
			}
			var params = {
				"HANDLERPARAMS": {
					"FUNC": func
				},
				"INPUTPARAMS": [sendData]
			};

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						that._oPopoverCreateGTP.close();
						that.onGo();
					}
				});
			}, that);
		},

		_checkGTPItems: function (gtpQuoteData) {
			var isFieldsFull = true;
			var i = 0;
			for (i = 0; i < gtpQuoteData.length; i++) {
				if (gtpQuoteData[i].MATNR === "") {
					if (gtpQuoteData[i].PRODUCT_TYPE === "" || gtpQuoteData[i].WERKS === "" || gtpQuoteData[i].VKORG === "" || gtpQuoteData[i].VTWEG ===
						"" || gtpQuoteData[i].CUSTOMERL2 === "" ||
						gtpQuoteData[i].SHIP_TO_PARTY === "" || gtpQuoteData[i].PAYER === "" || gtpQuoteData[i].INCO1 === "" || gtpQuoteData[i].ZTERM ===
						"" ||
						gtpQuoteData[i].VOLUME === "" || gtpQuoteData[i].VOLUME === 0 || gtpQuoteData[i].VOLUME_MEINS === "" || gtpQuoteData[i].ZZMBG ===
						"" ||
						gtpQuoteData[i].ZZMBG === 0 || gtpQuoteData[i].ZZMBG_MEINS === "" || gtpQuoteData[i].PRICE === "" || gtpQuoteData[i].PRICE ===
						0 ||
						gtpQuoteData[i].PRICE_WAERS === "" || gtpQuoteData[i].KPEIN === "" || gtpQuoteData[i].KPEIN === 0 || gtpQuoteData[i].KMEIN ===
						"" ||
						gtpQuoteData[i].QUOTFROM === "" || gtpQuoteData[i].QUOTTO === "" || gtpQuoteData[i].PRICEFROM === "" || gtpQuoteData[i].PRICETO ===
						"") {
						isFieldsFull = false;
					}
				}

				if (gtpQuoteData[i].MATNR !== "") {
					if (gtpQuoteData[i].WERKS === "" || gtpQuoteData[i].VKORG === "" || gtpQuoteData[i].VTWEG === "" || gtpQuoteData[i].CUSTOMERL2 ===
						"" ||
						gtpQuoteData[i].SHIP_TO_PARTY === "" || gtpQuoteData[i].PAYER === "" || gtpQuoteData[i].INCO1 === "" || gtpQuoteData[i].ZTERM ===
						"" ||
						gtpQuoteData[i].VOLUME === "" || gtpQuoteData[i].VOLUME === 0 || gtpQuoteData[i].VOLUME_MEINS === "" || gtpQuoteData[i].ZZMBG ===
						"" ||
						gtpQuoteData[i].ZZMBG === 0 || gtpQuoteData[i].ZZMBG_MEINS === "" || gtpQuoteData[i].PRICE === "" || gtpQuoteData[i].PRICE ===
						0 ||
						gtpQuoteData[i].PRICE_WAERS === "" || gtpQuoteData[i].KPEIN === "" || gtpQuoteData[i].KPEIN === 0 || gtpQuoteData[i].KMEIN ===
						"" ||
						gtpQuoteData[i].QUOTFROM === "" || gtpQuoteData[i].QUOTTO === "" || gtpQuoteData[i].PRICEFROM === "" || gtpQuoteData[i].PRICETO ===
						"" || gtpQuoteData[i].VSBED === "") {
						isFieldsFull = false;
					}
				}
			}
			return isFieldsFull;

		},

		/************************************************* create sap quote ***********************************************************************/

		onCreateSapQuote: function (oObject) {
			var isSelected = false;
			var selectedRow = {};
			var filteredData = sap.ui.getCore().getModel("quotePanelM").getData();
			var gtp_quoteData = this._quoteTableM.getProperty("/tableRows");
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyyMMdd"
			});
			var quotFrom = new Date();
			var todayDate = dateFormat.format(quotFrom);

			var table = this._tableId,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				aIndices = table.getBinding().aIndices;

			if (aSelIndex.length === 1) {
				isSelected = true;
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});
				aSelIndex = newSelIndex;
				selectedRow = gtp_quoteData[aSelIndex[0]];
			}

			var data = [];
			data.push(this._getSAPQuoteLine(this._panelModel, isSelected, selectedRow, filteredData, todayDate));

			this.createSAPquoteTableM.setProperty("/tableRows", data);
			var oControl = oObject.getSource();
			if (!this._oPopoverCreateSAP) {
				Fragment.load({
					id: this.getView().getId(),
					name: "com.doehler.Z_GTP.fragments.createSAPQuoteTable",
					controller: this
				}).then(function (oPopover) {
					this._oPopoverCreateSAP = oPopover;
					this.getView().addDependent(this._oPopoverCreateSAP);
					this._oPopoverCreateSAP.openBy(oControl);
				}.bind(this));
			} else {
				this._oPopoverCreateSAP.openBy(oControl);
			}

		},

		onAddSAPQuote: function (oEvent) {
			var isSelected = false;
			var selectedRow = {};
			var filteredData = sap.ui.getCore().getModel("quotePanelM").getData();
			var gtp_quoteData = this._quoteTableM.getProperty("/tableRows");
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyyMMdd"
			});
			var quotFrom = new Date();
			var todayDate = dateFormat.format(quotFrom);

			var table = this._tableId,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				aIndices = table.getBinding().aIndices;

			if (aSelIndex.length === 1) {
				isSelected = true;
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});
				aSelIndex = newSelIndex;
				selectedRow = gtp_quoteData[aSelIndex[0]];
			}

			var data = this.createSAPquoteTableM.getProperty("/tableRows");
			data.push(this._getSAPQuoteLine(this._panelModel, isSelected, selectedRow, filteredData, todayDate));
			this.createSAPquoteTableM.setProperty("/tableRows", data);
		},

		handleCloseButtonCreateSAP: function (oEvent) {
			this._oPopoverCreateSAP.close();
		},

		_checkValidationOfSAPQuote: function (fieldName, path) {
			var customer,
				salesOrg,
				distChannel,
				isReady = true;
			if (fieldName === "SALESORG") {
				customer = this.createSAPquoteTableM.getProperty(path + "/CUSTL2");
				if (customer === "") {
					MessageBox.warning("Please enter Customer l2 field data before the Sales Org");
					this.createSAPquoteTableM.setProperty(path + "/VKORG", "");
					isReady = false;
				}
			}

			if (fieldName === "SHIP_TO") {
				customer = this.createSAPquoteTableM.getProperty(path + "/CUSTL2");
				salesOrg = this.createSAPquoteTableM.getProperty(path + "/VKORG");
				distChannel = this.createSAPquoteTableM.getProperty(path + "/VTWEG");
				if (customer === "" || salesOrg === "" || distChannel === "") {
					MessageBox.warning("Please enter Customer l2, Sales Org and Distribution Channel fields datas before the Ship to");
					this.createSAPquoteTableM.setProperty(path + "/SHIP_TO_PARTY", "");
					isReady = false;
				}
			}

			if (fieldName === "PAYER") {
				customer = this.createSAPquoteTableM.getProperty(path + "/CUSTL2");
				salesOrg = this.createSAPquoteTableM.getProperty(path + "/VKORG");
				distChannel = this.createSAPquoteTableM.getProperty(path + "/VTWEG");
				if (customer === "" || salesOrg === "" || distChannel === "") {
					MessageBox.warning("Please enter Customer l2, Sales Org and Distribution Channel fields datas before the Payer");
					this.createSAPquoteTableM.setProperty(path + "/PAYER", "");
					isReady = false;
				}
			}

			if (fieldName === "CURRENCY") {
				salesOrg = this.createSAPquoteTableM.getProperty(path + "/VKORG");
				customer = this.createSAPquoteTableM.getProperty(path + "/CUSTL2");
				if (salesOrg === "" || customer === "") {
					MessageBox.warning("Please enter Customer l2 and Sales Org fields datas before the Currency");
					this.createSAPquoteTableM.setProperty(path + "/PRICE_WAERS", "");
					isReady = false;
				}
			}

			return isReady;
		},

		onTableRowChangeSAPQuote: function (oEvent) {
			var customer,
				salesOrg,
				distChannel;
			var fieldName = oEvent.getSource().getFieldName();
			var UserInput = oEvent.getParameter("value");
			var bindingContext = oEvent.getSource().getBindingContext("createSAPquoteTableM") !== undefined ? oEvent.getSource().getBindingContext(
					"createSAPquoteTableM") :
				sap.ui.getCore().getModel("globalM").getProperty("/pressTableValueControl").getBindingContext("createSAPquoteTableM");
			var path = bindingContext.getPath();
			if (fieldName === "CUSTOMERL2") {
				customer = this.createSAPquoteTableM.getProperty(path + "/CUSTL2");
				this.createSAPquoteTableM.setProperty(path + "/SHIP_TO_PARTY", customer);
				this.createSAPquoteTableM.setProperty(path + "/PAYER", customer);

			}

			if (fieldName === "PRICE") {
				if (UserInput === "")
					this.createSAPquoteTableM.setProperty(path + "/PRICE", 0);
			}

			this._checkValidationOfSAPQuote(fieldName, path);

		},

		handleSaveButtonCreateSAP: function (oEvent) {
			var aRows = {};
			aRows = this.createSAPquoteTableM.getProperty("/tableRows"); //mandatory fields

			var that = this;

			var sendData = {
				QUOTES: []
			};

			var isFieldsFull = this._checkCreateSapQuoteItems(aRows);
			if (isFieldsFull === false) {
				MessageBox.warning(
					"Please fill all fields."
				);
				return;
			}

			sendData.QUOTES = sendData.QUOTES.concat(aRows);
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "CREATE_QUOTE_SAP"
				},
				"INPUTPARAMS": [sendData]
			};

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						that._oPopovercreateSAP.close();
						that.onGo();
					}
				});
			}, that);
		},

		_checkCreateSapQuoteItems: function (aRows) {
			var isFieldsFull = true;
			var i = 0;
			for (i = 0; i < aRows.length; i++) {
				if (aRows[i].MATNR === "") {
					isFieldsFull = false;
				} else if (aRows[i].WERKS === "" || aRows[i].VKORG === "" || aRows[i].VTWEG === "" || aRows[i].CUSTOMERL2 ===
					"" ||
					aRows[i].SHIP_TO_PARTY === "" || aRows[i].PAYER === "" ||
					aRows[i].INCO1 === "" || aRows[i].ZTERM === "" || aRows[i].VOLUME === "" || aRows[i].VOLUME === 0 || aRows[i].VOLUME_MEINS ===
					"" || aRows[i].ZZMBG ===
					"" || aRows[i].ZZMBG === 0 ||
					aRows[i].ZZMBG_MEINS ===
					"" || aRows[i].PRICE === "" || aRows[i].PRICE === 0 || aRows[i].PRICE_WAERS === "" ||
					aRows[i].KPEIN === "" || aRows[i].KPEIN === 0 || aRows[i].KMEIN === "" || aRows[i].ANGDT === "" || aRows[i].BNDDT === "" ||
					aRows[i].GUEBG ===
					"" ||
					aRows[i].GUEEN === "" || aRows[i].VSBED === "") {
					isFieldsFull = false;
				}

			}
			return isFieldsFull;
		},

		onDelete: function (oEvent) {
			var that = this;
			var selectedRow = oEvent.getSource().getBindingInfo("visible").binding.aValues[0];
			var tableid = oEvent.getSource().getParent().getParent().getId().split("--")[1];
			var sendData = {
				GTP_NUMBER: selectedRow
			};
			MessageBox.warning("Selected line will be deleted!", {
				actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
				emphasizedAction: MessageBox.Action.OK,
				onClose: function (sAction) {
					if (sAction === "OK") {
						var params = {
							"HANDLERPARAMS": {
								"FUNC": "DELETE_QUOTE_GTP"
							},
							"INPUTPARAMS": [sendData]
						};

						dbcontext.callServer(params, function (oModel) {
							that.handleServerMessages(oModel, function (status) {
								if (status === "S") {
									if (that._tableIdText1 === tableid) {
										that.onCheckDuplicate();
									} else {
										that.onGo();
									}
								}

							});
						}, that);
					}

				}
			});

		},
		onTableRowChange: function (oEvent) {
			var fieldName = oEvent.getSource().data("id");
			var path = oEvent.getSource().getBindingContext("quoteTableM").getPath();
			var docNo = this._quoteTableM.getProperty(path).A0DOC_NUMBER;
			var value = oEvent.getSource().getSelectedKey();
			this._quoteTableM.setProperty(path + "/CHANGED", true);

			if (fieldName === 'INDICATOR' && value === '4') {
				this._quoteTableM.setProperty(path + "/A0CRM_REJECT", "C4");
			} else if (fieldName === 'INDICATOR' && value === '6') {
				this._quoteTableM.setProperty(path + "/A0CRM_REJECT", "N4");
			} else {
				this._quoteTableM.setProperty(path + "/A0CRM_REJECT", "");
			}
		},

		onSave: function (oEvent) {
			var that = this,
				tableData = this._quoteTableM.getData(),
				dataToSave = tableData.tableRows.filter(function (m) {
					return m.CHANGED === true;
				});

			if (!dataToSave.length) {
				MessageBox.warning("There is no change!");
				return;
			}

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_STATUS_CHANGE"
				},
				"INPUTPARAMS": dataToSave
			};

			// that._globalModel.setProperty("/isSaved", "X");

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						// var changed = tableData.tableRows.filter(function (m) {
						// 	return m.CHANGED === true;
						// });
						// changed.forEach(function (m) {
						// 	m.isSave = true;
						// });
						// var notchanged = tableData.tableRows.filter(function (m) {
						// 	return m.CHANGED !== true;
						// });
						that.onGo();
					}
				});
			}, that);

		},
		////////////////////////////////// DUPLICATE POPUP///////////////////////////////		
		onTableRowChangeDuplicate: function (oEvent) {
			var fieldName = oEvent.getSource().data("id");
			var path = oEvent.getSource().getBindingContext("duplicateQuoteTableM").getPath();
			var docNo = this._duplicateQuoteTableM.getProperty(path).A0DOC_NUMBER;
			var value = oEvent.getSource().getSelectedKey();
			this._duplicateQuoteTableM.setProperty(path + "/CHANGED", true);

			if (fieldName === 'INDICATOR' && value === '4') {
				this._duplicateQuoteTableM.setProperty(path + "/A0CRM_REJECT", "C4");
			} else if (fieldName === 'INDICATOR' && value === '6') {
				this._duplicateQuoteTableM.setProperty(path + "/A0CRM_REJECT", "N4");
			} else {
				this._duplicateQuoteTableM.setProperty(path + "/A0CRM_REJECT", "");
			}
		},
		handleSaveDuplicateStatus: function (oEvent) {
			var that = this,
				tableData = this._duplicateQuoteTableM.getData(),
				dataToSave = tableData.tableRows.filter(function (m) {
					return m.CHANGED === true;
				});

			if (!dataToSave.length) {
				MessageBox.warning("There is no change!");
				return;
			}

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_STATUS_CHANGE"
				},
				"INPUTPARAMS": dataToSave
			};

			// that._globalModel.setProperty("/isSaved", "X");

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						// var changed = tableData.tableRows.filter(function (m) {
						// 	return m.CHANGED === true;
						// });
						// changed.forEach(function (m) {
						// 	m.isSave = true;
						// });
						// var notchanged = tableData.tableRows.filter(function (m) {
						// 	return m.CHANGED !== true;
						// });
						that.onCheckDuplicate();
					}
				});
			}, that);

		},

		onCheckDuplicate: function (oEvent) {
			var that = this;
			var seasonInput = this._panelModel.getData()["SEASON"];
			var filterCurrent = this._panelModel.getData();

			if (seasonInput === undefined || seasonInput === "") {
				MessageBox.warning("Season input can not be empty");
				return;
			}

			var SFULLYEAR = this._panelModel.getData().SFULLYEAR;
			var EFULLYEAR = this._panelModel.getData().EFULLYEAR;
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				if (seasonInput.split("[").length > 1) {
					if (seasonInput.split("[")[1].split(" - ").length > 1) {
						SFULLYEAR = seasonInput.split("[")[1].split(" - ")[0];
						var sFormmat = SFULLYEAR.split(".")[2] + SFULLYEAR.split(".")[1] + SFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
						EFULLYEAR = seasonInput.split("[")[1].split(" - ")[1];
						var eFormmat = EFULLYEAR.split(".")[2] + EFULLYEAR.split(".")[1] + EFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormmat);
					}
				}
			}
			var dateFrom = SFULLYEAR.split(".")[1] + "." + SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[2];
			dateFrom = new Date(dateFrom);
			var yearFrom = dateFrom.getFullYear() - 1;
			var prevDateFrom = SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[1] + "." + yearFrom.toString();
			var dateTo = new Date(dateFrom - 24 * 60 * 60 * 1000);
			var prevDateTo = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(new Date(dateTo));

			this._panelModel.setProperty("/PREV_FROM", prevDateFrom);
			this._panelModel.setProperty("/PREV_TO", prevDateTo);
			this._panelModel.setProperty("/DATE_TO", sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(new Date()));

			var dateTo = new Date().setFullYear(new Date().getFullYear() - 2);
			this._panelModel.setProperty("/DATE_FROM", sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(new Date(dateTo)));
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "CHECK_DUPLICATES"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			// sap.ui.core.BusyIndicator.show();
			dbcontext.callServer(params, function (oModel) {

				var data = oModel.getData().RESULTS;
				that._duplicateQuoteTableM.setProperty("/tableRows", data);
				if (oEvent) {
					that.OpenDuplicatePopup();
				}
				// sap.ui.core.BusyIndicator.hide();

			}, that);

		},

		OpenDuplicatePopup: function () {
			var oControl = this.getView().byId("dupid");

			if (!this._oPopoverDuplicate) {
				Fragment.load({
					id: "fr1",
					name: "com.doehler.Z_GTP.fragments.CheckDuplicateQuotes",
					controller: this
				}).then(function (oPopover) {
					this._oPopoverDuplicate = oPopover;
					this.getView().addDependent(this._oPopoverDuplicate);
					this._tableId1 = Fragment.byId("fr1", this._tableIdText1);
					this.initVariant1();
					this._oPopoverDuplicate.openBy(oControl);
				}.bind(this));
			} else {
				this._oPopoverDuplicate.openBy(oControl);
			}

		},

		onAfterOpen: function (oEvent) {
			var table = Fragment.byId("fr1", "DuplicateQuoteTableId");
		},

		handleCloseDuplicatePopup: function (oEvent) {
			this._oPopoverDuplicate.close();
		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					that._findTableKeys();
				},
				function (error) { /* code if some error */ }
			);
		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);
		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

	});

});