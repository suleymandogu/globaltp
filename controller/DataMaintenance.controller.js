sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	"sap/m/MessageBox"
], function (BaseController, dbcontext, dbcontext2, excel, MessageBox) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.DataMaintenance", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.doehler.Z_GTP.view.DataMaintenance
		 */
		onInit: function () {
			this._setButtonsPressed("DataMainBtnId");

			this._tableId = this.getView().byId("dataMaintenanceTableid");
			this.bDialog = new sap.m.BusyDialog();
			//panel Model
			this._panelModel = this.getOwnerComponent().getModel("dataMaintenancePanelM");
			this._globalModel = this.getOwnerComponent().getModel("globalM");
			this._dataMaintenanceTableM = this.getOwnerComponent().getModel("dataMaintenanceTableM");

			//oDataModel
			this._initializePanelModel();
			this.getRouter().getRoute("DataMaintenance").attachPatternMatched(this._onRouteMatched, this);

		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("DataMainBtnId");
			this.onGo();

		},
		onValueHelpInit: function (oEvent) {

			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));

		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/MATERIAL", "");
			this._panelModel.setProperty("/FRUIT_SPEC", "");

		},
		onGo: function () {
			var that = this,
				data = [];

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_DATA_MAINTENANCE"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			dbcontext.callServer(params, function (oModel) {
				data = oModel.getData().RESULTS;
				data.forEach(function (line) {
					line.FS1_VALUE_T = that.getDetermineFSText(line.FS1_VALUE);
					line.FS2_VALUE_T = that.getDetermineFSText(line.FS2_VALUE);
					line.FS3_VALUE_T = that.getDetermineFSText(line.FS3_VALUE);
				});
				that._dataMaintenanceTableM.setProperty("/tableRows", data);
				that.initallData = data;
			}, that);
		},
		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();

				}
				if (controls[i].getMetadata().getName() === "sap.m.Input") {
					controls[i].setValue("");
				}
			}
		},
		onAdd: function () {
			var data = this._dataMaintenanceTableM.getProperty("/tableRows");

			if (data === undefined) data = [];
			data.unshift({
				MANDT: "",
				MATNR: "",
				FS1_VALUE: "",
				FS2_VALUE: "",
				FS3_VALUE: "",
				COMMENT_T: "",
				MAKTX: ""
			});

			this._dataMaintenanceTableM.setProperty("/tableRows", data);

		},

		onDelete: function (oEvent) {
			var that = this;

			var selectedRowMANDT = oEvent.getSource().getParent().getRowBindingContext().getProperty("MANDT");

			if (selectedRowMANDT === "") {
				var data = this._dataMaintenanceTableM.getProperty("/tableRows");
				var path = oEvent.getSource().getParent().getRowBindingContext().getPath();
				var index = path.split("/")[2];
				data.splice(index, "1");
				this._dataMaintenanceTableM.setProperty("/tableRows", data);
				return;
			}
			var selectedRow = oEvent.getSource().getParent().getRowBindingContext().getProperty("MATNR");
			var sendData = {
				MATNR: selectedRow
			};
			MessageBox.warning("Selected line will be deleted!", {
				actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
				emphasizedAction: MessageBox.Action.OK,
				onClose: function (sAction) {
					var params = {
						"HANDLERPARAMS": {
							"FUNC": "DELETE_FRUIT_MAT"
						},
						"INPUTPARAMS": [sendData]
					};

					dbcontext.callServer(params, function (oModel) {
						that.handleServerMessages(oModel, function (status) {
							if (status === "S") {
								that.onGo();
							}
						});
					}, that);
				}
			});

		},

		onTableRowChange: function (oEvent) {

			var bindingContext = oEvent.getSource().getBindingContext("dataMaintenanceTableM") !== undefined ? oEvent.getSource().getBindingContext(
					"dataMaintenanceTableM") :
				sap
				.ui.getCore().getModel("globalM").getProperty("/pressTableValueControl").getBindingContext("dataMaintenanceTableM");
			var path = bindingContext.getPath();

			this._dataMaintenanceTableM.setProperty(path + "/CHANGED", true);

			var line = this._dataMaintenanceTableM.getProperty(path);
			if (line.FS1_VALUE !== "" && line.FS1_VALUE !== undefined)
				line.FS1_VALUE_T = this.getDetermineFSText(line.FS1_VALUE);
			else
				line.FS1_VALUE_T = "";
			if (line.FS2_VALUE !== "" && line.FS2_VALUE !== undefined)
				line.FS2_VALUE_T = this.getDetermineFSText(line.FS2_VALUE);
			else
				line.FS2_VALUE_T = "";
			if (line.FS3_VALUE !== "" && line.FS3_VALUE !== undefined)
				line.FS3_VALUE_T = this.getDetermineFSText(line.FS3_VALUE);
			else
				line.FS3_VALUE_T = "";

		},
		onSave: function (oEvent) {
			var that = this,
				tableData = this._dataMaintenanceTableM.getData(),
				dataToSave = tableData.tableRows.filter(function (m) {
					return m.CHANGED === true;
				});

			if (!dataToSave.length) {
				MessageBox.warning("There is no change!");
				return;
			}

			var isEmpty = dataToSave.filter(function (m) {
				return m["MATNR"] === "";
			});
			if (isEmpty.length > 0) {
				MessageBox.warning("Please fill key field (Material)");
				return;
			}

			//for Fruit Specific checks

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_FRUIT_MAT"
				},
				"INPUTPARAMS": [{
					"DATA": dataToSave
				}]
			};

			// that._globalModel.setProperty("/isSaved", "X");

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						that.onGo();
					}
				});
			}, that);

		},

		onExport: function () {
			var data = [];
			var tableData = this._dataMaintenanceTableM.getData();
			var copiedData = tableData.tableRows;
			var aIndices = this._tableId.getBinding("rows").aIndices;

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}
			excel._downloadExcel("DM", this._tableId, data);
		},

	});

});