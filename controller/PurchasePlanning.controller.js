sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/formatter",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	'sap/ui/core/Fragment',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, formatter, dbcontext, dbcontext2, excel, Fragment, JSONModel, MessageToast,
	MessageBox, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.PurchasePlanning", {
		formatter: formatter,
		onInit: function () {
			this._setButtonsPressed("seasonalPlanToOfferId");
			this._tableId1 = this.getView().byId("purchasePlanCurrentTableId");
			this._tableId2 = this.getView().byId("purchasePlanHistoricalTableId");
			this._tableId1Text = "purchasePlanCurrentTableId";
			this._tableId2Text = "purchasePlanHistoricalTableId";
			this.bDialog = new sap.m.BusyDialog();
			//panel Model
			this._panelModel = this.getOwnerComponent().getModel("purchasePlanPanelM");
			this._globalModel = this.getOwnerComponent().getModel("globalM");
			this._globalModel.setProperty("/seasonText", "");
			this._globalModel.setProperty("/distributeTotal", 0);
			this._purchasePlanTableM = this.getOwnerComponent().getModel("purchasePlanTableM");
			this._globalModel.setProperty("/PurchTitle", 'Purchase Planning');
			this._globalModel.setProperty("/PurchHistoricalTitle", 'Purchase');

			//oDataModel
			this._seasonalODataModel = this.getOwnerComponent().getModel("QUOTE_MANAGEMENT_MODEL");
			sap.ui.getCore().setModel(this._panelModel, "purchasePlanPanelM");
			this._initializePanelModel();
			sap.ui.getCore().getModel("globalM").setProperty("/isPurchasePlanning", false);
			this.getRouter().getRoute("PurchasePlanning").attachPatternMatched(this._onRouteMatched, this);

			var organic = [{
				"ORGANIC_TEXT": "no",
				"ORGANIC": "CUST-X.0000000001563"
			}, {
				"ORGANIC_TEXT": "yes",
				"ORGANIC": "CUST-X.0000000001987"
			}];
			this._globalModel.setProperty("/ORGANIC", organic);

			var halal = [{
				"HALAL": "CUST-X.0000000001563",
				"HALAL_TEXT": "no"
			}, {
				"HALAL": "CUST-X.0000000001562",
				"HALAL_TEXT": "yes"
			}];
			this._globalModel.setProperty("/HALAL", halal);

			var kosher = [{
				"KOSHER": "CUST-X.0000000001563",
				"KOSHER_TEXT": "no"
			}, {
				"KOSHER": "CUST-X.0000000001562",
				"KOSHER_TEXT": "yes"
			}];
			this._globalModel.setProperty("/KOSHER", kosher);

			var purchHistSelection = [{
				"key": "1",
				"text": "Previous season"
			}, {
				"key": "2",
				"text": "Current season"
			}];
			this._globalModel.setProperty("/purchHistSelection", purchHistSelection);
			this._globalModel.setProperty("/purchHistSelectionKey", "1");
			this._globalModel.setProperty("/purchaseMonthColumnText", "Year n-1 (historical figure)");
		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("seasonalPlanToOfferId");
			if (sap.ui.getCore().getModel("globalM").getProperty("/isPurchasePlanning") === true)
				this.createToken();

		},

		onSelectHisSelection: function (oEvent) {
			var selectedKey = oEvent.getSource().getSelectedKey();
			switch (selectedKey) {
			case "1":
				this.onGo(false, true); //Previous
				break;
			case "2":
				this.onGo(false, true); //Current
				break;
			default:
			}
		},

		onGoFunc: function () {
			this.onGo(true, true);
		},

		onGo: function (_upperIsRead, _lowerIsRead) {
			var that = this,
				data = [],
				headerKpis = [],
				currentTableData = [],
				columnText,
				fruitTokens = this.getView().byId("fruitId").getTokens(),
				fruitFilterValue;
			if (fruitTokens.length === 0) {
				MessageBox.warning("Fruit input can not be empty");
				return;
			}
			fruitFilterValue = fruitTokens[0].getText();
			var seasonInput = this._panelModel.getData()["SEASON"];
			var fruitInput = this._panelModel.getData()["FRUIT"];
			var plantInput = this._panelModel.getData()["PLANT"];
			var filterCurrent = this._panelModel.getData();
			var seasonCheckMsg = this._globalModel.getProperty("/seasonCheckMsg");
			if (seasonCheckMsg !== undefined && seasonCheckMsg !== "") {
				MessageBox.warning(seasonCheckMsg);
				return;
			}
			if (fruitInput === undefined || fruitInput === "") {
				MessageBox.warning("Fruit input can not be empty");
				return;
			}
			if (plantInput === undefined || plantInput === "") {
				MessageBox.warning("Plant manufacturing input can not be empty");
				return;
			}
			if (seasonInput === "") {
				MessageBox.warning("Season can not be empty!");
				return;
			}

			var SFULLYEAR = this._panelModel.getData().SFULLYEAR;
			var EFULLYEAR = this._panelModel.getData().EFULLYEAR;
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				if (seasonInput.split("[").length > 1) {
					if (seasonInput.split("[")[1].split(" - ").length > 1) {
						SFULLYEAR = seasonInput.split("[")[1].split(" - ")[0];
						var sFormmat = SFULLYEAR.split(".")[2] + SFULLYEAR.split(".")[1] + SFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
						EFULLYEAR = seasonInput.split("[")[1].split(" - ")[1];
						var eFormmat = EFULLYEAR.split(".")[2] + EFULLYEAR.split(".")[1] + EFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormmat);
					}
				}
			}
			var dateFrom = SFULLYEAR.split(".")[1] + "." + SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[2];
			dateFrom = new Date(dateFrom);
			var yearFrom = dateFrom.getFullYear() - 1;
			var prevDateFrom = SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[1] + "." + yearFrom.toString();
			var dateTo = new Date(dateFrom - 24 * 60 * 60 * 1000);
			var prevDateTo = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(new Date(dateTo));

			if (this._globalModel.getData()["purchHistSelectionKey"] === "2") {
				prevDateFrom = SFULLYEAR;
				prevDateTo = EFULLYEAR;
			}

			this._globalModel.setProperty("/purchaseMonthColumnText", this._globalModel.getData()["purchHistSelectionKey"] === "1" ?
				"Year n-1 (historical figure)" : "Year n (historical figure)");

			this._panelModel.setProperty("/PREV_FROM", prevDateFrom);
			this._panelModel.setProperty("/PREV_TO", prevDateTo);
			this._tableId1.setSelectedIndex(-1);
			this._tableId2.setSelectedIndex(-1);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_PURCHASE_PLANNING_DATA"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			dbcontext.callServer(params, function (oModel) {
				columnText = oModel.getData().DYNAMIC;
				if (_upperIsRead) {
					that._globalModel.setProperty("/isSaved", "");
					currentTableData = oModel.getData().PROD_CURRENT;
					currentTableData.forEach(function (current) {
						current["FRUIT_TEXT"] = fruitFilterValue;
					});
					currentTableData.forEach(function (line) {
						line["ORGANIC_F"] = that.getDetermineOrganic(line["ORGANIC"]);
						line["HALAL_F"] = that.getDetermineHalal(line["HALAL"]);
						line["KOSHER_F"] = that.getDetermineKosher(line["KOSHER"]);
						line["FRUIT_SPCF_1_T"] = that.getDetermineFSText(line["FRUIT_SPCF_1"]);
						line["FRUIT_SPCF_2_T"] = that.getDetermineFSText(line["FRUIT_SPCF_2"]);
						line["FRUIT_SPCF_3_T"] = that.getDetermineFSText(line["FRUIT_SPCF_3"]);
					});

					currentTableData = that._calculateTotalCurrent(currentTableData);
					that._purchasePlanTableM.setProperty("/tableRows1", currentTableData);
					that._getSumRowHCurrent();
				}

				if (_lowerIsRead) {
					data = oModel.getData().RESULTS;
					headerKpis = oModel.getData().HEADER_KPIS;
					that._panelModel.setProperty("/CON_JC", headerKpis.CON_JC);
					that._panelModel.setProperty("/CON_NFC", headerKpis.CON_NFC);
					that._panelModel.setProperty("/CON_PUREE", headerKpis.CON_PUREE);
					that._panelModel.setProperty("/NON_CON_JC", headerKpis.NON_CON_JC);
					that._panelModel.setProperty("/NON_CON_NFC", headerKpis.NON_CON_NFC);
					that._panelModel.setProperty("/NON_CON_PUREE", headerKpis.NON_CON_PUREE);
					data.forEach(function (line) {
						if (line["A0MATERIAL__RMW11ZP16"] === "#") line["A0MATERIAL__RMW11ZP16"] = "";
						if (line["A0MATERIAL__DFRTSPC1"] === "#") line["A0MATERIAL__DFRTSPC1"] = "";
						if (line["A0MATERIAL__DFRTSPC2"] === "#") line["A0MATERIAL__DFRTSPC2"] = "";
						if (line["A0MATERIAL__DFRTSPC3"] === "#") line["A0MATERIAL__DFRTSPC3"] = "";
						line["A0MATERIAL__RMW11ZE03_T"] = that.getDetermineOrganic(line["A0MATERIAL__RMW11ZE03"]);
						line["A0MATERIAL__RMW21ZE03_T"] = that.getDetermineHalal(line["A0MATERIAL__RMW21ZE03"]);
						line["A0MATERIAL__RMW31ZE03_T"] = that.getDetermineKosher(line["A0MATERIAL__RMW31ZE03"]);
						line["A0MATERIAL__DFRTSPC1_T"] = that.getDetermineFSText(line["A0MATERIAL__DFRTSPC1"]);
						line["A0MATERIAL__DFRTSPC2_T"] = that.getDetermineFSText(line["A0MATERIAL__DFRTSPC2"]);
						line["A0MATERIAL__DFRTSPC3_T"] = that.getDetermineFSText(line["A0MATERIAL__DFRTSPC3"]);

					});
					data = that._calculateTotalHistorical(data);
					that._purchasePlanTableM.setProperty("/tableRows2", data);
					that._getSumRowHistorical();
					that.initallData = data;
					that._findTableKeys();
					that._findTableKeys2();
					that._calculateTiles(data);
				}

				that.setDynamiCol(columnText, _upperIsRead, _lowerIsRead);

			}, that);
		},

		getDetermineFSText: function (fs1) {
			var resultText = "";

			resultText = this._globalModel.getData().FS_TEXTS.filter(function (m) {
				return m.DOMVALUE_L === fs1;
			});

			if (resultText.length)
				resultText = resultText[0].DDTEXT;

			return resultText;
		},

		_calculateTiles: function (data) {

		},

		_calculateTotalHistorical: function (data) {
			var totalRow = 0;
			data.forEach(function (row) {
				totalRow = 0;
				Object.keys(row).forEach(function (rowAttr) {
					if (rowAttr.indexOf("COL_") > -1)
						totalRow = totalRow + row[rowAttr];
				});

				row["TOTAL"] = totalRow;
			});
			this._purchasePlanTableM.setProperty("/tableRows2", data);
			this._getSumRowHistorical();
			this._calculateTilesProducedLast12M(data);

			return data;

		},

		_calculateTotalCurrent: function (currentTableData) {
			var totalRow = 0;
			currentTableData.forEach(function (row) {
				totalRow = 0;
				Object.keys(row).forEach(function (rowAttr) {
					if (rowAttr.indexOf("MONTH_") > -1)
						totalRow = totalRow + row[rowAttr];
				});

				row["SEASON_TOTAL"] = totalRow;
			});

			return currentTableData;

		},

		_calculateTilesProducedLast12M: function (data) {
			// var prodType180 = data.filter(function (line) {
			// 	return line.A0MATERIAL__RMW11ZP16 === "180";
			// });
			// var prodType270 = data.filter(function (line) {
			// 	return line.A0MATERIAL__RMW11ZP16 === "270";
			// });
			// var prodType390 = data.filter(function (line) {
			// 	return line.A0MATERIAL__RMW11ZP16 === "390";
			// });

			// var total180 = 0;
			// prodType180.forEach(function (t1) {
			// 	total180 += t1["TOTAL"];
			// });
			// var total270 = 0;
			// prodType270.forEach(function (t1) {
			// 	total270 += t1["TOTAL"];
			// });
			// var total390 = 0;
			// prodType390.forEach(function (t1) {
			// 	total390 += t1["TOTAL"];
			// });

			// this._panelModel.setProperty("/nfc", total180);
			// this._panelModel.setProperty("/puree", total270);
			// this._panelModel.setProperty("/jc", total390);
		},

		onValueHelpInit: function (oEvent) {
			if (oEvent.getSource().getRelFieldModel() === "CreateGTPquoteTableM") {
				var pressed = sap.ui.getCore().getModel("globalM").getData()["pressTableValueControl"];
				var createGTPquoteTableModel = this.CreateGTPquoteTableM;
				if (pressed !== undefined) {
					if (this._checkValidationOfGTPQuote(oEvent.getSource().getFieldName(), pressed.getBindingContext("CreateGTPquoteTableM").getPath(),
							createGTPquoteTableModel)) {
						oEvent.getSource().setController(this);
						oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
					}
				}
			} else if (oEvent.getSource().getRelFieldModel() === "createSAPquoteTableM") {
				pressed = sap.ui.getCore().getModel("globalM").getData()["pressTableValueControl"];
				if (pressed !== undefined) {
					if (this._checkValidationOfSAPQuote(oEvent.getSource().getFieldName(), pressed.getBindingContext("createSAPquoteTableM").getPath())) {
						oEvent.getSource().setController(this);
						oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
					}
				}
			} else {
				oEvent.getSource().setController(this);
				oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
			}

		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/CERTIFICATE", "");
			this._panelModel.setProperty("/FS1", "");
			this._panelModel.setProperty("/FS2", "");
			this._panelModel.setProperty("/FS3", "");
			this._panelModel.setProperty("/SEASON", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");

			this._panelModel.setProperty("/SUPPLIER", "");
			this._panelModel.setProperty("/COUNTRY_SUPPLY", "");

			this._panelModel.setProperty("/CON_NFC", 0);
			this._panelModel.setProperty("/CON_JC", 0);
			this._panelModel.setProperty("/CON_PUREE", 0);

			this._panelModel.setProperty("/NON_CON_NFC", 0);
			this._panelModel.setProperty("/NON_CON_JC", 0);
			this._panelModel.setProperty("/NON_CON_PUREE", 0);

		},
		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();

				}
				if (controls[i].getMetadata().getName() === "sap.m.Input") {
					controls[i].setValue("");
				}
			}
		},

		onPressAggregation: function () {
			this.convertGroup(this.mainTableKeys);
			this.collectData();
		},

		convertGroup: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};
			if (this.initallData !== undefined) {
				var groupData = groupBy(this.initallData, groupCols);
				this.groupData = groupData;
			}
		},

		collectData: function () {
			var newCollectedData = [];
			var that = this;
			$.each(this.groupData, function (i, v) {
				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry.A0PLANT = k.A0PLANT;
					newEntry.A0PLANT_T = k.A0PLANT_T;
					newEntry.A0MATERIAL__RMW21ZH02 = k.A0MATERIAL__RMW21ZH02;
					newEntry.A0MATERIAL__RMW21ZH02_T = k.A0MATERIAL__RMW21ZH02_T;
					newEntry.A0MATERIAL__RMW11ZP16 = k.A0MATERIAL__RMW11ZP16;
					newEntry.A0MATERIAL__RMW11ZP16_T = k.A0MATERIAL__RMW11ZP16_T;
					newEntry.A0MATERIAL__RMW11ZE03 = k.A0MATERIAL__RMW11ZE03;
					newEntry.A0MATERIAL__RMW11ZE03_T = k.A0MATERIAL__RMW11ZE03_T;
					newEntry.A0MATERIAL__RMW21ZE03 = k.A0MATERIAL__RMW21ZE03;
					newEntry.A0MATERIAL__RMW21ZE03_T = k.A0MATERIAL__RMW21ZE03_T;
					newEntry.A0MATERIAL__RMW31ZE03 = k.A0MATERIAL__RMW31ZE03;
					newEntry.A0MATERIAL__RMW31ZE03_T = k.A0MATERIAL__RMW31ZE03_T;
					newEntry.A0MATERIAL__DFRTSPC1 = k.A0MATERIAL__DFRTSPC1;
					newEntry.A0MATERIAL__DFRTSPC1_T = k.A0MATERIAL__DFRTSPC1_T;
					newEntry.A0MATERIAL__DFRTSPC2 = k.A0MATERIAL__DFRTSPC2;
					newEntry.A0MATERIAL__DFRTSPC2_T = k.A0MATERIAL__DFRTSPC2_T;
					newEntry.A0MATERIAL__DFRTSPC3 = k.A0MATERIAL__DFRTSPC3;
					newEntry.A0MATERIAL__DFRTSPC3_T = k.A0MATERIAL__DFRTSPC3_T;
					newEntry.A0VENDOR__0COUNTRY = k.A0VENDOR__0COUNTRY;
					newEntry.A0VENDOR__0COUNTRY_T = k.A0VENDOR__0COUNTRY_T;
					newEntry.A0SUPPLIER = k.A0SUPPLIER;
					newEntry.A0SUPPLIER_T = k.A0SUPPLIER_T;
					// newEntry["A0MATERIAL__RMW11ZE03_F"] = that.getDetermineOrganic(k.A0MATERIAL__RMW11ZE03);
					// newEntry["A0MATERIAL__RMW21ZE03_F"] = that.getDetermineOrganic(k.A0MATERIAL__RMW21ZE03);
					// newEntry["A0MATERIAL__RMW31ZE03_F"] = that.getDetermineOrganic(k.A0MATERIAL__RMW31ZE03);
					newEntry.TOTAL = 0;
					for (var i = 1; i <= 12; i++) {
						var colnameCurrent = "COL_";
						colnameCurrent = colnameCurrent + i;
						newEntry[colnameCurrent] = newEntry[colnameCurrent] === undefined ? 0 : newEntry[colnameCurrent];
						newEntry[colnameCurrent] += k[colnameCurrent];
						newEntry.TOTAL += newEntry[colnameCurrent];
					}
				});
				newEntry = that._hasComment(newEntry, 1);
				newCollectedData.push(newEntry);
			});

			this._purchasePlanTableM.setProperty("/tableRows2", newCollectedData);
			this._getSumRowHistorical();
		},

		_getSumRowHistorical: function () {
			var newCollectedData = this._purchasePlanTableM.getProperty("/tableRows2");
			var data = newCollectedData.filter(function (line) {
				return line["isSum"] !== "X";
			});

			var totalRow = this.setTotalRow(data, "", this.columnfilterHis);
			data.unshift(totalRow);

			data.forEach(function (line) {
				if (line["A0MATERIAL__DFRTSPC1_T"] === "0") line["A0MATERIAL__DFRTSPC1_T"] = "";
				if (line["A0MATERIAL__DFRTSPC2_T"] === "0") line["A0MATERIAL__DFRTSPC2_T"] = "";
				if (line["A0MATERIAL__DFRTSPC3_T"] === "0") line["A0MATERIAL__DFRTSPC3_T"] = "";
			});
			this._purchasePlanTableM.setProperty("/tableRows2", data);
		},

		_getSumRowHCurrent: function () {
			var newCollectedData = this._purchasePlanTableM.getProperty("/tableRows1");
			var data = newCollectedData.filter(function (line) {
				return line["isSum"] !== "X";
			});

			var totalRow = this.setTotalRow(data, "", this.columnfilterCurr);
			data.unshift(totalRow);

			data.forEach(function (line) {
				if (line["FRUIT_SPCF_1_T"] === "0") line["FRUIT_SPCF_1_T"] = "";
				if (line["FRUIT_SPCF_2_T"] === "0") line["FRUIT_SPCF_2_T"] = "";
				if (line["FRUIT_SPCF_3_T"] === "0") line["FRUIT_SPCF_3_T"] = "";
			});
			this._purchasePlanTableM.setProperty("/tableRows1", data);
		},

		setDynamiCol: function (columnText, _upperIsRead, _lowerIsRead) {
			var that = this;
			var key = this._globalModel.getData()["purchHistSelectionKey"];
			var colname = "/PURCCOL_";
			var colnameCurrent = "/PURCMONTH_";

			var sdate = that._panelModel.getData().SFULLYEAR;
			var edate = that._panelModel.getData().EFULLYEAR;
			var smonth = sdate.split(".")[1];
			var syear = sdate.split(".")[2];
			var currentcoltxt = smonth + "." + syear;
			var prevcolTxt = smonth + "." + (parseInt(syear) - 1).toString();
			if (key === "2") prevcolTxt = currentcoltxt;
			var eyear = edate.split(".")[2];
			var nsyear = syear - 1;
			if (key === "2") nsyear = syear;
			var nsdate = sdate.split(".")[0] + "." + sdate.split(".")[1] + "." + nsyear;
			var neyear = eyear - 1;
			if (key === "2") neyear = eyear;
			var nedate = edate.split(".")[0] + "." + edate.split(".")[1] + "." + neyear;
			for (var i = 1; i <= 12; i++) {
				colname = "/PURCCOL_";
				colnameCurrent = "/PURCMONTH_";
				colname = colname + i;
				colnameCurrent = colnameCurrent + i;
				that._globalModel.setProperty(colname, "");
				if (_upperIsRead)
					that._globalModel.setProperty(colnameCurrent, currentcoltxt);
				if (_lowerIsRead)
					that._globalModel.setProperty(colname, prevcolTxt);
				if (smonth === "12") {
					smonth = "01";
					syear++;
				} else {
					smonth++;
					if (smonth < 10) {
						smonth = "0" + smonth.toString();
					} else {
						smonth = smonth.toString();
					}
				}

				currentcoltxt = smonth + "." + syear;
				prevcolTxt = smonth + "." + (parseInt(syear) - 1).toString();
				if (key === "2") prevcolTxt = currentcoltxt;
			}
			that._globalModel.setProperty("/lenghOfMonth", 12);

			columnText.forEach(function (item, index) {
				colname = "/PURCCOL_";
				index++;
				colname = colname + index;
				that._globalModel.setProperty(colname, item.CHAVL_EXT);
			});
			var title = 'Purchase Planning:' + sdate + ' - ' + edate;
			if (_upperIsRead)
				that._globalModel.setProperty("/PurchTitle", title);
			var historicalTitle = 'Purchase:' + nsdate + ' - ' + nedate;
			if (_lowerIsRead)
				that._globalModel.setProperty("/PurchHistoricalTitle", historicalTitle);
		},

		onAddRow: function () {
			var that = this;
			this._tableId1.setSelectedIndex(-1);
			var seasonInput = this._panelModel.getData()["SEASON"];
			var fruitInput = this._panelModel.getData()["FRUIT"];
			var plantInput = this._panelModel.getData()["PLANT"];
			// if (seasonInput === undefined || seasonInput === "") {
			// 	MessageBox.warning("Season input can not be empty");
			// 	return;
			// }
			if (fruitInput === undefined || fruitInput === "") {
				MessageBox.warning("Fruit input can not be empty");
				return;
			}
			if (plantInput === undefined || plantInput === "") {
				MessageBox.warning("Plant manufacturing input can not be empty");
				return;
			}
			var data = [];
			var fruitFilterValue = this.getView().byId("fruitId").getTokens()[0].getText();
			var fruitFilterKey = this.getView().byId("fruitId").getTokens()[0].getKey();
			var plantFilterValue = this._panelModel.getProperty("/PLANT");
			var productTypeFilterValue = this._panelModel.getProperty("/PRODUCT_TYPE");

			fruitFilterValue = fruitFilterValue === undefined ? "" : fruitFilterValue;
			plantFilterValue = plantFilterValue === undefined ? "" : plantFilterValue;
			
			

			if (plantFilterValue.indexOf(",") !== -1) plantFilterValue = "";
			
			productTypeFilterValue = productTypeFilterValue === undefined ? "" : productTypeFilterValue;
			var gtp_pruchData = this._purchasePlanTableM.getProperty("/tableRows2");
			var selectedRows = [];
			var isSelected = false;
			var table = this._tableId2,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				aIndices = table.getBinding().aIndices;

			var currentTableData = this._purchasePlanTableM.getProperty("/tableRows1");
			currentTableData = currentTableData === undefined ? [] : currentTableData;

			

			if (aSelIndex.length > 0) {
				isSelected = true;
				aSelIndex.forEach(function (selIndex) {
					selectedRows.push(gtp_pruchData[selIndex]);
				});

				if (selectedRows.filter(function (m) {
						return m["isSum"] === "X"
					}).length > 0) {
					MessageToast.show("Sum row cannot be selected..");
					return;
				}
			} else {

				var emptyRow = {
					ADDED: "X",
					FRUIT: isSelected ? selectedRow["A0MATERIAL__RMW21ZH02"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZH02"] : fruitFilterKey,
					FRUIT_TEXT: isSelected ? selectedRow["A0MATERIAL__RMW21ZH02_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZH02_T"] : fruitFilterValue,
					PLANT_MAN: isSelected ? selectedRow["A0PLANT"] === "#" ? "" : selectedRow["A0PLANT"] : plantFilterValue,
					PRODUCT_TYPE: isSelected ? selectedRow["A0MATERIAL__RMW11ZP16"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZP16"] : productTypeFilterValue,
					ORGANIC: isSelected ? selectedRow["A0MATERIAL__RMW11ZE03"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZE03"] : "CUST-X.0000000001563",
					ORGANIC_TEXT: isSelected ? selectedRow["A0MATERIAL__RMW11ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZE03_T"] : "no",
					HALAL: isSelected ? selectedRow["A0MATERIAL__RMW21ZE03"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZE03"] : "CUST-X.0000000001563",
					HALAL_TEXT: isSelected ? selectedRow["A0MATERIAL__RMW21ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZE03_T"] : "no",
					KOSHER: isSelected ? selectedRow["A0MATERIAL__RMW31ZE03"] === "#" ? "" : selectedRow["A0MATERIAL__RMW31ZE03"] : "CUST-X.0000000001563",
					KOSHER_TEXT: isSelected ? selectedRow["A0MATERIAL__RMW31ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW31ZE03_T"] : "no",
					FRUIT_SPCF_1: isSelected ? selectedRow["A0MATERIAL__DFRTSPC1"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC1"] : "",
					FRUIT_SPCF_2: isSelected ? selectedRow["A0MATERIAL__DFRTSPC2"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC2"] : "",
					FRUIT_SPCF_3: isSelected ? selectedRow["A0MATERIAL__DFRTSPC3"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC3"] : "",
					ORGANIC_F: isSelected ? selectedRow["A0MATERIAL__RMW11ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZE03_T"] : "no",
					HALAL_F: isSelected ? selectedRow["A0MATERIAL__RMW21ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZE03_T"] : "no",
					KOSHER_F: isSelected ? selectedRow["A0MATERIAL__RMW31ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW31ZE03_T"] : "no",
					LIFNR: isSelected ? selectedRow["A0SUPPLIER"] === "#" ? "" : selectedRow["A0SUPPLIER"] : "",
					LIFNR_LAND1: isSelected ? selectedRow["A0VENDOR__0COUNTRY"] === "#" ? "" : selectedRow["A0VENDOR__0COUNTRY"] : "",
					MONTH_1: 0,
					MONTH_2: 0,
					MONTH_3: 0,
					MONTH_4: 0,
					MONTH_5: 0,
					MONTH_6: 0,
					MONTH_7: 0,
					MONTH_8: 0,
					MONTH_9: 0,
					MONTH_10: 0,
					MONTH_11: 0,
					MONTH_12: 0,
					SEASON_TOTAL: 0,
					FRUIT_SPCF_1_ITEMS: [],
					FRUIT_SPCF_2_ITEMS: [],
					FRUIT_SPCF_3_ITEMS: []
				};
				currentTableData.unshift(emptyRow);

			}
			var counter = 0;
			selectedRows.forEach(function (selectedRow) {

				var emptyRow = {
					ADDED: "X",
					FRUIT: isSelected ? selectedRow["A0MATERIAL__RMW21ZH02"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZH02"] : fruitFilterKey,
					FRUIT_TEXT: isSelected ? selectedRow["A0MATERIAL__RMW21ZH02_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZH02_T"] : fruitFilterValue,
					PLANT_MAN: isSelected ? selectedRow["A0PLANT"] === "#" ? "" : selectedRow["A0PLANT"] : plantFilterValue,
					PRODUCT_TYPE: isSelected ? selectedRow["A0MATERIAL__RMW11ZP16"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZP16"] : productTypeFilterValue,
					ORGANIC: isSelected ? selectedRow["A0MATERIAL__RMW11ZE03"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZE03"] : "CUST-X.0000000001563",
					ORGANIC_TEXT: isSelected ? selectedRow["A0MATERIAL__RMW11ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZE03_T"] : "no",
					HALAL: isSelected ? selectedRow["A0MATERIAL__RMW21ZE03"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZE03"] : "CUST-X.0000000001563",
					HALAL_TEXT: isSelected ? selectedRow["A0MATERIAL__RMW21ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZE03_T"] : "no",
					KOSHER: isSelected ? selectedRow["A0MATERIAL__RMW31ZE03"] === "#" ? "" : selectedRow["A0MATERIAL__RMW31ZE03"] : "CUST-X.0000000001563",
					KOSHER_TEXT: isSelected ? selectedRow["A0MATERIAL__RMW31ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW31ZE03_T"] : "no",
					FRUIT_SPCF_1: isSelected ? selectedRow["A0MATERIAL__DFRTSPC1"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC1"] : "",
					FRUIT_SPCF_2: isSelected ? selectedRow["A0MATERIAL__DFRTSPC2"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC2"] : "",
					FRUIT_SPCF_3: isSelected ? selectedRow["A0MATERIAL__DFRTSPC3"] === "#" ? "" : selectedRow["A0MATERIAL__DFRTSPC3"] : "",
					ORGANIC_F: isSelected ? selectedRow["A0MATERIAL__RMW11ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW11ZE03_T"] : "no",
					HALAL_F: isSelected ? selectedRow["A0MATERIAL__RMW21ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW21ZE03_T"] : "no",
					KOSHER_F: isSelected ? selectedRow["A0MATERIAL__RMW31ZE03_T"] === "#" ? "" : selectedRow["A0MATERIAL__RMW31ZE03_T"] : "no",
					LIFNR: isSelected ? selectedRow["A0SUPPLIER"] === "#" ? "" : selectedRow["A0SUPPLIER"] : "",
					LIFNR_LAND1: isSelected ? selectedRow["A0VENDOR__0COUNTRY"] === "#" ? "" : selectedRow["A0VENDOR__0COUNTRY"] : "",
					MONTH_1: 0,
					MONTH_2: 0,
					MONTH_3: 0,
					MONTH_4: 0,
					MONTH_5: 0,
					MONTH_6: 0,
					MONTH_7: 0,
					MONTH_8: 0,
					MONTH_9: 0,
					MONTH_10: 0,
					MONTH_11: 0,
					MONTH_12: 0,
					SEASON_TOTAL: 0,
					FRUIT_SPCF_1_ITEMS: [],
					FRUIT_SPCF_2_ITEMS: [],
					FRUIT_SPCF_3_ITEMS: []
				};
				counter += 1;
				currentTableData.unshift(emptyRow);

			});

			that._purchasePlanTableM.setProperty("/tableRows1", currentTableData);
			that._getSumRowHCurrent();
			currentTableData = that._purchasePlanTableM.getProperty("/tableRows1");
			if (counter > 0) {
				for (let i = 1; i < counter + 1; i++) {
					var path = "/tableRows1/" + i;
					that._updateFS1(that._purchasePlanTableM, path, currentTableData[i]);
					that._updateFS2(that._purchasePlanTableM, path, currentTableData[i]);
					that._updateFS3(that._purchasePlanTableM, path, currentTableData[i]);
				}

			} else {
				that._updateFS1(that._purchasePlanTableM, "/tableRows1/1", emptyRow);
				that._updateFS2(that._purchasePlanTableM, "/tableRows1/1", emptyRow);
				that._updateFS3(that._purchasePlanTableM, "/tableRows1/1", emptyRow);
			}

		},

		onDelete: function (oEvent) {
			var that = this,
				params,
				aSelIndex = this._tableId1.getSelectedIndices(),
				aIndices = this._tableId1.getBinding().aIndices,
				newSelIndex = [];
			aSelIndex.forEach(function (selIndex) {
				newSelIndex.push(aIndices[selIndex]);
			});

			if (newSelIndex.length === 0) {
				MessageToast.show("Please select a row.");
				return;
			}

			var deletionData = [];
			newSelIndex.forEach(function (index) {
				deletionData.push(that._purchasePlanTableM.getProperty("/tableRows1")[index]);
			});

			var dbData = deletionData.filter(function (m) {
				return m.ADDED !== 'X';
			});
			if (dbData.length === 0) {
				newSelIndex.forEach(function (index) {
					that._purchasePlanTableM.getData().tableRows1.splice(index, "1");
					that._purchasePlanTableM.setData(that._purchasePlanTableM.getData());
				});
				that._tableId1.setSelectedIndex(-1);
				that._tableId2.setSelectedIndex(-1);
			} else {
				params = {
					"HANDLERPARAMS": {
						"FUNC": "DELETE_PURCHASE_PLAN_DATA"
					},
					"INPUTPARAMS": deletionData
				};

				dbcontext.callServer(params, function (oModel) {
					if (oModel.getData().MESSAGES.TEXTS[0].MSGTYP === "S") {
						MessageBox.success(oModel.getData().MESSAGES.TEXTS[0].MSG);
						that.onGo(true, false);
					} else {
						MessageBox.error(oModel.getData().MESSAGES.TEXTS[0].MSG);
					}
				}, that);
			}
		},

		onSelectCurrentTable: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("purchasePlanTableM").getPath(),
				row = this._purchasePlanTableM.getProperty(path),
				text = oEvent.getParameter("selectedItem").getText(),
				value = oEvent.getParameter("selectedItem").getProperty("text");

			this._purchasePlanTableM.setProperty(path + "/CHANGED", true);
			if (oEvent.getSource().getBindingInfo("items").path.indexOf("ORGANIC") > -1) {
				this._purchasePlanTableM.setProperty(path + "/ORGANIC_F", value === "" ? "no" : "yes");
			}
			if (oEvent.getSource().getBindingInfo("items").path.indexOf("HALAL") > -1) {
				this._purchasePlanTableM.setProperty(path + "/HALAL_F", value === "" ? "no" : "yes");
			}
			if (oEvent.getSource().getBindingInfo("items").path.indexOf("KOSHER") > -1) {
				this._purchasePlanTableM.setProperty(path + "/KOSHER_F", value === "" ? "no" : "yes");
			}
		},

		onFruitSpcfChange: function (oEvent) {

			var path = oEvent.getSource().getBindingContext("purchasePlanTableM");
			if (path !== undefined)
				path = path.getPath();
			else
				path = this._globalModel.getData().pressTableValueControl.getBindingContext("purchasePlanTableM").getPath();

			this._purchasePlanTableM.setProperty(path + "/CHANGED", true);

		},

		onInputChange: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName();
			var path = oEvent.getSource().getBindingContext("purchasePlanTableM");
			if (path !== undefined)
				path = path.getPath();
			else
				path = this._globalModel.getData().pressTableValueControl.getBindingContext("purchasePlanTableM").getPath();

			var row = this._purchasePlanTableM.getProperty(path);

			this._purchasePlanTableM.setProperty(path + "/CHANGED", true);

			if (fieldName === "SUPPLIER") {
				var supplier = row.LIFNR;
				if (supplier === "") return;
				var supplierArray = [];
				supplierArray.push({
					LIFNR: supplier
				});
				this._getCountryBySupplier("SUPPLIER", supplierArray, path);
			}

			this._updateFS1(this._purchasePlanTableM, path, row);
			this._updateFS2(this._purchasePlanTableM, path, row);
			this._updateFS3(this._purchasePlanTableM, path, row);

		},

		_getCountryBySupplier: function (name, supplierArray, path) {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "DETERMINE_SUPPLIER_FIELDS"
				},
				"INPUTPARAMS": supplierArray
			};

			dbcontext.callServer(params, function (oModel) {
				var result = oModel.getData().RESULT;
				var row = that._purchasePlanTableM.getProperty(path);

				switch (name) {
				case "SUPPLIER":
					that._purchasePlanTableM.setProperty(path + "/LIFNR_LAND1", result.LAND1);
					break;
				default:
				}

			}, that);
		},

		onChangeCurrentTable: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("purchasePlanTableM").getPath(),
				currentTableData = this._purchasePlanTableM.getProperty("/tableRows1"),
				row = this._purchasePlanTableM.getProperty(path);

			currentTableData = this._calculateTotalCurrent(currentTableData);

			this._purchasePlanTableM.setProperty(path + "/CHANGED", true);
			this._getSumRowHCurrent();

		},

		_checkIsSame: function (upperRow) {
			var found = false,
				data;
			var lowerTable = this._purchasePlanTableM.getProperty("/tableRows2");

			var hasSame = lowerTable.filter(function (low) {
				return upperRow["FRUIT"] === low["A0MATERIAL__RMW21ZH02"] &&
					upperRow["PLANT_MAN"] === low["A0PLANT"] &&
					upperRow["PRODUCT_TYPE"] === low["A0MATERIAL__RMW11ZP16"] &&
					upperRow["ORGANIC_F"] === low["A0MATERIAL__RMW11ZE03_T"] &&
					upperRow["HALAL_F"] === low["A0MATERIAL__RMW21ZE03_T"] &&
					upperRow["KOSHER_F"] === low["A0MATERIAL__RMW31ZE03_T"] &&
					upperRow["FRUIT_SPCF_1"] === low["A0MATERIAL__DFRTSPC1"] &&
					upperRow["FRUIT_SPCF_2"] === low["A0MATERIAL__DFRTSPC2"] &&
					upperRow["FRUIT_SPCF_3"] === low["A0MATERIAL__DFRTSPC3"] &&
					upperRow["LIFNR"] === low["A0SUPPLIER"] &&
					upperRow["LIFNR_LAND1"] === low["A0VENDOR__0COUNTRY"];
			});

			if (hasSame.length > 1) {
				MessageToast.show("More than one lines matched!");
				found = false;
			}
			if (hasSame.length === 0) {
				found = false;
			}
			if (hasSame.length === 1) {
				found = true;
				data = hasSame[0];
			}

			return {
				"found": found,
				"data": data
			};

		},

		onDistributeTotal: function (oEvent) {
			var monthlyBased = 0;
			var lengthOfMonth = this._globalModel.getProperty("/lenghOfMonth");
			var distributeTotal = this._globalModel.getData().distributeTotal;
			var aSelIndex = this._tableId1.getSelectedIndices(),
				aIndices = this._tableId1.getBinding().aIndices,
				newSelIndex = [];
			var index = 0;
			if (distributeTotal === "" || isNaN(distributeTotal)) {
				MessageToast.show("Input is invalid!");
				return;
			}
			//	var isSelected = false;
			var selectedIndex = aSelIndex;
			if (selectedIndex.length < 1 || selectedIndex.length > 1) {
				MessageBox.warning("Please select a row from the table");
				return;
			} else {
				selectedIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});
				index = newSelIndex[0];
			}

			var selectedRow = this._purchasePlanTableM.getProperty("/tableRows1/" + index);

			var hasSame = this._checkIsSame(selectedRow);
			var perc = 0;
			var lowerTotal = 0;
			if (hasSame.found) {
				lowerTotal = hasSame.data.TOTAL;
				for (var a = 1; a <= lengthOfMonth; a++) {
					perc = hasSame.data["COL_" + a.toString()] * 100 / lowerTotal;
					perc = distributeTotal * perc / 100;
					perc = parseFloat(perc.toFixed(2));
					this._purchasePlanTableM.setProperty("/tableRows1/" + index + "/MONTH_" + a.toString(), perc);
				}

			} else {
				monthlyBased = distributeTotal / lengthOfMonth;
				monthlyBased = parseFloat(monthlyBased.toFixed(2));
				for (var m = 1; m <= lengthOfMonth; m++) {
					this._purchasePlanTableM.setProperty("/tableRows1/" + index + "/MONTH_" + m.toString(), monthlyBased);
				}
			}

			this._purchasePlanTableM.setProperty("/tableRows1/" + index + "/CHANGED", true);
			this._purchasePlanTableM.setProperty("/tableRows1/" + index + "/SEASON_TOTAL", distributeTotal);
			this._getSumRowHCurrent();
		},

		onSave: function (oEvent) {
			var that = this,
				tableData = this._purchasePlanTableM.getData(),
				dataToSave = tableData.tableRows1.filter(function (m) {
					return m.CHANGED === true && m["isSum"] !== "X";
				});

			if (dataToSave.length === 0) {
				MessageBox.warning("There is no change!");
				return;
			}
			var msg = this._checkFruitSpecificsIsReady(dataToSave, "FRUIT_SPCF_1", "FRUIT_SPCF_2", "FRUIT_SPCF_3");
			if (msg !== "") {
				MessageBox.warning(msg);
				return;
			}

			var that = this;

			dataToSave.forEach(function (m) {
				m.FRUIT = m.FRUIT;
				m.PLANT_MAN = m.PLANT_MAN;
				m.PRODUCT_TYPE = m.PRODUCT_TYPE;
				m.ORGANIC = m.ORGANIC;
				m.HALAL = m.HALAL;
				m.KOSHER = m.KOSHER;
				m.FRUIT_SPCF_1 = m.FRUIT_SPCF_1;
				m.FRUIT_SPCF_2 = m.FRUIT_SPCF_2;
				m.FRUIT_SPCF_3 = m.FRUIT_SPCF_3;
				m.LIFNR = m.LIFNR;
				m.LIFNR_LAND1 = m.LIFNR_LAND1;
				m.MONTH_1 = m.MONTH_1;
				m.MONTH_2 = m.MONTH_2;
				m.MONTH_3 = m.MONTH_3;
				m.MONTH_4 = m.MONTH_4;
				m.MONTH_5 = m.MONTH_5;
				m.MONTH_6 = m.MONTH_6;
				m.MONTH_7 = m.MONTH_7;
				m.MONTH_8 = m.MONTH_8;
				m.MONTH_9 = m.MONTH_9;
				m.MONTH_10 = m.MONTH_10;
				m.MONTH_11 = m.MONTH_11;
				m.MONTH_12 = m.MONTH_12;
				m.SEASON_TOTAL = m.SEASON_TOTAL;
				m.SEASON_START = that._panelModel.getData().SFULLYEAR_FORMAT;
				m.SEASON_END = that._panelModel.getData().EFULLYEAR_FORMAT;
			});

			if (dataToSave.length === 0) {
				MessageBox.warning("There is no changed line");
				return;
			}

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_PURCHASE_PLANNING_DATA"
				},
				"INPUTPARAMS": [{
					"PROD_PLAN_DATA": dataToSave
				}]
			};

			that._globalModel.setProperty("/isSaved", "X");

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					that.onGo(true, false);
				});
			}, that);

		},

		onExportCurrent: function () {
			var data = [];
			var tableData = this._purchasePlanTableM.getData();
			var copiedData = tableData.tableRows1;
			var aIndices = this._tableId1.getBinding("rows").aIndices;

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			excel._downloadExcel("PURCHASE_PLANNING_CURRENT", this._tableId1, data);
		},

		onExportHistorical: function () {
			var data = [];
			var tableData = this._purchasePlanTableM.getData();
			var copiedData = tableData.tableRows2;
			var aIndices = this._tableId2.getBinding("rows").aIndices;

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			excel._downloadExcel("PURCHASE_PLANNING_HISTORICAL", this._tableId2, data);
		},

		//************************************************ Season search help**************************************

		checkSeason: function () {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_DATA"
				},
				"INPUTPARAMS": [{
					"FRUIT": this._panelModel.getProperty("/FRUIT"),
					"PLANT": this._panelModel.getProperty("/PLANT")
				}]
			};

			return params;

		},

		handleValueHelp: function (oEvent) {
			var params = "";
			var fruit = this._panelModel.getProperty("/FRUIT");
			if (fruit === "") {
				MessageToast.show("Fruit field can not be empty!");
				return;
			} else {
				params = this.checkSeason();

			}

			var oView = this.getView();
			this._sInputId = oEvent.getSource().getId();

			// create value help dialog
			if (!this._pValueHelpDialog) {
				this._pValueHelpDialog = Fragment.load({
					id: oView.getId(),
					name: "com.doehler.Z_GTP.fragments.SeasonSearchHelp",
					controller: this
				}).then(function (oValueHelpDialog) {
					oView.addDependent(oValueHelpDialog);
					return oValueHelpDialog;
				});
			}
			this._prepareSeason(params, this._globalModel, this._pValueHelpDialog, undefined, "X");
		},

		_handleValueHelpSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Name",
				FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

		_handleValueHelpClose: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var productInput = this.byId(this._sInputId);
				productInput.setValue(oSelectedItem.getTitle());
			}
			oEvent.getSource().getBinding("items").filter([]);

			var eFormat = "",
				sFormmat = "";
			if (this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath()) === undefined)
				return;

			var line = this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath());
			if (line.efullYear.split(".").length === 3) {
				eFormat = line.efullYear.split(".")[2] + line.efullYear.split(".")[1] + line.efullYear.split(".")[0];
			}
			if (line.sfullYear.split(".").length === 3) {
				sFormmat = line.sfullYear.split(".")[2] + line.sfullYear.split(".")[1] + line.sfullYear.split(".")[0];
			}
			this._panelModel.setProperty("/SFULLYEAR", line.sfullYear);
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
			this._panelModel
				.setProperty("/EFULLYEAR", line.efullYear);
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormat);
			this._panelModel.setProperty(
				"/SEASON_TEXT", line.text);
		},

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		currTableData: null,
		oTPC: null,
		oCC2: null,
		currTableData2: null,
		oTPC2: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableId1Text);
				oCC.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		onSettings2: function () {
			this.oTPC2.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC2) {
				this.oCC2 = oCC2;
				oCC2.delItem(thiz._tableId2Text);
				oCC2.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId1;
			var oTable2 = this._tableId2;
			var oVM = this.getView().byId("purchasePlanCurrentTableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableId1Text, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableId1Text);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData
				thiz._adjustHistorical(oItem[oItem.defaultKey].data);

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp

					if (oVM.getSelectionKey() !== "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
					thiz._adjustHistorical(data);
				});
			});
		},

		_adjustHistorical: function (data) {
			var oTable2 = this._tableId2;
			var columns = oTable2.getColumns();
			var columnFound;
			var that = this;

			columns.forEach(function (column) {

				columnFound = data.aColumns.filter(function (aColumn) {
					return aColumn.id.split("--")[aColumn.id.split("--").length - 1].split("_")[1] ===
						column.getId().split("--")[column.getId().split("--").length - 1].split("_")[1];
				});
				if (columnFound.length === 1) {
					that.getView().byId(column.getId()).setVisible(columnFound[0]["visible"]);
				}
			});
			that._findTableKeys();
			that._findTableKeys2();
		},

		initVariant2: function () {
			var thiz = this;
			var oTable = this._tableId2;
			var oVM2 = this.getView().byId("purchasePlanHistoricalTableVMId");
			oVM2.setModel(new sap.ui.model.json.JSONModel());
			this.setStandardVariant(this._tableId2Text, function (oCC2) {
				oVM2.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM2.removeVariantItem(thiz.getVariantByKey2(oVM2, "default")); // fix default 
				thiz.oCC2 = oCC2;
				var oItem2 = oCC2.getItemValue(thiz._tableId2Text);
				oVM2.getModel().setData(oItem2.items); // set data in model
				oVM2.setInitialSelectionKey(oItem2.defaultKey); // set initial default
				oVM2.setDefaultVariantKey(oItem2.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem2[oItem2.defaultKey].data); // apply data
				thiz.currTableData2 = oItem2[oItem2.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC2 = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData2 = data; // store data temp
					if (oVM2.getSelectionKey() !== "*standard*") {
						oVM2.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId1 = this._tableId1Text;
			var oTable = this._tableId1;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId1);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._adjustHistorical(oItem[selKey].data);
		},
		// onSelectVariant2: function (oEvent) {
		// 	var tableId2 = this._tableId2Text;
		// 	var oTable = this._tableId2;
		// 	var selKey = oEvent.getParameters().key;
		// 	var oItem = this.oCC2.getItemValue(tableId2);
		// 	this.setPersoData(oTable, oItem[selKey].data); // apply data
		// 	this._findTableKeys();
		// },

		_findTableKeys: function () {
			var thiz = this;
			var oTable = this._tableId2;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys.length)
				thiz.onPressAggregation();
		},

		_findTableKeys2: function () {
			var thiz = this;
			var oTable = this._tableId1;
			thiz.commentTableKeys2 = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						if (columns.data("comment") !== null)
							thiz.commentTableKeys2.push(columns.data("comment"));
					}
				}
			});
			if (this._purchasePlanTableM.getProperty("/tableRows1") !== undefined)
				this._purchasePlanTableM.getProperty("/tableRows1").forEach(function (row) {
					row = thiz._hasComment(row, 2);
				});
		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId1 = this._tableId1Text;
			var oTable = this._tableId1;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId1);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId1, ovar); // set updated obj 
				oCC.save();
			}
		},
		onSaveVariant2: function (oEvent) {
			var thiz = this;
			var oCC2 = this.oCC2;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName2(oEvent.getSource(), key);
			var tableId2 = this._tableId2Text;
			var oTable = this._tableId2;
			// 2 check if varName is available or not if not then create
			var ovar = oCC2.getItemValue(tableId2);
			if (this.currTableData2) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData2,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC2.setItemValue(tableId2, ovar); // set updated obj 
				oCC2.save();
			}
		},

		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId1 = this._tableId1Text;
			var ovar = oCC.getItemValue(tableId1);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId1, ovar);
			oCC.save(); // save all
		},

		onManageVM2: function (oEvent) {
			var oCC2 = this.oCC2;
			var tableId2 = this._tableId2Text;
			var ovar = oCC2.getItemValue(tableId2);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC2.setItemValue(tableId2, ovar);
			oCC2.save(); // save all
		},

		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey === item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},

		getVariantName2: function (oVM2, selKey) {
			var aItems = oVM2.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey === item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},

		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		getVariantByKey2: function (oVM2, selKey) {
			var aItems = oVM2.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey === item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		//************************************************table variant codes**************************************

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdPurchasePlan");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("purchasePlanPanelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//	that.addSearchFilter();
			}, function () {
				//	that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isPurchasePlanning", true);
			sap.ui.getCore().getModel("globalM").setProperty("/purchasePlanningInit", true);
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("purchasePlanPanelM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantQuote: function (oEvent) {

			//	this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("purchasePlanPanelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantQuote: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("purchasePlanPanelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMQuote: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1) {
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined)
				this._updatePanelData(fromData, sap.ui.getCore().getModel("purchasePlanPanelM"));
			var aControls = this.getView().byId("searchGridId").getContent();
			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));
					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {
									if (value != "") {
										if (item.getFieldName() === "FRUIT") {
											const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
											item.addToken(new sap.m.Token({
												key: value,
												text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
											}));
										} else {
											item.addToken(new sap.m.Token({
												key: value,
												text: value
											}));
										}
									}
								});

							}
						}
					});
				}, 1000);
		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			if (type === "added" || type === "removed" || type === "removedAll") {
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "WERKS") {
					this._panelModel.setProperty("/SFULLYEAR", "");
					this._panelModel.setProperty("/EFULLYEAR", "");
					this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/PREV_FROM", "");
					this._panelModel.setProperty("/PREV_TO", "");
					this._panelModel.setProperty("/SEASON", "");

					this._prefillSeason(oEvent);
				}
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "PRODUCT_TYPE") {
					this.getView().byId("FS1").removeAllTokens();
					this.getView().byId("FS2").removeAllTokens();
					this.getView().byId("FS3").removeAllTokens();
				}
			}
		},

		_prefillSeason: function (oEvent) {
			var fruit = this.getView().byId("fruitId");
			var params = this.checkSeason();
			var season = this._panelModel.getData()["SEASON"];
			if (fruit.getTokens().length === 1)
				this._prepareSeason(params, this._globalModel, null, this._panelModel, "");
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			//	this.clearSelectionFields();
			this.initVariant();
			//this.initVariant2();
			this.initSearchVariant();
		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					that._findTableKeys();
					that._findTableKeys2();
				},
				function (error) { /* code if some error */ }
			);
		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);
		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

	});

});