/* Developer Anuj Harshe */
sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"sap/ui/table/TablePersoController",
		"com/doehler/Z_GTP/controller/ErrorHandler",
		"com/doehler/Z_GTP/model/dbcontext",
		"sap/m/MessageBox",
		"sap/ui/core/Fragment",
		"sap/m/Dialog",
		"sap/m/RadioButton",
		"sap/m/Button",
		"sap/m/PDFViewer"
	],
	function (Controller, TablePersoController, ErrorHandler, dbcontext, MessageBox, Fragment, Dialog, RadioButton, Button, PDFViewer) {
		"use strict";
		return Controller.extend("com.doehler.Z_GTP.controller.BaseController", {

			_updatePanelData: function (fromData, toModel, param2, param3) {
				if (fromData === undefined) {
					return;
				}
				var toData = toModel.getData();
				var inputFrom = "",
					inputTo = "",
					inputFinal = "",
					inputLast = [];

				if (param2 !== undefined) {
					var filterCustomData = "";
					var keyCustomData = "";
					var oColumns = sap.ui.getCore().byId(param3).getColumns();
					Object.keys(toData).forEach(function (key) {
						toModel.setProperty("/" + key, "");
					});
					oColumns.forEach(function (column) {
						filterCustomData = column.data("filter");
						keyCustomData = column.data("key");
						if (filterCustomData !== null && filterCustomData !== undefined) {
							toModel.setProperty("/" + filterCustomData, param2[keyCustomData] === "#" ? "" : param2[keyCustomData]);
						}
					});
					// if (param2 !== undefined) {
					toModel.setProperty("/EFULLYEAR", param2["EFULLYEAR"]);
					toModel.setProperty("/SFULLYEAR", param2["SFULLYEAR"]);
					toModel.setProperty("/SFULLYEAR_FORMAT", param2["SFULLYEAR_FORMAT"]);
					toModel.setProperty("/EFULLYEAR_FORMAT", param2["EFULLYEAR_FORMAT"]);
					toModel.setProperty("/SEASON", param2["SEASON"]);
					// }

				} else {

					Object.keys(fromData).forEach(function (key) {
						inputLast = [];
						if (fromData[key] !== "" && fromData[key] !== undefined) {
							if (typeof fromData[key] !== "number") {

								if (key === "EFULLYEAR" || key === "EFULLYEAR_FORMAT" || key === "PREV_FROM" || key === "PREV_TO" || key === "SEASON" || key ===
									"SEASON_TEXT" || key === "SFULLYEAR" || key === "SFULLYEAR_FORMAT"
								) {
									toModel.setProperty("/" + key, fromData[key]);
								} else {
									inputFrom = fromData[key].split(",");

									if (toData[key] !== "" && toData[key] !== undefined) {
										inputTo = toData[key].split(",");

									} else if (toData[key] === undefined) {
										return;

									} else {
										inputTo = [];
									}
									inputFinal = inputTo.concat(inputFrom); //.join(",");

									inputFinal.forEach(function (row) {
										var hasRow = inputLast.filter(function (hrow) {
											return hrow === row;
										});
										if (hasRow.length === 0) {
											inputLast.push(row);
										}
									});
									inputLast = inputLast.join(",");

									toModel.setProperty("/" + key, inputLast);
								}

							}
						}
					});
				}
			},

			onTableRowLiveChangeCurr: function (oEvent) {
				var input = oEvent.getSource();
				input.setValue(input.getValue().toUpperCase());
			},

			onSwitchKGTON: function (oEvent) {
				var state = oEvent.getParameter("state"),
					oSource = oEvent.getSource(),
					table = oSource.getParent().getParent(),
					tableModelName = table.getBindingInfo("rows").model,
					tableModel = sap.ui.getCore().getModel(tableModelName),
					tablePath = table.getBindingInfo("rows").path,
					tableColumns = table.getColumns(),
					fieldNames = [],
					field,
					tableData = tableModel.getProperty(tablePath);

				tableColumns = tableColumns.filter(function (clm) {
					return clm.getFilterType() !== null && clm.data("perc") !== "X";
				});

				tableColumns.forEach(function (clm) {
					field = clm.getSortProperty();
					fieldNames.push(field);
				});

				switch (state) {
				case true: //TON

					tableData.forEach(function (row) {
						fieldNames.forEach(function (fieldItem) {
							row[fieldItem] = row[fieldItem] / 1000;
						});
					});

					break;
				case false: //KG
					tableData.forEach(function (row) {
						fieldNames.forEach(function (fieldItem) {
							row[fieldItem] = row[fieldItem] * 1000;
						});
					});
					break;
				default:
				}

				tableModel.setProperty(tablePath, tableData);
			},

			//--------------------------------------------------->START COMMENT<---------------------------------->

			_hasComment: function (row, number) {
				var allCommentData = sap.ui.getCore().getModel("globalM").getProperty("/allCommentData"),
					visibleKeys = number === 1 ? this.commentTableKeys : number === 2 ? this.commentTableKeys2 : this.commentTableKeys3,
					hasComment = false,
					rowForSearch = "",
					commentRowForSearch = "";

				visibleKeys.forEach(function (visible) {
					rowForSearch = rowForSearch + row[visible.split(";")[1]];
				});

				hasComment = allCommentData.filter(function (all) {
					commentRowForSearch = "";
					visibleKeys.forEach(function (visible) {
						commentRowForSearch = commentRowForSearch + all[visible.split(";")[0]];
					});
					return commentRowForSearch === rowForSearch;
				}).length > 0;

				row.HAS_COMMENT = hasComment;

				return row;

			},

			_getComment: function (oEvent, myResolve, myReject, prevInputJson) {
				var oSource, viewId, table, modelName, path, oModel, commentModel, row,
					columnsVisible = [],
					commentCustomData,
					inputJson = {},
					editableFields,
					commentData,
					value = "",
					emptyRow,
					visibleKeys;

				if (oEvent !== null && oEvent !== undefined) {

					oSource = oEvent.getSource();
					viewId = oSource.data("viewId");

					switch (viewId) {
					case "01":
					case "08":
					case "05":
					case "07":
					case "06":
					case "10":
					case "12":
					case "13":
					case "11":
					case "14":
					case "15":
						visibleKeys = this.commentTableKeys;
						break;
					case "02":
					case "09":
					case "04":
					case "16":
					case "17":
						visibleKeys = this.commentTableKeys2;
						break;
					case "03":
						visibleKeys = this.commentTableKeys3;
						break;
					default:
					}

					table = oSource.getParent().getParent();
					modelName = table.getBindingInfo("rows").model;
					path = oSource.getBindingContext(modelName).getPath();
					oModel = sap.ui.getCore().getModel(modelName);
					row = oModel.getProperty(path);
					columnsVisible = table.getColumns().filter(function (column) {
						return column.getVisible() && column.data("key") !== null && column.data("key") !== undefined;
					});
					inputJson.VIEW_NAME = viewId;
					sap.ui.getCore().getModel("globalM").setProperty("/viewId", viewId);
					visibleKeys.forEach(function (column) {
						commentCustomData = column;
						if (commentCustomData !== null && commentCustomData !== undefined) {
							switch (commentCustomData.split(";")["1"]) {
							case "RMW11ZE03___T":
							case "A0MATERIAL__RMW11ZE03_T":
							case "MATERIAL__RMW11ZE03___T":
							case "ARMW11ZE03_T":
							case "ORGANIC_F":
								value = row[commentCustomData.split(";")["1"]] === "yes" ? "CUST-X.0000000001987" : "CUST-X.0000000001563";
								inputJson[commentCustomData.split(";")[0]] = value;
								break;
							case "RMW21ZE03___T":
							case "A0MATERIAL__RMW21ZE03_T":
							case "MATERIAL__RMW71ZE03___T":
							case "A0MATERIAL__RMW71ZE03_T":
							case "ARMW21ZE03_T":
							case "HALAL_F":
								value = row[commentCustomData.split(";")["1"]] === "yes" ? "CUST-X.0000000001562" : "CUST-X.0000000001563";
								inputJson[commentCustomData.split(";")[0]] = value;
								break;
							case "RMW31ZE03___T":
							case "A0MATERIAL__RMW31ZE03_T":
							case "MATERIAL__RMW81ZE03___T":
							case "A0MATERIAL__RMW81ZE03_T":
							case "ARMW31ZE03_T":
							case "KOSHER_F":
								value = row[commentCustomData.split(";")["1"]] === "yes" ? "CUST-X.0000000001562" : "CUST-X.0000000001563";
								inputJson[commentCustomData.split(";")[0]] = value;
								break;
							default:
								inputJson[commentCustomData.split(";")[0]] = row[commentCustomData.split(";")["1"]];
								inputJson[commentCustomData.split(";")[0]] = inputJson[commentCustomData.split(";")[0]] === "#" ? "" : inputJson[
									commentCustomData.split(";")[0]];

								inputJson[commentCustomData.split(";")[0]] = inputJson[commentCustomData.split(";")[0]] === "" ? "," : inputJson[
									commentCustomData.split(";")[0]];
							}
						}

					});
				} else {
					inputJson = prevInputJson;
				}
				commentModel = sap.ui.getCore().getModel("commentTableM");

				var that = this;
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "GET_COMMENTS"
					},
					"INPUTPARAMS": [inputJson]
				};
				dbcontext.callServer(params, function (oModel) {
					commentData = oModel.getData().COMMENTS;
					commentData.forEach(function (line) {
						line.CREATED_ON = line.CREATED_ON.split("-")[0] + line.CREATED_ON.split("-")[1] + line.CREATED_ON.split("-")[2];
						line.CHANGED_ON = line.CHANGED_ON.split("-")[0] + line.CHANGED_ON.split("-")[1] + line.CHANGED_ON.split("-")[2];
						line.FRUIT_SPCF_1_T = that.getDetermineFSText(line.FRUIT_SPCF_1);
						line.FRUIT_SPCF_2_T = that.getDetermineFSText(line.FRUIT_SPCF_2);
						line.FRUIT_SPCF_3_T = that.getDetermineFSText(line.FRUIT_SPCF_3);
					});
					editableFields = oModel.getData().FIELDS;
					emptyRow = that.getEmptyCommentRow(commentModel, editableFields, columnsVisible, row);
					var rowExtend = $.extend(true, {}, emptyRow);
					var inputJsonExtend = $.extend(true, {}, inputJson);
					sap.ui.getCore().getModel("globalM").setProperty("/extendEmptyRow", rowExtend);
					sap.ui.getCore().getModel("globalM").setProperty("/inputJsonBeforeOpenPopup", inputJsonExtend);
					commentData.unshift(emptyRow);
					commentModel.setProperty("/tableRows", commentData);

					var lastIndex = (0).toString();
					that._updateFS1(commentModel, "/tableRows/" + lastIndex, commentData[0]);
					that._updateFS2(commentModel, "/tableRows/" + lastIndex, commentData[0]);
					that._updateFS3(commentModel, "/tableRows/" + lastIndex, commentData[0]);
					myResolve();
				});
			},

			handleRefresh: function (oEvent) {
				var commentModel = sap.ui.getCore().getModel("commentTableM"),
					commentData = commentModel.getData().tableRows,
					addedRow = [];
				addedRow = commentData.filter(function (e) {
					return e.ADDED === "X";
				});

				if (addedRow.length === 0) {
					MessageBox.warning("You have to add at least one row for refreshing the table.");
					return;
				}

				var viewId = sap.ui.getCore().getModel("globalM").getProperty("/viewId");
				var row = addedRow[0];
				addedRow[0].VIEW_NAME = viewId;
				var rowExtend = $.extend(true, {}, addedRow[0]);

				// Object.keys(addedRow[0]).forEach(function (key) {
				// 	addedRow[0][key] = addedRow[0][key] === "" && key !== "ORGANIC" && key !== "HALAL" && key !== "KOSHER" ? "," : addedRow[0][key];
				// });

				var that = this;
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "GET_COMMENTS"
					},
					"INPUTPARAMS": [addedRow[0]]
				};
				dbcontext.callServer(params, function (oModel) {
					var commentData = oModel.getData().COMMENTS;
					var editableFields = oModel.getData().FIELDS;
					commentData.forEach(function (line) {
						line.CREATED_ON = line.CREATED_ON.split("-")[0] + line.CREATED_ON.split("-")[1] + line.CREATED_ON.split("-")[2];
						line.CHANGED_ON = line.CHANGED_ON.split("-")[0] + line.CHANGED_ON.split("-")[1] + line.CHANGED_ON.split("-")[2];
						line.FRUIT_SPCF_1_T = that.getDetermineFSText(line.FRUIT_SPCF_1);
						line.FRUIT_SPCF_2_T = that.getDetermineFSText(line.FRUIT_SPCF_2);
						line.FRUIT_SPCF_3_T = that.getDetermineFSText(line.FRUIT_SPCF_3);
					});

					// var emptyRow = that.getEmptyCommentRow(commentModel, editableFields, null, row);
					//var rowExtend = $.extend(true, {}, addedRow[0]);
					sap.ui.getCore().getModel("globalM").setProperty("/extendEmptyRow", rowExtend);
					commentData.unshift(rowExtend);
					commentModel.setProperty("/tableRows", commentData);

					var lastIndex = (0).toString();
					that._updateFS1(commentModel, "/tableRows/" + lastIndex, commentData[0]);
					that._updateFS2(commentModel, "/tableRows/" + lastIndex, commentData[0]);
					that._updateFS3(commentModel, "/tableRows/" + lastIndex, commentData[0]);
				});
			},

			_addComment: function (oEvent) {
				var emptyRow = sap.ui.getCore().getModel("globalM").getProperty("/extendEmptyRow");
				var rowExtend = $.extend(true, {}, emptyRow);
				var commentModel = sap.ui.getCore().getModel("commentTableM");
				var commentData = commentModel.getProperty("/tableRows");
				commentData.unshift(rowExtend);
				commentModel.setProperty("/tableRows", commentData);
				var lastIndex = (0).toString();
				this._updateFS1(commentModel, "/tableRows/" + lastIndex, commentData[0]);
				this._updateFS2(commentModel, "/tableRows/" + lastIndex, commentData[0]);
				this._updateFS3(commentModel, "/tableRows/" + lastIndex, commentData[0]);

				var path = "/tableRows/0";
				var customer = commentModel.getProperty(path).CUSTOMERL2;
				var area = commentModel.getProperty(path).AREA;
				var plant = commentModel.getProperty(path).PLANT_MAN;
				if (customer !== "") {
					var customerArray = [];
					customerArray.push({
						KUNNR: customer,
						LAND1: "",
						AREA: ""
					});
					this._getRegionAreCountryByCustomerL2ForComment("CUSTOMERL2", customerArray, commentModel, path);
				} else {
					if (area !== "") {
						var areaArray = [];
						areaArray.push({
							KUNNR: "",
							LAND1: "",
							AREA: area
						});
						this._getRegionAreCountryByCustomerL2ForComment("AREA", areaArray, commentModel, path);
					}
				}

				if (plant !== "" && plant !== undefined) {
					var plantArray = [];
					plantArray.push({
						PLANT: plant
					});
					this._getPlantClusterForComment(plantArray, commentModel, path);
				}
			},

			_deleteComment: function (oEvent, myResolve) { //DELETE_COMMENTS
				var that = this;
				var oSource = oEvent.getSource(),
					table = oSource.getParent().getParent(),
					modelName = table.getBindingInfo("rows").model,
					path = oSource.getBindingContext(modelName).getPath(),
					oModel = sap.ui.getCore().getModel(modelName),
					row = oModel.getProperty(path);
				if (row.ADDED === "X") {
					oModel.getData()[path.split("/")[1]].splice(path.split("/")[2], "1");
					oModel.setData(oModel.getData());
				} else {

					var params = {
						"HANDLERPARAMS": {
							"FUNC": "DELETE_COMMENTS"
						},
						"INPUTPARAMS": [row]
					};
					dbcontext.callServer(params, function (oModel) {
						that.handleServerMessages(oModel, function (status) {
							if (status === "S") {
								var deletedindex = Number(path.split("/")[2]);
								var commentModel = sap.ui.getCore().getModel(modelName);
								commentModel.getData().tableRows.splice(deletedindex, 1);
								commentModel.setProperty("/tableRows", commentModel.getData().tableRows);
								myResolve();
							}
						});
					});
				}
			},

			_handleLoadItems: function (oEvent) {
				var table = oEvent.getSource().getParent().getParent(),
					modelName = table.getBindingInfo("rows").model,
					path = oEvent.getSource().getBindingContext(modelName).getPath(),
					oModel = sap.ui.getCore().getModel(modelName),
					row = oModel.getProperty(path);
				this._updateFS1(oModel, path, row);
				this._updateFS2(oModel, path, row);
				this._updateFS3(oModel, path, row);
			},

			onChangeEntryComment: function (oEvent) {
				var oSource = oEvent.getSource(),
					table = oSource.getParent().getParent(),
					modelName = table.getBindingInfo("rows").model,
					path = oSource.getBindingContext(modelName).getPath(),
					oModel = sap.ui.getCore().getModel(modelName);
				//	row = oModel.getProperty(path);

				if (table.getMetadata().getName() === "sap.ui.table.Row") {
					table = table.getParent();
				}
				oModel.setProperty(path + "/CHANGED", "X");
			},

			onInputCustomChangeComment: function (oEvent) {
				var oSource = oEvent.getSource(),
					table = oSource.getParent().getParent(),
					modelName = table.getBindingInfo("rows").model,
					path = oSource.getBindingContext(modelName),
					fieldName = oEvent.getSource().getFieldName(),
					oModel = sap.ui.getCore().getModel(modelName);
				//	row = oModel.getProperty(path);
				if (path === undefined) {
					path = sap.ui.getCore().getModel("globalM").getData().pressTableValueControl.getBindingContext(modelName).getPath();
				} else {
					path = path.getPath();
				}
				var row = oModel.getProperty(path);
				if (fieldName === "PRODUCT_TYPE") {
					this._updateFS1(oModel, path, row);
					this._updateFS2(oModel, path, row);
					this._updateFS3(oModel, path, row);
				}

				if (fieldName === "CUSTOMERL2") {
					var customer = oModel.getProperty(path).CUSTOMERL2;
					if (customer === "") {
						return;
					}
					var customerArray = [];
					customerArray.push({
						KUNNR: customer,
						LAND1: "",
						AREA: ""
					});
					this._getRegionAreCountryByCustomerL2ForComment("CUSTOMERL2", customerArray, oModel, path);
				}

				if (fieldName === "AREA") {
					// var country = oModel.getProperty(path + "/A0CUSTOMER__0COUNTRY");
					// if (country === "" || country === undefined) {
					var area = oModel.getProperty(path).AREA;
					if (area === "") {
						return;
					}
					var areaArray = [];
					areaArray.push({
						KUNNR: "",
						LAND1: "",
						AREA: area
					});
					this._getRegionAreCountryByCustomerL2ForComment("AREA", areaArray, oModel, path);
					// }
				}

				if (fieldName === "WERKS") {
					var plant = oModel.getProperty(path).PLANT_MAN;
					if (plant === "") {
						return;
					}
					var plantArray = [];
					plantArray.push({
						PLANT: plant
					});
					this._getPlantClusterForComment(plantArray, oModel, path);
				}

				oModel.setProperty(path + "/CHANGED", "X");
			},

			onOrgHalalKosherChangeComment: function (oEvent) {
				var oSource = oEvent.getSource(),
					table = oSource.getParent().getParent(),
					modelName = table.getBindingInfo("rows").model,
					path = oSource.getBindingContext(modelName).getPath(),
					oModel = sap.ui.getCore().getModel(modelName);
				//	row = oModel.getProperty(path);

				oModel.setProperty(path + "/CHANGED", "X");
			},

			onFSChangeComment: function (oEvent) {
				var oSource = oEvent.getSource(),
					table = oSource.getParent().getParent(),
					modelName = table.getBindingInfo("rows").model,
					path = oSource.getBindingContext(modelName).getPath(),
					oModel = sap.ui.getCore().getModel(modelName);
				//	row = oModel.getProperty(path);

				oModel.setProperty(path + "/CHANGED", "X");
			},

			_getPlantClusterForComment: function (plantArray, oModel, path) {
				var that = this;
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "DETERMINE_SALESORG_BY_PLANT"
					},
					"INPUTPARAMS": plantArray
				};

				dbcontext.callServer(params, function (o) {
					var result = o.getData().PLANT_CLUSTER;

					oModel.setProperty(path + "/PLANT_CLUSTER", result);
				}, that);
			},

			_getRegionAreCountryByCustomerL2ForComment: function (name, customerArray, oModel, path) {
				var that = this;
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "DETERMINE_CUSTOMER_FIELDS"
					},
					"INPUTPARAMS": customerArray
				};

				dbcontext.callServer(params, function (o) {
					var result = o.getData().RESULT;

					switch (name) {
					case "CUSTOMERL2":
						oModel.setProperty(path + "/REGION", result.REGION);
						oModel.setProperty(path + "/AREA", result.AREA);
						oModel.setProperty(path + "/CHANGED", true);
						// oModel.setProperty(path + "/A0CUSTOMER__0COUNTRY", result.LAND1);
						// oModel.setProperty(path + "/A0CUSTOMER__DOEBKUNDE", result.KUNN2);
						break;
					case "COUNTRY":
						// oModel.setProperty(path + "/A0CUSTOMER__DSDREGION", result.REGION);
						// oModel.setProperty(path + "/A0CUSTOMER__DSDAREA", result.AREA);
						break;
					case "AREA":
						oModel.setProperty(path + "/REGION", result.REGION);
						oModel.setProperty(path + "/CHANGED", true);
						break;
					default:
					}

				}, that);
			},

			getDetermineFSText: function (fs1) {
				var resultText = "";

				resultText = sap.ui.getCore().getModel("globalM").getData().FS_TEXTS.filter(function (m) {
					return m.DOMVALUE_L === fs1;
				});

				if (resultText.length) {
					resultText = resultText[0].DDTEXT;
				} else
					resultText = "Not assigned";

				return resultText;
			},

			getEmptyCommentRow: function (commentModel, editableFields, columnsVisible, row) {
				var emptyRow = {},
					commentCustomData,
					value = "";

				emptyRow.ADDED = "X";
				emptyRow.COUNTER_ID = "";
				emptyRow.FRUIT = "";
				emptyRow.PRODUCT_TYPE = "";
				emptyRow.PLANT_MAN = "";
				emptyRow.COLL_NUMBER = "";
				emptyRow.PLANT_CLUSTER = "";
				emptyRow.REGION = "";
				emptyRow.AREA = "";
				emptyRow.CUSTOMERL2 = "";
				emptyRow.MATERIAL_CN = "";
				emptyRow.FRUIT_SPCF_1 = "";
				emptyRow.FRUIT_SPCF_2 = "";
				emptyRow.FRUIT_SPCF_3 = "";
				emptyRow.ORGANIC = columnsVisible === null ? "" : "CUST-X.0000000001563";
				emptyRow.HALAL = columnsVisible === null ? "" : "CUST-X.0000000001563";
				emptyRow.KOSHER = columnsVisible === null ? "" : "CUST-X.0000000001563";
				emptyRow.MATERIAL_FG = "";
				emptyRow.COMMENT_ATTR = "";
				emptyRow.CREATED_BY = "";
				emptyRow.CREATED_ON = "";
				emptyRow.CHANGED_BY = "";
				emptyRow.CHANGED_ON = "";
				emptyRow.DELETION_FLG = "";
				emptyRow.FRUIT_SPCF_1_ITEMS = [];
				emptyRow.FRUIT_SPCF_2_ITEMS = [];
				emptyRow.FRUIT_SPCF_3_ITEMS = [];
				if (columnsVisible !== null) {
					columnsVisible.forEach(function (column) {
						commentCustomData = column.data("comment");
						if (commentCustomData !== null && commentCustomData !== undefined) {
							switch (commentCustomData.split(";")["1"]) {
							case "RMW11ZE03___T":
							case "A0MATERIAL__RMW11ZE03_T":
							case "MATERIAL__RMW11ZE03___T":
							case "ARMW11ZE03_T":
							case "ORGANIC_F":
								value = row[commentCustomData.split(";")["1"]] === "yes" ? "CUST-X.0000000001987" : "CUST-X.0000000001563";
								emptyRow[commentCustomData.split(";")[0]] = value;
								break;
							case "RMW21ZE03___T":
							case "A0MATERIAL__RMW21ZE03_T":
							case "MATERIAL__RMW71ZE03___T":
							case "A0MATERIAL__RMW71ZE03_T":
							case "ARMW21ZE03_T":
							case "HALAL_F":
								value = row[commentCustomData.split(";")["1"]] === "yes" ? "CUST-X.0000000001562" : "CUST-X.0000000001563";
								emptyRow[commentCustomData.split(";")[0]] = value;
								break;
							case "RMW31ZE03___T":
							case "A0MATERIAL__RMW31ZE03_T":
							case "MATERIAL__RMW81ZE03___T":
							case "A0MATERIAL__RMW81ZE03_T":
							case "ARMW31ZE03_T":
							case "KOSHER_F":
								value = row[commentCustomData.split(";")["1"]] === "yes" ? "CUST-X.0000000001562" : "CUST-X.0000000001563";
								emptyRow[commentCustomData.split(";")[0]] = value;
								break;
							default:
								emptyRow[commentCustomData.split(";")[0]] = row[commentCustomData.split(";")["1"]];
								emptyRow[commentCustomData.split(";")[0]] = emptyRow[commentCustomData.split(";")[0]] === "#" ? "" : emptyRow[
									commentCustomData.split(";")[0]];
							}
						}
					});
				}
				commentModel.setProperty("/ROW", {});
				Object.keys(editableFields).forEach(function (key) {
					commentModel.setProperty("/ROW/EDITABLE_" + editableFields[key], true);
				});
				return emptyRow;
			},

			_saveComment: function (dialog) {
				var commentData = sap.ui.getCore().getModel("commentTableM").getData().tableRows,
					isReady = true,
					isFruitOrProdTypeEmpty = [],
					sendData = {
						COMMENTS: []
					};

				var changedData = commentData.filter(function (line) {
					return line.CHANGED === "X";
				});

				if (changedData.length === 0) {
					MessageBox.warning("There is no change!");
					return;
				}

				isFruitOrProdTypeEmpty = changedData.filter(function (line) {
					return line.FRUIT === "" || line.PRODUCT_TYPE === "";
				});

				isReady = !(isFruitOrProdTypeEmpty.length > 0);

				if (!isReady) {
					MessageBox.warning("Fruit and Product type can not be empty!");
					return;
				}

				var that = this;
				sendData.COMMENTS = sendData.COMMENTS.concat(changedData);
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "SAVE_COMMENTS"
					},
					"INPUTPARAMS": [sendData]
				};
				dbcontext.callServer(params, function (oModel) {
					that.handleServerMessages(oModel, function (status) {
						if (status === "S") {
							that._getComment(null, null, null, sap.ui.getCore().getModel("globalM").getProperty("/inputJsonBeforeOpenPopup"));
							//dialog.close();
						}
					});
				});
			},

			//--------------------------------------------------->END COMMENT<---------------------------------->

			onGoQuotation: function (oEvent) {
				/* Quotation 0020102655 is created. */
				var quotationNumber = oEvent.getSource().getText();
				var printOutSelected = false;
				var that = this;
				var dialogQuo = new Dialog({
					state: "Information",
					title: "Quotation",
					type: that.getOwnerComponent().getModel("i18n").getProperty("DLG_T_Message"),
					content: [
						new sap.m.ObjectIdentifier({
							title: "Quotation " + quotationNumber
						}),
						new sap.m.Text(),
						new sap.m.RadioButtonGroup({
							columns: 1,
							selectedIndex: 0,
							buttons: [
								new RadioButton({
									//visible:false,    									
									id: "printOutQuotationId",
									groupName: "GroupA",
									text: that.getOwnerComponent().getModel("i18n").getProperty("quotationPrintOut"),
									selected: true
								}),
								new RadioButton({
									id: "changeQuotationId",
									groupName: "GroupA",
									text: that.getOwnerComponent().getModel("i18n").getProperty("quotationChangeQuotation"),
									selected: false
								})
							]
						})

					],

					beginButton: new Button({
						type: sap.m.ButtonType.Emphasized,
						text: that.getOwnerComponent().getModel("i18n").getProperty("DLG_T_OK"),
						press: function (oEvent) {
							printOutSelected = sap.ui.getCore().byId("printOutQuotationId").getSelected();
							dialogQuo.close();
							if (printOutSelected) {
								that._openQuotationInPDF(quotationNumber);
							} else {
								that._runLinkForQuo(quotationNumber);
							}

						}
					}),
					endButton: new Button({
						type: sap.m.ButtonType.Reject,
						text: that.getOwnerComponent().getModel("i18n").getProperty("DLG_T_Cancel"),
						press: function () {
							dialogQuo.close();
						}
					}),
					afterClose: function () {
						dialogQuo.destroy();
					}
				});

				dialogQuo.open();

			},

			_openQuotationInPDF: function (quotationNumber) {
				var that = this,
					oSample1Model = null,
					sSource,
					link = "";

				var sServiceUrl = this.getOwnerComponent().getModel("Pricing").sServiceUrl;

				that._pdfViewer = new PDFViewer({
					sourceValidationFailed: function (e) {
						e.preventDefault();
					},
					error: function (e) {
						e.preventDefault();
					}
				});
				that.getView().addDependent(that._pdfViewer);
				var url = sServiceUrl + "/pdfLinkSet(quotation='" + quotationNumber + "')/$value";
				that._pdfViewer.setSource(url);
				that._pdfViewer.open();

			},

			_runLinkForQuo: function (quotationNumber) {
				var quotationNumber = quotationNumber,
					gModel = sap.ui.getCore().getModel("globalM"),
					link = gModel.getProperty("/tcodeHostName"),
					// uiRole = gModel.getProperty("/UIROLE"),
					// switchV = gModel.getProperty("/SWITCH"),
					tCode = "VA22";

				//quotationNumber = "0020103048";
				//	link = link + "&~transaction=" + tCode + "+vbak" + quotationNumber + ";&~OKCODE=ENT2#";
				link = link + "&~transaction=" + tCode + "+vbak-vbeln=" + quotationNumber + ";&~OKCODE=ENT2#";
				sap.m.URLHelper.redirect(link, true);
			},

			_reFillPanelFilterValues: function (gridFrom, gridTo) {
				var that = this,
					m = 0,
					customFromKey,
					foundInput;
				if (gridFrom === undefined || gridTo === undefined) {
					return;
				}
				var fromInputs = gridFrom.filter(function (f) {
					return f.getMetadata().getName() !== "sap.m.Label";
				});
				var toInputs = gridTo.filter(function (f) {
					return f.getMetadata().getName() !== "sap.m.Label";
				});

				for (m = 0; m < fromInputs.length; m++) {
					customFromKey = fromInputs[m].data("filterCustom");
					if (customFromKey !== null) {
						foundInput = toInputs.filter(function (f) {
							return f.data("filterCustom") === customFromKey;
						});
						if (foundInput.length > 0) {
							that._applyHandoveredData(fromInputs[m], foundInput[0]);
						}
					}
				}
			},

			_applyHandoveredData: function (from, to) {
				var ind = 0,
					multiInputFrom,
					multiInputTo,
					customInputFrom,
					customInputTo,

					tokensDefault,
					tokensNew;
				switch (from.getMetadata().getName()) {
				case "sap.m.MultiInput":
					multiInputFrom = from;
					break;
				default:
				}
				switch (to.getMetadata().getName()) {
				case "sap.m.MultiInput":
					multiInputTo = to;
					break;
				default:
				}

				if (multiInputTo !== undefined) {
					tokensDefault = multiInputTo.getTokens();
					if (multiInputFrom !== undefined) {
						tokensNew = multiInputFrom.getTokens();
						if (multiInputTo.getMaxTokens() === undefined || (tokensDefault.length === 0 && multiInputTo.getMaxTokens() === 1 &&
								tokensNew.length <= 1)) {
							tokensNew = tokensNew.concat(tokensDefault);
							multiInputTo.removeAllTokens();
							multiInputTo.destroyTokens();
							for (ind = 0; ind < tokensNew.length; ind++) {
								multiInputTo.addToken(tokensNew[ind]);
							}
						}
					}

				}
			},

			onFilter: function (oEvent) {
				var dateFilterStatement = "";
				var statement = sap.ui.model.FilterOperator.EQ;
				var filterProperty = oEvent.getParameter("column").getFilterProperty();
				var filterValue = oEvent.getParameter("value");
				filterValue = filterValue.replace(".", "");
				oEvent.preventDefault();
				var aFilter = [];
				if (oEvent.getParameter("column").getFilterType() !== null) {
					if (oEvent.getParameter("column").getFilterType().getName() === "Float" || oEvent.getParameter("column").getFilterType().getName() ===
						"Integer") {
						var filterArray = filterValue.match(/\D+|\d+/g);
						var operator = filterValue.split(/\d/g).filter(Boolean)[0];
						if (operator === undefined) {
							aFilter.push(new sap.ui.model.Filter(filterProperty, sap.ui.model.FilterOperator.EQ, filterValue));
						} else {
							var operatorIndex = filterValue.search(operator);
							switch (operator) {
							case '>':
								var filterType = operatorIndex === 0 ? sap.ui.model.FilterOperator.GT : sap.ui.model.FilterOperator.LT;
								break;
							case '<':
								filterType = operatorIndex === 0 ? sap.ui.model.FilterOperator.LT : sap.ui.model.FilterOperator.GT;
								break;
							case '<=':
								filterType = operatorIndex === 0 ? sap.ui.model.FilterOperator.LE : sap.ui.model.FilterOperator.GE;
								break;
							case '>=':
								filterType = operatorIndex === 0 ? sap.ui.model.FilterOperator.GE : sap.ui.model.FilterOperator.LE;
								break;
							default:
								filterType = sap.ui.model.FilterOperator.EQ;
							}
							filterValue = operatorIndex === 0 ? filterArray[1] : filterArray[0];
							aFilter.push(new sap.ui.model.Filter(filterProperty, filterType, filterValue));

						}

					} else {
						aFilter.push(new sap.ui.model.Filter(filterProperty, sap.ui.model.FilterOperator.Contains, filterValue));
					}
				} else {
					aFilter.push(new sap.ui.model.Filter(filterProperty, sap.ui.model.FilterOperator.Contains, filterValue));
				}
				if (filterProperty === "QUOT_FROM" || filterProperty === "QUOT_TO" || filterProperty === "DOC_DATE" || filterProperty ===
					"A0DOC_DATE" || filterProperty === "A0QUOT_FROM" || filterProperty === "A0QUOT_TO" || filterProperty === "ADGUEBG" ||
					filterProperty === "ADGUEEN" || filterProperty === "A0CALDAY" || filterProperty === "A0CONTRACT__0VAL_START" || filterProperty ===
					"A0CONTRACT__0VAL_END" || filterProperty === "A0CALMONTH" || filterProperty === "A0DATETO" || filterProperty === "A0QUOT_FROM_TO"
				) {
					dateFilterStatement = sap.ui.model.FilterOperator.Contains;
					if (oEvent.getParameter("value") !== "") {
						var defaultValue = oEvent.getParameter("value");
						if (oEvent.getParameter("value").indexOf(">") > -1) {
							defaultValue = oEvent.getParameter("value").replace(">", "").replace(/\s/g, "");
							dateFilterStatement = sap.ui.model.FilterOperator.GT;
						}
						if (oEvent.getParameter("value").indexOf("<") > -1) {
							defaultValue = oEvent.getParameter("value").replace("<", "").replace(/\s/g, "");
							dateFilterStatement = sap.ui.model.FilterOperator.LT;
						}

						aFilter[0].oValue1 = defaultValue.split(".")[2] + defaultValue.split(".")[1] + defaultValue.split(".")[0];
						aFilter[0].sOperator = dateFilterStatement;
					}
				}
				aFilter.push(new sap.ui.model.Filter("isSum", sap.ui.model.FilterOperator.EQ, "X"));
				var combinedFilter = new sap.ui.model.Filter(aFilter, false);
				var oBinding = oEvent.getSource().getBinding("rows");
				oBinding.filter(combinedFilter);
			},

			_getSAPQuoteLine: function (_panelModel, isSelected, selectedRow, filteredData, todayDate) {
				var PlantFilterValue = filteredData.PLANT;
				if (PlantFilterValue.indexOf(",") !== -1) PlantFilterValue = "";
				return {
					MATNR: isSelected ? selectedRow.A0MATERIAL === "#" ? "" : selectedRow.A0MATERIAL : "",
					WERKS: isSelected ? selectedRow.A0PLANT === "#" ? "" : selectedRow.A0PLANT : PlantFilterValue,
					VKORG: isSelected ? selectedRow.A0SALESORG === "#" ? "" : selectedRow.A0SALESORG : filteredData.SALESORG,
					VTWEG: isSelected ? selectedRow.A0DISTR_CHAN : "10",
					CUSTL2: isSelected ? selectedRow.A0CUSTOMER === "#" ? "" : selectedRow.A0CUSTOMER : filteredData.CUSTOMERL2,
					SHIP_TO_PARTY: isSelected ? selectedRow.A0SHIP_TO === "#" ? "" : selectedRow.A0SHIP_TO : filteredData.CUSTOMERL2,
					PAYER: isSelected ? selectedRow.A0CUSTOMER === "#" ? "" : selectedRow.A0CUSTOMER : filteredData.CUSTOMERL2,
					INCO1: isSelected ? selectedRow.A0INCOTERMS : "FCA",
					ZTERM: isSelected ? selectedRow.A0PMNTTRMS : "00",
					VOLUME: "",
					VOLUME_MEINS: "KG",
					ZZMBG: "",
					ZZMBG_MEINS: "KG",
					PRICE: isSelected ? selectedRow.GROSS_PRICE_CURR / selectedRow.PRICE_UNIT : 0,
					PRICE_WAERS: isSelected ? selectedRow.A0DOC_CURRCY === "#" ? "" : selectedRow.A0DOC_CURRCY : "",
					KPEIN: 1,
					KMEIN: "KG",
					ANGDT: todayDate, //TODAY
					BNDDT: "", //ZVIS_QT_VLDT
					GUEBG: filteredData.GUEBG === undefined ? "" : filteredData.GUEBG,
					GUEEN: filteredData.GUEEN === undefined ? "" : filteredData.GUEEN,
					SEASON_START: _panelModel.getData().SFULLYEAR_FORMAT,
					SEASON_END: _panelModel.getData().EFULLYEAR_FORMAT,
					GTP_QUOTE_NO: "", // GTP00000121,
					VSBED: "05"
				};
			},

			_checkFruitSpecificsIsReady: function (dataToSave, fs1, fs2, fs3) {
				var msg = "";
				var index,
					fs1ItemText = "FRUIT_SPCF_1_ITEMS",
					fs2ItemText = "FRUIT_SPCF_2_ITEMS",
					fs3ItemText = "FRUIT_SPCF_3_ITEMS";

				for (index = 0; index < dataToSave.length; index++) {
					if (dataToSave[index].PRODUCT_TYPE_EDITABLE !== false) {
						if (dataToSave[index][fs1ItemText] === undefined && dataToSave[index][fs2ItemText] === undefined && dataToSave[index][fs3ItemText] ===
							undefined) {
							if (msg !== "X") {
								msg = "";
							}
						} else {
							if ((dataToSave[index][fs1] === "" && dataToSave[index][fs1ItemText].length > 1) ||
								(dataToSave[index][fs2] === "" && dataToSave[index][fs2ItemText].length > 1) ||
								(dataToSave[index][fs3] === "" && dataToSave[index][fs3ItemText].length > 1)) {
								msg = "X";
								index = dataToSave.length;
							}
						}
					}
				}

				return msg === "X" ? "For this fruit and product type the maintenance of the fruit-specific fields is mandatory." : "";
			},

			_viewUpdate: function (id) {
				this.getView().byId(id).setVisible(false);
				this.getView().byId(id).setVisible(true);
			},

			_updateFS1: function (model, path, row) {
				var that = this;
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "SEARCH_F4"
					},
					"INPUTPARAMS": [{
						"PARIDN": "FS1",
						"RELFLDVAL1": row.FRUIT === undefined ? row.ARMW21ZH02 : row.FRUIT,
						"RELFLDVAL2": row.PRODUCT_TYPE === undefined ? row.ARMW11ZP16 : row.PRODUCT_TYPE,
						"NOENTRY": ""
					}]
				};
				dbcontext.callServer(params, function (oModel) {
					var data = oModel.getData().F4_VALUE_DYN;
					data.unshift({
						FS1: "",
						FS1_T: ""
					});
					model.setProperty(path + "/FRUIT_SPCF_1_ITEMS", data);
				});
			},

			_updateFS2: function (model, path, row) {
				var that = this;
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "SEARCH_F4"
					},
					"INPUTPARAMS": [{
						"PARIDN": "FS2",
						"RELFLDVAL1": row.FRUIT === undefined ? row.ARMW21ZH02 : row.FRUIT,
						"RELFLDVAL2": row.PRODUCT_TYPE === undefined ? row.ARMW11ZP16 : row.PRODUCT_TYPE,
						"NOENTRY": ""
					}]
				};
				dbcontext.callServer(params, function (oModel) {
					var data = oModel.getData().F4_VALUE_DYN;
					data.unshift({
						FS2: "",
						FS2_T: ""
					});
					model.setProperty(path + "/FRUIT_SPCF_2_ITEMS", data);
				});
			},

			_updateFS3: function (model, path, row) {
				var that = this;
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "SEARCH_F4"
					},
					"INPUTPARAMS": [{
						"PARIDN": "FS3",
						"RELFLDVAL1": row.FRUIT === undefined ? row.ARMW21ZH02 : row.FRUIT,
						"RELFLDVAL2": row.PRODUCT_TYPE === undefined ? row.ARMW11ZP16 : row.PRODUCT_TYPE,
						"NOENTRY": ""
					}]
				};
				dbcontext.callServer(params, function (oModel) {
					var data = oModel.getData().F4_VALUE_DYN;
					data.unshift({
						FS3: "",
						FS3_T: ""
					});
					model.setProperty(path + "/FRUIT_SPCF_3_ITEMS", data);
				});
			},

			getDetermineOrganic: function (organic) {

				if (organic === "CUST-X.0000000001987" || organic === "CUST-X.0000000001562" || organic === "CUST-X.0000000013115" || organic ===
					"CUST-X.0000000006711" ||
					organic === "CUST-X.0000000005518" || organic === "CUST-X.0000000011973" || organic === "CUST-X.0000000013117" || organic ===
					"CUST-X.0000000010338" ||
					organic === "CUST-X.0000000003360") {
					return "yes";
				} else {
					return "no";
				}
			},

			getDetermineHalal: function (halal) {

				if (halal === "CUST-X.0000000001562") {
					return "yes";
				} else {
					return "no";
				}
			},

			getDetermineKosher: function (kosher) {

				if (kosher === "CUST-X.0000000001562") {
					return "yes";
				} else {
					return "no";
				}
			},

			getLastDay: function (y, m) {
				return new Date(y, m + 1, 0).getDate();
			},

			_prepareSeason: function (params, gModel, dialog, _panel, showMsg, view) {
				var that = this;
				dbcontext.callServer(params, function (oModel) {
					var season = oModel.getData().EV_SEASON;
					var msg = oModel.getData().CHECK_MSG;
					gModel.setProperty("/seasonCheckMsg", msg);
					if (msg !== "") {
						_panel.setProperty("/EFULLYEAR", "");
						_panel.setProperty("/SFULLYEAR_FORMAT", "");
						_panel.setProperty("/EFULLYEAR_FORMAT", "");
						_panel.setProperty("/PREV_FROM", "");
						_panel.setProperty("/PREV_TO", "");
						_panel.setProperty("/SEASON", "");
						gModel.setProperty("/seasonDates", []);
						if (showMsg === "X") {
							MessageBox.warning(msg);
						}
						return;

					} else {
						if (season.STARTMONTH1 === "00") {
							if (showMsg === "X") {
								MessageBox.warning("No season!");
								return;
							}
						}
					}
					var startMonth1 = season.STARTMONTH1,
						endMonth1 = season.ENDMONTH1,
						currentDate = new Date(),
						currentMonth = currentDate.getMonth(),
						currentYear = new Date().getFullYear(),
						currentMonth = currentMonth + 1,
						lastSeason = {},
						formatDate, date,
						currentSeason = {},
						cloneSeason = {},
						newSeason = {};
					if (startMonth1 !== "00" && endMonth1 !== "00") {
						startMonth1 = parseInt(startMonth1);
						endMonth1 = parseInt(endMonth1);
						if (startMonth1 > currentMonth) {
							currentSeason.text = "Current season";
							newSeason.text = "New season";
							lastSeason.text = "Last season";

							currentSeason.year = currentYear - 1;
							currentSeason.month = startMonth1;
							currentSeason.day = 1;
							date = currentSeason.month.toString() + ".1." + currentSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							currentSeason.sfullYear = formatDate;

							lastSeason.year = currentYear - 2;
							lastSeason.month = startMonth1;
							lastSeason.day = 1;
							date = lastSeason.month.toString() + ".1." + lastSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							lastSeason.sfullYear = formatDate;

							newSeason.year = currentYear;
							newSeason.month = startMonth1;
							newSeason.day = 1;
							date = newSeason.month.toString() + ".1." + newSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							newSeason.sfullYear = formatDate;
						}

						if (startMonth1 <= currentMonth) {
							currentSeason.text = "Current season";
							newSeason.text = "New season";
							lastSeason.text = "Last season";

							currentSeason.year = currentYear;
							currentSeason.month = startMonth1;
							currentSeason.day = 1;
							date = currentSeason.month.toString() + ".1." + currentSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							currentSeason.sfullYear = formatDate;

							lastSeason.year = currentYear - 1;
							lastSeason.month = startMonth1;
							lastSeason.day = 1;
							date = lastSeason.month.toString() + ".1." + lastSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							lastSeason.sfullYear = formatDate;

							newSeason.year = currentYear + 1;
							newSeason.month = startMonth1;
							newSeason.day = 1;
							date = newSeason.month.toString() + ".1." + newSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							newSeason.sfullYear = formatDate;
						}

						if (endMonth1 >= currentMonth) {
							currentSeason.year = currentYear;
							currentSeason.month = endMonth1;
							currentSeason.day = that.getLastDay(currentSeason.year, endMonth1 - 1);
							date = currentSeason.month.toString() + "." + currentSeason.day.toString() + "." + currentSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							currentSeason.efullYear = formatDate;

							lastSeason.year = currentYear - 1;
							lastSeason.month = endMonth1;
							lastSeason.day = that.getLastDay(lastSeason.year, endMonth1 - 1);
							date = lastSeason.month.toString() + "." + lastSeason.day.toString() + "." + lastSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							lastSeason.efullYear = formatDate;

							newSeason.year = currentYear + 1;
							newSeason.month = endMonth1;
							newSeason.day = that.getLastDay(newSeason.year, endMonth1 - 1);
							date = newSeason.month.toString() + "." + newSeason.day.toString() + "." + newSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							newSeason.efullYear = formatDate;
						}
						if (endMonth1 < currentMonth) {
							currentSeason.year = currentYear + 1;
							currentSeason.month = endMonth1;
							currentSeason.day = that.getLastDay(currentSeason.year, endMonth1 - 1);
							date = currentSeason.month.toString() + "." + currentSeason.day.toString() + "." + currentSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							currentSeason.efullYear = formatDate;

							lastSeason.year = currentYear;
							lastSeason.month = endMonth1;
							lastSeason.day = that.getLastDay(lastSeason.year, endMonth1 - 1);
							date = lastSeason.month.toString() + "." + lastSeason.day.toString() + "." + lastSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							lastSeason.efullYear = formatDate;

							newSeason.year = currentYear + 2;
							newSeason.month = endMonth1;
							newSeason.day = that.getLastDay(newSeason.year, endMonth1 - 1);
							date = newSeason.month.toString() + "." + newSeason.day.toString() + "." + newSeason.year.toString();
							formatDate = sap.ui.core.format.DateFormat.getDateInstance({
								pattern: "dd.MM.YYYY"
							}).format(new Date(date));
							newSeason.efullYear = formatDate;
						}
						var result = [lastSeason, currentSeason, newSeason];
						var isSame = false;
						if (JSON.stringify(gModel.getProperty("/seasonDates")) === JSON.stringify(result)) {
							isSame = true;
						}
						gModel.setProperty("/seasonDates", result);
						if (dialog !== null) {
							// open value help dialog
							if (dialog !== undefined) {
								dialog.then(function (oValueHelpDialog) {
									oValueHelpDialog.open();
								});
							}
						} else {
							var eFormat, sFormat;

							if (_panel.getProperty("/SEASON_TEXT") !== undefined && _panel.getProperty("/SEASON_TEXT") !== "") {
								if (_panel.getProperty("/SEASON_TEXT") === "Current season") {
									cloneSeason = currentSeason;
								}
								if (_panel.getProperty("/SEASON_TEXT") === "Last season") {
									cloneSeason = lastSeason;
								}
								if (_panel.getProperty("/SEASON_TEXT") === "New season") {
									cloneSeason = newSeason;
								}
							} else {
								cloneSeason = currentSeason;
							}

							if (isSame !== true) {
								cloneSeason = currentSeason;
								_panel.setProperty("/EFULLYEAR", "");
								_panel.setProperty("/SFULLYEAR_FORMAT", "");
								_panel.setProperty("/EFULLYEAR_FORMAT", "");
								_panel.setProperty("/PREV_FROM", "");
								_panel.setProperty("/PREV_TO", "");
								_panel.setProperty("/SEASON", "");
							}

							eFormat = cloneSeason.efullYear.split(".")[2] + cloneSeason.efullYear.split(".")[1] + cloneSeason.efullYear.split(
								".")[0];
							sFormat = cloneSeason.sfullYear.split(".")[2] + cloneSeason.sfullYear.split(".")[1] + cloneSeason.sfullYear.split(
								".")[0];

							//	if (view !== "Q") {
							_panel.setProperty("/SFULLYEAR", cloneSeason.sfullYear);
							_panel.setProperty("/SFULLYEAR_FORMAT", sFormat);
							_panel
								.setProperty("/EFULLYEAR", cloneSeason.efullYear);
							_panel.setProperty("/EFULLYEAR_FORMAT", eFormat);
							_panel.setProperty(
								"/SEASON_TEXT", cloneSeason.text);
							_panel.setProperty("/SEASON", cloneSeason.text + "[" + cloneSeason.sfullYear + " - " + cloneSeason.efullYear + "]");
							//	}

						}

					} else {
						//sap.m.MessageToast.show("There is no season data!");
						gModel.setProperty("/seasonDates", []);
					}
				}, that);
			},

			onReset: function (oEvent) {
				var tableId = oEvent.getSource().getAriaDescribedBy().toString();
				var oTable = sap.ui.getCore().byId(tableId);
				var columns = oTable.getColumns();
				var len = columns.length;
				for (var i = 0; i < len; i++) {
					columns[i].setFilterValue("").setFiltered(false).setSorted(false);
				}
				oTable.getBinding("rows").filter(null).sort(null);

				var model = oTable.getBindingInfo("rows").model;

				switch (model) {
				case "quoteTableM":
					var view = "QUOTE";
					break;
				case "productionListTableM":
					view = "PROD";
					break;
				case "seasonTableM":
					view = "SPT";
					break;
				default:
					view = "";
				}
				var binding = oTable.getBinding().getPath().split('/')[1];
				var data = oTable.getBinding().getModel().getData()[binding];

				var notSumRow = _.filter(_.cloneDeep(data), function (item) {
					return item.isSum !== "X";
				});
				var totalRow = this.setTotalRow(notSumRow, view);
				notSumRow.unshift(totalRow);
				oTable.getBinding().getModel().setProperty(oTable.getBinding().getPath(), notSumRow);
				//this.columnfilterHis = {};
			},

			/* End of Function Required for Custom F4 */
			handleServerMessages: function (oModel, fnOnClose, showSuccessMessage) {
				ErrorHandler.handleMessages(this, oModel, fnOnClose, false, showSuccessMessage);
			},

			getRouter: function () {
				return this.getOwnerComponent().getRouter();
			},

			_setButtonsPressed: function (id) {
				var that = this;
				var buttons = ["overViewBtnId", "supplyBtnId", "demandViewBtnId"];
				buttons.forEach(function (m) {
					//that.getView().byId(m).setPressed(false);
					that.getView().byId(m).setType("Default");
				});
				if (id !== "seasonalPlanToOfferId") {
					that.getView().byId(id).setType("Emphasized");
					that.getView().byId("seasonalPlanToOfferId").setType("Default");
				}
				if (id !== "supplyBtnId") {
					that.getView().byId(id).setType("Emphasized");
					that.getView().byId("supplyBtnId").setType("Default");
				}
				if (id !== "demandViewBtnId") {
					that.getView().byId(id).setType("Emphasized");
					that.getView().byId("demandViewBtnId").setType("Default");
				}

				that.getView().byId(id).setType("Emphasized");

			},

			onChangeSwitch: function (oEvent) {
				var value = oEvent.getParameters();
				sap.ui.getCore().getModel("globalM").setProperty("/adminVisible", value.state);
			},

			onNavToPages: function (oEvent) {
				var id, oRouter, customData;

				id = oEvent.getSource().getId().split("--")[1];
				oRouter = this.getOwnerComponent().getRouter();
				if (id !== undefined) {
					// oEvent.getSource().setPressed(false);
					oEvent.getSource().setType("Default");
				} else {
					customData = oEvent.getSource().data("target");
					if (customData === "p1") {
						id = "seasonalPlanToOfferId";
					}
					if (customData === "p2") {
						id = "quoteManagementId";
					}
					if (customData === "p3") {
						id = "SeasonSummaryId";
					}
					if (customData === "p4" || customData === "supply3") {
						id = "purchasePlanningId";
					}
					if (customData === "p5" || customData === "supply2") {
						id = "productionPlanningId";
					}
					if (customData === "supply1") {
						id = "supplyBtnId";
					}
					if (customData === "supply4") {
						id = "productionListId";
					}
					if (customData === "supply5") {
						id = "purchaseContractsId";
					}
					if (customData === "demand1") {
						id = "demandViewBtnId";
					}
					if (customData === "demand2") {
						id = "salesContractsId";
					}
				}

				switch (id) {
				case "overViewBtnId":
					oRouter.navTo("Main");
					break;
				case "demandViewBtnId":
					oRouter.navTo("Demand");
					break;
				case "salesContractsId":
					oRouter.navTo("SalesContracts");
					break;
				case "supplyBtnId":
					oRouter.navTo("Supply");
					break;
				case "productionListId":
					oRouter.navTo("ProductionList");
					break;
				case "seasonalPlanToOfferId":
					oRouter.navTo("SeasonalPlanToOffer");
					break;
				case "quoteManagementId":
					oRouter.navTo("QuoteManagement");
					break;
				case "SeasonSummaryId":
					oRouter.navTo("SeasonSummary");
					break;
				case "purchasePlanningId":
					oRouter.navTo("PurchasePlanning");
					break;
				case "productionPlanningId":
					oRouter.navTo("ProductionPlanning");
					break;
				case "purchaseContractsId":
					oRouter.navTo("PurchaseContracts");
					break;
				case "DataMainBtnId":
					oRouter.navTo("DataMaintenance");
					break;
				default:
				}
			},

			//*************************************variant codes********************* 
			setStandardVariantF: function (tableId, fragment, callback) {
				var thiz = this;
				var oTable = Fragment.byId(fragment, tableId);
				sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
					thiz.oCC = oCC; //oContextContainer
					// 1 check if item is available or not if not then create
					if (!oCC.containsItem(tableId)) {
						oCC.setItemValue(tableId, {
							items: []
						});
					}
					// 2 check if standard is available or not if not then create
					var ovar = oCC.getItemValue(tableId);
					//if (!ovar.hasOwnProperty("*standard*")) {
					thiz.getDefaultPersoData(oTable, function (defaultSet) {
						ovar["*standard*"] = {
							key: "*standard*",
							text: "Standard",
							data: defaultSet
						}; //set default data
						if (!ovar.hasOwnProperty("defaultKey")) {
							ovar.defaultKey = "*standard*";
						}

						// quotation new layout
						var oModel = new sap.ui.model.json.JSONModel();
						// if (tableId === "gtpTableId") {
						// 	oModel.loadData(jQuery.sap.getModulePath("com.doehler.Z_GTP", "/model/settings.json"), "", false);

						// 	var oBom = oModel.getProperty("/tableData");
						// 	for (var lay in oBom) {
						// 		ovar[lay] = {
						// 			key: lay,
						// 			text: oBom[lay].text,
						// 			data: oBom[lay].settings
						// 		};
						// 		ovar.items = ovar.items.filter(function (key) {
						// 			return key.key !== "layout_new";
						// 		});
						// 		ovar.items.push({
						// 			key: lay,
						// 			text: oBom[lay].text
						// 		});
						// 	}
						// }

						oCC.setItemValue(tableId, ovar);
						oCC.save().done(function () {
							if (callback) {
								callback(oCC, defaultSet);
							}
						}).fail(function (err) {});
					});
				});
			},
			setStandardVariant: function (tableId, callback) {
				var thiz = this;
				var oTable = this.getView().byId(tableId);
				sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
					thiz.oCC = oCC; //oContextContainer
					// 1 check if item is available or not if not then create
					if (!oCC.containsItem(tableId)) {
						oCC.setItemValue(tableId, {
							items: []
						});
					}
					// 2 check if standard is available or not if not then create
					var ovar = oCC.getItemValue(tableId);
					//if (!ovar.hasOwnProperty("*standard*")) {
					thiz.getDefaultPersoData(oTable, function (defaultSet) {
						ovar["*standard*"] = {
							key: "*standard*",
							text: "Standard",
							data: defaultSet
						}; //set default data
						if (!ovar.hasOwnProperty("defaultKey")) {
							ovar.defaultKey = "*standard*";
						}

						// quotation new layout
						var oModel = new sap.ui.model.json.JSONModel();
						// if (tableId === "gtpTableId") {
						// 	oModel.loadData(jQuery.sap.getModulePath("com.doehler.Z_GTP", "/model/settings.json"), "", false);

						// 	var oBom = oModel.getProperty("/tableData");
						// 	for (var lay in oBom) {
						// 		ovar[lay] = {
						// 			key: lay,
						// 			text: oBom[lay].text,
						// 			data: oBom[lay].settings
						// 		};
						// 		ovar.items = ovar.items.filter(function (key) {
						// 			return key.key !== "layout_new";
						// 		});
						// 		ovar.items.push({
						// 			key: lay,
						// 			text: oBom[lay].text
						// 		});
						// 	}
						// }

						oCC.setItemValue(tableId, ovar);
						oCC.save().done(function () {
							if (callback) {
								callback(oCC, defaultSet);
							}
						}).fail(function (err) {});
					});
				});
			},

			/* get default perso data of table */
			getDefaultPersoData: function (oTable, callback) {
				var data = null;
				var oPersonalizer = {
					getPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					},
					setPersData: function (oBundle) {
						var oDeferred = jQuery.Deferred();
						data = oBundle;
						oDeferred.resolve();
						return oDeferred.promise();
					},
					delPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					}
				};
				var oTPC = new TablePersoController({
					table: oTable,
					persoService: oPersonalizer
				});
				oTPC.savePersonalizations().done(function () {
					callback(data);
				}).fail(function (err) {});
			},
			/* set perso data to table */
			setPersoData: function (oTable, data, callback) {
				var viewId = this.getView().getId();
				if (data.hasOwnProperty("aColumns")) {
					data.aColumns.forEach(function (item) {
						item.id = item.id.replace(/__xmlview[0-9]+/g, viewId);
					});
				}
				var oPersonalizer = {
					getPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve(data);
						return oDeferred.promise();
					},
					setPersData: function (oBundle) {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					},
					delPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					}
				};
				var oTPC = new TablePersoController({
					table: oTable,
					persoService: oPersonalizer,
					autoSave: false
				});
				oTPC.savePersonalizations().done(function () {
					if (callback) {
						callback();
						oTPC.refresh();
					}
				}).fail(function (err) {});
			},

			/* Get Perso Service with new data it is use to set new layout to table */
			getPersoService: function (oTable, callback) {
				var oPersonalizer = {
					getPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					},
					setPersData: function (oBundle) {
						var oDeferred = jQuery.Deferred();
						if (callback) {
							callback(oBundle);
						}
						oDeferred.resolve();
						return oDeferred.promise();
					},
					delPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					}
				};
				var oTPC = new TablePersoController({
					table: oTable,
					persoService: oPersonalizer
				});
				return oTPC;
			},

			/************************************Header search variant*******************************/
			setManageVM: function (oEvent, oCC, itemName, callback) {
				var ovar = oCC.getItemValue(itemName);
				// Rename
				var renameKeys = oEvent.getParameters().renamed;
				if (renameKeys.length > 0) {
					ovar.items.forEach(function (item) {
						renameKeys.forEach(function (reitem) {
							if (reitem.key === item.key) {
								item.text = reitem.name;
								ovar[item.key].text = reitem.name;
							}
						});
					});
				}
				// Delete
				var deletedKeys = oEvent.getParameters().deleted;
				if (deletedKeys.length > 0) {
					for (var i = ovar.items.length - 1; i >= 0; i--) {
						for (var j = 0; j < deletedKeys.length; j++) {
							if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
								ovar.items.splice(i, 1);
								delete ovar[deletedKeys[j]];
							}
						}
					}
				}
				ovar.defaultKey = oEvent.getParameters().def; // Default
				oCC.setItemValue(itemName, ovar);
				oCC.save().done(function () {
					if (callback) {
						callback(oCC);
					}
				}); // save all
			},

			/* remove default and make standard */
			fixVariant: function (oVM) {
				oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({ // set default
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(this.getVariantByKey(oVM, "default")); // remove default
			},

			setFilterVariant: function (itemName, key, text, data, bDefault, callback, fnError) {
				sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").done(function (oCC) {

					if (!oCC.containsItem(itemName)) {
						oCC.setItemValue(itemName, {
							items: []
						});
					}
					var ovar = oCC.getItemValue(itemName);
					if (!ovar.hasOwnProperty("defaultKey")) {
						ovar.defaultKey = "*standard*";
					}
					if (bDefault) {
						ovar.defaultKey = key;
					}
					if (key != "*standard*") {
						ovar.items.push({
							key: key,
							text: text
						});
					} // if not standard then push into items
					if (data) {
						ovar[key] = JSON.parse(JSON.stringify(data));
					} // if data is available then add data
					oCC.setItemValue(itemName, ovar);
					oCC.save().done(function () {
						if (callback) {
							callback(oCC);
						}
					}).fail(function (err) {
						if (fnError) {
							fnError();
						}
					});
				});
			},

			filterItems: function (arr, query, field) {
				return arr.filter(function (el) {
					return el[field].toString().toLowerCase().indexOf(query.toLowerCase()) !== -1;
				});
			},

			setTotalRow: function (oTable, view, filters) {

				var totalRow = {
					isSum: "X"
				};
				// filters.forEach(function (key) {
				if (_.isEmpty(filters) === false) {
					oTable = this.filterItems(oTable, filters.val, filters.col);
				}
				// });
				var tablength = oTable.length;

				// if (tablength === 1) {
				// 	oTable.forEach(function (row) {
				// 	Object.keys(row).forEach(function (key) {
				// 		totalRow[key] = totalRow[key] === undefined ? "" : totalRow[key];
				// 		 totalRow[key] = row[key];
				// 	}); });
				// } else {

				oTable.forEach(function (row) {
					Object.keys(row).forEach(function (key) {
						if (typeof row[key] === "string" || typeof row[key] === "boolean") {
							if (typeof row[key] === "boolean") {
								totalRow[key] = totalRow[key] === "" ? false : totalRow[key];

							} else {

								if (view === "PROD") {
									if (key === "WAERS") {
										totalRow[key] = totalRow[key] === undefined ? "" : totalRow[key];
										totalRow[key] = row[key];
									} else {
										totalRow[key] = "";
									}

								} else {
									totalRow[key] = "";
								}
							}

						} else {
							if (view === "QUOTE") {
								if (key === "QUOTATION_QUANTITY") {
									totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
									totalRow[key] += row[key];
								} else {
									totalRow[key] = "";
								}

							} else if (view === "PROD") {
								if (key === "BRIX_OF_FRUIT" ||
									key === "PRICE_OF_FRUIT" ||
									key === "PREIS_TAGESKURS" ||
									key === "PER_CA") {
									totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
									totalRow[key] = totalRow[key] + (row[key] * row.INPUT_FRUIT);
								} else if (key === "YIELD_REAL_BRIX" ||
									key === "YIELD_STANDARD_BRIX" ||
									key === "COST_CONCENTRATE_EUR_TAGESKURS") {
									totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
									totalRow[key] = totalRow[key] + (row[key] * row.OUTPUT_CONCENTRATE);
								} else {
									totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
									totalRow[key] += row[key];
								}
							} else if (view === "SPT") {
								if (key === "QUOTED_CROSS_PRICE") {
									totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
									totalRow[key] = totalRow[key] + (row[key] * row.OPEN_QUOTES);
								} else {
									totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
									totalRow[key] += row[key];
								}
							} else {
								if (view === "SALES_CONTRACTS") {
									// if (key === "GROSSPRICE") {
									// 	totalRow[key] = totalRow["CONT_QUAN_GROSS"] / totalRow["CONTQUANTITY"];
									// } else {
									totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
									totalRow[key] += row[key];
									// }
								} else {
									totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
									totalRow[key] += row[key];
								}
							}
						}
					});

				});

				// }

				if (view === "PROD" ||
					view === "SPT") {
					Object.keys(totalRow).forEach(function (key) {
						if (view === "PROD") {
							if (key === "BRIX_OF_FRUIT" ||
								key === "PRICE_OF_FRUIT" ||
								key === "PREIS_TAGESKURS" ||
								key === "PER_CA") {
								totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
								totalRow[key] = totalRow[key] / totalRow.INPUT_FRUIT;
							} else if (key === "YIELD_REAL_BRIX" ||
								key === "YIELD_STANDARD_BRIX" ||
								key === "COST_CONCENTRATE_EUR_TAGESKURS") {
								totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
								totalRow[key] = totalRow[key] / totalRow.OUTPUT_CONCENTRATE;
							}
						} else {
							if (key === "QUOTED_CROSS_PRICE") {
								totalRow[key] = totalRow[key] === undefined ? 0 : totalRow[key];
								totalRow[key] = totalRow[key] / totalRow.OPEN_QUOTES;
							}
						}
					});
				}
				if (_.isEmpty(filters) === false) {
					//	totalRow[filters.col] = filters.val;------->Sor
				}

				totalRow["isSum"] = "X";

				return totalRow;
			},

			getSearchHelpData: function (identifier, Resolve, myReject) {
				var that = this;
				var param = {
					HANDLERPARAMS: {
						FUNC: "SEARCH_F4"
					},
					INPUTPARAMS: []
				};

				param.INPUTPARAMS.push({
					"PARIDN": identifier,
					"KOTAB": "",
					"RELFLDVAL1": "",
					"RELFLDVAL2": "",
					"RELFLDVAL3": "",
					"RELFLDVAL4": "",
					"RELFLDVAL5": "",
					"RELFLDVAL6": "",
					"RELFLDVAL7": "",
					"RELFLDVAL8": "",
					"RELFLDVAL9": "",
					"RELFLDVAL10": "",
					"RELFLDVAL11": "",
					"RELFLDVAL12": "",
					"NOENTRY": "",
					"SEARCHT": ""
				});

				dbcontext.callServer(param, function (oModel) {
					var data = oModel.getData().F4_VALUE_DYN;
					data.unshift();
					Resolve(data);
					// return data;

				}, that);
			},

			_checkValidationOfGTPQuote: function (fieldName, path, createGTPquoteTableModel) {
				var customer,
					salesOrg,
					distChannel,
					isReady = true;
				if (fieldName === "SALESORG") {
					customer = createGTPquoteTableModel.getProperty(path + "/CUSTL2");
					if (customer === "") {
						MessageBox.warning("Please enter Customer l2 field data before the Sales Org");
						createGTPquoteTableModel.setProperty(path + "/VKORG", "");
						isReady = false;
					}
				}

				if (fieldName === "SHIP_TO") {
					customer = createGTPquoteTableModel.getProperty(path + "/CUSTL2");
					salesOrg = createGTPquoteTableModel.getProperty(path + "/VKORG");
					distChannel = createGTPquoteTableModel.getProperty(path + "/VTWEG");
					if (customer === "" || salesOrg === "" || distChannel === "") {
						MessageBox.warning("Please enter Customer l2, Sales Org and Distribution Channel fields datas before the Ship to");
						createGTPquoteTableModel.setProperty(path + "/SHIP_TO_PARTY", "");
						isReady = false;
					}
				}

				if (fieldName === "PAYER") {
					customer = createGTPquoteTableModel.getProperty(path + "/CUSTL2");
					salesOrg = createGTPquoteTableModel.getProperty(path + "/VKORG");
					distChannel = createGTPquoteTableModel.getProperty(path + "/VTWEG");
					if (customer === "" || salesOrg === "" || distChannel === "") {
						MessageBox.warning("Please enter Customer l2, Sales Org and Distribution Channel fields datas before the Payer");
						createGTPquoteTableModel.setProperty(path + "/PAYER", "");
						isReady = false;
					}
				}

				if (fieldName === "CURRENCY") {
					salesOrg = createGTPquoteTableModel.getProperty(path + "/VKORG");
					customer = createGTPquoteTableModel.getProperty(path + "/CUSTL2");
					if (salesOrg === "" || customer === "") {
						MessageBox.warning("Please enter Customer l2 and Sales Org fields datas before the Currency");
						createGTPquoteTableModel.setProperty(path + "/PRICE_WAERS", "");
						isReady = false;
					}
				}

				return isReady;
			},

			_checkFruitMantr: function (matnr, fruit, myResolve) {
				var that = this;
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "CHECK_FRUIT_MATNR"
					},
					"INPUTPARAMS": [{
						"MATNR": matnr,
						"FRUIT": fruit
					}]
				};
				dbcontext.callServer(params, function (oModel) {
					myResolve(oModel.getData().MESSAGES.STATUS);
					that.handleServerMessages(oModel, function (status) {
						// myResolve(status);
					}, false);

				});
			},

			_fillPackingClass: function (matnr, plant, myResolve) {
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "FILL_PACKING_CLASS"
					},
					"INPUTPARAMS": [{
						"MATNR": matnr,
						"WERKS": plant
					}]
				};
				dbcontext.callServer(params, function (oModel) {
					myResolve(oModel.getData().RESULTS.ZZKALK05);

				});
			}

		});
	});