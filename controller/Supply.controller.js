sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	"com/doehler/Z_GTP/model/formatter",
	"sap/ui/core/Fragment",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, dbcontext, dbcontext2, excel, formatter, Fragment, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.Supply", {
		formatter: formatter,
		onInit: function () {
			this._setButtonsPressed("supplyBtnId");
			this._tableId = this.getView().byId("gtpTableId");
			this._tableId2 = this.getView().byId("gtpTableId2");
			this._tableIdText = "gtpTableId";
			this._tableIdText2 = "gtpTableId2";
			this.columnfilter = {};
			this.columnfilter2 = {};

			//panel Model
			this._panelModel = this.getOwnerComponent().getModel("supplyPanelM");
			sap.ui.getCore().setModel(this._panelModel, "supplyPanelM");
			this._tableModel = this.getOwnerComponent().getModel("supplyTableM");
			this._tableModel.setProperty("/tableRows", []);
			this._tableModel.setProperty("/tableRows2", []);
			this._panelModel.setData({});
			this._initializePanelModel();
			this.getView().byId("gtpTableId").setVisible(true);
			this.getView().byId("gtpTableId2").setVisible(false);
			sap.ui.getCore().getModel("globalM").setProperty("/PageTitle", "(L/S Sourcing)");
			sap.ui.getCore().getModel("globalM").setProperty("/isSupply", false);

			this.getRouter().getRoute("Supply").attachPatternMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function (oEvent) {
			debugger;
			this._setButtonsPressed("supplyBtnId");
			if (sap.ui.getCore().getModel("globalM").getProperty("/isSupply") === true)
				this.createToken();

		},

		onSortActual: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._tableModel.getProperty("/tableRows2");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this._tableId2.getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);

			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._tableModel);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("gtpTableId2").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._tableModel.setProperty("/tableRows2", notSumRow);
			// 	that.getView().byId("gtpTableId2").bindRows("supplyTableM>/tableRows2");

			// }, 100)
		},

		onSortSourcing: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._tableModel.getProperty("/tableRows");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this._tableId.getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);
			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._tableModel);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("gtpTableId").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._tableModel.setProperty("/tableRows", notSumRow);
			// 	that.getView().byId("gtpTableId").bindRows("supplyTableM>/tableRows");

			// }, 100)
		},

		onFilterActual: function (oEvent) {
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter2.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter2.val = oEvent.getParameter("value");
			} else {
				this.columnfilter2 = {};
			}

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		onFilterSourcing: function (oEvent) {
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/COLLECTIVE_NUMBER", "");
			this._panelModel.setProperty("/SUB_GROUP", "");
			this._panelModel.setProperty("/PLANT_EXPLODED", "");
			this._panelModel.setProperty("/COUNTRY_SELLING", "");
			this._panelModel.setProperty("/SUPPLIER", "");
			this._panelModel.setProperty("/SUPPLIER_COUNTRY", "");
			this._panelModel.setProperty("/MAT_GRP_1", "");
			this._panelModel.setProperty("/MAT_GRP_2", "");
			this._panelModel.setProperty("/MAT_GRP_3", "");
			this._panelModel.setProperty("/MAT_GRP_4", "");
			this._panelModel.setProperty("/MAT_GRP_5", "");
			this._panelModel.setProperty("/FS1", "");
			this._panelModel.setProperty("/FS2", "");
			this._panelModel.setProperty("/FS3", "");
			this._panelModel.setProperty("/IC_BUSINESS", "");
		    this._panelModel.setProperty("/ITEM_CATEG", "");

			var that = this;
			setTimeout(
				function () {
					if (that.getView().byId("icbusiness") !== undefined) {
						that.getView().byId("icbusiness").removeAllTokens();
						that.getView().byId("icbusiness").destroyTokens();
						// if (sap.ui.getCore().getModel("globalM").getProperty("/QuoteManagementNavto") === undefined) {
						that.getView().byId("icbusiness").addToken(new sap.m.Token({
							key: ".",
							text: "False"
						}));
						that._panelModel.setProperty("/IC_BUSINESS", ".");
						// }
					}
				}, 1000);
		},

		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
			}
		},

		onValueHelpInit: function (oEvent) {
			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
		},

		onValueHelpInit2: function (oEvent) {
			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext2));
		},

		onGo: function () {
			//Fruits -------------->  DEXMAT__RMW21ZH02   CUST-X.0000000001004
			//Substance group -------------->  DEXMAT__DSUBCHAR   02
			///sap/opu/odata/sap/ZBW_DEMO_SRV/getListSet?$filter=Plant eq '1300'
			var that = this;
			var filter = [];
			var plant = this._panelModel.getProperty("/PLANT");
			var fruit = this._panelModel.getProperty("/FRUIT");
			var subStance = this._panelModel.getProperty("/SUB_GROUP");
			var collectiveNumber = this._panelModel.getProperty("/COLLECTIVE_NUMBER");
			var plantArray = plant.split(",");
			var fruitArray = fruit.split(",");
			var subStanceArray = subStance.split(",");
			var collectiveNumberArray = collectiveNumber.split(",");
			if (plant !== "")
				plantArray.forEach(function (_plant) {
					filter.push(new sap.ui.model.Filter("Plant", FilterOperator.EQ, _plant));
				});
			if (fruit !== "")
				fruitArray.forEach(function (_fruit) {
					filter.push(new sap.ui.model.Filter("bicrmw21zh02", FilterOperator.EQ, _fruit));
				});
			if (subStance !== "")
				subStanceArray.forEach(function (_sub) {
					filter.push(new sap.ui.model.Filter("bicdsubchar", FilterOperator.EQ, _sub));
				});
			if (collectiveNumber !== "")
				collectiveNumberArray.forEach(function (_coll) {
					filter.push(new sap.ui.model.Filter("bicdcollnr", FilterOperator.EQ, _coll));
				});
			//clear table data
			// this._tableModel.setProperty("/tableRows", []);
			// this._tableModel.setProperty("/tableRows2", []);
			// this._tableModel.setProperty("/tableRows3", []);
			this._callList();

		},

		_callList: function () {

			var that = this;
			var funcName = "";

			if (that._tableId.getVisible()) {
				funcName = "GET_DATA_MAIN";
			} else if (that._tableId2.getVisible()) {
				funcName = "GET_DATA_MAIN_BU_PBN";
			} else if (that._tableId3.getVisible()) {
				funcName = "GET_DATA_MAIN_MONTHLY";
			}

			var params = {
				"HANDLERPARAMS": {
					"FUNC": funcName
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};
			dbcontext.callServer(params, function (oModel) {

				var data = oModel.getData().RESULTS;
				var columnText = oModel.getData().DYNAMIC;
				if (that._tableId.getVisible()) {
					data.forEach(function (row) {
						row["RMW11ZE03___T"] = that.getDetermineOrganic(row["RMW11ZE03"]);
						row["RMW21ZE03___T"] = that.getDetermineHalal(row["RMW21ZE03"]);
						row["RMW31ZE03___T"] = that.getDetermineKosher(row["RMW31ZE03"]);
						row["ZSCOUNTRY_F"] = row["ZSCOUNTRY"] + row["ZSCOUNTRY___T"];
						row["ZCOUNTRY_F"] = row["ZCOUNTRY"] + row["ZCOUNTRY___T"];
					});
					that._tableModel.setProperty("/tableRows", data);
					that.initallData = data;
					that._findTableKeys();
				} else if (that._tableId2.getVisible()) {
					data.forEach(function (row) {
						row["RMW11ZE03___T"] = that.getDetermineOrganic(row["RMW11ZE03"]);
						row["RMW21ZE03___T"] = that.getDetermineHalal(row["RMW21ZE03"]);
						row["RMW31ZE03___T"] = that.getDetermineKosher(row["RMW31ZE03"]);
						row["ZSCOUNTRY_F"] = row["ZSCOUNTRY"] + row["ZSCOUNTRY___T"];
						row["ZCOUNTRY_F"] = row["ZCOUNTRY"] + row["ZCOUNTRY___T"];
					});
					that._tableModel.setProperty("/tableRows2", data);
					that.initallData2 = data;
					that._findTableKeys2();
				} else if (that._tableId3.getVisible()) {
					that._tableModel.setProperty("/tableRows3", data);
					that.initallData3 = data;
					that._findTableKeys3();
					that.setDynamiCol(columnText);

				}
			}, that);
		},

		onExport: function () {
			var data = [];
			var userId = sap.ushell.Container.getUser().getId();
			if (this._tableId.getVisible()) {
				var tableData = this._tableModel.getData();
				var copiedData = tableData.tableRows;
				var aIndices = this._tableId.getBinding("rows").aIndices;
			} else {
				if (this._tableId2.getVisible()) {
					var tableData = this._tableModel.getData();
					var copiedData = tableData.tableRows2;
					var aIndices = this._tableId2.getBinding("rows").aIndices;
				} else {
					var tableData = this._tableModel.getData();
					var copiedData = tableData.tableRows3;
					var aIndices = this._tableId3.getBinding("rows").aIndices;
				}
			}
			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			if (this._tableId.getVisible())
				excel._downloadExcel("SupplySourcing", this._tableId, data);
			else {
				if (this._tableId2.getVisible())
					excel._downloadExcel("SupplyActualDate", this._tableId2, data);
				else
					excel._downloadExcel("OMONTHLY", this._tableId3, data);
			}
		},

		onSelectionChangeTableView: function (oEvent) {
			var key = oEvent.getParameter("item").getProperty("key");
			var table1 = this._tableModel.getProperty("/tableRows2");
			var table2 = this._tableModel.getProperty("/tableRows");
			if (key === "1") {
				sap.ui.getCore().getModel("globalM").setProperty("/PageTitle", "(L/S BU PBN)");
				this.getView().byId("gtpTableId").setVisible(false);
				this.getView().byId("gtpTableId2").setVisible(true);
				this.getView().byId("sgmBtn1").setSelectedKey("1");
			}
			if (key === "2") {
				sap.ui.getCore().getModel("globalM").setProperty("/PageTitle", "(L/S Sourcing)");
				this.getView().byId("gtpTableId").setVisible(true);
				this.getView().byId("gtpTableId2").setVisible(false);
				this.getView().byId("sgmBtn").setSelectedKey("2");
			}
		},

		onPressSACView: function () {
			var userId = sap.ushell.Container.getUser().getId();
			var selIndices = this._tableId.getSelectedIndices();
			if (this._tableId2.getVisible())
				selIndices = this._tableId2.getSelectedIndices();
			if (selIndices.length > 0) {
				if (userId === "EX_OEZDEMC" || userId === "DEFAULT_USER" || userId === "EX_DOGUS" || userId === "EX_YILMAZO" || userId ===
					"MATTHEC" || userId === "FISCHEA") {
					this.getView().byId("query").setVisible(false);
					this.getView().byId("query2").setVisible(true);
				}
			} else
				sap.m.URLHelper.redirect(
					"https://doehler-test1.eu10.sapanalytics.cloud/sap/fpa/ui/app.html#;view_id=story;storyId=9261A304E6CBEA888CF0E091D2CAEE7B;mode=embed;f01Val=44059;f01Dim=MSCUSTOM2;f01Model=t.1:C8v4uf8xnsvanv2lqbyn9q5ji8",
					true);
		},

		onPressBWView: function () {
			var that = this;
			var selectedTable = this._tableId.getVisible() ? this._tableId : this._tableId2.getVisible() ? this._tableId2 : this._tableId3;
			var selIndices = selectedTable.getSelectedIndices(),
				newSelIndex = [],
				aIndices = selectedTable.getBinding().aIndices;

			selIndices.forEach(function (selIndex) {
				newSelIndex.push(aIndices[selIndex]);
			});

			selIndices = newSelIndex;

			var data = this._tableModel.getData().tableRows;
			if (this._tableId2.getVisible())
				data = this._tableModel.getData().tableRows2;
			if (this._tableId3.getVisible())
				data = this._tableModel.getData().tableRows3;

			var plant = "",
				subchar = "",
				fruit = "",
				coll = "",
				compCode = "";

			selIndices.forEach(function (ind, index) {
				if (that._tableId3.getVisible()) {
					data[ind]["DSUBCHAR"] = "";
					if (data[ind]["A0PLANT"] !== "")
						plant = plant + data[ind]["A0PLANT"] + ",";
					if (data[ind]["DSUBCHAR"] !== "")
						subchar = subchar + data[ind]["DSUBCHAR"] + ",";
					if (data[ind]["RMW21ZH02"] !== "")
						fruit = fruit + data[ind]["ADEXMAT__RMW21ZH02"] + ",";
					if (data[ind]["DCOLLNR"] !== "")
						coll = coll + data[ind]["ADEXMAT__DCOLLNR"] + ",";
					if (data[ind]["A0PLANT__0COMP_CODE"] !== "")
						compCode = compCode + data[ind]["A0PLANT__0COMP_CODE"] + ",";
				} else {

					if (data[ind]["PLANT"] !== "")
						plant = plant + data[ind]["PLANT"] + ",";
					if (data[ind]["DSUBCHAR"] !== "")
						subchar = subchar + data[ind]["DSUBCHAR"] + ",";
					if (data[ind]["RMW21ZH02"] !== "")
						fruit = fruit + data[ind]["RMW21ZH02"] + ",";
					if (data[ind]["DCOLLNR"] !== "")
						coll = coll + data[ind]["DCOLLNR"] + ",";
				}

			});

			if (selIndices.length === 0) {
				plant = this._panelModel.getProperty("/PLANT");
				subchar = this._panelModel.getProperty("/SUB_GROUP");
				fruit = this._panelModel.getProperty("/FRUIT");
				coll = this._panelModel.getProperty("/COLLECTIVE_NUMBER");
			}
			var queryCode = this._tableId.getVisible() ? "DJCVCP011_Q004" : this._tableId2.getVisible() ? "DJCVCP011_Q001" : "DJCVCP011_Q022";
			var HDSUBCHAR = subchar.split(",").join(";"),
				MSFRUIT2 = fruit.split(",").join(";"),
				MSWERK4 = "",
				MSSWERK = plant.split(",").join(";"),
				MSDCOLLN = coll.split(",").join(";"),
				MSCOMPCO = compCode.split(",").join(";"),
				MSDEXMAT = "";
			var hostName = sap.ui.getCore().getModel("globalM").getProperty("/HOST_PORT");
			var url = hostName +
				"/irj/servlet/prt/portal/prtroot/pcd!3aportal_content!2fcom.sap.pct!2fplatform_add_ons!2fcom.sap.ip.bi!2fiViews!2fcom.sap.ip.bi.bex?QUERY=" +
				queryCode +
				"&VARIABLE_SCREEN='X'&DUMMY=1&BI_COMMAND_1-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=HDSUBCHAR&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
				HDSUBCHAR +
				"&BI_COMMAND_2-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSSWERK&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
				MSSWERK +
				"&BI_COMMAND_3-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSFRUIT2&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
				MSFRUIT2 +
				"&BI_COMMAND_4-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSDCOLLN&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
				MSDCOLLN;

			if (this._tableId3.getVisible()) {
				url = hostName +
					"/irj/servlet/prt/portal/prtroot/pcd!3aportal_content!2fcom.sap.pct!2fplatform_add_ons!2fcom.sap.ip.bi!2fiViews!2fcom.sap.ip.bi.bex?QUERY=" +
					queryCode +
					"&VARIABLE_SCREEN='X'&DUMMY=1&BI_COMMAND_1-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSSWERK&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					MSSWERK +
					"&BI_COMMAND_2-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSCOMPCO&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					MSCOMPCO +
					"&BI_COMMAND_3-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSFRUIT2&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					MSFRUIT2 +
					"&BI_COMMAND_4-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=HDSUBCHAR&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					HDSUBCHAR +
					"&BI_COMMAND_5-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_5-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSDCOLLN&BI_COMMAND_5-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_5-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					MSDCOLLN;
			}

			sap.m.URLHelper.redirect(url, true);

		},

		goToDetail: function () {
			this.getOwnerComponent().getRouter().navTo("Detail", {
				param1: "1"
			});
		},

		setDynamiCol: function (columnText) {
			this._ColumnMonthM = this.getOwnerComponent().getModel("ColumnMonthM");
			var that = this;

			columnText.forEach(function (item, index) {
				var colname = "/COL_";
				index++;
				colname = colname + index;
				that._ColumnMonthM.setProperty(colname, item.CAPTION);
			});
		},

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		oCC2: null,
		oCC3: null,
		currTableData: null,
		currTableData2: null,
		currTableData3: null,
		oTPC: null,
		oTPC2: null,
		oTPC3: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},
		onSettings2: function () {
			this.oTPC2.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC2 = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},
		onSettings3: function () {
			this.oTPC3.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC3 = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId;
			var oVM = this.getView().byId("supplytableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}

					// Find Table Key
					thiz._findTableKeys();

				});
			});
		},

		_findTableKeys: function () {
			var thiz = this;
			var oTable = this._tableId;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys.length)
				thiz.onPressAggregation();
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys();
		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		///for second view
		initVariant2: function () {
			var thiz = this;
			var oTable = this._tableId2;
			var oVM = this.getView().byId("supplytableVMId2");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText2, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC2 = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText2);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData2 = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC2 = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData2 = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}

					// Find Table Key
					thiz._findTableKeys2();

				});
			});
		},

		_findTableKeys2: function () {
			var thiz = this;
			var oTable = this._tableId2;
			thiz.mainTableKeys2 = [];
			thiz.commentTableKeys2 = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys2.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys2.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys2.length)
				thiz.onPressAggregation2();
		},

		/* on select variant */
		onSelectVariant2: function (oEvent) {
			var tableId = this._tableIdText2;
			var oTable = this._tableId2;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys2();
		},
		/* on save new variant */
		onSaveVariant2: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText2;
			var oTable = this._tableId2;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData2) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData2,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM2: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText2;
			var ovar = oCC.getItemValue(tableId2);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdSupply");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("supplyPanelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isSupply", true);
			sap.ui.getCore().getModel("globalM").setProperty("/supplyInit", true);
			debugger;
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("supplyPanelM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantOverview: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("supplyPanelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantOverview: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("supplyPanelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVariantOverview: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1) {
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined)
				this._updatePanelData(fromData, sap.ui.getCore().getModel("supplyPanelM"));
			var aControls = this.getView().byId("searchGridId").getContent();

			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));
					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {
									if (value != "") {
										if (item.getFieldName() === "FRUIT") {
											const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
											item.addToken(new sap.m.Token({
												key: value,
												text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
											}));
										} else {
											item.addToken(new sap.m.Token({
												key: value,
												text: value
											}));
										}
									}
								});

							}
						}
					});
				}, 1000);
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () { // TODO: 
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			// this.clearSelectionFields();
			this.initVariant();
			this.initVariant2();
			this.initSearchVariant();
		},

		onPressAggregation: function () {
			this.convertGroup(this.mainTableKeys);
			this.collectData();
		},

		onPressAggregation2: function () {
			this.convertGroup2(this.mainTableKeys2);
			this.collectData2();
		},

		convertGroup: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData, groupCols);
			this.groupData = groupData;
		},

		convertGroup2: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData2, groupCols);
			this.groupData2 = groupData;
		},

		collectData: function () {
			var that = this;
			var newCollectedData = [];
			$.each(this.groupData, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.COUNTRY = k.COUNTRY;
					newEntry.DSDREGION = k.DSDREGION;
					newEntry.DZZB2B_L2 = k.DZZB2B_L2;
					newEntry.DZZB2B_L2___T = k.DZZB2B_L2___T;
					newEntry.RMW11ZP16 = k.RMW11ZP16;
					newEntry.RMW11ZP16___T = k.RMW11ZP16___T;
					newEntry.MATERIAL = k.MATERIAL;
					newEntry.MATERIAL___T = k.MATERIAL___T;
					newEntry.RMW11ZE03 = k.RMW11ZE03;
					newEntry.RMW11ZE03___T = k.RMW11ZE03___T;
					newEntry.RMW21ZE03 = k.RMW21ZE03;
					newEntry.RMW21ZE03___T = k.RMW21ZE03___T;
					newEntry.RMW31ZE03 = k.RMW31ZE03;
					newEntry.RMW31ZE03___T = k.RMW31ZE03___T;

					newEntry.ITEM_CATEG = k.ITEM_CATEG;
					newEntry.ITEM_CATEG___T = k.ITEM_CATEG___T;
					
					newEntry.STOCKTYPE = k.STOCKTYPE;

					newEntry.DOEMGL1 = k.DOEMGL1;
					newEntry.DOEMGL2 = k.DOEMGL2;
					newEntry.DOEMGL3 = k.DOEMGL3;
					newEntry.DOEMGL4 = k.DOEMGL4;
					newEntry.DOEMGL5 = k.DOEMGL5;
					newEntry.DOEMGL1___T = k.DOEMGL1___T;
					newEntry.DOEMGL2___T = k.DOEMGL2___T;
					newEntry.DOEMGL3___T = k.DOEMGL3___T;
					newEntry.DOEMGL4___T = k.DOEMGL4___T;
					newEntry.DOEMGL5___T = k.DOEMGL5___T;
					newEntry.CONTRACT = k.CONTRACT;
					newEntry.VENDOR = k.VENDOR;
					newEntry.VENDOR___T = k.VENDOR___T;
					newEntry.STOCKTYPE = k.STOCKTYPE;
					newEntry.STOCKTYPE___T = k.STOCKTYPE___T;
					newEntry.DEXMAT = k.DEXMAT;
					newEntry.DEXMAT___T = k.DEXMAT___T;
					newEntry.DEXPLANT = k.DEXPLANT;
					newEntry.DEXPLANT___T = k.DEXPLANT___T;
					newEntry.DSDREG = k.DSDREG;
					newEntry.DSDREG___T = k.DSDREG___T;
					newEntry.ZCOUNTRY = k.ZCOUNTRY;
					newEntry.ZCOUNTRY_F = k.ZCOUNTRY_F;
					newEntry.ZCOUNTRY___T = k.ZCOUNTRY___T;
					newEntry.ZSCOUNTRY = k.ZSCOUNTRY;
					newEntry.ZSCOUNTRY_F = k.ZSCOUNTRY_F;
					newEntry.ZSCOUNTRY___T = k.ZSCOUNTRY___T;

					newEntry.PLANT = k.PLANT;
					newEntry.PLANT___T = k.PLANT___T;
					newEntry.RMW21ZH02 = k.RMW21ZH02;
					newEntry.RMW21ZH02___T = k.RMW21ZH02___T;
					newEntry.DSUBCHAR = k.DSUBCHAR;
					newEntry.DSUBCHAR___T = k.DSUBCHAR___T;
					newEntry.DCOLLNR = k.DCOLLNR;
					newEntry.DCOLLNR___T = k.DCOLLNR___T;

					newEntry.DFRTSPC1 = k.DFRTSPC1;
					newEntry.DFRTSPC1___T = k.DFRTSPC1___T;
					newEntry.DFRTSPC2 = k.DFRTSPC2;
					newEntry.DFRTSPC2___T = k.DFRTSPC2___T;
					newEntry.DFRTSPC3 = k.DFRTSPC3;
					newEntry.DFRTSPC3___T = k.DFRTSPC3___T;

					newEntry.RMW11ZE03 = k.RMW11ZE03;
					newEntry.RMW11ZE03___T = k.RMW11ZE03___T;
					newEntry.RMW21ZE03 = k.RMW21ZE03;
					newEntry.RMW21ZE03___T = k.RMW21ZE03___T;
					newEntry.RMW31ZE03 = k.RMW31ZE03;
					newEntry.RMW31ZE03___T = k.RMW31ZE03___T;

					newEntry.DJCICFLAG = k.DJCICFLAG;
					newEntry.DJCICFLAG___T = k.DJCICFLAG___T;

					newEntry.DJCDAD094_LONGSHORTRFQ = newEntry.DJCDAD094_LONGSHORTRFQ === undefined ? 0 : newEntry.DJCDAD094_LONGSHORTRFQ;
					newEntry.DJCDAD094_LONGSHORTRFQ = (newEntry.DJCDAD094_LONGSHORTRFQ);
					if (k.DJCDAD094_LONGSHORTRFQ === undefined) k.DJCDAD094_LONGSHORTRFQ = 0;
					newEntry.DJCDAD094_LONGSHORTRFQ = (k.DJCDAD094_LONGSHORTRFQ) + newEntry.DJCDAD094_LONGSHORTRFQ;
					
					
					newEntry.DJCDAD094_TBLOCKEDSTOCKS = newEntry.DJCDAD094_TBLOCKEDSTOCKS === undefined ? 0 : newEntry.DJCDAD094_TBLOCKEDSTOCKS;
					newEntry.DJCDAD094_TBLOCKEDSTOCKS = (newEntry.DJCDAD094_TBLOCKEDSTOCKS);
					if (k.DJCDAD094_TBLOCKEDSTOCKS === undefined) k.DJCDAD094_TBLOCKEDSTOCKS = 0;
					newEntry.DJCDAD094_TBLOCKEDSTOCKS = (k.DJCDAD094_TBLOCKEDSTOCKS) + newEntry.DJCDAD094_TBLOCKEDSTOCKS;

					newEntry.DJCDAD094_LONGSHORTQUOTES = newEntry.DJCDAD094_LONGSHORTQUOTES === undefined ? 0 : newEntry.DJCDAD094_LONGSHORTQUOTES;
					newEntry.DJCDAD094_LONGSHORTQUOTES = (newEntry.DJCDAD094_LONGSHORTQUOTES);
					if (k.DJCDAD094_LONGSHORTQUOTES === undefined) k.DJCDAD094_LONGSHORTQUOTES = 0;
					newEntry.DJCDAD094_LONGSHORTQUOTES = (k.DJCDAD094_LONGSHORTQUOTES) + newEntry.DJCDAD094_LONGSHORTQUOTES;

					newEntry.DJCDAD094_OPENQUOTES = newEntry.DJCDAD094_OPENQUOTES === undefined ? 0 : newEntry.DJCDAD094_OPENQUOTES;
					newEntry.DJCDAD094_OPENQUOTES = (newEntry.DJCDAD094_OPENQUOTES);
					if (k.DJCDAD094_OPENQUOTES === undefined) k.DJCDAD094_OPENQUOTES = 0;
					newEntry.DJCDAD094_OPENQUOTES = (k.DJCDAD094_OPENQUOTES) + newEntry.DJCDAD094_OPENQUOTES;

					newEntry.DJCDAD094_OPENRFQS = newEntry.DJCDAD094_OPENRFQS === undefined ? 0 : newEntry.DJCDAD094_OPENRFQS;
					newEntry.DJCDAD094_OPENRFQS = (newEntry.DJCDAD094_OPENRFQS);
					if (k.DJCDAD094_OPENRFQS === undefined) k.DJCDAD094_OPENRFQS = 0;
					newEntry.DJCDAD094_OPENRFQS = (k.DJCDAD094_OPENRFQS) + newEntry.DJCDAD094_OPENRFQS;
					//Aggregations
					newEntry.DJCDAD094_ACTUAL_CONSUMPTION = newEntry.DJCDAD094_ACTUAL_CONSUMPTION === undefined ? 0 : newEntry.DJCDAD094_ACTUAL_CONSUMPTION;
					newEntry.DJCDAD094_ACTUAL_CONSUMPTION = (newEntry.DJCDAD094_ACTUAL_CONSUMPTION);
					newEntry.DJCDAD094_ACTUAL_CONSUMPTION = (k.DJCDAD094_ACTUAL_CONSUMPTION) + newEntry.DJCDAD094_ACTUAL_CONSUMPTION;
					newEntry.DJCDAD094_CONSUMPTION = newEntry.DJCDAD094_CONSUMPTION === undefined ? 0 : newEntry.DJCDAD094_CONSUMPTION;
					newEntry.DJCDAD094_CONSUMPTION = (newEntry.DJCDAD094_CONSUMPTION);
					newEntry.DJCDAD094_CONSUMPTION = (k.DJCDAD094_CONSUMPTION) + newEntry.DJCDAD094_CONSUMPTION;
					newEntry.DJCDAD094_STOCK_RAW_MAT = newEntry.DJCDAD094_STOCK_RAW_MAT === undefined ? 0 : newEntry.DJCDAD094_STOCK_RAW_MAT;
					newEntry.DJCDAD094_STOCK_RAW_MAT = (newEntry.DJCDAD094_STOCK_RAW_MAT);
					newEntry.DJCDAD094_STOCK_RAW_MAT = (k.DJCDAD094_STOCK_RAW_MAT) + newEntry.DJCDAD094_STOCK_RAW_MAT;
					newEntry.DJCDAD094_STOCK_FINISHED_PROD = newEntry.DJCDAD094_STOCK_FINISHED_PROD === undefined ? 0 : newEntry.DJCDAD094_STOCK_FINISHED_PROD;
					newEntry.DJCDAD094_STOCK_FINISHED_PROD = (newEntry.DJCDAD094_STOCK_FINISHED_PROD);
					newEntry.DJCDAD094_STOCK_FINISHED_PROD = (k.DJCDAD094_STOCK_FINISHED_PROD) + newEntry.DJCDAD094_STOCK_FINISHED_PROD;
					newEntry.DJCDAD094_STOCK_SEMI_MAT = newEntry.DJCDAD094_STOCK_SEMI_MAT === undefined ? 0 : newEntry.DJCDAD094_STOCK_SEMI_MAT;
					newEntry.DJCDAD094_STOCK_SEMI_MAT = (newEntry.DJCDAD094_STOCK_SEMI_MAT);
					newEntry.DJCDAD094_STOCK_SEMI_MAT = (k.DJCDAD094_STOCK_SEMI_MAT) + newEntry.DJCDAD094_STOCK_SEMI_MAT;
					newEntry.DJCDAD094_PURCONTRACT_CALLOFF = newEntry.DJCDAD094_PURCONTRACT_CALLOFF === undefined ? 0 : newEntry.DJCDAD094_PURCONTRACT_CALLOFF;
					newEntry.DJCDAD094_PURCONTRACT_CALLOFF = (newEntry.DJCDAD094_PURCONTRACT_CALLOFF);
					newEntry.DJCDAD094_PURCONTRACT_CALLOFF = (k.DJCDAD094_PURCONTRACT_CALLOFF) + newEntry.DJCDAD094_PURCONTRACT_CALLOFF;
					newEntry.DJCDAD094_PURCH_OPENQUAN = newEntry.DJCDAD094_PURCH_OPENQUAN === undefined ? 0 : newEntry.DJCDAD094_PURCH_OPENQUAN;
					newEntry.DJCDAD094_PURCH_OPENQUAN = (newEntry.DJCDAD094_PURCH_OPENQUAN);
					newEntry.DJCDAD094_PURCH_OPENQUAN = (k.DJCDAD094_PURCH_OPENQUAN) + newEntry.DJCDAD094_PURCH_OPENQUAN;
					newEntry.DJCDAD094_SALESCONTR_CALLOFF = newEntry.DJCDAD094_SALESCONTR_CALLOFF === undefined ? 0 : newEntry.DJCDAD094_SALESCONTR_CALLOFF;
					newEntry.DJCDAD094_SALESCONTR_CALLOFF = (newEntry.DJCDAD094_SALESCONTR_CALLOFF);
					newEntry.DJCDAD094_SALESCONTR_CALLOFF = (k.DJCDAD094_SALESCONTR_CALLOFF) + newEntry.DJCDAD094_SALESCONTR_CALLOFF;
					newEntry.DJCDAD094_SALESCONTR_OPENQUAN = newEntry.DJCDAD094_SALESCONTR_OPENQUAN === undefined ? 0 : newEntry.DJCDAD094_SALESCONTR_OPENQUAN;
					newEntry.DJCDAD094_SALESCONTR_OPENQUAN = (newEntry.DJCDAD094_SALESCONTR_OPENQUAN);
					newEntry.DJCDAD094_SALESCONTR_OPENQUAN = (k.DJCDAD094_SALESCONTR_OPENQUAN) + newEntry.DJCDAD094_SALESCONTR_OPENQUAN;
					newEntry.DJCDAD094_NONCONTRAC = newEntry.DJCDAD094_NONCONTRAC === undefined ? 0 : newEntry.DJCDAD094_NONCONTRAC;
					newEntry.DJCDAD094_NONCONTRAC = (newEntry.DJCDAD094_NONCONTRAC);
					newEntry.DJCDAD094_NONCONTRAC = (k.DJCDAD094_NONCONTRAC) + newEntry.DJCDAD094_NONCONTRAC;
					newEntry.DJCDAD094_FORECAST_CONTRACT = newEntry.DJCDAD094_FORECAST_CONTRACT === undefined ? 0 : newEntry.DJCDAD094_FORECAST_CONTRACT;
					newEntry.DJCDAD094_FORECAST_CONTRACT = (newEntry.DJCDAD094_FORECAST_CONTRACT);
					newEntry.DJCDAD094_FORECAST_CONTRACT = (k.DJCDAD094_FORECAST_CONTRACT) + newEntry.DJCDAD094_FORECAST_CONTRACT;
					/*
						Descriptions	Tech. Name	Formula	Formula
						Total Stocks	TOTALSTOCKS	Stock Raw  Materials + Stock Semi  Finished +  Stock Finished  Products	STOCK_RAW_MAT + STOCK_SEMI_MAT + DJCDAD094_STOCK_FINISHED_PROD
									
						Total Open Purch. Contr.	TOTAL_OPENPURCH	(Purchase  Contracts  Call Offs + Orders) +  (Purchase Contracts Open Quantity)	DJCDAD094_PURCONTRACT_CALLOFF + PURCH_OPENQUAN
									
						Availability	AVAILABILITY	 Total  Stocks + Total Open Purch. Contr.	TOTALSTOCKS + TOTAL_OPENPURCH
									
						Long/Short 1	LONGSHORT	(Availability) -  (Sales  Contracts Call-Offs) -  (Sales  Contracts Open Quantity)	AVAILABILITY - DJCDAD094_SALESCONTR_CALLOFF - DJCDAD094_SALESCONTR_OPENQUAN
									
						Long/Short 2	LONGSHORT2	(Long/Short 1) - (Forecast non-Contract)	LONGSHORT - DJCDAD094_NONCONTRAC
									
						Long/Short 3	LONGSHORT3	(Long/Short 2) - (Forecast  Contract)	LONGSHORT2 - DJCDAD094_FORECAST_CONTRACT
									
						Total Demand	TOTALDEMAND	(Sales  Contracts Call-Offs) + ( Sales  Contracts Open Quantity) +  (Forecast non-Contract) + ( Forecast  Contract)	
						DJCDAD094_SALESCONTR_CALLOFF + DJCDAD094_SALESCONTR_OPENQUAN + DJCDAD094_NONCONTRAC + DJCDAD094_FORECAST_CONTRACT
							
						
						*/

					//formulas
					newEntry.DJCDAD094_TOTALSTOCKS = newEntry.DJCDAD094_TOTALSTOCKS === undefined ? 0 : newEntry.DJCDAD094_TOTALSTOCKS;
					newEntry.DJCDAD094_TOTALSTOCKS = (newEntry.DJCDAD094_TOTALSTOCKS);
					newEntry.DJCDAD094_TOTALSTOCKS = newEntry.DJCDAD094_TOTALSTOCKS + (k.DJCDAD094_STOCK_RAW_MAT) +
						(k.DJCDAD094_STOCK_SEMI_MAT) +
						(k.DJCDAD094_STOCK_FINISHED_PROD);
					newEntry.DJCDAD094_TOTAL_OPENPURCH = newEntry.DJCDAD094_TOTAL_OPENPURCH === undefined ? 0 : newEntry.DJCDAD094_TOTAL_OPENPURCH;
					newEntry.DJCDAD094_TOTAL_OPENPURCH = (newEntry.DJCDAD094_TOTAL_OPENPURCH);
					newEntry.DJCDAD094_TOTAL_OPENPURCH = newEntry.DJCDAD094_TOTAL_OPENPURCH + (k.DJCDAD094_PURCONTRACT_CALLOFF) +
						(k.DJCDAD094_PURCH_OPENQUAN);
					newEntry.DJCDAD094_AVAILABILITY = newEntry.DJCDAD094_AVAILABILITY === undefined ? 0 : newEntry.DJCDAD094_AVAILABILITY;
					newEntry.DJCDAD094_AVAILABILITY = (newEntry.DJCDAD094_AVAILABILITY);
					newEntry.DJCDAD094_AVAILABILITY = newEntry.DJCDAD094_AVAILABILITY + (k.DJCDAD094_TOTALSTOCKS) +
						(k.DJCDAD094_TOTAL_OPENPURCH);
					newEntry.DJCDAD094_LONGSHORT = newEntry.DJCDAD094_LONGSHORT === undefined ? 0 : newEntry.DJCDAD094_LONGSHORT;
					newEntry.DJCDAD094_LONGSHORT = (newEntry.DJCDAD094_LONGSHORT);
					newEntry.DJCDAD094_LONGSHORT = newEntry.DJCDAD094_LONGSHORT + ((k.DJCDAD094_AVAILABILITY) - (k
							.DJCDAD094_SALESCONTR_CALLOFF) -
						(k.DJCDAD094_SALESCONTR_OPENQUAN));
					newEntry.DJCDAD094_LONGSHORT2 = newEntry.DJCDAD094_LONGSHORT2 === undefined ? 0 : newEntry.DJCDAD094_LONGSHORT2;
					newEntry.DJCDAD094_LONGSHORT2 = (newEntry.DJCDAD094_LONGSHORT2);
					newEntry.DJCDAD094_LONGSHORT2 = newEntry.DJCDAD094_LONGSHORT2 + ((k.DJCDAD094_LONGSHORT) - (k.DJCDAD094_NONCONTRAC));
					newEntry.DJCDAD094_LONGSHORT3 = newEntry.DJCDAD094_LONGSHORT3 === undefined ? 0 : newEntry.DJCDAD094_LONGSHORT3;
					newEntry.DJCDAD094_LONGSHORT3 = (newEntry.DJCDAD094_LONGSHORT3);
					newEntry.DJCDAD094_LONGSHORT3 = newEntry.DJCDAD094_LONGSHORT3 + ((k.DJCDAD094_LONGSHORT2) - (k.DJCDAD094_FORECAST_CONTRACT));
					newEntry.DJCDAD094_TOTALDEMAND = newEntry.DJCDAD094_TOTALDEMAND === undefined ? 0 : newEntry.DJCDAD094_TOTALDEMAND;
					newEntry.DJCDAD094_TOTALDEMAND = (newEntry.DJCDAD094_TOTALDEMAND);
					newEntry.DJCDAD094_TOTALDEMAND = newEntry.DJCDAD094_TOTALDEMAND + (k.DJCDAD094_SALESCONTR_CALLOFF) + (k.DJCDAD094_SALESCONTR_OPENQUAN) +
						(k.DJCDAD094_NONCONTRAC) + (k.DJCDAD094_FORECAST_CONTRACT);

				});
				newEntry = that._hasComment(newEntry, 1);
				newCollectedData.push(newEntry);
			});

			var totalRow = this.setTotalRow(newCollectedData, "", this.columnfilter);
			newCollectedData.unshift(totalRow);
			newCollectedData.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this._tableModel.setProperty("/tableRows", newCollectedData);
		},

		collectData2: function () {
			var that = this;
			var newCollectedData2 = [];
			$.each(this.groupData2, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.COUNTRY = k.COUNTRY;
					newEntry.DSDREGION = k.DSDREGION;
					newEntry.DZZB2B_L2 = k.DZZB2B_L2;
					newEntry.DZZB2B_L2___T = k.DZZB2B_L2___T;
					newEntry.RMW11ZP16 = k.RMW11ZP16;
					newEntry.RMW11ZP16___T = k.RMW11ZP16___T;
					newEntry.MATERIAL = k.MATERIAL;
					newEntry.MATERIAL___T = k.MATERIAL___T;
					newEntry.RMW11ZE03 = k.RMW11ZE03;
					newEntry.RMW11ZE03___T = k.RMW11ZE03___T;
					newEntry.RMW21ZE03 = k.RMW21ZE03;
					newEntry.RMW21ZE03___T = k.RMW21ZE03___T;
					newEntry.RMW31ZE03 = k.RMW31ZE03;
					newEntry.RMW31ZE03___T = k.RMW31ZE03___T;

					newEntry.ITEM_CATEG = k.ITEM_CATEG;
					newEntry.ITEM_CATEG___T = k.ITEM_CATEG___T;

					newEntry.DOEMGL1 = k.DOEMGL1;
					newEntry.DOEMGL2 = k.DOEMGL2;
					newEntry.DOEMGL3 = k.DOEMGL3;
					newEntry.DOEMGL4 = k.DOEMGL4;
					newEntry.DOEMGL5 = k.DOEMGL5;
					newEntry.DOEMGL1___T = k.DOEMGL1___T;
					newEntry.DOEMGL2___T = k.DOEMGL2___T;
					newEntry.DOEMGL3___T = k.DOEMGL3___T;
					newEntry.DOEMGL4___T = k.DOEMGL4___T;
					newEntry.DOEMGL5___T = k.DOEMGL5___T;
					newEntry.CONTRACT = k.CONTRACT;
					newEntry.VENDOR = k.VENDOR;
					newEntry.VENDOR___T = k.VENDOR___T;
					newEntry.STOCKTYPE = k.STOCKTYPE;
					newEntry.STOCKTYPE___T = k.STOCKTYPE___T;
					newEntry.DEXMAT = k.DEXMAT;
					newEntry.DEXMAT___T = k.DEXMAT___T;
					newEntry.DEXPLANT = k.DEXPLANT;
					newEntry.DEXPLANT___T = k.DEXPLANT___T;
					newEntry.DSDREG = k.DSDREG;
					newEntry.DSDREG___T = k.DSDREG___T;
					newEntry.ZCOUNTRY = k.ZCOUNTRY;
					newEntry.ZCOUNTRY_F = k.ZCOUNTRY_F;
					newEntry.ZCOUNTRY___T = k.ZCOUNTRY___T;
					newEntry.ZSCOUNTRY = k.ZSCOUNTRY;
					newEntry.ZSCOUNTRY_F = k.ZSCOUNTRY_F;
					newEntry.ZSCOUNTRY___T = k.ZSCOUNTRY___T;

					newEntry.PLANT = k.PLANT;
					newEntry.PLANT___T = k.PLANT___T;
					newEntry.RMW21ZH02 = k.RMW21ZH02;
					newEntry.RMW21ZH02___T = k.RMW21ZH02___T;
					newEntry.DSUBCHAR = k.DSUBCHAR;
					newEntry.DSUBCHAR___T = k.DSUBCHAR___T;
					newEntry.DCOLLNR = k.DCOLLNR;
					newEntry.DCOLLNR___T = k.DCOLLNR___T;

					newEntry.DFRTSPC1 = k.DFRTSPC1;
					newEntry.DFRTSPC1___T = k.DFRTSPC1___T;
					newEntry.DFRTSPC2 = k.DFRTSPC2;
					newEntry.DFRTSPC2___T = k.DFRTSPC2___T;
					newEntry.DFRTSPC3 = k.DFRTSPC3;
					newEntry.DFRTSPC3___T = k.DFRTSPC3___T;

					newEntry.RMW11ZE03 = k.RMW11ZE03;
					newEntry.RMW11ZE03___T = k.RMW11ZE03___T;
					newEntry.RMW21ZE03 = k.RMW21ZE03;
					newEntry.RMW21ZE03___T = k.RMW21ZE03___T;
					newEntry.RMW31ZE03 = k.RMW31ZE03;
					newEntry.RMW31ZE03___T = k.RMW31ZE03___T;

					newEntry.DJCICFLAG = k.DJCICFLAG;
					newEntry.DJCICFLAG___T = k.DJCICFLAG___T;

					newEntry.DJCDAD091_LONGSHORTQUOTES = newEntry.DJCDAD091_LONGSHORTQUOTES === undefined ? 0 : newEntry.DJCDAD091_LONGSHORTQUOTES;
					newEntry.DJCDAD091_LONGSHORTQUOTES = (newEntry.DJCDAD091_LONGSHORTQUOTES);
					if (k.DJCDAD091_LONGSHORTQUOTES === undefined) k.DJCDAD091_LONGSHORTQUOTES = 0;
					newEntry.DJCDAD091_LONGSHORTQUOTES = (k.DJCDAD091_LONGSHORTQUOTES) + newEntry.DJCDAD091_LONGSHORTQUOTES;

					newEntry.DJCDAD091_LONGSHORTRFQ = newEntry.DJCDAD091_LONGSHORTRFQ === undefined ? 0 : newEntry.DJCDAD091_LONGSHORTRFQ;
					newEntry.DJCDAD091_LONGSHORTRFQ = (newEntry.DJCDAD091_LONGSHORTRFQ);
					if (k.DJCDAD091_LONGSHORTRFQ === undefined) k.DJCDAD091_LONGSHORTRFQ = 0;
					newEntry.DJCDAD091_LONGSHORTRFQ = (k.DJCDAD091_LONGSHORTRFQ) + newEntry.DJCDAD091_LONGSHORTRFQ;

					newEntry.DJCDAD091_OPENQUOTES = newEntry.DJCDAD091_OPENQUOTES === undefined ? 0 : newEntry.DJCDAD091_OPENQUOTES;
					newEntry.DJCDAD091_OPENQUOTES = (newEntry.DJCDAD091_OPENQUOTES);
					if (k.DJCDAD091_OPENQUOTES === undefined) k.DJCDAD091_OPENQUOTES = 0;
					newEntry.DJCDAD091_OPENQUOTES = (k.DJCDAD091_OPENQUOTES) + newEntry.DJCDAD091_OPENQUOTES;

					newEntry.DJCDAD091_OPENRFQS = newEntry.DJCDAD091_OPENRFQS === undefined ? 0 : newEntry.DJCDAD091_OPENRFQS;
					newEntry.DJCDAD091_OPENRFQS = (newEntry.DJCDAD091_OPENRFQS);
					if (k.DJCDAD091_OPENRFQS === undefined) k.DJCDAD091_OPENRFQS = 0;
					newEntry.DJCDAD091_OPENRFQS = (k.DJCDAD091_OPENRFQS) + newEntry.DJCDAD091_OPENRFQS;
					//Aggregations
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = newEntry.DJCDAD091_ACTUAL_CONSUMPTION === undefined ? 0 : newEntry.DJCDAD091_ACTUAL_CONSUMPTION;
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = (newEntry.DJCDAD091_ACTUAL_CONSUMPTION);
					if (k.DJCDAD091_ACTUAL_CONSUMPTION === undefined) k.DJCDAD091_ACTUAL_CONSUMPTION = 0;
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = (k.DJCDAD091_ACTUAL_CONSUMPTION) + newEntry.DJCDAD091_ACTUAL_CONSUMPTION;
					newEntry.DJCDAD091_CONSUMPTION = newEntry.DJCDAD091_CONSUMPTION === undefined ? 0 : newEntry.DJCDAD091_CONSUMPTION;
					newEntry.DJCDAD091_CONSUMPTION = (newEntry.DJCDAD091_CONSUMPTION);
					if (k.DJCDAD091_CONSUMPTION === undefined) k.DJCDAD091_CONSUMPTION = 0;
					newEntry.DJCDAD091_CONSUMPTION = (k.DJCDAD091_CONSUMPTION) + newEntry.DJCDAD091_CONSUMPTION;
					newEntry.DJCDAD091_STOCK_RAW_MAT = newEntry.DJCDAD091_STOCK_RAW_MAT === undefined ? 0 : newEntry.DJCDAD091_STOCK_RAW_MAT;
					newEntry.DJCDAD091_STOCK_RAW_MAT = (newEntry.DJCDAD091_STOCK_RAW_MAT);
					if (k.DJCDAD091_STOCK_RAW_MAT === undefined) k.DJCDAD091_STOCK_RAW_MAT = 0;
					newEntry.DJCDAD091_STOCK_RAW_MAT = (k.DJCDAD091_STOCK_RAW_MAT) + newEntry.DJCDAD091_STOCK_RAW_MAT;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = newEntry.DJCDAD091_STOCK_FINISHED_PROD === undefined ? 0 : newEntry.DJCDAD091_STOCK_FINISHED_PROD;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = (newEntry.DJCDAD091_STOCK_FINISHED_PROD);
					if (k.DJCDAD091_STOCK_FINISHED_PROD === undefined) k.DJCDAD091_STOCK_FINISHED_PROD = 0;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = (k.DJCDAD091_STOCK_FINISHED_PROD) + newEntry.DJCDAD091_STOCK_FINISHED_PROD;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = newEntry.DJCDAD091_STOCK_SEMI_MAT === undefined ? 0 : newEntry.DJCDAD091_STOCK_SEMI_MAT;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = (newEntry.DJCDAD091_STOCK_SEMI_MAT);
					if (k.DJCDAD091_STOCK_SEMI_MAT === undefined) k.DJCDAD091_STOCK_SEMI_MAT = 0;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = (k.DJCDAD091_STOCK_SEMI_MAT) + newEntry.DJCDAD091_STOCK_SEMI_MAT;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = newEntry.DJCDAD091_PURCONTRACT_CALLOFF === undefined ? 0 : newEntry.DJCDAD091_PURCONTRACT_CALLOFF;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = (newEntry.DJCDAD091_PURCONTRACT_CALLOFF);
					if (k.DJCDAD091_PURCONTRACT_CALLOFF === undefined) k.DJCDAD091_PURCONTRACT_CALLOFF = 0;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = (k.DJCDAD091_PURCONTRACT_CALLOFF) + newEntry.DJCDAD091_PURCONTRACT_CALLOFF;
					newEntry.DJCDAD091_PURCH_OPENQUAN = newEntry.DJCDAD091_PURCH_OPENQUAN === undefined ? 0 : newEntry.DJCDAD091_PURCH_OPENQUAN;
					newEntry.DJCDAD091_PURCH_OPENQUAN = (newEntry.DJCDAD091_PURCH_OPENQUAN);
					if (k.DJCDAD091_PURCH_OPENQUAN === undefined) k.DJCDAD091_PURCH_OPENQUAN = 0;
					newEntry.DJCDAD091_PURCH_OPENQUAN = (k.DJCDAD091_PURCH_OPENQUAN) + newEntry.DJCDAD091_PURCH_OPENQUAN;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = newEntry.DJCDAD091_SALESCONTR_CALLOFF === undefined ? 0 : newEntry.DJCDAD091_SALESCONTR_CALLOFF;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = (newEntry.DJCDAD091_SALESCONTR_CALLOFF);
					if (k.DJCDAD091_SALESCONTR_CALLOFF === undefined) k.DJCDAD091_SALESCONTR_CALLOFF = 0;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = (k.DJCDAD091_SALESCONTR_CALLOFF) + newEntry.DJCDAD091_SALESCONTR_CALLOFF;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = newEntry.DJCDAD091_SALESCONTR_OPENQUAN === undefined ? 0 : newEntry.DJCDAD091_SALESCONTR_OPENQUAN;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = (newEntry.DJCDAD091_SALESCONTR_OPENQUAN);
					if (k.DJCDAD091_SALESCONTR_OPENQUAN === undefined) k.DJCDAD091_SALESCONTR_OPENQUAN = 0;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = (k.DJCDAD091_SALESCONTR_OPENQUAN) + newEntry.DJCDAD091_SALESCONTR_OPENQUAN;
					newEntry.DJCDAD091_NONCONTRAC = newEntry.DJCDAD091_NONCONTRAC === undefined ? 0 : newEntry.DJCDAD091_NONCONTRAC;
					newEntry.DJCDAD091_NONCONTRAC = (newEntry.DJCDAD091_NONCONTRAC);
					if (k.DJCDAD091_NONCONTRAC === undefined) k.DJCDAD091_NONCONTRAC = 0;
					newEntry.DJCDAD091_NONCONTRAC = (k.DJCDAD091_NONCONTRAC) + newEntry.DJCDAD091_NONCONTRAC;
					newEntry.DJCDAD091_FORECAST_CONTRACT = newEntry.DJCDAD091_FORECAST_CONTRACT === undefined ? 0 : newEntry.DJCDAD091_FORECAST_CONTRACT;
					newEntry.DJCDAD091_FORECAST_CONTRACT = (newEntry.DJCDAD091_FORECAST_CONTRACT);
					if (k.DJCDAD091_FORECAST_CONTRACT === undefined) k.DJCDAD091_FORECAST_CONTRACT = 0;
					newEntry.DJCDAD091_FORECAST_CONTRACT = (k.DJCDAD091_FORECAST_CONTRACT) + newEntry.DJCDAD091_FORECAST_CONTRACT;

					//formulas
					newEntry.DJCDAD091_TOTALSTOCKS = newEntry.DJCDAD091_TOTALSTOCKS === undefined ? 0 : newEntry.DJCDAD091_TOTALSTOCKS;
					newEntry.DJCDAD091_TOTALSTOCKS = (newEntry.DJCDAD091_TOTALSTOCKS);

					newEntry.DJCDAD091_TOTALSTOCKS = newEntry.DJCDAD091_TOTALSTOCKS + (k.DJCDAD091_STOCK_RAW_MAT) +
						(k.DJCDAD091_STOCK_SEMI_MAT) +
						(k.DJCDAD091_STOCK_FINISHED_PROD);
					newEntry.DJCDAD091_TOTAL_OPENPURCH = newEntry.DJCDAD091_TOTAL_OPENPURCH === undefined ? 0 : newEntry.DJCDAD091_TOTAL_OPENPURCH;
					newEntry.DJCDAD091_TOTAL_OPENPURCH = (newEntry.DJCDAD091_TOTAL_OPENPURCH);
					newEntry.DJCDAD091_TOTAL_OPENPURCH = newEntry.DJCDAD091_TOTAL_OPENPURCH + (k.DJCDAD091_PURCONTRACT_CALLOFF) +
						(k.DJCDAD091_PURCH_OPENQUAN);
					newEntry.DJCDAD091_AVAILABILITY = newEntry.DJCDAD091_AVAILABILITY === undefined ? 0 : newEntry.DJCDAD091_AVAILABILITY;
					newEntry.DJCDAD091_AVAILABILITY = (newEntry.DJCDAD091_AVAILABILITY);
					newEntry.DJCDAD091_AVAILABILITY = newEntry.DJCDAD091_AVAILABILITY + (k.DJCDAD091_TOTALSTOCKS) +
						(k.DJCDAD091_TOTAL_OPENPURCH);
					newEntry.DJCDAD091_LONGSHORT = newEntry.DJCDAD091_LONGSHORT === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT;
					newEntry.DJCDAD091_LONGSHORT = (newEntry.DJCDAD091_LONGSHORT);
					newEntry.DJCDAD091_LONGSHORT = newEntry.DJCDAD091_LONGSHORT + ((k.DJCDAD091_AVAILABILITY) - (k
							.DJCDAD091_SALESCONTR_CALLOFF) -
						(k.DJCDAD091_SALESCONTR_OPENQUAN) - (k.DJCDAD091_OPENQUOTESWON));
					newEntry.DJCDAD091_LONGSHORT2 = newEntry.DJCDAD091_LONGSHORT2 === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT2;
					newEntry.DJCDAD091_LONGSHORT2 = (newEntry.DJCDAD091_LONGSHORT2);
					newEntry.DJCDAD091_LONGSHORT2 = newEntry.DJCDAD091_LONGSHORT2 + ((k.DJCDAD091_LONGSHORT) - (k.DJCDAD091_NONCONTRAC));
					newEntry.DJCDAD091_LONGSHORT3 = newEntry.DJCDAD091_LONGSHORT3 === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT3;
					newEntry.DJCDAD091_LONGSHORT3 = (newEntry.DJCDAD091_LONGSHORT3);
					newEntry.DJCDAD091_LONGSHORT3 = newEntry.DJCDAD091_LONGSHORT3 + ((k.DJCDAD091_LONGSHORT2) - (k.DJCDAD091_FORECAST_CONTRACT));
					newEntry.DJCDAD091_TOTALDEMAND = newEntry.DJCDAD091_TOTALDEMAND === undefined ? 0 : newEntry.DJCDAD091_TOTALDEMAND;
					newEntry.DJCDAD091_TOTALDEMAND = (newEntry.DJCDAD091_TOTALDEMAND);
					newEntry.DJCDAD091_TOTALDEMAND = newEntry.DJCDAD091_TOTALDEMAND + (k.DJCDAD091_SALESCONTR_CALLOFF) + (k.DJCDAD091_SALESCONTR_OPENQUAN) +
						(k.DJCDAD091_NONCONTRAC) + (k.DJCDAD091_FORECAST_CONTRACT);

				});
				newEntry = that._hasComment(newEntry, 2);
				newCollectedData2.push(newEntry);
			});
			var totalRow = this.setTotalRow(newCollectedData2, "", this.columnfilter2);
			newCollectedData2.unshift(totalRow);
			newCollectedData2.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this._tableModel.setProperty("/tableRows2", newCollectedData2);
		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			if (type === "added" || type === "removed" || type === "removedAll") {
				this.getView().byId("FS1").removeAllTokens();
				this.getView().byId("FS2").removeAllTokens();
				this.getView().byId("FS3").removeAllTokens();
			}
		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					that._findTableKeys();
					that._findTableKeys2();
				},
				function (error) { /* code if some error */ }
			);
		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);
		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

	});

});