sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	'sap/ui/core/Fragment',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, dbcontext, dbcontext2, excel, Fragment, JSONModel, MessageToast, MessageBox, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.SeasonalPlanToOffer", {
		onInit: function () {
			this._setButtonsPressed("seasonalPlanToOfferId");
			this._tableId = this.getView().byId("seasonalTableId");
			this._tableId1 = this.getView().byId("seasonalTableId1");
			this._tableIdText = "seasonalTableId";
			this._tableIdText1 = "seasonalTableId1";
			this.columnfilter = {};

			this.bDialog = new sap.m.BusyDialog();
			//panel Model
			this._panelModel = this.getOwnerComponent().getModel("seasonPanelM");
			this._globalModel = this.getOwnerComponent().getModel("globalM");
			this._globalModel.setProperty("/seasonText", "");
			this._seasonTableM = this.getOwnerComponent().getModel("seasonTableM");
			this._seasonTableM.setProperty("/tableRows", []);
			this._seasonTableM.setProperty("/tableRows1", []);
			this.getView().byId("seasonalTableId").setVisible(true);
			this.getView().byId("seasonalTableId1").setVisible(false);
			this.CreateGTPquoteTableM = this.getOwnerComponent().getModel("CreateGTPquoteTableM");
			//oDataModel
			this._seasonalODataModel = this.getOwnerComponent().getModel("SEASONAL_MODEL");
			sap.ui.getCore().setModel(this._panelModel, "seasonPanelM");
			this._initializePanelModel();

			var organic = [{
				"ORGANIC_TEXT": "no",
				"ORGANIC": "CUST-X.0000000001563"
			}, {
				"ORGANIC_TEXT": "yes",
				"ORGANIC": "CUST-X.0000000001562"
			}];
			this._globalModel.setProperty("/ORGANIC", organic);

			var halal = [{
				"HALAL": "CUST-X.0000000001563",
				"HALAL_TEXT": "no"
			}, {
				"HALAL": "CUST-X.0000000001562",
				"HALAL_TEXT": "yes"
			}];
			this._globalModel.setProperty("/HALAL", halal);

			var kosher = [{
				"KOSHER": "CUST-X.0000000001563",
				"KOSHER_TEXT": "no"
			}, {
				"KOSHER": "CUST-X.0000000001562",
				"KOSHER_TEXT": "yes"
			}];
			this._globalModel.setProperty("/KOSHER", kosher);

			// ***Season search help***
			// set explored app's demo model on this sample
			var oModel = new JSONModel(sap.ui.require.toUrl("sap/ui/demo/mock/products.json"));
			this.getView().setModel(oModel);
			sap.ui.getCore().getModel("globalM").setProperty("/isSeasonalPlanToOffer", false);
			this.getRouter().getRoute("SeasonalPlanToOffer").attachPatternMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("seasonalPlanToOfferId");
			if (this._globalModel.getProperty("/seasonText") === undefined)
				this._globalModel.setProperty("/seasonText", "");
			this._tableId.setVisible(false);
			this._tableId.setVisible(true);
			if (sap.ui.getCore().getModel("globalM").getProperty("/isSeasonalPlanToOffer") === true)
				this.createToken();

		},

		onSortSTO: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._seasonTableM.getProperty("/tableRows");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this.getView().byId("seasonalTableId").getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);

			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._seasonTableM);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("seasonalTableId").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._seasonTableM.setProperty("/tableRows", notSumRow);
			// 	that.getView().byId("seasonalTableId").bindRows("seasonTableM>/tableRows");

			// }, 100);
		},

		onSortSTO1: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._seasonTableM.getProperty("/tableRows1");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this.getView().byId("seasonalTableId").getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);

			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._seasonTableM);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("seasonalTableId").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._seasonTableM.setProperty("/tableRows", notSumRow);
			// 	that.getView().byId("seasonalTableId").bindRows("seasonTableM>/tableRows");

			// }, 100);
		},
		onFilterSTO: function (oEvent) {
			this.onFilter(oEvent);
			var data = this._seasonTableM.getProperty("/tableRows");
				var dataIndices = [];
			if (oEvent.getParameter("value") !== "") {
				// this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				// this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}

			var aIndices = oEvent.getSource().getBindingInfo("rows").binding.aIndices;
			
			aIndices.forEach(function (ind) {
				     if ( data[ind].isSum !== "X" )
					dataIndices.push(data[ind]);
				});

			var notSumRow = _.filter(_.cloneDeep(data), function (item) {
				return item.isSum !== "X";
			});

			this._getTotalRow(notSumRow,dataIndices);

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		onFilterSTO1: function (oEvent) {
			this.onFilter(oEvent);
			var data = this._seasonTableM.getProperty("/tableRows1");
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}

			var notSumRow = _.filter(_.cloneDeep(data), function (item) {
				return item.isSum !== "X";
			});

			this._getTotalRow1(notSumRow);

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		onValueHelpInit: function (oEvent) {
			if (oEvent.getSource().getRelFieldModel() === "CreateGTPquoteTableM") {
				var pressed = sap.ui.getCore().getModel("globalM").getData()["pressTableValueControl"];
				var createGTPquoteTableModel = this.CreateGTPquoteTableM;
				if (pressed !== undefined) {
					if (this._checkValidationOfGTPQuote(oEvent.getSource().getFieldName(), pressed.getBindingContext("CreateGTPquoteTableM").getPath(),
							createGTPquoteTableModel)) {
						oEvent.getSource().setController(this);
						oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
					}
				}

			} else {
				oEvent.getSource().setController(this);
				oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
			}

		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/SALESORG", "");
			this._panelModel.setProperty("/AREA", "");
			this._panelModel.setProperty("/COUNTRY", "");
			this._panelModel.setProperty("/SEASON", "");
			this._panelModel.setProperty("/CUSTOMERL1", "");
			this._panelModel.setProperty("/CUSTOMERL2", "");
			this._panelModel.setProperty("/SFULLYEAR", "");
			this._panelModel.setProperty("/EFULLYEAR", "");
			this._panelModel.setProperty("/FS1", "");
			this._panelModel.setProperty("/FS2", "");
			this._panelModel.setProperty("/FS3", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
		},

		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
				if (controls[i].getMetadata().getName() === "sap.m.Input") {
					controls[i].setValue("");
				}
			}
		},

		onGo: function () {
			var that = this;
			var isSaved = that._globalModel.getProperty("/isSaved");
			var seasonInput = this._panelModel.getData()["SEASON"];

			var seasonCheckMsg = this._globalModel.getProperty("/seasonCheckMsg");
			if (seasonCheckMsg !== undefined && seasonCheckMsg !== "") {
				MessageBox.warning(seasonCheckMsg);
				return;
			}
			if (seasonInput === "") {
				MessageBox.warning("Season can not be empty!");
				return;
			}
			var SFULLYEAR = this._panelModel.getData().SFULLYEAR;
			var EFULLYEAR = this._panelModel.getData().EFULLYEAR;
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				if (seasonInput.split("[").length > 1) {
					if (seasonInput.split("[")[1].split(" - ").length > 1) {
						SFULLYEAR = seasonInput.split("[")[1].split(" - ")[0];
						var sFormmat = SFULLYEAR.split(".")[2] + SFULLYEAR.split(".")[1] + SFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
						EFULLYEAR = seasonInput.split("[")[1].split(" - ")[1];
						var eFormmat = EFULLYEAR.split(".")[2] + EFULLYEAR.split(".")[1] + EFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormmat);
					}
				}
			}
			var dateFrom = SFULLYEAR.split(".")[1] + "." + SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[2];
			dateFrom = new Date(dateFrom);
			var yearFrom = dateFrom.getFullYear() - 1;
			var prevDateFrom = SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[1] + "." + yearFrom.toString();
			var dateTo = new Date(dateFrom - 24 * 60 * 60 * 1000);
			var prevDateTo = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(new Date(dateTo));
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				prevDateFrom = "";
				prevDateTo = "";
			}

			this._panelModel.setProperty("/PREV_FROM", prevDateFrom);
			this._panelModel.setProperty("/PREV_TO", prevDateTo);
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_LIST"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			dbcontext.callServer(params, function (oModel) {
				that._globalModel.setProperty("/isSaved", "");
				var data = oModel.getData().RESULTS;
				data.forEach(function (m) {
					m["A0CUSTOMER__DOEBKUNDESEARCH_T"] = m["A0CUSTOMER__DOEBKUNDE"] + " " + m["A0CUSTOMER__DOEBKUNDE_T"];
					m["A0CUSTOMER__SEARCH_T"] = m["A0CUSTOMER"] + " " + m["A0CUSTOMER_T"];
					m["QUOTED_CROSS_PRICE"] = 0;
					if (m["OPEN_QUOTES"] !== 0) m["QUOTED_CROSS_PRICE"] = m["SALES_GROSS_EUR"] / m["OPEN_QUOTES"];

					if (m["PLAN_TO_OFFER"] === 0) {
						m["IS_EDITABLE"] = false;
					} else {
						m["IS_EDITABLE"] = true;
					}

					m["FRUIT_EDITABLE"] = false;
					m["PRODUCT_TYPE_EDITABLE"] = false;
					m["ORGANIC_EDITABLE"] = false;
					m["HALAL_EDITABLE"] = false;
					m["KOSHER_EDITABLE"] = false;
					m["FS1_EDITABLE"] = false;
					m["FS2_EDITABLE"] = false;
					m["FS3_EDITABLE"] = false;
					m["A0CUSTOMER__DSDREGION_EDITABLE"] = false;
					m["A0CUSTOMER__DSDAREA_EDITABLE"] = false;
					m["A0CUSTOMER__0COUNTRY_EDITABLE"] = false;
					m["A0SALESORG_EDITABLE"] = false;
					m["ADOEWERK_EDITABLE"] = false;
					m["A0CUSTOMER_EDITABLE"] = false;

					m["POTENTIAL"] = 0;
					m["ARMW11ZE03_T"] = that.getDetermineOrganic(m["ARMW11ZE03"]);
					m["ARMW21ZE03_T"] = that.getDetermineHalal(m["ARMW21ZE03"]);
					m["ARMW31ZE03_T"] = that.getDetermineKosher(m["ARMW31ZE03"]);
					m["ADFRTSPC1"] = m["ADFRTSPC1"] === "#" ? "" : m["ADFRTSPC1"];
					m["ADFRTSPC2"] = m["ADFRTSPC2"] === "#" ? "" : m["ADFRTSPC2"];
					m["ADFRTSPC3"] = m["ADFRTSPC3"] === "#" ? "" : m["ADFRTSPC3"];

					m["ADOEWERK"] = m["ADOEWERK"] === "#" ? "" : m["ADOEWERK"];
					m["A0SALESORG"] = m["A0SALESORG"] === "#" ? "" : m["A0SALESORG"];
					if (m["ADFRTSPC1_T"] === "")
						m.ADFRTSPC1_T = that.getDetermineFSText(m.ADFRTSPC1);
					if (m["ADFRTSPC2_T"] === "")
						m.ADFRTSPC2_T = that.getDetermineFSText(m.ADFRTSPC2);
					if (m["ADFRTSPC3_T"] === "")
						m.ADFRTSPC3_T = that.getDetermineFSText(m.ADFRTSPC3);

					m["isSum"] = "";
				});

				if (that._tableId.getVisible()) {
					that._getTotalRow(data);
				} else {
					that._getTotalRow1(data);
				}
			}, that);

		},

		_getTotalRow: function (data,dataIndices) {
			var groupedKeys = ["A0CUSTOMER", "ARMW21ZH02", "ARMW11ZP16", "ADFRTSPC1", "ADFRTSPC2", "ADFRTSPC3", "ARMW11ZE03", "ARMW21ZE03",
				"ARMW31ZE03"
			];
			var totalRow = this.setTotalRow(dataIndices === undefined ? data : dataIndices, "SPT", this.columnfilter);
			data.unshift(totalRow);
			data.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						if (key === "QUOTED_CROSS_PRICE" || key === "SALES_GROSS_EUR")
							row[key] = parseFloat(row[key].toFixed(2));
						else
							row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this._seasonTableM.setProperty("/tableRows", data);
			this._findTableKeys();
		},

		_getTotalRow1: function (data) {
			// var totalRow = this.setTotalRow(data, "SPT", this.columnfilter);
			// data.unshift(totalRow);
			// data.forEach(function (row) {
			// 	Object.keys(row).forEach(function (key) {
			// 		if (typeof row[key] === "number") {
			// 			if (key === "QUOTED_CROSS_PRICE" || key === "SALES_GROSS_EUR")
			// 				row[key] = parseFloat(row[key].toFixed(2));
			// 			else
			// 				row[key] = parseFloat(row[key].toFixed(0));
			// 		}
			// 	});

			// });
			// this._seasonTableM.setProperty("/tableRows1", data);
			this.initallData = data;
			this._findTableKeys1();
		},

		_findTableKeys: function () {
			var thiz = this;
			var oTable = this._tableId;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});

			var data = this._seasonTableM.getProperty("/tableRows");

			data.forEach(function (row) {
				row = thiz._hasComment(row, 1);
			});

			this._seasonTableM.setProperty("/tableRows", data);

		},

		_findTableKeys1: function () {
			var thiz = this;
			var oTable = this._tableId1;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});

			this._seasonTableM.getProperty("/tableRows1").forEach(function (row) {
				row = thiz._hasComment(row, 1);
			});
			if (thiz.mainTableKeys.length)
				thiz.onPressAggregation();
		},

		onPressAggregation: function () {
			this.convertGroup(this.mainTableKeys);
			this.collectData();
		},

		convertGroup: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData, groupCols);
			this.groupData = groupData;
		},

		collectData: function () {
			var that = this;
			var newCollectedData = [];
			var totalEntry = {};
			// var plantVisible = this.getView().byId("colGTP_AD_2").getVisible();

			$.each(this.groupData, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.ARMW21ZH02 = k.ARMW21ZH02;
					newEntry.ARMW21ZH02_T = k.ARMW21ZH02_T;
					newEntry.ARMW11ZP16 = k.ARMW11ZP16;
					newEntry.ARMW11ZP16_T = k.ARMW11ZP16_T;
					newEntry.A0CUSTOMER__DSDREGION = k.A0CUSTOMER__DSDREGION;
					newEntry.A0CUSTOMER__DSDREGION_T = k.A0CUSTOMER__DSDREGION_T;
					newEntry.A0CUSTOMER__DSDAREA = k.A0CUSTOMER__DSDAREA;
					newEntry.A0CUSTOMER__DSDAREA_T = k.A0CUSTOMER__DSDAREA_T;
					newEntry.A0CUSTOMER__0COUNTRY = k.A0CUSTOMER__0COUNTRY;
					newEntry.A0CUSTOMER__0COUNTRY_T = k.A0CUSTOMER__0COUNTRY_T;
					newEntry.A0SALESORG = k.A0SALESORG;
					newEntry.A0SALESORG_T = k.A0SALESORG_T;
					newEntry.ADOEWERK = k.ADOEWERK;
					newEntry.ADOEWERK_T = k.ADOEWERK_T;
					newEntry.A0CUSTOMER__DOEBKUNDE = k.A0CUSTOMER__DOEBKUNDE;
					newEntry.A0CUSTOMER__DOEBKUNDE_T = k.A0CUSTOMER__DOEBKUNDE_T;
					newEntry.A0CUSTOMER = k.A0CUSTOMER;
					newEntry.A0CUSTOMER_T = k.A0CUSTOMER_T;
					newEntry.ADFRTSPC1 = k.ADFRTSPC1;
					newEntry.ADFRTSPC1_T = k.ADFRTSPC1_T;
					newEntry.ADFRTSPC2 = k.ADFRTSPC2;
					newEntry.ADFRTSPC2_T = k.ADFRTSPC2_T;
					newEntry.ADFRTSPC3 = k.ADFRTSPC3;
					newEntry.ADFRTSPC3_T = k.ADFRTSPC3_T;
					newEntry.ARMW11ZE03 = k.ARMW11ZE03;
					newEntry.ARMW11ZE03_T = k.ARMW11ZE03_T;
					newEntry.ARMW21ZE03 = k.ARMW21ZE03;
					newEntry.ARMW21ZE03_T = k.ARMW21ZE03_T;
					newEntry.ARMW31ZE03 = k.ARMW31ZE03;
					newEntry.ARMW31ZE03_T = k.ARMW31ZE03_T;

					newEntry.CON_QUAN = newEntry.CON_QUAN === undefined ? 0 : newEntry.CON_QUAN;
					if (k.CON_QUAN === undefined) k.CON_QUAN = 0;
					newEntry.CON_QUAN = k.CON_QUAN + newEntry.CON_QUAN;

					newEntry.OPEN_QUAN = newEntry.OPEN_QUAN === undefined ? 0 : newEntry.OPEN_QUAN;
					if (k.OPEN_QUAN === undefined) k.OPEN_QUAN = 0;
					newEntry.OPEN_QUAN = k.OPEN_QUAN + newEntry.OPEN_QUAN;

					newEntry.POTENTIAL = newEntry.POTENTIAL === undefined ? 0 : newEntry.POTENTIAL;
					if (k.POTENTIAL === undefined) k.POTENTIAL = 0;
					newEntry.POTENTIAL = k.POTENTIAL + newEntry.POTENTIAL;

					newEntry.FY_BUD = newEntry.FY_BUD === undefined ? 0 : newEntry.FY_BUD;
					if (k.FY_BUD === undefined) k.FY_BUD = 0;
					newEntry.FY_BUD = k.FY_BUD + newEntry.FY_BUD;

					newEntry.NON_CON_SALES_VOL = newEntry.NON_CON_SALES_VOL === undefined ? 0 : newEntry.NON_CON_SALES_VOL;
					if (k.NON_CON_SALES_VOL === undefined) k.NON_CON_SALES_VOL = 0;
					newEntry.NON_CON_SALES_VOL = k.NON_CON_SALES_VOL + newEntry.NON_CON_SALES_VOL;

					newEntry.NON_CON_SALES = newEntry.NON_CON_SALES === undefined ? 0 : newEntry.NON_CON_SALES;
					if (k.NON_CON_SALES === undefined) k.NON_CON_SALES = 0;
					newEntry.NON_CON_SALES = k.NON_CON_SALES + newEntry.NON_CON_SALES;

					newEntry.PLAN_TO_OFFER = newEntry.PLAN_TO_OFFER === undefined ? 0 : newEntry.PLAN_TO_OFFER;
					if (k.PLAN_TO_OFFER === undefined) k.PLAN_TO_OFFER = 0;
					newEntry.PLAN_TO_OFFER = k.PLAN_TO_OFFER + newEntry.PLAN_TO_OFFER;

					newEntry.OPEN_QUOTES = newEntry.OPEN_QUOTES === undefined ? 0 : newEntry.OPEN_QUOTES;
					if (k.OPEN_QUOTES === undefined) k.OPEN_QUOTES = 0;
					newEntry.OPEN_QUOTES = k.OPEN_QUOTES + newEntry.OPEN_QUOTES;

					newEntry.WON_QUOTES = newEntry.WON_QUOTES === undefined ? 0 : newEntry.WON_QUOTES;
					if (k.WON_QUOTES === undefined) k.WON_QUOTES = 0;
					newEntry.WON_QUOTES = k.WON_QUOTES + newEntry.WON_QUOTES;

					newEntry.SALES_GROSS_EUR = newEntry.SALES_GROSS_EUR === undefined ? 0 : newEntry.SALES_GROSS_EUR;
					if (k.SALES_GROSS_EUR === undefined) k.SALES_GROSS_EUR = 0;
					newEntry.SALES_GROSS_EUR = k.SALES_GROSS_EUR + newEntry.SALES_GROSS_EUR;

					newEntry.CUR_CON_QUAN = newEntry.CUR_CON_QUAN === undefined ? 0 : newEntry.CUR_CON_QUAN;
					if (k.CUR_CON_QUAN === undefined) k.CUR_CON_QUAN = 0;
					newEntry.CUR_CON_QUAN = k.CUR_CON_QUAN + newEntry.CUR_CON_QUAN;

					newEntry.QUOTE_HIST = newEntry.QUOTE_HIST === undefined ? 0 : newEntry.QUOTE_HIST;
					if (k.QUOTE_HIST === undefined) k.QUOTE_HIST = 0;
					newEntry.QUOTE_HIST = k.QUOTE_HIST + newEntry.QUOTE_HIST;

					newEntry.LAST_OPEN_CONTR = newEntry.LAST_OPEN_CONTR === undefined ? 0 : newEntry.LAST_OPEN_CONTR;
					if (k.LAST_OPEN_CONTR === undefined) k.LAST_OPEN_CONTR = 0;
					newEntry.LAST_OPEN_CONTR = k.LAST_OPEN_CONTR + newEntry.LAST_OPEN_CONTR;

					newEntry.POTENTIAL_BU = newEntry.POTENTIAL_BU === undefined ? 0 : newEntry.POTENTIAL_BU;
					if (k.POTENTIAL_BU === undefined) k.POTENTIAL_BU = 0;
					newEntry.POTENTIAL_BU = k.POTENTIAL_BU + newEntry.POTENTIAL_BU;

					newEntry.QUOTED_CROSS_PRICE = newEntry.QUOTED_CROSS_PRICE === undefined ? 0 : newEntry.QUOTED_CROSS_PRICE;
					if (k.QUOTED_CROSS_PRICE === undefined) k.QUOTED_CROSS_PRICE = 0;
					newEntry.QUOTED_CROSS_PRICE = k.QUOTED_CROSS_PRICE + newEntry.QUOTED_CROSS_PRICE;

				});
				newEntry = that._hasComment(newEntry, 1);
				newCollectedData.push(newEntry);

			});

			var totalRow = this.setTotalRow(newCollectedData, "SPT", this.columnfilter);

			newCollectedData.unshift(totalRow);
			newCollectedData.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						if (key === "QUOTED_CROSS_PRICE" || key === "SALES_GROSS_EUR")
							row[key] = parseFloat(row[key].toFixed(2));
						else
							row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});

			this._seasonTableM.setProperty("/tableRows1", newCollectedData);
		},

		onExport: function () {
			var data = [];

			var tableData = this._seasonTableM.getData();
			if (this._tableId.getVisible()) {
				var copiedData = tableData.tableRows;
				var aIndices = this._tableId.getBinding("rows").aIndices;
			} else {
				var copiedData = tableData.tableRows1;
				var aIndices = this._tableId1.getBinding("rows").aIndices;

			}

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}
			excel._downloadExcel("SPO", this._tableId, data);
		},

		onChangePlanToOffer: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("seasonTableM").getPath();
			this._seasonTableM.setProperty(path + "/CHANGED", true);
		},

		_checkIsSame: function (row) {
			var that = this;
			var tableData = this._seasonTableM.getData();
			var rows = tableData.tableRows;
			var row;
			var sameResult = [];
			var addedRows = rows.filter(function (m) {
				return m.isSave === false;
			});
			var isReady = true;
			var msg = "";

			if (addedRows.length > 0) {

				var emptyRows = addedRows.filter(function (m) {
					return m.A0CUSTOMER === "" && m.A0CUSTOMER__DSDREGION === "" && m.A0CUSTOMER__DSDAREA ===
						"" && m.A0CUSTOMER__0COUNTRY === "" && m.ADOEWERK === "" && m.ARMW11ZP16 === "" && m.ADFRTSPC1 === "" && m.ADFRTSPC2 ===
						"" &&
						m.ADFRTSPC3 === "" && m.ARMW11ZE03 === "" && m.ARMW21ZE03 === "" && m.ARMW31ZE03 === "";
				});
				if (emptyRows.length) {
					isReady = false;
					msg = "Please check rows!. Some of them empty!";

				} else {

					for (var a = 0; a < addedRows.length; a++) {
						sameResult = [];
						row = addedRows[a];
						row["ARMW11ZE03_T"] = that.getDetermineOrganic(row["ARMW11ZE03"]);
						row["ARMW21ZE03_T"] = that.getDetermineHalal(row["ARMW21ZE03"]);
						row["ARMW31ZE03_T"] = that.getDetermineKosher(row["ARMW31ZE03"]);
						sameResult = rows.filter(function (m) {
							return m.A0CUSTOMER.replace(/\s/g, '') === row.A0CUSTOMER.replace(/\s/g, '') && m.A0CUSTOMER__DSDREGION.replace(/\s/g,
									'') ===
								row.A0CUSTOMER__DSDREGION.replace(/\s/g, '') && m.A0CUSTOMER__DSDAREA.replace(/\s/g, '') ===
								row.A0CUSTOMER__DSDAREA.replace(/\s/g, '') &&
								m.A0CUSTOMER__0COUNTRY.replace(/\s/g, '') ===
								row.A0CUSTOMER__0COUNTRY.replace(/\s/g, '') && m.ADOEWERK.replace(/\s/g, '') === row.ADOEWERK.replace(/\s/g, '') &&
								row.ARMW11ZP16
								.replace(/\s/g, '') === m.ARMW11ZP16.replace(/\s/g, '') && row.ADFRTSPC1.replace(/\s/g, '') === m.ADFRTSPC1.replace(
									/\s/g,
									'') &&
								row.ADFRTSPC2 === m.ADFRTSPC2 && row.ADFRTSPC3 === m.ADFRTSPC3 && row.ARMW11ZE03_T === m.ARMW11ZE03_T && row.ARMW21ZE03_T ===
								m.ARMW21ZE03_T &&
								row.ARMW31ZE03_T === m.ARMW31ZE03_T;
						});
						if (sameResult.length > 1) {
							isReady = false;

							if (msg !== "") {
								msg = msg + "\n" + "Customer:" + sameResult[0].A0CUSTOMER + ", Region:" + sameResult[0].A0CUSTOMER__DSDREGION + ",Area:" +
									sameResult[
										0].A0CUSTOMER__DSDAREA + ",Country:" + sameResult[0].A0CUSTOMER__0COUNTRY + ",Plant:" + sameResult[0].ADOEWERK +
									",Product type:" + sameResult[0].ARMW11ZP16 + ",Fruit Specific 1:" + sameResult[0].ADFRTSPC1 + ",Fruit Specific 2:" +
									sameResult[0].ADFRTSPC2 + ",Fruit Specific 3:" + sameResult[0].ADFRTSPC3 +
									",Organic:" + sameResult[0].ARMW11ZE03_T +
									",Halal:" + sameResult[0].ARMW21ZE03_T +
									",Kosher:" + sameResult[0].ARMW31ZE03_T +
									" has already exist in table";
							} else {
								msg = "Customer:" + sameResult[0].A0CUSTOMER + ", Region:" + sameResult[0].A0CUSTOMER__DSDREGION + ",Area:" +
									sameResult[
										0].A0CUSTOMER__DSDAREA + ",Country:" + sameResult[0].A0CUSTOMER__0COUNTRY + ",Plant:" + sameResult[0].ADOEWERK +
									",Product type:" + sameResult[0].ARMW11ZP16 + ",Fruit Specific 1:" + sameResult[0].ADFRTSPC1 + ",Fruit Specific 2:" +
									sameResult[0].ADFRTSPC2 + ",Fruit Specific 3:" + sameResult[0].ADFRTSPC3 +
									",Organic:" + sameResult[0].ARMW11ZE03_T +
									",Halal:" + sameResult[0].ARMW21ZE03_T +
									",Kosher:" + sameResult[0].ARMW31ZE03_T +
									" has already exist in table";
							}
						}

					}
				}

			}
			if (msg !== "") {
				MessageBox.warning(msg);
			}
			return isReady;
		},

		onSave: function (oEvent) {
			var that = this,
				tableData = this._seasonTableM.getData(),
				dataToSave = tableData.tableRows.filter(function (m) {
					return m.CHANGED === true;
				});

			if (this._checkIsSame() === false) {
				return;
			}

			var sYear = this._panelModel.getProperty("/SFULLYEAR_FORMAT");
			var eYear = this._panelModel.getProperty("/EFULLYEAR_FORMAT");

			if (!dataToSave.length) {
				MessageBox.warning("There is no change!");
				return;
			}

			dataToSave.forEach(function (m) {
				//adding row after selection
				if (m["ARMW21ZH02"] === "" && m["ARMW21ZH02_T"] !== "")
					m["ARMW21ZH02"] = m["ARMW21ZH02_T"];
				m.A0PLANT = m.ADOEWERK;
				if (m.A0CUSTOMER === "")
					m.A0CUSTOMER = m.A0CUSTOMER__DOEBKUNDE;

				if (m["ARMW11ZE03"] === "") {
					m["ARMW11ZE03"] = "CUST-X.0000000001563";
				}
				if (m["ARMW21ZE03"] === "") {
					m["ARMW21ZE03"] = "CUST-X.0000000001563";
				}
				if (m["ARMW31ZE03"] === "") {
					m["ARMW31ZE03"] = "CUST-X.0000000001563";
				}
			});

			var isEmpty = dataToSave.filter(function (m) {
				return m["A0CUSTOMER"] === "" || m["COUNTRY"] ===
					"" ||
					m["DSDREGION"] === "" || m["DSDAREA"] === "";
			});
			if (isEmpty.length > 0) {
				MessageBox.warning("Please fill all key fields");
				return;
			}

			//for Fruit Specific checks
			var msg = this._checkFruitSpecificsIsReady(dataToSave, "ADFRTSPC1", "ADFRTSPC2", "ADFRTSPC3");
			if (msg !== "") {
				MessageBox.warning(msg);
				return;
			}

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_SEASON_LIST"
				},
				"INPUTPARAMS": [{
					"SYEAR": sYear,
					"EYEAR": eYear,
					"SEASON_DATA": dataToSave
				}]
			};

			that._globalModel.setProperty("/isSaved", "X");

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						var changed = tableData.tableRows.filter(function (m) {
							return m.CHANGED === true;
						});
						changed.forEach(function (m) {
							m.isSave = true;
						});
						that.onGo();
					}
				});
			}, that);

		},

		onAdd: function () {
			var that = this;

			var data = this._seasonTableM.getProperty("/tableRows");
			var fruitFilterValue = this._panelModel.getProperty("/FRUIT");
			var productTypeFilterValue = this._panelModel.getProperty("/PRODUCT_TYPE");
			var plantFilterValue = this._panelModel.getProperty("/PLANT");
			var customerFilterValue = this._panelModel.getProperty("/CUSTOMERL2");
			if (customerFilterValue.indexOf(",") !== -1) customerFilterValue = "";
			var fruitPanelText = "";
			var index = 0;
			var isSelected = false;
			var selectedRows = [];
			fruitFilterValue = fruitFilterValue === undefined ? "" : fruitFilterValue;
			if (this.getView().byId("fruitId").getTokens().length) {
				try {
					fruitPanelText = this.getView().byId("fruitId").getTokens()[0].getText();
				} catch (err) {}

			}
			if (data === undefined) data = [];

			if (plantFilterValue.indexOf(",") !== -1) plantFilterValue = "";

			var table = this._tableId,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				aIndices = table.getBinding().aIndices;

			if (aSelIndex.length > 0) {
				isSelected = true;
				aSelIndex.forEach(function (selIndex) {
					selectedRows.push(data[aIndices[selIndex]]);
				});

			} else {

				var emptyRow = {
					isSave: false,
					lastRow: true,
					PLAN_TO_OFFER: 0,
					POTENTIAL_BU: 0,
					ARMW21ZH02: fruitFilterValue,
					ARMW21ZH02_T: fruitPanelText,
					FRUIT_EDITABLE: fruitFilterValue === "",
					ARMW11ZP16: productTypeFilterValue,
					PRODUCT_TYPE_EDITABLE: productTypeFilterValue === "",
					ARMW11ZE03: "CUST-X.0000000001563",
					ARMW21ZE03: "CUST-X.0000000001563",
					ARMW31ZE03: "CUST-X.0000000001563",
					ARMW11ZE03_T: that.getDetermineOrganic("CUST-X.0000000001563"),
					ARMW21ZE03_T: that.getDetermineHalal("CUST-X.0000000001563"),
					ARMW31ZE03_T: that.getDetermineKosher("CUST-X.0000000001563"),
					ADFRTSPC1: "",
					ADFRTSPC2: "",
					ADFRTSPC3: "",
					ADOEWERK: plantFilterValue,
					PLANT_EDITABLE: plantFilterValue === "",
					A0CUSTOMER: customerFilterValue,
					A0CUSTOMER_EDITABLE: customerFilterValue === "",
					A0CUSTOMER__DSDREGION: "",
					A0CUSTOMER__DSDREGION_EDITABLE: customerFilterValue === "",
					A0CUSTOMER__DSDAREA: "",
					A0CUSTOMER__DSDAREA_EDITABLE: customerFilterValue === "",
					A0CUSTOMER__0COUNTRY: "",
					A0CUSTOMER__0COUNTRY_EDITABLE: customerFilterValue === "",
					IS_EDITABLE: true,
					FRUIT_SPCF_1_ITEMS: [],
					FRUIT_SPCF_2_ITEMS: [],
					FRUIT_SPCF_3_ITEMS: [],
					ORGANIC_EDITABLE: true,
					HALAL_EDITABLE: true,
					KOSHER_EDITABLE: true,
					FS1_EDITABLE: true,
					FS2_EDITABLE: true,
					FS3_EDITABLE: true,
					A0SALESORG_EDITABLE: true,
					ADOEWERK_EDITABLE: true,
				}

				data.unshift(emptyRow);

			}

			var counter = 0;
			selectedRows.forEach(function (selectedRow) {

				emptyRow = {
					isSave: false,
					lastRow: true,
					PLAN_TO_OFFER: 0,
					POTENTIAL_BU: 0,
					ARMW21ZH02: isSelected ? selectedRow["ARMW21ZH02"] === "#" ? "" : selectedRow["ARMW21ZH02"] : fruitFilterKey,
					ARMW21ZH02_T: isSelected ? selectedRow["ARMW21ZH02_T"] === "#" ? "" : selectedRow["ARMW21ZH02_T"] : fruitFilterValue,
					FRUIT_EDITABLE: fruitFilterValue === "",
					ARMW11ZP16: isSelected ? selectedRow["ARMW11ZP16"] === "#" ? "" : selectedRow["ARMW11ZP16"] : productTypeFilterValue,
					PRODUCT_TYPE_EDITABLE: productTypeFilterValue === "",
					ARMW11ZE03: isSelected ? selectedRow["ARMW11ZE03"] === "#" ? "" : selectedRow["ARMW11ZE03"] : "CUST-X.0000000001563",
					ARMW21ZE03: isSelected ? selectedRow["ARMW21ZE03"] === "#" ? "" : selectedRow["ARMW21ZE03"] : "CUST-X.0000000001563",
					ARMW31ZE03: isSelected ? selectedRow["ARMW31ZE03"] === "#" ? "" : selectedRow["ARMW31ZE03"] : "CUST-X.0000000001563",
					ARMW11ZE03_T: isSelected ? selectedRow["ARMW11ZE03_T"] === "#" ? "" : selectedRow["ARMW11ZE03_T"] : "no",
					ARMW21ZE03_T: isSelected ? selectedRow["ARMW21ZE03_T"] === "#" ? "" : selectedRow["ARMW21ZE03_T"] : "no",
					ARMW31ZE03_T: isSelected ? selectedRow["ARMW31ZE03_T"] === "#" ? "" : selectedRow["ARMW31ZE03_T"] : "no",
					ADFRTSPC1: isSelected ? selectedRow["ADFRTSPC1"] === "#" ? "" : selectedRow["ADFRTSPC1"] : "",
					ADFRTSPC2: isSelected ? selectedRow["ADFRTSPC2"] === "#" ? "" : selectedRow["ADFRTSPC2"] : "",
					ADFRTSPC3: isSelected ? selectedRow["ADFRTSPC3"] === "#" ? "" : selectedRow["ADFRTSPC3"] : "",
					ADOEWERK: isSelected ? selectedRow["ADOEWERK"] === "#" ? "" : selectedRow["ADOEWERK"] : "",
					PLANT_EDITABLE: plantFilterValue === "",
					A0CUSTOMER: isSelected ? selectedRow["A0CUSTOMER"] === "#" ? "" : selectedRow["A0CUSTOMER"] : "",
					A0CUSTOMER__DOEBKUNDE: isSelected ? selectedRow["A0CUSTOMER__DOEBKUNDE"] === "#" ? "" : selectedRow["A0CUSTOMER__DOEBKUNDE"] : "",
					A0CUSTOMER_EDITABLE: customerFilterValue === "",
					A0CUSTOMER__DSDREGION: isSelected ? selectedRow["A0CUSTOMER__DSDREGION"] === "#" ? "" : selectedRow["A0CUSTOMER__DSDREGION"] : "",
					A0CUSTOMER__DSDREGION_EDITABLE: customerFilterValue === "",
					A0CUSTOMER__DSDAREA: isSelected ? selectedRow["A0CUSTOMER__DSDAREA"] === "#" ? "" : selectedRow["A0CUSTOMER__DSDAREA"] : "",
					A0CUSTOMER__DSDAREA_EDITABLE: customerFilterValue === "",
					A0CUSTOMER__0COUNTRY: isSelected ? selectedRow["A0CUSTOMER__0COUNTRY"] === "#" ? "" : selectedRow["A0CUSTOMER__0COUNTRY"] : "",
					A0CUSTOMER__0COUNTRY_EDITABLE: customerFilterValue === "",
					IS_EDITABLE: true,
					FRUIT_SPCF_1_ITEMS: [],
					FRUIT_SPCF_2_ITEMS: [],
					FRUIT_SPCF_3_ITEMS: [],
					ORGANIC_EDITABLE: true,
					HALAL_EDITABLE: true,
					KOSHER_EDITABLE: true,
					FS1_EDITABLE: true,
					FS2_EDITABLE: true,
					FS3_EDITABLE: true,
					A0SALESORG_EDITABLE: true,
					ADOEWERK_EDITABLE: true,
				};
				counter += 1;
				data.unshift(emptyRow);

			});

			that._seasonTableM.setProperty("/tableRows", data);

			var sumRow = data.filter(function (line) {
				return line["isSum"] === "X";
			});

			if (sumRow.length > 0) {
				that._getSumRowHCurrent();
				data = that._seasonTableM.getProperty("/tableRows");
				index = 1;
			}
			if (counter > 0) {
				for (let i = 1; i < counter + 1; i++) {
					var path = "/tableRows/" + i;
					that._updateFS1(that._seasonTableM, path, data[i]);
					that._updateFS2(that._seasonTableM, path, data[i]);
					that._updateFS3(that._seasonTableM, path, data[i]);
				}

			} else {
				that._updateFS1(that._seasonTableM, "/tableRows/" + index, emptyRow);
				that._updateFS2(that._seasonTableM, "/tableRows/" + index, emptyRow);
				that._updateFS3(that._seasonTableM, "/tableRows/" + index, emptyRow);

				if (plantFilterValue !== "" && plantFilterValue !== undefined) {
					var path = "/tableRows/" + index;
					var plant = this._seasonTableM.getProperty(path).ADOEWERK;
					var plantArray = [];
					plantArray.push({
						PLANT: plant
					});
					this._getSalesOrg(plantArray, path);
				}

				if (customerFilterValue !== "" && customerFilterValue !== undefined) {
					var path = "/tableRows/" + index;
					var customer = this._seasonTableM.getProperty(path).A0CUSTOMER;
					if (customer === "") return;
					var customerArray = [];
					customerArray.push({
						KUNNR: customer,
						LAND1: "",
						AREA: ""
					});
					this._getRegionAreCountryByCustomerL2("CUSTOMERL2", customerArray, path);
				}
			}

			// this._updateFS1(this._seasonTableM, "/tableRows/0", data[0]);
			// this._updateFS2(this._seasonTableM, "/tableRows/0", data[0]);
			// this._updateFS3(this._seasonTableM, "/tableRows/0", data[0]);

		},

		_getSumRowHCurrent: function () {
			var newCollectedData = this._seasonTableM.getProperty("/tableRows");
			var data = newCollectedData.filter(function (line) {
				return line["isSum"] !== "X" && line["IS_EDITABLE"] === false;
			});

			var totalRow = this.setTotalRow(data, "SPT", this.columnfilter);

			var DataWithEditable = newCollectedData.filter(function (line) {
				return line["isSum"] !== "X";
			});
			DataWithEditable.unshift(totalRow);

			DataWithEditable.forEach(function (line) {
				if (line["FRUIT_SPCF_1_T"] === "0") line["FRUIT_SPCF_1_T"] = "";
				if (line["FRUIT_SPCF_2_T"] === "0") line["FRUIT_SPCF_2_T"] = "";
				if (line["FRUIT_SPCF_3_T"] === "0") line["FRUIT_SPCF_3_T"] = "";
			});
			this._seasonTableM.setProperty("/tableRows", DataWithEditable);
		},

		deleteRows: function (oEvent) {
			var that = this,
				dataToDelete = [],
				sYear = this._panelModel.getProperty("/SFULLYEAR_FORMAT"),
				eYear = this._panelModel.getProperty("/EFULLYEAR_FORMAT");
			var path = oEvent.getSource().getBindingContext("seasonTableM").getPath();
			var index = path.split("/")[2];
			if (this._seasonTableM.getProperty(path)["isSave"] === false) {
				this._seasonTableM.getData().tableRows.splice(index, "1");
				this._seasonTableM.setData(this._seasonTableM.getData());
			} else {
				dataToDelete.push(this._seasonTableM.getProperty(path));
				var params = {
					"HANDLERPARAMS": {
						"FUNC": "DELETE_SEASON_LIST"
					},
					"INPUTPARAMS": [{
						"SYEAR": sYear,
						"EYEAR": eYear,
						"SEASON_DATA": dataToDelete
					}]
				};

				dbcontext.callServer(params, function (oModel) {
					that.handleServerMessages(oModel, function (status) {
						if (status === "S") {
							that.onGo();
						}
					});
				}, that);

			}
		},

		onEditRow: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("seasonTableM").getPath();
			this._seasonTableM.setProperty(path + "/lastRow", true);
			this._seasonTableM.setProperty(path + "/IS_UNDO", true);
			this._seasonTableM.setProperty(path + "/IS_EDITABLE", false);
			var rowExtend = $.extend(true, {}, this._seasonTableM.getProperty(path));
			this._seasonTableM.setProperty(path + "/undoData", rowExtend);
		},

		onUndoRow: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("seasonTableM").getPath();
			this._seasonTableM.setProperty(path, this._seasonTableM.getProperty(path + "/undoData"));
			this._seasonTableM.setProperty(path + "/lastRow", false);
			this._seasonTableM.setProperty(path + "/IS_UNDO", false);
			this._seasonTableM.setProperty(path + "/IS_EDITABLE", true);

		},

		onTableRowChange: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName();
			var value = oEvent.getParameter("newValue");

			var bindingContext = oEvent.getSource().getBindingContext("seasonTableM") !== undefined ? oEvent.getSource().getBindingContext(
					"seasonTableM") :
				sap
				.ui.getCore().getModel("globalM").getProperty("/pressTableValueControl").getBindingContext("seasonTableM");
			var path = bindingContext.getPath();

			if (fieldName === "CUSTOMERL2") {
				var customer = this._seasonTableM.getProperty(path).A0CUSTOMER;
				if (customer === "") return;
				var customerArray = [];
				customerArray.push({
					KUNNR: customer,
					LAND1: "",
					AREA: ""
				});
				this._getRegionAreCountryByCustomerL2("CUSTOMERL2", customerArray, path);
			}

			if (fieldName === "COUNTRY") {
				var country = this._seasonTableM.getProperty(path).A0CUSTOMER__0COUNTRY;
				if (country === "") return;
				var countryArray = [];
				countryArray.push({
					KUNNR: "",
					LAND1: country,
					AREA: ""
				});
				this._getRegionAreCountryByCustomerL2("COUNTRY", countryArray, path);
			}

			if (fieldName === "AREA") {
				var country = this._seasonTableM.getProperty(path + "/A0CUSTOMER__0COUNTRY");
				if (country === "" || country === undefined) {
					var area = this._seasonTableM.getProperty(path).A0CUSTOMER__DSDAREA;
					if (area === "") return;
					var areaArray = [];
					areaArray.push({
						KUNNR: "",
						LAND1: "",
						AREA: area
					});
					this._getRegionAreCountryByCustomerL2("AREA", areaArray, path);
				}
			}

			if (fieldName === "WERKS") {
				var plant = this._seasonTableM.getProperty(path).ADOEWERK;
				if (plant === "") return;
				var plantArray = [];
				plantArray.push({
					PLANT: plant
				});

				this._getSalesOrg(plantArray, path);
			}

			if (fieldName === "PRODUCT_TYPE") {
				var row = this._seasonTableM.getProperty(path);
				this._updateFS1(this._seasonTableM, path, row);
				this._updateFS2(this._seasonTableM, path, row);
				this._updateFS3(this._seasonTableM, path, row);
			}

			this._seasonTableM.setProperty(path + "/CHANGED", true);

		},

		onTableRowLiveChange: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName();
			var bindingContext = oEvent.getSource().getBindingContext("seasonTableM") !== undefined ? oEvent.getSource().getBindingContext(
					"seasonTableM") :
				sap
				.ui.getCore().getModel("globalM").getProperty("/pressTableValueControl").getBindingContext("seasonTableM");
			var path = bindingContext.getPath();
			if (fieldName === "CUSTOMERL2") {
				var customer = this._seasonTableM.getProperty(path + "/A0CUSTOMER");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION", "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION_EDITABLE", customer === "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDAREA", "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDAREA_EDITABLE", customer === "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__0COUNTRY", "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__0COUNTRY_EDITABLE", customer === "");

			}
			if (fieldName === "COUNTRY") {
				var country = this._seasonTableM.getProperty(path + "/A0CUSTOMER__0COUNTRY");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION", "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDAREA", "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION_EDITABLE", country === "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDAREA_EDITABLE", country === "");
			}

			if (fieldName === "AREA") {
				var area = this._seasonTableM.getProperty(path + "/A0CUSTOMER__DSDAREA");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION", "");
				this._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION_EDITABLE", area === "");
			}

		},

		_getSalesOrg: function (plantArray, path) {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "DETERMINE_SALESORG_BY_PLANT"
				},
				"INPUTPARAMS": plantArray
			};

			dbcontext.callServer(params, function (oModel) {
				var result = oModel.getData().RESULT;
				var rowPlant = that._seasonTableM.getProperty(path).ADOEWERK;
				var salesOrg = result.filter(function (m) {
					return m.DWERK === rowPlant;
				});
				if (salesOrg.length > 0) {
					that._seasonTableM.setProperty(path + "/A0SALESORG", salesOrg[0].VKORG);
				} else {
					MessageToast.show("Sales org not found!");
				}
			}, that);
		},

		_getRegionAreCountryByCustomerL2: function (name, customerArray, path) {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "DETERMINE_CUSTOMER_FIELDS"
				},
				"INPUTPARAMS": customerArray
			};

			dbcontext.callServer(params, function (oModel) {
				var result = oModel.getData().RESULT;
				var row = that._seasonTableM.getProperty(path);

				switch (name) {
				case "CUSTOMERL2":
					that._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION", result.REGION);
					that._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDAREA", result.AREA);
					that._seasonTableM.setProperty(path + "/A0CUSTOMER__0COUNTRY", result.LAND1);
					that._seasonTableM.setProperty(path + "/A0CUSTOMER__DOEBKUNDE", result.KUNN2);
					break;
				case "COUNTRY":
					that._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION", result.REGION);
					that._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDAREA", result.AREA);
					break;
				case "AREA":
					that._seasonTableM.setProperty(path + "/A0CUSTOMER__DSDREGION", result.REGION);
					break;
				default:
				}

			}, that);
		},

		onChange: function (oEvent) {
			this._panelModel.setProperty("/EFULLYEAR", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
			this._panelModel.setProperty("/SEASON", "");
			this._panelModel.setProperty("/SFULLYEAR", "");
		},

		onLiveChange: function (oEvent) {
			this._panelModel.setProperty("/EFULLYEAR", "");
			this._panelModel.setProperty("/SFULLYEAR", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
			this._panelModel.setProperty("/SEASON", "");
		},

		//************************************************ Season search help**************************************

		checkSeason: function () {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_DATA"
				},
				"INPUTPARAMS": [{
					"FRUIT": this._panelModel.getProperty("/FRUIT"),
					"PLANT": this._panelModel.getProperty("/PLANT")
				}]
			};

			return params;

		},

		handleValueHelp: function (oEvent) {
			var params = "";
			var fruit = this._panelModel.getProperty("/FRUIT");
			if (fruit === "") {
				MessageToast.show("Fruit field can not be empty!");
				return;
			} else {
				params = this.checkSeason();

			}

			var oView = this.getView();
			this._sInputId = oEvent.getSource().getId();

			// create value help dialog
			if (!this._pValueHelpDialog) {
				this._pValueHelpDialog = Fragment.load({
					id: oView.getId(),
					name: "com.doehler.Z_GTP.fragments.SeasonSearchHelp",
					controller: this
				}).then(function (oValueHelpDialog) {
					oView.addDependent(oValueHelpDialog);
					return oValueHelpDialog;
				});
			}
			this._prepareSeason(params, this._globalModel, this._pValueHelpDialog, undefined, "X");
		},

		_handleValueHelpSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Name",
				FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

		onseasonTableSelect: function (oEvent) {
			try {
				var row = this._seasonTableM.getProperty(oEvent.getParameter("rowContext").getPath());

				if (row.isSum === "X") {
					oEvent.getSource().removeSelectionInterval(oEvent.getParameter("rowIndex"), 0);
				}
			} catch (err) {}

		},

		_handleValueHelpClose: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var productInput = this.byId(this._sInputId);
				productInput.setValue(oSelectedItem.getTitle());
			}
			oEvent.getSource().getBinding("items").filter([]);

			var eFormat = "",
				sFormmat = "";
			if (this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath()) === undefined)
				return;

			var line = this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath());
			if (line.efullYear.split(".").length === 3) {
				eFormat = line.efullYear.split(".")[2] + line.efullYear.split(".")[1] + line.efullYear.split(".")[0];
			}
			if (line.sfullYear.split(".").length === 3) {
				sFormmat = line.sfullYear.split(".")[2] + line.sfullYear.split(".")[1] + line.sfullYear.split(".")[0];
			}
			this._panelModel.setProperty("/SFULLYEAR", line.sfullYear);
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
			this._panelModel
				.setProperty("/EFULLYEAR", line.efullYear);
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormat);
			this._panelModel.setProperty(
				"/SEASON_TEXT", line.text);
		},

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		oCC1: null,
		currTableData: null,
		currTableData1: null,
		oTPC: null,
		oTPC1: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},

		onSettings1: function () {
			this.oTPC1.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC1 = oCC;
				oCC.delItem(thiz._tableIdText1);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId;
			var oVM = this.getView().byId("seasonToOffertableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
					// Find Table Key
					thiz._findTableKeys();

				});
			});
		},

		initVariant1: function () {
			var thiz = this;
			var oTable = this._tableId1;
			var oVM = this.getView().byId("seasonToOffertableVMId1");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText1, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC1 = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText1);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData1 = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC1 = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData1 = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}

					// Find Table Key
					thiz._findTableKeys1();

				});
			});
		},

		onSelectionChangeTableView: function (oEvent) {
			var key = oEvent.getParameter("item").getProperty("key");
			var table1 = this._seasonTableM.getProperty("/tableRows");
			var table2 = this._seasonTableM.getProperty("/tableRows1");
			if (key === "1") {
				this.getView().byId("seasonalTableId").setVisible(true);
				this.getView().byId("seasonalTableId1").setVisible(false);
				this.getView().byId("savePRBId").setVisible(true);
				this.getView().byId("addPRBId").setVisible(true);
				this.getView().byId("crtPRBId").setVisible(true);
				this.getView().byId("sgmBtn").setSelectedKey("1");
			}
			if (key === "2") {
				this.getView().byId("seasonalTableId").setVisible(false);
				this.getView().byId("seasonalTableId1").setVisible(true);
				this.getView().byId("savePRBId").setVisible(false);
				this.getView().byId("addPRBId").setVisible(false);
				this.getView().byId("crtPRBId").setVisible(false);
				this.getView().byId("sgmBtn1").setSelectedKey("2");
			}
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys();
		},

		/* on select variant */
		onSelectVariant1: function (oEvent) {
			var tableId = this._tableIdText1;
			var oTable = this._tableId1;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys1();
		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},

		/* on save new variant */
		onSaveVariant1: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC1;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText1;
			var oTable = this._tableId1;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData1) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData1,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},

		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},

		onManageVM1: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText1;
			var ovar = oCC.getItemValue(tableId1);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		//************************************************table variant codes**************************************

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdSTO");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("seasonPanelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isSeasonalPlanToOffer", true);
			sap.ui.getCore().getModel("globalM").setProperty("/seasonalInit", true);
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("seasonPanelM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantSTO: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("seasonPanelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantSTO: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("seasonPanelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMSTO: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1) {
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined)
				this._updatePanelData(fromData, sap.ui.getCore().getModel("seasonPanelM"));
			var aControls = this.getView().byId("searchGridId").getContent();
			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));
					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {
									if (item.getFieldName() === "FRUIT") {
										const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
										item.addToken(new sap.m.Token({
											key: value,
											text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
										}));
									} else {
										item.addToken(new sap.m.Token({
											key: value,
											text: value
										}));
									}
								});

							}
						}
					});
				}, 1000);
		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			if (type === "added" || type === "removed" || type === "removedAll") {
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "WERKS") {

					this._panelModel.setProperty("/EFULLYEAR", "");
					this._panelModel.setProperty("/SFULLYEAR", "");
					this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/PREV_FROM", "");
					this._panelModel.setProperty("/PREV_TO", "");
					this._panelModel.setProperty("/SEASON", "");

					this._prefillSeason(oEvent);
				}
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "PRODUCT_TYPE") {
					this.getView().byId("FS1").removeAllTokens();
					this.getView().byId("FS2").removeAllTokens();
					this.getView().byId("FS3").removeAllTokens();
				}
			}

		},

		_prefillSeason: function (oEvent) {
			var fruit = this.getView().byId("fruitId");
			var params = this.checkSeason();
			if (fruit.getTokens().length === 1)
				this._prepareSeason(params, this._globalModel, null, this._panelModel, "");
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			//this.clearSelectionFields();
			this.initVariant();
			this.initVariant1();
			this.initSearchVariant();
		},

		onCreateGtp: function (oObject) {
			var isSelected = false;
			var selectedRow = {};
			var filteredData = sap.ui.getCore().getModel("seasonPanelM").getData();
			var gtp_quoteData = this._seasonTableM.getProperty("/tableRows");
			if (filteredData.FRUIT === "") {
				MessageBox.warning("Please select a fruit before you start quote creation.");
				return;
			}
			if (filteredData.FRUIT !== "") {
				try {
					var fruitPanelText = this.getView().byId("fruitId").getTokens()[0].getText();
				} catch (err) {}

			}
			var currentDay = new Date();
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyyMMdd"
			});

			var priceQuantityUnit = [{
				"KMEIN": ""
			}, {
				"KMEIN": "KG"
			}, {
				"KMEIN": "TO"
			}, {
				"KMEIN": "GAL"
			}];
			this._globalModel.setProperty("/PriceQuantityUnit", priceQuantityUnit);

			var priceUnit = [{
				"KPEIN": ""
			}, {
				"KPEIN": "1"
			}, {
				"KPEIN": "10"
			}, {
				"KPEIN": "100"
			}, {
				"KPEIN": "1000"
			}];
			this._globalModel.setProperty("/PriceUnit", priceUnit);

			var table = this._tableId,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				aIndices = table.getBinding().aIndices;

			if (aSelIndex.length === 1) {
				isSelected = true;
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});
				aSelIndex = newSelIndex;
				selectedRow = gtp_quoteData[aSelIndex[0]];
			}

			/*	for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.BU_MEASURE_STATUS = selectedKey;
				}*/

			var PlantFilterValue = filteredData.PLANT;
			if (PlantFilterValue.indexOf(",") !== -1) PlantFilterValue = "";

			var data = [{
				MATNR: "",
				FRUIT: isSelected ? selectedRow["ARMW21ZH02"] === "#" ? "" : selectedRow["ARMW21ZH02"] : filteredData.FRUIT,
				FRUIT_T: isSelected ? selectedRow["ARMW21ZH02_T"] === "#" ? "" : selectedRow["ARMW21ZH02_T"] : fruitPanelText,
				FRUIT_EDITABLE: true,
				PRODUCT_TYPE: isSelected ? selectedRow["ARMW11ZP16"] === "#" ? "" : selectedRow["ARMW11ZP16"] : filteredData
					.PRODUCT_TYPE,
				PRODUCT_TYPE_EDITABLE: true,
				WERKS: isSelected ? selectedRow["ADOEWERK"] === "#" ? "" : selectedRow["ADOEWERK"] : PlantFilterValue,
				VKORG: isSelected ? selectedRow["A0SALESORG"] === "#" ? "" : selectedRow["A0SALESORG"] : filteredData.SALESORG,
				VTWEG: "10", //isSelected ? selectedRow["A0DISTR_CHAN"] : "10",
				CUSTL2: isSelected ? selectedRow["A0CUSTOMER"] === "#" ? "" : selectedRow["A0CUSTOMER"] : filteredData.CUSTOMERL2,
				SHIP_TO_PARTY: isSelected ? selectedRow["A0CUSTOMER"] === "#" ? "" : selectedRow["A0CUSTOMER"] : filteredData.CUSTOMERL2,
				PAYER: isSelected ? selectedRow["A0CUSTOMER"] === "#" ? "" : selectedRow["A0CUSTOMER"] : filteredData.CUSTOMERL2,
				INCO1: "FCA", //isSelected ? selectedRow["A0INCOTERMS"] : "FCA",
				ZTERM: "00", //isSelected ? selectedRow["A0PMNTTRMS"] : "00",
				VOLUME: 0,
				VOLUME_MEINS: "KG",
				ZZMBG: 0,
				ZZMBG_MEINS: "KG",
				PRICE: 0,
				PRICE_WAERS: "",
				KPEIN: "1",
				KMEIN: "KG",
				ANGDT: dateFormat.format(currentDay),
				BNDDT: "",
				GUEBG: filteredData.GUEBG === undefined ? "" : filteredData.GUEBG,
				GUEEN: filteredData.GUEEN === undefined ? "" : filteredData.GUEEN,
				SEASON_START: this._panelModel.getData().SFULLYEAR_FORMAT,
				SEASON_END: this._panelModel.getData().EFULLYEAR_FORMAT,
				QUOTE_COMMENT: "",
				PACKAGING_CLASS: "",
				PACKAGING_CLASS_EDITABLE: true,
				VSBED: "",
				ARMW11ZE03: isSelected ? selectedRow["ARMW11ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001987" : "CUST-X.0000000001563",
				ARMW21ZE03: isSelected ? selectedRow["ARMW21ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001562" : "CUST-X.0000000001563",
				ARMW31ZE03: isSelected ? selectedRow["ARMW31ZE03_T"] === "no" ? "CUST-X.0000000001563" : "0000000001562" : "CUST-X.0000000001563",
				ARMW11ZE03_T: this.getDetermineOrganic(isSelected ? selectedRow["A0MATERIAL__RMW11ZE03_T"] === "no" ?
					"CUST-X.0000000001563" :
					"0000000001987" : "CUST-X.0000000001563"),
				ARMW21ZE03_T: this.getDetermineHalal(isSelected ? selectedRow["A0MATERIAL__RMW71ZE03_T"] === "no" ? "CUST-X.0000000001563" :
					"0000000001562" : "CUST-X.0000000001563"),
				ARMW31ZE03_T: this.getDetermineKosher(isSelected ? selectedRow["A0MATERIAL__RMW81ZE03_T"] === "no" ?
					"CUST-X.0000000001563" :
					"0000000001562" : "CUST-X.0000000001563"),
				FRUIT_SPCF_1_ITEMS: [],
				FRUIT_SPCF_2_ITEMS: [],
				FRUIT_SPCF_3_ITEMS: [],
				ADFRTSPC1: isSelected ? selectedRow["ADFRTSPC1"] === "#" ? "" : selectedRow["ADFRTSPC1"] : "",
				ADFRTSPC2: isSelected ? selectedRow["ADFRTSPC2"] === "#" ? "" : selectedRow["ADFRTSPC2"] : "",
				ADFRTSPC3: isSelected ? selectedRow["ADFRTSPC3"] === "#" ? "" : selectedRow["ADFRTSPC3"] : ""
			}];

			this.CreateGTPquoteTableM.setProperty("/tableRows", data);
			this._updateFS1(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);
			this._updateFS2(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);
			this._updateFS3(this.CreateGTPquoteTableM, "/tableRows/0", data[0]);

			var oControl = oObject.getSource();
			if (!this._oPopoverCreateGTP) {
				Fragment.load({
					id: this.getView().getId(),
					name: "com.doehler.Z_GTP.fragments.CreateGTPQuoteTable",
					controller: this
				}).then(function (oPopover) {
					this._oPopoverCreateGTP = oPopover;
					this.getView().addDependent(this._oPopoverCreateGTP);
					this._oPopoverCreateGTP.openBy(oControl);
				}.bind(this));
			} else {
				this._oPopoverCreateGTP.openBy(oControl);
			}

		},
		handleCloseButtonCreateGtp: function (oEvent) {
			this._oPopoverCreateGTP.close();
		},

		onTableRowChangeGTPQuote: function (oEvent) {
			var that = this;
			var createGTPquoteTableModel = this.CreateGTPquoteTableM;
			var customer,
				salesOrg,
				distChannel,
				matnr;
			var fieldName = oEvent.getSource().getFieldName();
			var UserInput = oEvent.getParameter("value") === "" || oEvent.getParameter("value") === undefined ? oEvent.getSource().getProperty(
				"desc") : oEvent.getParameter("value");
			var bindingContext = oEvent.getSource().getBindingContext("CreateGTPquoteTableM") !== undefined ? oEvent.getSource().getBindingContext(
					"CreateGTPquoteTableM") :
				sap
				.ui.getCore().getModel("globalM").getProperty("/pressTableValueControl").getBindingContext("CreateGTPquoteTableM");
			var path = bindingContext.getPath();

			if (fieldName === "CUSTOMERL2") {
				customer = createGTPquoteTableModel.getProperty(path + "/CUSTL2");
				createGTPquoteTableModel.setProperty(path + "/SHIP_TO_PARTY", customer);
				createGTPquoteTableModel.setProperty(path + "/PAYER", customer);

			}

			if (fieldName === "PRICE") {
				if (UserInput === "")
					createGTPquoteTableModel.setProperty(path + "/PRICE", 0);
			}

			if (fieldName === "PLANT") {
				if (UserInput === "" || UserInput === undefined) {
					this.getView().byId("plantfilterid").removeAllTokens();
					this.getView().byId("plantfilterid").destroyTokens();
				} else {
					this._panelModel.setProperty("/PLANT", UserInput);
					this.getView().byId("plantfilterid").addToken(new sap.m.Token({
						key: UserInput,
						text: UserInput
					}));
				}

			}

			if (fieldName === "MATERIAL_GTP_QUOTE") {
				matnr = createGTPquoteTableModel.getProperty(path + "/MATNR");
				if (matnr !== "") {

					var Promise1 = new Promise(function (myResolve, myReject) {
						that._checkFruitMantr(UserInput, that._panelModel.getProperty("/FRUIT"), myResolve);
					});

					Promise1.then(
						function (value) {
							if (value === "E") {
								createGTPquoteTableModel.setProperty(path + "/MATNR", "");
							} else {
								createGTPquoteTableModel.setProperty(path + "/FRUIT", "");
								createGTPquoteTableModel.setProperty(path + "/FRUIT_T", "");
								createGTPquoteTableModel.setProperty(path + "/ADFRTSPC1", "");
								createGTPquoteTableModel.setProperty(path + "/ADFRTSPC2", "");
								createGTPquoteTableModel.setProperty(path + "/ADFRTSPC3", "");
								createGTPquoteTableModel.setProperty(path + "/ARMW11ZE03", "CUST-X.0000000001563");
								createGTPquoteTableModel.setProperty(path + "/ARMW21ZE03", "CUST-X.0000000001563");
								createGTPquoteTableModel.setProperty(path + "/ARMW31ZE03", "CUST-X.0000000001563");
								createGTPquoteTableModel.setProperty(path + "/PRODUCT_TYPE", "");
								that._getQuoteValidTo(path);
								createGTPquoteTableModel.setProperty(path + "/FRUIT_EDITABLE", false);
								createGTPquoteTableModel.setProperty(path + "/PRODUCT_TYPE_EDITABLE", false);
							}

						}
					);

				} else {
					createGTPquoteTableModel.setProperty(path + "/FRUIT_EDITABLE", true);
					createGTPquoteTableModel.setProperty(path + "/PRODUCT_TYPE_EDITABLE", true);
				}

			}

			if (fieldName === "FRUIT") {
				createGTPquoteTableModel.setProperty(path + "/FRUIT", createGTPquoteTableModel.getProperty(path + "/FRUIT_T"));
			}

			if (fieldName === "PRODUCT_TYPE") {
				var row = createGTPquoteTableModel.getProperty(path);
				this._updateFS1(this.CreateGTPquoteTableM, path, row);
				this._updateFS2(this.CreateGTPquoteTableM, path, row);
				this._updateFS3(this.CreateGTPquoteTableM, path, row);
			}

			if (fieldName === "MATERIAL_GTP_QUOTE" || fieldName === "PLANT") {
				var plant = createGTPquoteTableModel.getProperty(path + "/WERKS");
				matnr = createGTPquoteTableModel.getProperty(path + "/MATNR");
				if (plant !== "" && matnr !== "") {
					var Promise2 = new Promise(function (myResolve, myReject) {
						that._fillPackingClass(matnr, plant, myResolve);
					});

					Promise2.then(
						function (value) {
							if (value !== "") {
								createGTPquoteTableModel.setProperty(path + "/PACKAGING_CLASS", value);
								createGTPquoteTableModel.setProperty(path + "/PACKAGING_CLASS_EDITABLE", false);

							} else {
								createGTPquoteTableModel.setProperty(path + "/PACKAGING_CLASS_EDITABLE", true);
							}
						})

				}
			}

			this._checkValidationOfGTPQuote(fieldName, path, createGTPquoteTableModel);

		},

		_getQuoteValidTo: function (path) {
			var that = this,
				sendData = {},
				weekNumber = 0,
				today = new Date(),
				line = this.CreateGTPquoteTableM.getProperty(path);
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyyMMdd"
			});
			if ((line.MATNR === "" || line.MATNR === undefined) || (line.BNDDT !== "" && line.BNDDT !== undefined)) return;
			sendData.MATERIAL = line.MATNR;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "DETERMINE_QUOTE_VALID_TO"
				},
				"INPUTPARAMS": [sendData]
			};

			dbcontext.callServer(params, function (oModel) {
				weekNumber = oModel.getData().LV_WEEKS;
				if (weekNumber !== 0) {
					today.setDate(today.getDate() + weekNumber * 7);
					that.CreateGTPquoteTableM.setProperty(path + "/BNDDT", dateFormat.format(today));
				}
			}, that);

		},

		handleCreateGTPQuote: function () {
			var that = this;
			var gtp_quoteData = this.CreateGTPquoteTableM.getProperty("/tableRows");
			var sendData = {
				QUOTES: []
			};
			var isFieldsFull = this._checkGTPItems(gtp_quoteData);
			if (isFieldsFull === false) {
				MessageBox.warning(
					"Please fill all fields."
				);
				return;
			}

			//for Fruit Specific checks
			var msg = this._checkFruitSpecificsIsReady(gtp_quoteData, "ADFRTSPC1", "ADFRTSPC2", "ADFRTSPC3");
			if (msg !== "") {
				MessageBox.warning(msg);
				return;
			}

			if (gtp_quoteData.length > 0) {
				if (gtp_quoteData[0]["SEASON_START"] === "" && gtp_quoteData[0]["SEASON_END"] === "") {
					if (this._globalModel.getData()["seasonDates"] !== undefined) {
						if (this._globalModel.getData()["seasonDates"].length > 0) {
							var sYear = this._globalModel.getData()["seasonDates"][1].sfullYear;
							var eYear = this._globalModel.getData()["seasonDates"][1].efullYear;
							if (sYear.split(".").length === 3) {
								gtp_quoteData[0]["SEASON_START"] = sYear.split(".")[2] + sYear.split(".")[1] + sYear.split(".")[0];
							}
							if (eYear.split(".").length === 3) {
								gtp_quoteData[0]["SEASON_END"] = eYear.split(".")[2] + eYear.split(".")[1] + eYear.split(".")[0];
							}

						}
					}
					if (gtp_quoteData[0]["SEASON_START"] === "" && gtp_quoteData[0]["SEASON_END"] === "") {
						MessageBox.warning("Season could not be determined!");
						return;
					}
				}
			}

			sendData.QUOTES = sendData.QUOTES.concat(gtp_quoteData);
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "CREATE_QUOTE_GTP"
				},
				"INPUTPARAMS": [sendData]
			};

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						that._oPopoverCreateGTP.close();
						that.onGo();
					}
				});
			}, that);
		},

		_checkGTPItems: function (gtpQuoteData) {
			var isFieldsFull = true;
			var i = 0;
			for (i = 0; i < gtpQuoteData.length; i++) {
				if (gtpQuoteData[i].MATNR === "") {
					if (gtpQuoteData[i].PRODUCT_TYPE === "" || gtpQuoteData[i].WERKS === "" || gtpQuoteData[i].VKORG === "" || gtpQuoteData[i].VTWEG ===
						"" || gtpQuoteData[i].CUSTOMERL2 === "" ||
						gtpQuoteData[i].SHIP_TO_PARTY === "" || gtpQuoteData[i].PAYER === "" || gtpQuoteData[i].INCO1 === "" || gtpQuoteData[i].ZTERM ===
						"" ||
						gtpQuoteData[i].VOLUME === "" || gtpQuoteData[i].VOLUME === 0 || gtpQuoteData[i].VOLUME_MEINS === "" || gtpQuoteData[i].ZZMBG ===
						"" ||
						gtpQuoteData[i].ZZMBG === 0 || gtpQuoteData[i].ZZMBG_MEINS === "" || gtpQuoteData[i].PRICE === "" || gtpQuoteData[i].PRICE ===
						0 ||
						gtpQuoteData[i].PRICE_WAERS === "" || gtpQuoteData[i].KPEIN === "" || gtpQuoteData[i].KPEIN === 0 || gtpQuoteData[i].KMEIN ===
						"" ||
						gtpQuoteData[i].QUOTFROM === "" || gtpQuoteData[i].QUOTTO === "" || gtpQuoteData[i].PRICEFROM === "" || gtpQuoteData[i].PRICETO ===
						"") {
						isFieldsFull = false;
					}
				}

				if (gtpQuoteData[i].MATNR !== "") {
					if (gtpQuoteData[i].WERKS === "" || gtpQuoteData[i].VKORG === "" || gtpQuoteData[i].VTWEG === "" || gtpQuoteData[i].CUSTOMERL2 ===
						"" ||
						gtpQuoteData[i].SHIP_TO_PARTY === "" || gtpQuoteData[i].PAYER === "" || gtpQuoteData[i].INCO1 === "" || gtpQuoteData[i].ZTERM ===
						"" ||
						gtpQuoteData[i].VOLUME === "" || gtpQuoteData[i].VOLUME === 0 || gtpQuoteData[i].VOLUME_MEINS === "" || gtpQuoteData[i].ZZMBG ===
						"" ||
						gtpQuoteData[i].ZZMBG === 0 || gtpQuoteData[i].ZZMBG_MEINS === "" || gtpQuoteData[i].PRICE === "" || gtpQuoteData[i].PRICE ===
						0 ||
						gtpQuoteData[i].PRICE_WAERS === "" || gtpQuoteData[i].KPEIN === "" || gtpQuoteData[i].KPEIN === 0 || gtpQuoteData[i].KMEIN ===
						"" ||
						gtpQuoteData[i].QUOTFROM === "" || gtpQuoteData[i].QUOTTO === "" || gtpQuoteData[i].PRICEFROM === "" || gtpQuoteData[i].PRICETO ===
						"" || gtpQuoteData[i].VSBED === "") {
						isFieldsFull = false;
					}
				}
			}
			return isFieldsFull;

		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();

			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					that._findTableKeys();
					// that._findTableKeys2();
				},
				function (error) { /* code if some error */ }
			);
		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);
		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

		onGoOpenQuotes: function (oEvent) {
			var Source = oEvent.getSource();
			var ViewName = "";
			var srow = _.cloneDeep(Source.getBindingContext('seasonTableM').getObject());

			switch (oEvent.getSource().getBindingPath("text")) {
			case "OPEN_QUOTES":
				ViewName = "QuoteManagement";
				srow["SFULLYEAR"] = this._globalModel.getData().seasonDates[1].sfullYear;
				srow["EFULLYEAR"] = this._globalModel.getData().seasonDates[1].efullYear;
				srow["EFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[1].efullYear.split(".")[2] + this._globalModel.getData().seasonDates[
					0].efullYear.split(".")[1] + this._globalModel.getData().seasonDates[1].efullYear.split(".")[0];
				srow["SFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[1].sfullYear.split(".")[2] + this._globalModel.getData().seasonDates[
					0].sfullYear.split(".")[1] + this._globalModel.getData().seasonDates[1].sfullYear.split(".")[0];
				var lastseason = this._globalModel.getData().seasonDates[1];
				srow["SEASON"] = lastseason.text + "[" + lastseason.sfullYear + " - " + lastseason.efullYear + "]";
				break;
			case "CON_QUAN":
				ViewName = "SalesContracts";
				srow["salesHandover"] = "X";
				srow["SFULLYEAR"] = this._globalModel.getData().seasonDates[0].sfullYear;
				srow["EFULLYEAR"] = this._globalModel.getData().seasonDates[0].efullYear;
				srow["EFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[0].efullYear.split(".")[2] + this._globalModel.getData().seasonDates[
					0].efullYear.split(".")[1] + this._globalModel.getData().seasonDates[0].efullYear.split(".")[0];
				srow["SFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[0].sfullYear.split(".")[2] + this._globalModel.getData().seasonDates[
					0].sfullYear.split(".")[1] + this._globalModel.getData().seasonDates[0].sfullYear.split(".")[0];
				lastseason = this._globalModel.getData().seasonDates[0];
				srow["SEASON"] = lastseason.text + "[" + lastseason.sfullYear + " - " + lastseason.efullYear + "]";

				break;
			default:
				ViewName = "SalesContracts";
				srow["SFULLYEAR"] = this._globalModel.getData().seasonDates[1].sfullYear;
				srow["EFULLYEAR"] = this._globalModel.getData().seasonDates[1].efullYear;
				srow["EFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[1].efullYear.split(".")[2] + this._globalModel.getData().seasonDates[
					1].efullYear.split(".")[1] + this._globalModel.getData().seasonDates[1].efullYear.split(".")[0];
				srow["SFULLYEAR_FORMAT"] = this._globalModel.getData().seasonDates[1].sfullYear.split(".")[2] + this._globalModel.getData().seasonDates[
					1].sfullYear.split(".")[1] + this._globalModel.getData().seasonDates[1].sfullYear.split(".")[0];
				lastseason = this._globalModel.getData().seasonDates[1];
				srow["SEASON"] = lastseason.text + "[" + lastseason.sfullYear + " - " + lastseason.efullYear + "]";
			}

			Object.keys(srow).forEach(function (key) {
				var splitkey = key.split("_")[1];
				var splitkey_2x = key.split("__");
				if (splitkey_2x.length === 2 && splitkey === "") {
					splitkey = splitkey_2x[1].split("_")[1];
				}
				if (splitkey === "T")
					delete srow[key];
			});

			this.getRouter().navTo(ViewName, {
				row: JSON.stringify(srow),
				viewid: this._tableId.getId()
			}, false);

		}

	});

});