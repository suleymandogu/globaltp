sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	"com/doehler/Z_GTP/model/formatter",
	"sap/ui/core/Fragment",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, dbcontext, dbcontext2, excel, formatter, Fragment, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.Main", {
		formatter: formatter,
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.doehler.Z_GTP.view.Main
		 */
		onInit: function () {
			this._setButtonsPressed("overViewBtnId");
			this._tableId = this.getView().byId("gtpTableId");
			this._tableId2 = this.getView().byId("gtpTableId2");
			this._tableId3 = this.getView().byId("gtpTableId3");
			this._tableIdText = "gtpTableId";
			this._tableIdText2 = "gtpTableId2";
			this._tableIdText3 = "gtpTableId3";
			this.columnfilter = {};
			this.columnfilter2 = {};
			//panel Model
			this._panelModel = this.getOwnerComponent().getModel("panelM");
			sap.ui.getCore().setModel(this._panelModel, "panelM");

			this._tableModel = this.getOwnerComponent().getModel("tableM");
			this._tableModel.setProperty("/tableRows", []);
			this._tableModel.setProperty("/tableRows2", []);
			this._tableModel.setProperty("/tableRows3", []);

			this._oDataModel = this.getOwnerComponent().getModel("DJCVCP011_Q004_GTP");

			this._gtpMainModel = this.getOwnerComponent().getModel("gtpMainModel");
			this._initializePanelModel();

			var userId = sap.ushell.Container.getUser().getId();
			if (userId === "EX_OEZDEMC" || userId === "DEFAULT_USER" || userId === "EX_DOGUS" || userId === "EX_YILMAZO" || userId ===
				"EX_PIRIMO" || userId === "EX_GURPINS") {
				this.getView().byId("query").setVisible(false);
				this.getView().byId("query2").setVisible(false);
			} else {
				if (userId === "MATTHEC" || userId === "FISCHEA") {
					this.getView().byId("query").setVisible(false);
					this.getView().byId("query2").setVisible(false);
				} else {
					this.getView().byId("query").setVisible(false);
					this.getView().byId("query2").setVisible(false);
				}
			}

			this._globalModel = sap.ui.getCore().getModel("globalM");

			this.getView().byId("gtpTableId").setVisible(true);
			this.getView().byId("gtpTableId2").setVisible(false);
			this.getView().byId("gtpTableId3").setVisible(false);
			this._globalModel.setProperty("/tableOrder", "1");
			sap.ui.getCore().getModel("globalM").setProperty("/PageTitle", "(L/S Actual date)");
			//actual table
			this._globalModel.setProperty("/ls1_volume", 0);
			this._globalModel.setProperty("/ls1_value", 0);
			this._globalModel.setProperty("/ls2_volume", 0);
			this._globalModel.setProperty("/ls2_value", 0);
			this._globalModel.setProperty("/ls3_volume", 0);
			this._globalModel.setProperty("/ls3_value", 0);
			//quoted table
			this._globalModel.setProperty("/ls1_volume_quoted", 0);
			this._globalModel.setProperty("/ls1_value_quoted", 0);
			this._globalModel.setProperty("/lsOnQuotes_volume_quoted", 0);
			this._globalModel.setProperty("/lsOnQuotes_value_quoted", 0);
			this._globalModel.setProperty("/lsOnPlannedProd_volume_quoted", 0);
			this._globalModel.setProperty("/lsOnPlannedProd_value_quoted", 0);
			this._globalModel.setProperty("/lsOnPlannedPurc_volume_quoted", 0);
			this._globalModel.setProperty("/lsOnPlannedPurc_value_quoted", 0);

			var userId = sap.ushell.Container.getUser().getId();
			if (userId === "EX_GURPINS" || userId === "EX_DOGUS" || userId === "EX_PIRIMO" || userId === "DEFAULT_USER") {
				this._globalModel.setProperty("/commentVisible", true);
			}

			this._commentTableModel = this.getOwnerComponent().getModel("commentTableM");
			this._commentTableModel.setProperty("/tableRows", []);
			sap.ui.getCore().getModel("globalM").setProperty("/isOverview", false);
			this.getRouter().getRoute("Main").attachPatternMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("overViewBtnId");
			if (sap.ui.getCore().getModel("globalM").getProperty("/isOverview") === true)
				this.createToken();

		},

		onSortActual: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._tableModel.getProperty("/tableRows");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this._tableId.getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);

			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._tableModel);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// var oSorter2 = new sap.ui.model.Sorter("isSum", true, true);
			// var oSorter1 = new sap.ui.model.Sorter("RMW11ZP16", sortOrder !== "Ascending", true);

			// var oSorter1 = new sap.ui.model.Sorter({
			// 	path: 'isSum',
			// 	descending: true,
			// 	group: false
			// });
			// var oSorter2 = new sap.ui.model.Sorter({
			// 	path: 'RMW11ZP16',
			// 	descending: true,
			// 	group: false
			// });

			// oBinding.sort([oSorter1, oSorter2]);

		},

		onSortQuoted: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._tableModel.getProperty("/tableRows2");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this._tableId2.getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);

			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._tableModel);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("gtpTableId2").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._tableModel.setProperty("/tableRows2", notSumRow);
			// 	that.getView().byId("gtpTableId2").bindRows("tableM>/tableRows2");

			// }, 100)
		},

		onSortMonthly: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._tableModel.getProperty("/tableRows3");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this._tableId3.getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);

			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._tableModel);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("gtpTableId3").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._tableModel.setProperty("/tableRows3", notSumRow);
			// 	that.getView().byId("gtpTableId3").bindRows("tableM>/tableRows3");

			// }, 100)
		},

		onFilterActual: function (oEvent) {
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		onFilterQuoted: function (oEvent) {
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter2.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter2.val = oEvent.getParameter("value");
			} else {
				this.columnfilter2 = {};
			}

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		onValueHelpInit: function (oEvent) {
			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
		},

		onValueHelpInit2: function (oEvent) {
			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext2));
		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/B2B2", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/PLANT___T", "");
			this._panelModel.setProperty("/SUB_GROUP", "");
			this._panelModel.setProperty("/REGION", "");
			this._panelModel.setProperty("/COUNTRY", "");
			this._panelModel.setProperty("/PLANT_CLUSTER", "");
			this._panelModel.setProperty("/FRUIT_SPECIFIC", "");
			this._panelModel.setProperty("/CERTIFICATES", "");
			this._panelModel.setProperty("/COLLECTIVE_NUMBER", "");
			this._panelModel.setProperty("/FS1", "");
			this._panelModel.setProperty("/FS2", "");
			this._panelModel.setProperty("/FS3", "");
			// this._panelModel.setProperty("/FLAG_SOURCE", "");
			// this._panelModel.setProperty("/FLAG_JCOCKPIT", "");

			// 	var that = this;
			// setTimeout(
			// 	function () {
			// 		if (that.getView().byId("icJCockpit") !== undefined) {
			// 			that.getView().byId("icJCockpit").removeAllTokens();
			// 			that.getView().byId("icJCockpit").destroyTokens();
			// 			// if (sap.ui.getCore().getModel("globalM").getProperty("/QuoteManagementNavto") === undefined) {
			// 			that.getView().byId("icJCockpit").addToken(new sap.m.Token({
			// 				key: "X",
			// 				text: "True"
			// 			}));
			// 			that._panelModel.setProperty("/FLAG_JCOCKPIT", "X");
			// 			// }
			// 		}
			// 	}, 1000);

		},

		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
			}
		},

		onGo: function () {
			//Fruit -------------->  DEXMAT__RMW21ZH02   CUST-X.0000000001004
			//Substance group -------------->  DEXMAT__DSUBCHAR   02
			///sap/opu/odata/sap/ZBW_DEMO_SRV/getListSet?$filter=Plant eq '1300'
			var that = this;
			var filter = [];
			var plant = this._panelModel.getProperty("/PLANT");
			var fruit = this._panelModel.getProperty("/FRUIT");
			var subStance = this._panelModel.getProperty("/SUB_GROUP");
			var collectiveNumber = this._panelModel.getProperty("/COLLECTIVE_NUMBER");
			var plantArray = plant.split(",");
			var fruitArray = fruit.split(",");
			var subStanceArray = subStance.split(",");
			var collectiveNumberArray = collectiveNumber.split(",");
			if (plant !== "")
				plantArray.forEach(function (_plant) {
					filter.push(new sap.ui.model.Filter("Plant", FilterOperator.EQ, _plant));
				});
			if (fruit !== "")
				fruitArray.forEach(function (_fruit) {
					filter.push(new sap.ui.model.Filter("bicrmw21zh02", FilterOperator.EQ, _fruit));
				});
			if (subStance !== "")
				subStanceArray.forEach(function (_sub) {
					filter.push(new sap.ui.model.Filter("bicdsubchar", FilterOperator.EQ, _sub));
				});
			if (collectiveNumber !== "")
				collectiveNumberArray.forEach(function (_coll) {
					filter.push(new sap.ui.model.Filter("bicdcollnr", FilterOperator.EQ, _coll));
				});
			this._callList();
		},

		_callList: function () {

			var that = this;
			var funcName = "";

			if (that._tableId.getVisible()) {
				//funcName = "GET_DATA_MAIN";
				funcName = "GET_DATA_MAIN_BU_PBN";
			} else if (that._tableId2.getVisible()) {
				funcName = "GET_DATA_MAIN_BU_PBN";
			} else if (that._tableId3.getVisible()) {
				funcName = "GET_DATA_MAIN_MONTHLY";
			}

			var params = {
				"HANDLERPARAMS": {
					"FUNC": funcName
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};
			dbcontext.callServer(params, function (oModel) {
				var data = oModel.getData().RESULTS;
				var columnText = oModel.getData().DYNAMIC;
				if (that._tableId.getVisible()) {
					data.forEach(function (row) {
						row["RMW11ZE03___T"] = that.getDetermineOrganic(row["RMW11ZE03"]);
						row["RMW21ZE03___T"] = that.getDetermineHalal(row["RMW21ZE03"]);
						row["RMW31ZE03___T"] = that.getDetermineKosher(row["RMW31ZE03"]);
					});
					that._tableModel.setProperty("/tableRows", data);
					that.initallData = data;
					that._findTableKeys();
				} else if (that._tableId2.getVisible()) {
					data.forEach(function (row) {
						row["RMW11ZE03___T"] = that.getDetermineOrganic(row["RMW11ZE03"]);
						row["RMW21ZE03___T"] = that.getDetermineHalal(row["RMW21ZE03"]);
						row["RMW31ZE03___T"] = that.getDetermineKosher(row["RMW31ZE03"]);
					});
					that._tableModel.setProperty("/tableRows2", data);
					that.initallData2 = data;
					that._findTableKeys2();
				} else if (that._tableId3.getVisible()) {
					data.forEach(function (row) {
						row["A0MATERIAL__RMW11ZE03_T"] = that.getDetermineOrganic(row["A0MATERIAL__RMW11ZE03"]);
						row["A0MATERIAL__RMW21ZE03_T"] = that.getDetermineHalal(row["A0MATERIAL__RMW21ZE03"]);
						row["A0MATERIAL__RMW31ZE03_T"] = that.getDetermineKosher(row["A0MATERIAL__RMW31ZE03"]);
					});
					that._tableModel.setProperty("/tableRows3", data);
					that.initallData3 = data;
					that._findTableKeys3();
					that.setDynamiCol(columnText);

				}
			}, that);
		},

		onExport: function () {
			var data = [];
			var userId = sap.ushell.Container.getUser().getId();
			if (this._tableId.getVisible()) {
				var tableData = this._tableModel.getData();
				var copiedData = tableData.tableRows;
				var aIndices = this._tableId.getBinding("rows").aIndices;
			} else {
				if (this._tableId2.getVisible()) {
					var tableData = this._tableModel.getData();
					var copiedData = tableData.tableRows2;
					var aIndices = this._tableId2.getBinding("rows").aIndices;
				} else {
					var tableData = this._tableModel.getData();
					var copiedData = tableData.tableRows3;
					var aIndices = this._tableId3.getBinding("rows").aIndices;
				}
			}
			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			if (this._tableId.getVisible())
				excel._downloadExcel("OLS", this._tableId, data);
			else {
				if (this._tableId2.getVisible())
					excel._downloadExcel("OBU", this._tableId2, data);
				else
					excel._downloadExcel("OMONTHLY", this._tableId3, data);
			}
		},

		onSelectionChangeTableView: function (oEvent) {
			var key = oEvent.getParameter("item").getProperty("key");
			var table1 = this._tableModel.getProperty("/tableRows");
			var table2 = this._tableModel.getProperty("/tableRows2");
			var table3 = this._tableModel.getProperty("/tableRows3");
			if (key === "1") {
				this.getView().byId("sgmBtn").setSelectedKey("1");
				this.getView().byId("gtpTableId").setVisible(true);
				this._globalModel.setProperty("/tableOrder", "1");
				sap.ui.getCore().getModel("globalM").setProperty("/PageTitle", "(L/S Actual date)");
				this.getView().byId("gtpTableId2").setVisible(false);
				this.getView().byId("gtpTableId3").setVisible(false);
			}
			if (key === "2") {
				this.getView().byId("sgmBtn1").setSelectedKey("2");
				sap.ui.getCore().getModel("globalM").setProperty("/PageTitle", "(L/S Quoted)");
				this.getView().byId("gtpTableId").setVisible(false);
				this.getView().byId("gtpTableId2").setVisible(true);
				this._globalModel.setProperty("/tableOrder", "2");
				this.getView().byId("gtpTableId3").setVisible(false);
			}
			if (key === "3") {
				this.getView().byId("sgmBtn2").setSelectedKey("3");
				sap.ui.getCore().getModel("globalM").setProperty("/PageTitle", "(L/S Monthly)");
				this.getView().byId("gtpTableId").setVisible(false);
				this.getView().byId("gtpTableId2").setVisible(false);
				this.getView().byId("gtpTableId3").setVisible(true);
				this._globalModel.setProperty("/tableOrder", "3");
			}
		},

		onPressSACView: function () {
			var userId = sap.ushell.Container.getUser().getId();
			var selIndices = this._tableId.getSelectedIndices();
			if (this._tableId2.getVisible())
				selIndices = this._tableId2.getSelectedIndices();
			if (selIndices.length > 0) {
				// if (userId === "EX_OEZDEMC" || userId === "DEFAULT_USER" || userId === "EX_DOGUS" || userId === "EX_YILMAZO" || userId ===
				// 	"MATTHEC" || userId === "FISCHEA") {
				this.getView().byId("query").setVisible(false);
				this.getView().byId("query2").setVisible(true);
				// }
			} else
				sap.m.URLHelper.redirect(
					"https://doehler-test1.eu10.sapanalytics.cloud/sap/fpa/ui/app.html#;view_id=story;storyId=9261A304E6CBEA888CF0E091D2CAEE7B;mode=embed;f01Val=44059;f01Dim=MSCUSTOM2;f01Model=t.1:C8v4uf8xnsvanv2lqbyn9q5ji8",
					true);
		},

		onPressBWView: function () {
			var that = this;
			var selectedTable = this._tableId.getVisible() ? this._tableId : this._tableId2.getVisible() ? this._tableId2 : this._tableId3;
			var selIndices = selectedTable.getSelectedIndices(),
				newSelIndex = [],
				aIndices = selectedTable.getBinding().aIndices;

			selIndices.forEach(function (selIndex) {
				newSelIndex.push(aIndices[selIndex]);
			});

			selIndices = newSelIndex;

			var data = this._tableModel.getData().tableRows;
			if (this._tableId2.getVisible())
				data = this._tableModel.getData().tableRows2;
			if (this._tableId3.getVisible())
				data = this._tableModel.getData().tableRows3;

			var plant = "",
				subchar = "",
				fruit = "",
				coll = "",
				compCode = "";

			selIndices.forEach(function (ind, index) {
				if (that._tableId3.getVisible()) {
					data[ind]["DSUBCHAR"] = "";
					if (data[ind]["A0PLANT"] !== "")
						plant = plant + data[ind]["A0PLANT"] + ",";
					if (data[ind]["DSUBCHAR"] !== "")
						subchar = subchar + data[ind]["DSUBCHAR"] + ",";
					if (data[ind]["RMW21ZH02"] !== "")
						fruit = fruit + data[ind]["ADEXMAT__RMW21ZH02"] + ",";
					if (data[ind]["DCOLLNR"] !== "")
						coll = coll + data[ind]["ADEXMAT__DCOLLNR"] + ",";
					if (data[ind]["A0PLANT__0COMP_CODE"] !== "")
						compCode = compCode + data[ind]["A0PLANT__0COMP_CODE"] + ",";
				} else {

					if (data[ind]["PLANT"] !== "")
						plant = plant + data[ind]["PLANT"] + ",";
					if (data[ind]["DSUBCHAR"] !== "")
						subchar = subchar + data[ind]["DSUBCHAR"] + ",";
					if (data[ind]["RMW21ZH02"] !== "")
						fruit = fruit + data[ind]["RMW21ZH02"] + ",";
					if (data[ind]["DCOLLNR"] !== "")
						coll = coll + data[ind]["DCOLLNR"] + ",";
				}

			});

			if (selIndices.length === 0) {
				plant = this._panelModel.getProperty("/PLANT");
				subchar = this._panelModel.getProperty("/SUB_GROUP");
				fruit = this._panelModel.getProperty("/FRUIT");
				coll = this._panelModel.getProperty("/COLLECTIVE_NUMBER");
			}
			var queryCode = this._tableId.getVisible() ? "DJCVCP011_Q001" : this._tableId2.getVisible() ? "DJCVCP011_Q001" :
				"DJCVCP011_Q022";
			var HDSUBCHAR = subchar.split(",").join(";"),
				MSFRUIT2 = fruit.split(",").join(";"),
				MSWERK4 = "",
				MSSWERK = plant.split(",").join(";"),
				MSDCOLLN = coll.split(",").join(";"),
				MSCOMPCO = compCode.split(",").join(";"),
				MSDEXMAT = "";
			var hostName = sap.ui.getCore().getModel("globalM").getProperty("/BW_HOST_PORT");
			var url = hostName +
				"/irj/servlet/prt/portal/prtroot/pcd!3aportal_content!2fcom.sap.pct!2fplatform_add_ons!2fcom.sap.ip.bi!2fiViews!2fcom.sap.ip.bi.bex?QUERY=" +
				queryCode +
				"&VARIABLE_SCREEN='X'&DUMMY=1&BI_COMMAND_1-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=HDSUBCHAR&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
				HDSUBCHAR +
				"&BI_COMMAND_2-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSSWERK&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
				MSSWERK +
				"&BI_COMMAND_3-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSFRUIT2&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
				MSFRUIT2 +
				"&BI_COMMAND_4-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSDCOLLN&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
				MSDCOLLN;

			if (this._tableId3.getVisible()) {
				url = hostName +
					"/irj/servlet/prt/portal/prtroot/pcd!3aportal_content!2fcom.sap.pct!2fplatform_add_ons!2fcom.sap.ip.bi!2fiViews!2fcom.sap.ip.bi.bex?QUERY=" +
					queryCode +
					"&VARIABLE_SCREEN='X'&DUMMY=1&BI_COMMAND_1-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSSWERK&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_1-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					MSSWERK +
					"&BI_COMMAND_2-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSCOMPCO&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_2-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					MSCOMPCO +
					"&BI_COMMAND_3-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSFRUIT2&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_3-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					MSFRUIT2 +
					"&BI_COMMAND_4-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=HDSUBCHAR&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_4-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					HDSUBCHAR +
					"&BI_COMMAND_5-BI_COMMAND_TYPE=SET_VARIABLES_STATE&BI_COMMAND_5-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE=MSDCOLLN&BI_COMMAND_5-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE=VARIABLE_INPUT_STRING&BI_COMMAND_5-VARIABLE_VALUES-VARIABLE_VALUE_1-VARIABLE_TYPE-VARIABLE_INPUT_STRING=" +
					MSDCOLLN;
			}

			sap.m.URLHelper.redirect(url, true);

		},

		goToDetail: function () {
			this.getOwnerComponent().getRouter().navTo("Detail", {
				param1: "1"
			});
		},

		setDynamiCol: function (columnText) {
			this._ColumnMonthM = this.getOwnerComponent().getModel("ColumnMonthM");
			var that = this;

			columnText.forEach(function (item, index) {
				var colname = "/COL_";
				index++;
				colname = colname + index;
				that._ColumnMonthM.setProperty(colname, item.CAPTION);
			});
		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();

			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// if (that._tableId.getVisible()) {ö
					that._findTableKeys()
						// } else if (this._tableId2.getVisible()) {
					that._findTableKeys2();
					// } else if (this._tableId3.getVisible()) {
					that._findTableKeys3();
					// }
				},
				function (error) { /* code if some error */ }
			);

		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);

		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		oCC2: null,
		oCC3: null,
		currTableData: null,
		currTableData2: null,
		currTableData3: null,
		oTPC: null,
		oTPC2: null,
		oTPC3: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},
		onSettings2: function () {
			this.oTPC2.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC2 = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},
		onSettings3: function () {
			this.oTPC3.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC3 = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId;
			var oVM = this.getView().byId("overviewtableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}

					// Find Table Key
					if (thiz._globalModel.getProperty("/subTotalMainActual") === true) {
						thiz._findTableKeys("X");
						thiz._globalModel.setProperty("/subTotalMainActual", false);
					} else {
						thiz._findTableKeys();
					}

				});
			});
		},

		_findTableKeys: function (isSub) {
			var thiz = this;
			var oTable = this._tableId;

			/***************sub total*******************/
			var data = this.initallData;
			this.getView().byId("subTotalSelectId").setSelectedKey("");
			this._tableModel.setProperty("/tableRows", data);
			thiz.mainKeysForSubTotal = [{
				key: "",
				text: ""
			}];
			/***************sub total*******************/

			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						/***************sub total*******************/
						thiz.mainKeysForSubTotal.push({
							key: columns.data("key"),
							text: columns.getLabel().getText()
						});
						/***************sub total*******************/
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});
			/***************sub total*******************/
			thiz._globalModel.setProperty("/SubList", thiz.mainKeysForSubTotal);
			/***************sub total*******************/
			if (thiz.mainTableKeys.length)
				thiz.onPressAggregation(isSub);
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys();
		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		///for second view
		initVariant2: function () {
			var thiz = this;
			var oTable = this._tableId2;
			var oVM = this.getView().byId("overviewtableVMId2");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText2, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC2 = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText2);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData2 = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC2 = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData2 = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}

					// Find Table Key
					thiz._findTableKeys2();

				});
			});
		},

		_findTableKeys2: function () {
			var thiz = this;
			var oTable = this._tableId2;
			thiz.mainTableKeys2 = [];
			thiz.commentTableKeys2 = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys2.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys2.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys2.length)
				thiz.onPressAggregation2();
		},

		/* on select variant */
		onSelectVariant2: function (oEvent) {
			var tableId = this._tableIdText2;
			var oTable = this._tableId2;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys2();
		},
		/* on save new variant */
		onSaveVariant2: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText2;
			var oTable = this._tableId2;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData2) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData2,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM2: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText2;
			var ovar = oCC.getItemValue(tableId2);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},

		//for third view (monthly)
		initVariant3: function () {
			var thiz = this;
			var oTable = this._tableId3;
			var oVM = this.getView().byId("overviewtableVMId3");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText3, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC3 = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText3);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData3 = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC3 = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData3 = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}

					// Find Table Key
					thiz._findTableKeys3();

				});
			});
		},

		_findTableKeys3: function () {
			var thiz = this;
			var oTable = this._tableId3;
			thiz.mainTableKeys3 = [];
			thiz.commentTableKeys3 = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys3.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys3.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys3.length)
				thiz.onPressAggregation3();
		},

		/* on select variant */
		onSelectVariant3: function (oEvent) {
			var tableId = this._tableIdText3;
			var oTable = this._tableId3;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys3();
		},
		/* on save new variant */
		onSaveVariant3: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText3;
			var oTable = this._tableId3;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData3) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData3,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM3: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText3;
			var ovar = oCC.getItemValue(tableId3);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},

		//************************************************table variant codes**************************************

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdOverview");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("panelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isOverview", true);
			sap.ui.getCore().getModel("globalM").setProperty("/oviewInit", true);
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("panelM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantOverview: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("panelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantOverview: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("panelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVariantOverview: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1) {
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined)
				this._updatePanelData(fromData, sap.ui.getCore().getModel("panelM"));
			var aControls = this.getView().byId("searchGridId").getContent();
			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));

					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {
									if (value != "") {
										if (item.getFieldName() === "FRUIT") {
											const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
											item.addToken(new sap.m.Token({
												key: value,
												text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
											}));
										} else {
											item.addToken(new sap.m.Token({
												key: value,
												text: value
											}));
										}
									}
								});

							}
						}
					});
				}, 1000);

		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () { // TODO: 
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			this.initVariant();
			this.initVariant2();
			this.initVariant3();
			this.initSearchVariant();
		},

		onPressAggregation: function (isSub) {
			this.convertGroup(this.mainTableKeys);
			this.collectData(isSub);
		},

		onPressAggregation2: function () {
			this.convertGroup2(this.mainTableKeys2);
			this.collectData2();
		},

		onPressAggregation3: function () {
			this.convertGroup3(this.mainTableKeys3);
			this.collectData3();
		},

		convertGroup: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData, groupCols);
			this.groupData = groupData;
		},

		convertGroup2: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData2, groupCols);
			this.groupData2 = groupData;
		},

		convertGroup3: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData3, groupCols);
			this.groupData3 = groupData;
		},

		collectData: function (isSub) {
			var newCollectedData = [];
			var that = this;
			$.each(this.groupData, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.HAS_COMMENT = false;

					newEntry.COUNTRY = k.COUNTRY;
					newEntry.DSDREGION = k.DSDREGION;
					newEntry.DZZB2B_L2 = k.DZZB2B_L2;
					newEntry.DZZB2B_L2___T = k.DZZB2B_L2___T;
					newEntry.RMW11ZP16 = k.RMW11ZP16;
					newEntry.RMW11ZP16___T = k.RMW11ZP16___T;
					newEntry.MATERIAL = k.MATERIAL;

					newEntry.ITEM_CATEG = k.ITEM_CATEG;
					newEntry.ITEM_CATEG___T = k.ITEM_CATEG___T;

					newEntry.STOCKTYPE = k.STOCKTYPE;
					newEntry.STOCKTYPE___T = k.STOCKTYPE___T;

					newEntry.PLANT = k.PLANT;
					newEntry.PLANT___T = k.PLANT___T;
					newEntry.RMW21ZH02 = k.RMW21ZH02;
					newEntry.RMW21ZH02___T = k.RMW21ZH02___T;
					newEntry.DSUBCHAR = k.DSUBCHAR;
					newEntry.DSUBCHAR___T = k.DSUBCHAR___T;
					newEntry.DCOLLNR = k.DCOLLNR;
					newEntry.DCOLLNR___T = k.DCOLLNR___T;

					newEntry.RMW11ZE03 = k.RMW11ZE03;
					newEntry.RMW11ZE03___T = k.RMW11ZE03___T;
					newEntry.RMW21ZE03 = k.RMW21ZE03;
					newEntry.RMW21ZE03___T = k.RMW21ZE03___T;
					newEntry.RMW31ZE03 = k.RMW31ZE03;
					newEntry.RMW31ZE03___T = k.RMW31ZE03___T;

					newEntry.DFRTSPC1 = k.DFRTSPC1;
					newEntry.DFRTSPC1___T = k.DFRTSPC1___T;
					newEntry.DFRTSPC2 = k.DFRTSPC2;
					newEntry.DFRTSPC2___T = k.DFRTSPC2___T;
					newEntry.DFRTSPC3 = k.DFRTSPC3;
					newEntry.DFRTSPC3___T = k.DFRTSPC3___T;

					newEntry.DJCICFLAG = k.DJCICFLAG;
					newEntry.DJCICFLAG___T = k.DJCICFLAG___T;

					newEntry.DJCDAD091_LONGSHORTQUOTES = newEntry.DJCDAD091_LONGSHORTQUOTES === undefined ? 0 : newEntry.DJCDAD091_LONGSHORTQUOTES;
					newEntry.DJCDAD091_LONGSHORTQUOTES = (newEntry.DJCDAD091_LONGSHORTQUOTES);
					if (k.DJCDAD091_LONGSHORTQUOTES === undefined) k.DJCDAD091_LONGSHORTQUOTES = 0;
					newEntry.DJCDAD091_LONGSHORTQUOTES = (k.DJCDAD091_LONGSHORTQUOTES) + newEntry.DJCDAD091_LONGSHORTQUOTES;

					newEntry.DJCDAD091_LONGSHORTRFQ = newEntry.DJCDAD091_LONGSHORTRFQ === undefined ? 0 : newEntry.DJCDAD091_LONGSHORTRFQ;
					newEntry.DJCDAD091_LONGSHORTRFQ = (newEntry.DJCDAD091_LONGSHORTRFQ);
					if (k.DJCDAD091_LONGSHORTRFQ === undefined) k.DJCDAD091_LONGSHORTRFQ = 0;
					newEntry.DJCDAD091_LONGSHORTRFQ = (k.DJCDAD091_LONGSHORTRFQ) + newEntry.DJCDAD091_LONGSHORTRFQ;

					newEntry.DJCDAD091_OPENQUOTES = newEntry.DJCDAD091_OPENQUOTES === undefined ? 0 : newEntry.DJCDAD091_OPENQUOTES;
					newEntry.DJCDAD091_OPENQUOTES = (newEntry.DJCDAD091_OPENQUOTES);
					if (k.DJCDAD091_OPENQUOTES === undefined) k.DJCDAD091_OPENQUOTES = 0;
					newEntry.DJCDAD091_OPENQUOTES = (k.DJCDAD091_OPENQUOTES) + newEntry.DJCDAD091_OPENQUOTES;

					newEntry.DJCDAD091_OPENRFQS = newEntry.DJCDAD091_OPENRFQS === undefined ? 0 : newEntry.DJCDAD091_OPENRFQS;
					newEntry.DJCDAD091_OPENRFQS = (newEntry.DJCDAD091_OPENRFQS);
					if (k.DJCDAD091_OPENRFQS === undefined) k.DJCDAD091_OPENRFQS = 0;
					newEntry.DJCDAD091_OPENRFQS = (k.DJCDAD091_OPENRFQS) + newEntry.DJCDAD091_OPENRFQS;

					newEntry.DJCDAD091_OPENQUOTESWON = (newEntry.DJCDAD091_OPENQUOTESWON);
					if (newEntry.DJCDAD091_OPENQUOTESWON === undefined) newEntry.DJCDAD091_OPENQUOTESWON = 0;
					newEntry.DJCDAD091_OPENQUOTESWON = (k.DJCDAD091_OPENQUOTESWON) + newEntry.DJCDAD091_OPENQUOTESWON;

					newEntry.DJCDAD091_TBLOCKEDSTOCKS = (newEntry.DJCDAD091_TBLOCKEDSTOCKS);
					if (newEntry.DJCDAD091_TBLOCKEDSTOCKS === undefined) newEntry.DJCDAD091_TBLOCKEDSTOCKS = 0;
					newEntry.DJCDAD091_TBLOCKEDSTOCKS = (k.DJCDAD091_TBLOCKEDSTOCKS) + newEntry.DJCDAD091_TBLOCKEDSTOCKS;

					//Aggregations
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = newEntry.DJCDAD091_ACTUAL_CONSUMPTION === undefined ? 0 : newEntry.DJCDAD091_ACTUAL_CONSUMPTION;
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = (newEntry.DJCDAD091_ACTUAL_CONSUMPTION);
					if (k.DJCDAD091_ACTUAL_CONSUMPTION === undefined) k.DJCDAD091_ACTUAL_CONSUMPTION = 0;
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = (k.DJCDAD091_ACTUAL_CONSUMPTION) + newEntry.DJCDAD091_ACTUAL_CONSUMPTION;
					newEntry.DJCDAD091_CONSUMPTION = newEntry.DJCDAD091_CONSUMPTION === undefined ? 0 : newEntry.DJCDAD091_CONSUMPTION;
					newEntry.DJCDAD091_CONSUMPTION = (newEntry.DJCDAD091_CONSUMPTION);
					if (k.DJCDAD091_CONSUMPTION === undefined) k.DJCDAD091_CONSUMPTION = 0;
					newEntry.DJCDAD091_CONSUMPTION = (k.DJCDAD091_CONSUMPTION) + newEntry.DJCDAD091_CONSUMPTION;
					newEntry.DJCDAD091_STOCK_RAW_MAT = newEntry.DJCDAD091_STOCK_RAW_MAT === undefined ? 0 : newEntry.DJCDAD091_STOCK_RAW_MAT;
					newEntry.DJCDAD091_STOCK_RAW_MAT = (newEntry.DJCDAD091_STOCK_RAW_MAT);
					if (k.DJCDAD091_STOCK_RAW_MAT === undefined) k.DJCDAD091_STOCK_RAW_MAT = 0;
					newEntry.DJCDAD091_STOCK_RAW_MAT = (k.DJCDAD091_STOCK_RAW_MAT) + newEntry.DJCDAD091_STOCK_RAW_MAT;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = newEntry.DJCDAD091_STOCK_FINISHED_PROD === undefined ? 0 : newEntry.DJCDAD091_STOCK_FINISHED_PROD;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = (newEntry.DJCDAD091_STOCK_FINISHED_PROD);
					if (k.DJCDAD091_STOCK_FINISHED_PROD === undefined) k.DJCDAD091_STOCK_FINISHED_PROD = 0;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = (k.DJCDAD091_STOCK_FINISHED_PROD) + newEntry.DJCDAD091_STOCK_FINISHED_PROD;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = newEntry.DJCDAD091_STOCK_SEMI_MAT === undefined ? 0 : newEntry.DJCDAD091_STOCK_SEMI_MAT;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = (newEntry.DJCDAD091_STOCK_SEMI_MAT);
					if (k.DJCDAD091_STOCK_SEMI_MAT === undefined) k.DJCDAD091_STOCK_SEMI_MAT = 0;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = (k.DJCDAD091_STOCK_SEMI_MAT) + newEntry.DJCDAD091_STOCK_SEMI_MAT;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = newEntry.DJCDAD091_PURCONTRACT_CALLOFF === undefined ? 0 : newEntry.DJCDAD091_PURCONTRACT_CALLOFF;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = (newEntry.DJCDAD091_PURCONTRACT_CALLOFF);
					if (k.DJCDAD091_PURCONTRACT_CALLOFF === undefined) k.DJCDAD091_PURCONTRACT_CALLOFF = 0;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = (k.DJCDAD091_PURCONTRACT_CALLOFF) + newEntry.DJCDAD091_PURCONTRACT_CALLOFF;
					newEntry.DJCDAD091_PURCH_OPENQUAN = newEntry.DJCDAD091_PURCH_OPENQUAN === undefined ? 0 : newEntry.DJCDAD091_PURCH_OPENQUAN;
					newEntry.DJCDAD091_PURCH_OPENQUAN = (newEntry.DJCDAD091_PURCH_OPENQUAN);
					if (k.DJCDAD091_PURCH_OPENQUAN === undefined) k.DJCDAD091_PURCH_OPENQUAN = 0;
					newEntry.DJCDAD091_PURCH_OPENQUAN = (k.DJCDAD091_PURCH_OPENQUAN) + newEntry.DJCDAD091_PURCH_OPENQUAN;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = newEntry.DJCDAD091_SALESCONTR_CALLOFF === undefined ? 0 : newEntry.DJCDAD091_SALESCONTR_CALLOFF;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = (newEntry.DJCDAD091_SALESCONTR_CALLOFF);
					if (k.DJCDAD091_SALESCONTR_CALLOFF === undefined) k.DJCDAD091_SALESCONTR_CALLOFF = 0;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = (k.DJCDAD091_SALESCONTR_CALLOFF) + newEntry.DJCDAD091_SALESCONTR_CALLOFF;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = newEntry.DJCDAD091_SALESCONTR_OPENQUAN === undefined ? 0 : newEntry.DJCDAD091_SALESCONTR_OPENQUAN;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = (newEntry.DJCDAD091_SALESCONTR_OPENQUAN);
					if (k.DJCDAD091_SALESCONTR_OPENQUAN === undefined) k.DJCDAD091_SALESCONTR_OPENQUAN = 0;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = (k.DJCDAD091_SALESCONTR_OPENQUAN) + newEntry.DJCDAD091_SALESCONTR_OPENQUAN;
					newEntry.DJCDAD091_NONCONTRAC = newEntry.DJCDAD091_NONCONTRAC === undefined ? 0 : newEntry.DJCDAD091_NONCONTRAC;
					newEntry.DJCDAD091_NONCONTRAC = (newEntry.DJCDAD091_NONCONTRAC);
					if (k.DJCDAD091_NONCONTRAC === undefined) k.DJCDAD091_NONCONTRAC = 0;
					newEntry.DJCDAD091_NONCONTRAC = (k.DJCDAD091_NONCONTRAC) + newEntry.DJCDAD091_NONCONTRAC;
					newEntry.DJCDAD091_FORECAST_CONTRACT = newEntry.DJCDAD091_FORECAST_CONTRACT === undefined ? 0 : newEntry.DJCDAD091_FORECAST_CONTRACT;
					newEntry.DJCDAD091_FORECAST_CONTRACT = (newEntry.DJCDAD091_FORECAST_CONTRACT);
					if (k.DJCDAD091_FORECAST_CONTRACT === undefined) k.DJCDAD091_FORECAST_CONTRACT = 0;
					newEntry.DJCDAD091_FORECAST_CONTRACT = (k.DJCDAD091_FORECAST_CONTRACT) + newEntry.DJCDAD091_FORECAST_CONTRACT;

					//formulas
					newEntry.DJCDAD091_TOTALSTOCKS = newEntry.DJCDAD091_TOTALSTOCKS === undefined ? 0 : newEntry.DJCDAD091_TOTALSTOCKS;
					newEntry.DJCDAD091_TOTALSTOCKS = (newEntry.DJCDAD091_TOTALSTOCKS);

					newEntry.DJCDAD091_TOTALSTOCKS = newEntry.DJCDAD091_TOTALSTOCKS + (k.DJCDAD091_STOCK_RAW_MAT) +
						(k.DJCDAD091_STOCK_SEMI_MAT) +
						(k.DJCDAD091_STOCK_FINISHED_PROD);
					newEntry.DJCDAD091_TOTAL_OPENPURCH = newEntry.DJCDAD091_TOTAL_OPENPURCH === undefined ? 0 : newEntry.DJCDAD091_TOTAL_OPENPURCH;
					newEntry.DJCDAD091_TOTAL_OPENPURCH = (newEntry.DJCDAD091_TOTAL_OPENPURCH);
					newEntry.DJCDAD091_TOTAL_OPENPURCH = newEntry.DJCDAD091_TOTAL_OPENPURCH + (k.DJCDAD091_PURCONTRACT_CALLOFF) +
						(k.DJCDAD091_PURCH_OPENQUAN);
					newEntry.DJCDAD091_AVAILABILITY = newEntry.DJCDAD091_AVAILABILITY === undefined ? 0 : newEntry.DJCDAD091_AVAILABILITY;
					newEntry.DJCDAD091_AVAILABILITY = (newEntry.DJCDAD091_AVAILABILITY);
					newEntry.DJCDAD091_AVAILABILITY = newEntry.DJCDAD091_AVAILABILITY + (k.DJCDAD091_TOTALSTOCKS) +
						(k.DJCDAD091_TOTAL_OPENPURCH);
					newEntry.DJCDAD091_LONGSHORT = newEntry.DJCDAD091_LONGSHORT === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT;
					newEntry.DJCDAD091_LONGSHORT = (newEntry.DJCDAD091_LONGSHORT);
					newEntry.DJCDAD091_LONGSHORT = newEntry.DJCDAD091_LONGSHORT + ((k.DJCDAD091_AVAILABILITY) - (k
							.DJCDAD091_SALESCONTR_CALLOFF) -
						(k.DJCDAD091_SALESCONTR_OPENQUAN) - (k.DJCDAD091_OPENQUOTESWON));
					newEntry.DJCDAD091_LONGSHORT2 = newEntry.DJCDAD091_LONGSHORT2 === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT2;
					newEntry.DJCDAD091_LONGSHORT2 = (newEntry.DJCDAD091_LONGSHORT2);
					newEntry.DJCDAD091_LONGSHORT2 = newEntry.DJCDAD091_LONGSHORT2 + ((k.DJCDAD091_LONGSHORT) - (k.DJCDAD091_NONCONTRAC));
					newEntry.DJCDAD091_LONGSHORT3 = newEntry.DJCDAD091_LONGSHORT3 === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT3;
					newEntry.DJCDAD091_LONGSHORT3 = (newEntry.DJCDAD091_LONGSHORT3);
					newEntry.DJCDAD091_LONGSHORT3 = newEntry.DJCDAD091_LONGSHORT3 + ((k.DJCDAD091_LONGSHORT2) - (k.DJCDAD091_FORECAST_CONTRACT));
					newEntry.DJCDAD091_TOTALDEMAND = newEntry.DJCDAD091_TOTALDEMAND === undefined ? 0 : newEntry.DJCDAD091_TOTALDEMAND;
					newEntry.DJCDAD091_TOTALDEMAND = (newEntry.DJCDAD091_TOTALDEMAND);
					newEntry.DJCDAD091_TOTALDEMAND = newEntry.DJCDAD091_TOTALDEMAND + (k.DJCDAD091_SALESCONTR_CALLOFF) + (k.DJCDAD091_SALESCONTR_OPENQUAN) +
						(k.DJCDAD091_NONCONTRAC) + (k.DJCDAD091_FORECAST_CONTRACT);

				});
				newEntry = that._hasComment(newEntry, 1);
				newEntry["order"] = newCollectedData.length - 1;
				newCollectedData.push(newEntry);
			});
			var totalRow = this.setTotalRow(newCollectedData, "", this.columnfilter);
			this._globalModel.setProperty("/ls1_volume", totalRow.DJCDAD091_LONGSHORT);
			this._globalModel.setProperty("/ls1_value", 0);
			this._globalModel.setProperty("/ls2_volume", totalRow.DJCDAD091_LONGSHORT2);
			this._globalModel.setProperty("/ls2_value", 0);
			this._globalModel.setProperty("/ls3_volume", totalRow.DJCDAD091_LONGSHORT3);
			this._globalModel.setProperty("/ls3_value", 0);
			newCollectedData.unshift(totalRow);

			newCollectedData.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this.initallDataSubTotal = newCollectedData;

			this._tableModel.setProperty("/tableRows", newCollectedData);
			if (isSub === "X") {
				this._adjustmentForSubTotalActual(true);
				this._globalModel.setProperty("/subTotalMainActual", true);
				this.convertGroupSubTotal(["PLANT"]);
				this.collectDataSubTotal();
			} else
				this._adjustmentForSubTotalActual(false);
		},

		collectDataSubTotal: function (key) {
			var that = this;
			var table = this.getView().byId("gtpTableId");
			var newCollectedData = [];
			var newLines = [];
			var groupLength = 0;
			var orderNo = 0;
			var originalData = this._tableModel.getProperty("/tableRows");
			debugger;
			$.each(this.groupDataSubTotal, function (i, v) {

				groupLength = v.length;
				if (i !== '{' + '"' + key + '"' + ':""}') {
					var total = 0;
					var keyGrouped = JSON.parse(i).PLANT;
					var newEntry = {};
					orderNo = 0;
					$.each(v, function (j, k) {

						//originalData.forEach(function (row) {
						Object.keys(k).forEach(function (key) {
							if (typeof k[key] === "number") {
								if (newEntry[key] === undefined) {
									newEntry[key] = 0;
								}
								newEntry[key] += k[key];
							} else {
								newEntry[key] = "";
							}
						});
						//	});

						// total += k["DJCDAD091_STOCK_FINISHED_PROD"];
						// if (orderNo > k["order"])
						// 	orderNo = k["order"];
					});
					var line = JSON.parse(JSON.stringify(v[0]));
					line = {};
					line = newEntry;
					line[key] = v[0][key];
					line[key + "___T"] = v[0][key + "___T"];
					line["sub"] = "X";
					// line["DJCDAD091_STOCK_FINISHED_PROD_SUB"] = total;
					Object.keys(line).forEach(function (keyline) {
						if (typeof line[keyline] === "number") {
							line[keyline] = parseFloat(line[keyline].toFixed(0));
						}

					});
					newLines.push(line);
				}
			});

			originalData = originalData.concat(newLines);

			function sortByKey(array, key) {
				return array.sort(function (a, b) {
					var x = a[key];
					var y = b[key];
					return ((x > y) ? 1 : ((x < y) ? -1 : 0));
				});
			}
			originalData = sortByKey(originalData, key);
			// this._tableModel.setProperty("/tableRows", originalData);
			this._tableModel.setProperty("/tableRows", originalData);
		},

		_adjustmentForSubTotalActual: function (state) {
			var table = this.getView().byId("gtpTableId");
			var originalData = this._tableModel.getProperty("/tableRows");
			var allColumns = table.getColumns();
			var column = this.getView().byId("act_col_BU_PBN_1");
			var len = allColumns.length;
			for (var i = 0; i < len; i++) {
				allColumns[i].setSorted(false);
			}

			table.getBinding("rows").filter(null).sort(null);
		},

		onChangeSubTotal: function (oEvent) {
			this._adjustmentForSubTotalActual();
			var key = oEvent.getParameter("selectedItem").getProperty("key");
			var data = this._tableModel.getProperty("/tableRows");
			data = data.filter(function (m) {
				return m["sub"] !== "X";
			});
			this._tableModel.setProperty("/tableRows", data);
			if (key === "") return;
			debugger;

			this._tableModel.setProperty("/tableRows", data);
			this.convertGroupSubTotal([key]);
			this.collectDataSubTotal(key);

		},

		convertGroupSubTotal: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallDataSubTotal, groupCols);
			this.groupDataSubTotal = groupData;
		},

		collectData2: function () {
			var that = this;
			var newCollectedData2 = [];
			var plantVisible = this.getView().byId("col_BU_PBN_1").getVisible();
			$.each(this.groupData2, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.COUNTRY = k.COUNTRY;
					newEntry.DSDREGION = k.DSDREGION;
					newEntry.DZZB2B_L2 = k.DZZB2B_L2;
					newEntry.DZZB2B_L2___T = k.DZZB2B_L2___T;
					newEntry.RMW11ZP16 = k.RMW11ZP16;
					newEntry.RMW11ZP16___T = k.RMW11ZP16___T;
					newEntry.MATERIAL = k.MATERIAL;

					newEntry.ITEM_CATEG = k.ITEM_CATEG;
					newEntry.ITEM_CATEG___T = k.ITEM_CATEG___T;

					newEntry.STOCKTYPE = k.STOCKTYPE;
					newEntry.STOCKTYPE___T = k.STOCKTYPE___T;

					newEntry.PLANT = k.PLANT;
					newEntry.PLANT___T = k.PLANT___T;
					newEntry.RMW21ZH02 = k.RMW21ZH02;
					newEntry.RMW21ZH02___T = k.RMW21ZH02___T;
					newEntry.DSUBCHAR = k.DSUBCHAR;
					newEntry.DSUBCHAR___T = k.DSUBCHAR___T;
					newEntry.DCOLLNR = k.DCOLLNR;
					newEntry.DCOLLNR___T = k.DCOLLNR___T;

					newEntry.DFRTSPC1 = k.DFRTSPC1;
					newEntry.DFRTSPC1___T = k.DFRTSPC1___T;
					newEntry.DFRTSPC2 = k.DFRTSPC2;
					newEntry.DFRTSPC2___T = k.DFRTSPC2___T;
					newEntry.DFRTSPC3 = k.DFRTSPC3;
					newEntry.DFRTSPC3___T = k.DFRTSPC3___T;

					newEntry.RMW11ZE03 = k.RMW11ZE03;
					newEntry.RMW11ZE03___T = k.RMW11ZE03___T;
					newEntry.RMW21ZE03 = k.RMW21ZE03;
					newEntry.RMW21ZE03___T = k.RMW21ZE03___T;
					newEntry.RMW31ZE03 = k.RMW31ZE03;
					newEntry.RMW31ZE03___T = k.RMW31ZE03___T;

					newEntry.DJCICFLAG = k.DJCICFLAG;
					newEntry.DJCICFLAG___T = k.DJCICFLAG___T;

					// if (k.S_ORD_ITEM === "000000") k.S_ORD_ITEM = "";
					// if (plantVisible) {
					// 	if (newEntry.DOC_NUMBER === undefined || newEntry.DOC_NUMBER === "") {
					newEntry.DOC_NUMBER = k.DOC_NUMBER;
					newEntry.S_ORD_ITEM = k.S_ORD_ITEM;
					// }
					// }

					newEntry.DJCDAD091_LONGSHORTQUOTES = newEntry.DJCDAD091_LONGSHORTQUOTES === undefined ? 0 : newEntry.DJCDAD091_LONGSHORTQUOTES;
					newEntry.DJCDAD091_LONGSHORTQUOTES = (newEntry.DJCDAD091_LONGSHORTQUOTES);
					if (k.DJCDAD091_LONGSHORTQUOTES === undefined) k.DJCDAD091_LONGSHORTQUOTES = 0;
					newEntry.DJCDAD091_LONGSHORTQUOTES = (k.DJCDAD091_LONGSHORTQUOTES) + newEntry.DJCDAD091_LONGSHORTQUOTES;

					newEntry.DJCDAD091_LONGSHORTRFQ = newEntry.DJCDAD091_LONGSHORTRFQ === undefined ? 0 : newEntry.DJCDAD091_LONGSHORTRFQ;
					newEntry.DJCDAD091_LONGSHORTRFQ = (newEntry.DJCDAD091_LONGSHORTRFQ);
					if (k.DJCDAD091_LONGSHORTRFQ === undefined) k.DJCDAD091_LONGSHORTRFQ = 0;
					newEntry.DJCDAD091_LONGSHORTRFQ = (k.DJCDAD091_LONGSHORTRFQ) + newEntry.DJCDAD091_LONGSHORTRFQ;

					newEntry.DJCDAD091_OPENQUOTES = newEntry.DJCDAD091_OPENQUOTES === undefined ? 0 : newEntry.DJCDAD091_OPENQUOTES;
					newEntry.DJCDAD091_OPENQUOTES = (newEntry.DJCDAD091_OPENQUOTES);
					if (k.DJCDAD091_OPENQUOTES === undefined) k.DJCDAD091_OPENQUOTES = 0;
					newEntry.DJCDAD091_OPENQUOTES = (k.DJCDAD091_OPENQUOTES) + newEntry.DJCDAD091_OPENQUOTES;

					newEntry.DJCDAD091_OPENRFQS = newEntry.DJCDAD091_OPENRFQS === undefined ? 0 : newEntry.DJCDAD091_OPENRFQS;
					newEntry.DJCDAD091_OPENRFQS = (newEntry.DJCDAD091_OPENRFQS);
					if (k.DJCDAD091_OPENRFQS === undefined) k.DJCDAD091_OPENRFQS = 0;
					newEntry.DJCDAD091_OPENRFQS = (k.DJCDAD091_OPENRFQS) + newEntry.DJCDAD091_OPENRFQS;

					newEntry.DJCDAD091_OPENQUOTESWON = (newEntry.DJCDAD091_OPENQUOTESWON);
					if (newEntry.DJCDAD091_OPENQUOTESWON === undefined) newEntry.DJCDAD091_OPENQUOTESWON = 0;
					newEntry.DJCDAD091_OPENQUOTESWON = (k.DJCDAD091_OPENQUOTESWON) + newEntry.DJCDAD091_OPENQUOTESWON;

					newEntry.DJCDAD091_TBLOCKEDSTOCKS = (newEntry.DJCDAD091_TBLOCKEDSTOCKS);
					if (newEntry.DJCDAD091_TBLOCKEDSTOCKS === undefined) newEntry.DJCDAD091_TBLOCKEDSTOCKS = 0;
					newEntry.DJCDAD091_TBLOCKEDSTOCKS = (k.DJCDAD091_TBLOCKEDSTOCKS) + newEntry.DJCDAD091_TBLOCKEDSTOCKS;

					//Aggregations
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = newEntry.DJCDAD091_ACTUAL_CONSUMPTION === undefined ? 0 : newEntry.DJCDAD091_ACTUAL_CONSUMPTION;
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = (newEntry.DJCDAD091_ACTUAL_CONSUMPTION);
					if (k.DJCDAD091_ACTUAL_CONSUMPTION === undefined) k.DJCDAD091_ACTUAL_CONSUMPTION = 0;
					newEntry.DJCDAD091_ACTUAL_CONSUMPTION = (k.DJCDAD091_ACTUAL_CONSUMPTION) + newEntry.DJCDAD091_ACTUAL_CONSUMPTION;
					newEntry.DJCDAD091_CONSUMPTION = newEntry.DJCDAD091_CONSUMPTION === undefined ? 0 : newEntry.DJCDAD091_CONSUMPTION;
					newEntry.DJCDAD091_CONSUMPTION = (newEntry.DJCDAD091_CONSUMPTION);
					if (k.DJCDAD091_CONSUMPTION === undefined) k.DJCDAD091_CONSUMPTION = 0;
					newEntry.DJCDAD091_CONSUMPTION = (k.DJCDAD091_CONSUMPTION) + newEntry.DJCDAD091_CONSUMPTION;
					newEntry.DJCDAD091_STOCK_RAW_MAT = newEntry.DJCDAD091_STOCK_RAW_MAT === undefined ? 0 : newEntry.DJCDAD091_STOCK_RAW_MAT;
					newEntry.DJCDAD091_STOCK_RAW_MAT = (newEntry.DJCDAD091_STOCK_RAW_MAT);
					if (k.DJCDAD091_STOCK_RAW_MAT === undefined) k.DJCDAD091_STOCK_RAW_MAT = 0;
					newEntry.DJCDAD091_STOCK_RAW_MAT = (k.DJCDAD091_STOCK_RAW_MAT) + newEntry.DJCDAD091_STOCK_RAW_MAT;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = newEntry.DJCDAD091_STOCK_FINISHED_PROD === undefined ? 0 : newEntry.DJCDAD091_STOCK_FINISHED_PROD;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = (newEntry.DJCDAD091_STOCK_FINISHED_PROD);
					if (k.DJCDAD091_STOCK_FINISHED_PROD === undefined) k.DJCDAD091_STOCK_FINISHED_PROD = 0;
					newEntry.DJCDAD091_STOCK_FINISHED_PROD = (k.DJCDAD091_STOCK_FINISHED_PROD) + newEntry.DJCDAD091_STOCK_FINISHED_PROD;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = newEntry.DJCDAD091_STOCK_SEMI_MAT === undefined ? 0 : newEntry.DJCDAD091_STOCK_SEMI_MAT;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = (newEntry.DJCDAD091_STOCK_SEMI_MAT);
					if (k.DJCDAD091_STOCK_SEMI_MAT === undefined) k.DJCDAD091_STOCK_SEMI_MAT = 0;
					newEntry.DJCDAD091_STOCK_SEMI_MAT = (k.DJCDAD091_STOCK_SEMI_MAT) + newEntry.DJCDAD091_STOCK_SEMI_MAT;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = newEntry.DJCDAD091_PURCONTRACT_CALLOFF === undefined ? 0 : newEntry.DJCDAD091_PURCONTRACT_CALLOFF;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = (newEntry.DJCDAD091_PURCONTRACT_CALLOFF);
					if (k.DJCDAD091_PURCONTRACT_CALLOFF === undefined) k.DJCDAD091_PURCONTRACT_CALLOFF = 0;
					newEntry.DJCDAD091_PURCONTRACT_CALLOFF = (k.DJCDAD091_PURCONTRACT_CALLOFF) + newEntry.DJCDAD091_PURCONTRACT_CALLOFF;
					newEntry.DJCDAD091_PURCH_OPENQUAN = newEntry.DJCDAD091_PURCH_OPENQUAN === undefined ? 0 : newEntry.DJCDAD091_PURCH_OPENQUAN;
					newEntry.DJCDAD091_PURCH_OPENQUAN = (newEntry.DJCDAD091_PURCH_OPENQUAN);
					if (k.DJCDAD091_PURCH_OPENQUAN === undefined) k.DJCDAD091_PURCH_OPENQUAN = 0;
					newEntry.DJCDAD091_PURCH_OPENQUAN = (k.DJCDAD091_PURCH_OPENQUAN) + newEntry.DJCDAD091_PURCH_OPENQUAN;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = newEntry.DJCDAD091_SALESCONTR_CALLOFF === undefined ? 0 : newEntry.DJCDAD091_SALESCONTR_CALLOFF;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = (newEntry.DJCDAD091_SALESCONTR_CALLOFF);
					if (k.DJCDAD091_SALESCONTR_CALLOFF === undefined) k.DJCDAD091_SALESCONTR_CALLOFF = 0;
					newEntry.DJCDAD091_SALESCONTR_CALLOFF = (k.DJCDAD091_SALESCONTR_CALLOFF) + newEntry.DJCDAD091_SALESCONTR_CALLOFF;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = newEntry.DJCDAD091_SALESCONTR_OPENQUAN === undefined ? 0 : newEntry.DJCDAD091_SALESCONTR_OPENQUAN;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = (newEntry.DJCDAD091_SALESCONTR_OPENQUAN);
					if (k.DJCDAD091_SALESCONTR_OPENQUAN === undefined) k.DJCDAD091_SALESCONTR_OPENQUAN = 0;
					newEntry.DJCDAD091_SALESCONTR_OPENQUAN = (k.DJCDAD091_SALESCONTR_OPENQUAN) + newEntry.DJCDAD091_SALESCONTR_OPENQUAN;
					newEntry.DJCDAD091_NONCONTRAC = newEntry.DJCDAD091_NONCONTRAC === undefined ? 0 : newEntry.DJCDAD091_NONCONTRAC;
					newEntry.DJCDAD091_NONCONTRAC = (newEntry.DJCDAD091_NONCONTRAC);
					if (k.DJCDAD091_NONCONTRAC === undefined) k.DJCDAD091_NONCONTRAC = 0;
					newEntry.DJCDAD091_NONCONTRAC = (k.DJCDAD091_NONCONTRAC) + newEntry.DJCDAD091_NONCONTRAC;
					newEntry.DJCDAD091_FORECAST_CONTRACT = newEntry.DJCDAD091_FORECAST_CONTRACT === undefined ? 0 : newEntry.DJCDAD091_FORECAST_CONTRACT;
					newEntry.DJCDAD091_FORECAST_CONTRACT = (newEntry.DJCDAD091_FORECAST_CONTRACT);
					if (k.DJCDAD091_FORECAST_CONTRACT === undefined) k.DJCDAD091_FORECAST_CONTRACT = 0;
					newEntry.DJCDAD091_FORECAST_CONTRACT = (k.DJCDAD091_FORECAST_CONTRACT) + newEntry.DJCDAD091_FORECAST_CONTRACT;
					/*
						Descriptions	Tech. Name	Formula	Formula
						Total Stocks	TOTALSTOCKS	Stock Raw  Materials + Stock Semi  Finished +  Stock Finished  Products	STOCK_RAW_MAT + STOCK_SEMI_MAT + DJCDAD091_STOCK_FINISHED_PROD
									
						Total Open Purch. Contr.	TOTAL_OPENPURCH	(Purchase  Contracts  Call Offs + Orders) +  (Purchase Contracts Open Quantity)	DJCDAD091_PURCONTRACT_CALLOFF + PURCH_OPENQUAN
									
						Availability	AVAILABILITY	 Total  Stocks + Total Open Purch. Contr.	TOTALSTOCKS + TOTAL_OPENPURCH
									
						Long/Short 1	LONGSHORT	(Availability) -  (Sales  Contracts Call-Offs) -  (Sales  Contracts Open Quantity)	AVAILABILITY - DJCDAD091_SALESCONTR_CALLOFF - DJCDAD091_SALESCONTR_OPENQUAN
									
						Long/Short 2	LONGSHORT2	(Long/Short 1) - (Forecast non-Contract)	LONGSHORT - DJCDAD091_NONCONTRAC
									
						Long/Short 3	LONGSHORT3	(Long/Short 2) - (Forecast  Contract)	LONGSHORT2 - DJCDAD091_FORECAST_CONTRACT
									
						Total Demand	TOTALDEMAND	(Sales  Contracts Call-Offs) + ( Sales  Contracts Open Quantity) +  (Forecast non-Contract) + ( Forecast  Contract)	
						DJCDAD091_SALESCONTR_CALLOFF + DJCDAD091_SALESCONTR_OPENQUAN + DJCDAD091_NONCONTRAC + DJCDAD091_FORECAST_CONTRACT
							
						
						*/

					//formulas
					newEntry.DJCDAD091_TOTALSTOCKS = newEntry.DJCDAD091_TOTALSTOCKS === undefined ? 0 : newEntry.DJCDAD091_TOTALSTOCKS;
					newEntry.DJCDAD091_TOTALSTOCKS = (newEntry.DJCDAD091_TOTALSTOCKS);

					newEntry.DJCDAD091_TOTALSTOCKS = newEntry.DJCDAD091_TOTALSTOCKS + (k.DJCDAD091_STOCK_RAW_MAT) +
						(k.DJCDAD091_STOCK_SEMI_MAT) +
						(k.DJCDAD091_STOCK_FINISHED_PROD);
					newEntry.DJCDAD091_TOTAL_OPENPURCH = newEntry.DJCDAD091_TOTAL_OPENPURCH === undefined ? 0 : newEntry.DJCDAD091_TOTAL_OPENPURCH;
					newEntry.DJCDAD091_TOTAL_OPENPURCH = (newEntry.DJCDAD091_TOTAL_OPENPURCH);
					newEntry.DJCDAD091_TOTAL_OPENPURCH = newEntry.DJCDAD091_TOTAL_OPENPURCH + (k.DJCDAD091_PURCONTRACT_CALLOFF) +
						(k.DJCDAD091_PURCH_OPENQUAN);
					newEntry.DJCDAD091_AVAILABILITY = newEntry.DJCDAD091_AVAILABILITY === undefined ? 0 : newEntry.DJCDAD091_AVAILABILITY;
					newEntry.DJCDAD091_AVAILABILITY = (newEntry.DJCDAD091_AVAILABILITY);
					newEntry.DJCDAD091_AVAILABILITY = newEntry.DJCDAD091_AVAILABILITY + (k.DJCDAD091_TOTALSTOCKS) +
						(k.DJCDAD091_TOTAL_OPENPURCH);
					newEntry.DJCDAD091_LONGSHORT = newEntry.DJCDAD091_LONGSHORT === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT;
					newEntry.DJCDAD091_LONGSHORT = (newEntry.DJCDAD091_LONGSHORT);
					newEntry.DJCDAD091_LONGSHORT = newEntry.DJCDAD091_LONGSHORT + ((k.DJCDAD091_AVAILABILITY) - (k
							.DJCDAD091_SALESCONTR_CALLOFF) -
						(k.DJCDAD091_SALESCONTR_OPENQUAN) - (k.DJCDAD091_OPENQUOTESWON));
					newEntry.DJCDAD091_LONGSHORT2 = newEntry.DJCDAD091_LONGSHORT2 === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT2;
					newEntry.DJCDAD091_LONGSHORT2 = (newEntry.DJCDAD091_LONGSHORT2);
					newEntry.DJCDAD091_LONGSHORT2 = newEntry.DJCDAD091_LONGSHORT2 + ((k.DJCDAD091_LONGSHORT) - (k.DJCDAD091_NONCONTRAC));
					newEntry.DJCDAD091_LONGSHORT3 = newEntry.DJCDAD091_LONGSHORT3 === undefined ? 0 : newEntry.DJCDAD091_LONGSHORT3;
					newEntry.DJCDAD091_LONGSHORT3 = (newEntry.DJCDAD091_LONGSHORT3);
					newEntry.DJCDAD091_LONGSHORT3 = newEntry.DJCDAD091_LONGSHORT3 + ((k.DJCDAD091_LONGSHORT2) - (k.DJCDAD091_FORECAST_CONTRACT));
					newEntry.DJCDAD091_TOTALDEMAND = newEntry.DJCDAD091_TOTALDEMAND === undefined ? 0 : newEntry.DJCDAD091_TOTALDEMAND;
					newEntry.DJCDAD091_TOTALDEMAND = (newEntry.DJCDAD091_TOTALDEMAND);
					newEntry.DJCDAD091_TOTALDEMAND = newEntry.DJCDAD091_TOTALDEMAND + (k.DJCDAD091_SALESCONTR_CALLOFF) + (k.DJCDAD091_SALESCONTR_OPENQUAN) +
						(k.DJCDAD091_NONCONTRAC) + (k.DJCDAD091_FORECAST_CONTRACT);

				});
				newEntry = that._hasComment(newEntry, 2);
				newCollectedData2.push(newEntry);
			});
			var totalRow = this.setTotalRow(newCollectedData2, "", this.columnfilter2);
			this._globalModel.setProperty("/ls1_volume_quoted", totalRow.DJCDAD091_LONGSHORT);
			this._globalModel.setProperty("/ls1_value_quoted", 0);
			this._globalModel.setProperty("/lsOnQuotes_volume_quoted", totalRow.DJCDAD091_LONGSHORTQUOTES);
			this._globalModel.setProperty("/lsOnQuotes_value_quoted", 0);
			this._globalModel.setProperty("/lsOnPlannedProd_volume_quoted", 0);
			this._globalModel.setProperty("/lsOnPlannedProd_value_quoted", 0);
			this._globalModel.setProperty("/lsOnPlannedPurc_volume_quoted", 0);
			this._globalModel.setProperty("/lsOnPlannedPurc_value_quoted", 0);

			newCollectedData2.unshift(totalRow);
			newCollectedData2.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this._tableModel.setProperty("/tableRows2", newCollectedData2);
		},

		collectData3: function () {
			var that = this;
			var newCollectedData3 = [];
			$.each(this.groupData3, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.A0PLANT = k.A0PLANT;
					newEntry.A0PLANT_T = k.A0PLANT_T;
					newEntry.ADEXMAT__RMW21ZH02 = k.ADEXMAT__RMW21ZH02;
					newEntry.ADEXMAT__RMW21ZH02_T = k.ADEXMAT__RMW21ZH02_T;
					newEntry.ADEXMAT__RMW11ZP16 = k.ADEXMAT__RMW11ZP16;
					newEntry.ADEXMAT__RMW11ZP16_T = k.ADEXMAT__RMW11ZP16_T;
					newEntry.A0PLANT__0COMP_CODE = k.A0PLANT__0COMP_CODE;
					newEntry.A0PLANT__0COMP_CODE_T = k.A0PLANT__0COMP_CODE_T;
					newEntry.ADEXMAT__DCOLLNR = k.ADEXMAT__DCOLLNR;
					newEntry.ADEXMAT__DCOLLNR_T = k.ADEXMAT__DCOLLNR_T;

					newEntry.A0ITEM_CATEG = k.A0ITEM_CATEG;
					newEntry.A0ITEM_CATEG_T = k.A0ITEM_CATEG_T;
					newEntry.A0MATERIAL = k.A0MATERIAL;
					newEntry.A0MATERIAL_T = k.A0MATERIAL_T;
					newEntry.A0MATERIAL__DZZB2B_L1 = k.A0MATERIAL__DZZB2B_L1;
					newEntry.A0MATERIAL__DZZB2B_L1_T = k.A0MATERIAL__DZZB2B_L1_T;
					newEntry.A0CUSTOMER = k.A0CUSTOMER;
					newEntry.A0CUSTOMER_T = k.A0CUSTOMER_T;
					newEntry.A0CUSTOMER__DCUSTSEG = k.A0CUSTOMER__DCUSTSEG;
					newEntry.A0CUSTOMER__DCUSTSEG_T = k.A0CUSTOMER__DCUSTSEG_T;
					newEntry.A0CUSTOMER__DSDREGION = k.A0CUSTOMER__DSDREGION;
					newEntry.A0CUSTOMER__0COUNTRY = k.A0CUSTOMER__0COUNTRY;
					newEntry.A0MATERIAL__DZZB2B_L2 = k.A0MATERIAL__DZZB2B_L2;
					newEntry.A00O2TMMF5M4OK7IQA8Z2BGK0E = k.A00O2TMMF5M4OK7IQA8Z2BGK0E;
					newEntry.A00O2TMMF5M4OK7IQA8Z2BGK0E_T = k.A00O2TMMF5M4OK7IQA8Z2BGK0E_T;
					newEntry.ADEXMAT__DFRTSPC1 = k.ADEXMAT__DFRTSPC1;
					newEntry.ADEXMAT__DFRTSPC1_T = k.ADEXMAT__DFRTSPC1_T;
					newEntry.ADEXMAT__DFRTSPC2 = k.ADEXMAT__DFRTSPC2;
					newEntry.ADEXMAT__DFRTSPC2_T = k.ADEXMAT__DFRTSPC2_T;
					newEntry.ADEXMAT__DFRTSPC3 = k.ADEXMAT__DFRTSPC3;
					newEntry.ADEXMAT__DFRTSPC3_T = k.ADEXMAT__DFRTSPC3_T;

					newEntry.A0MATERIAL__RMW11ZE03 = k.A0MATERIAL__RMW11ZE03;
					newEntry.A0MATERIAL__RMW11ZE03_T = k.A0MATERIAL__RMW11ZE03_T;
					newEntry.A0MATERIAL__RMW21ZE03 = k.A0MATERIAL__RMW21ZE03;
					newEntry.A0MATERIAL__RMW21ZE03_T = k.A0MATERIAL__RMW21ZE03_T;
					newEntry.A0MATERIAL__RMW31ZE03 = k.A0MATERIAL__RMW31ZE03;
					newEntry.A0MATERIAL__RMW31ZE03_T = k.A0MATERIAL__RMW31ZE03_T;

					newEntry.ADEXMAT__DSUBCHAR = k.ADEXMAT__DSUBCHAR;
					newEntry.ADEXMAT__DSUBCHAR_T = k.ADEXMAT__DSUBCHAR_T;

					newEntry.ADJCICFLAG = k.ADJCICFLAG;
					newEntry.ADJCICFLAG_T = k.ADJCICFLAG_T;

					newEntry.A0ITEM_CATEG = k.A0ITEM_CATEG;
					newEntry.A0ITEM_CATEG_T = k.A0ITEM_CATEG_T;

					if (newEntry.COL_1 === undefined) newEntry.COL_1 = 0;
					if (newEntry.COL_2 === undefined) newEntry.COL_2 = 0;
					if (newEntry.COL_3 === undefined) newEntry.COL_3 = 0;
					if (newEntry.COL_4 === undefined) newEntry.COL_4 = 0;
					if (newEntry.COL_5 === undefined) newEntry.COL_5 = 0;
					if (newEntry.COL_6 === undefined) newEntry.COL_6 = 0;
					if (newEntry.COL_7 === undefined) newEntry.COL_7 = 0;
					if (newEntry.COL_8 === undefined) newEntry.COL_8 = 0;
					if (newEntry.COL_9 === undefined) newEntry.COL_9 = 0;
					if (newEntry.COL_10 === undefined) newEntry.COL_10 = 0;
					if (newEntry.COL_11 === undefined) newEntry.COL_11 = 0;
					if (newEntry.COL_12 === undefined) newEntry.COL_12 = 0;
					if (newEntry.CURMONTH === undefined) newEntry.CURMONTH = 0;
					if (newEntry.TOTAL === undefined) newEntry.TOTAL = 0;
					newEntry.COL_1 += k.COL_1;
					newEntry.COL_2 += k.COL_2;
					newEntry.COL_3 += k.COL_3;
					newEntry.COL_4 += k.COL_4;
					newEntry.COL_5 += k.COL_5;
					newEntry.COL_6 += k.COL_6;
					newEntry.COL_7 += k.COL_7;
					newEntry.COL_8 += k.COL_8;
					newEntry.COL_9 += k.COL_9;
					newEntry.COL_10 += k.COL_10;
					newEntry.COL_11 += k.COL_11;
					newEntry.COL_12 += k.COL_12;
					newEntry.CURMONTH += k.CURMONTH;
					newEntry.TOTAL += k.TOTAL;

				});
				newEntry = that._hasComment(newEntry, 3);
				newCollectedData3.push(newEntry);
			});

			newCollectedData3.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});

			this._tableModel.setProperty("/tableRows3", newCollectedData3);
		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			if (type === "added" || type === "removed" || type === "removedAll") {
				this.getView().byId("FS1").removeAllTokens();
				this.getView().byId("FS2").removeAllTokens();
				this.getView().byId("FS3").removeAllTokens();
			}
		}

	});

});