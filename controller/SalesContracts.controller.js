sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/formatter",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	'sap/ui/core/Fragment',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, formatter, dbcontext, dbcontext2, excel, Fragment, JSONModel, MessageToast,
	MessageBox, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.SalesContracts", {
		formatter: formatter,
		onInit: function () {

			this._setButtonsPressed("demandViewBtnId");
			this._salesContractsPanelM = this.getOwnerComponent().getModel("salesContractsPanelM");
			this._salesContractsTableM = this.getOwnerComponent().getModel("salesContractsTableM");
			this._globalModel = this.getOwnerComponent().getModel("globalM");
			this.columnfilter = {};

			this._tableId = this.getView().byId("SalesContractsTableId");
			this._tableIdText = "SalesContractsTableId";
			this._salesContractsTableM.setProperty("/tableRows", []);
			this._panelModel = this.getOwnerComponent().getModel("salesContractsPanelM");
			this._initializePanelModel();
			sap.ui.getCore().getModel("globalM").setProperty("/isSalesContracts", false);
			sap.ui.getCore().getModel("globalM").setProperty("/SalesContractsNavto", undefined);
			this.getRouter().getRoute("SalesContracts").attachPatternMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("demandViewBtnId");
            var that = this;
			var argum = oEvent.getParameter("arguments");
			try {
				var row = JSON.parse(argum.row);
				var viewid = argum.viewid;
				this.getView().byId("contractSID").removeAllTokens();
				this.getView().byId("contractSID").destroyTokens();
				this.createToken(undefined, row, viewid);
				sap.ui.getCore().getModel("globalM").setProperty("/SalesContractsNavto", argum);
				this.getView().byId("contractSID").removeAllTokens();
				this.getView().byId("contractSID").destroyTokens();
				this.onGo();
			} catch (err) {
				setTimeout(
					function () {
						if (that.getView().byId("contractSID") !== undefined) {
							that.getView().byId("contractSID").removeAllTokens();
							that.getView().byId("contractSID").destroyTokens();
							// if (sap.ui.getCore().getModel("globalM").getProperty("/QuoteManagementNavto") === undefined) {
							that.getView().byId("contractSID").addToken(new sap.m.Token({
								key: "C1",
								text: "open"
							}));
							that._panelModel.setProperty("/CONTRACT_STATUS", "C1");
							// }
						}

					}, 1000);
				sap.ui.getCore().getModel("globalM").setProperty("/SalesContractsNavto", undefined);
				if (sap.ui.getCore().getModel("globalM").getProperty("/isSalesContracts") === true)
					this.createToken();
			}

		},

		onSortTable: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._salesContractsTableM.getProperty("/tableRows");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this.getView().byId("SalesContractsTableId").getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);
			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._salesContractsTableM);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("SalesContractsTableId").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._salesContractsTableM.setProperty("/tableRows", notSumRow);
			// 	that.getView().byId("SalesContractsTableId").bindRows("salesContractsTableM>/tableRows");

			// }, 100)
		},

		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
			}
		},

		onExport: function () {
			var data = [];
			var tableData = this._salesContractsTableM.getData();
			var copiedData = tableData.tableRows;
			var aIndices = this._tableId.getBinding("rows").aIndices;

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			excel._downloadExcel("SALES_CONTRACTS", this._tableId, data);
		},

		onValueHelpInit: function (oEvent) {
			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/PLANTM", "");
			this._panelModel.setProperty("/COUNTRY_PLANT", "");
			this._panelModel.setProperty("/REGION", "");
			this._panelModel.setProperty("/DOC_DATE", "");
			this._panelModel.setProperty("/VALID_FROM", "");
			this._panelModel.setProperty("/CONTRACT_STATUS", "");
			this._panelModel.setProperty("/VALID_TO", "");
			this._panelModel.setProperty("/COUNTRY_CUSTOMER", "");
			this._panelModel.setProperty("/CUSTOMERL1", "");
			this._panelModel.setProperty("/CUSTOMERL2", "");
			this._panelModel.setProperty("/B2B_L2", "");
			this._panelModel.setProperty("/SALESORG", "");
			this._panelModel.setProperty("/MATERIAL", "");
			this._panelModel.setProperty("/SFULLYEAR", "");
			this._panelModel.setProperty("/EFULLYEAR", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
			this._panelModel.setProperty("/SEASON", "");
			this._panelModel.setProperty("/FS1", "");
			this._panelModel.setProperty("/FS2", "");
			this._panelModel.setProperty("/FS3", "");
			this._panelModel.setProperty("/CERTIFICATE", "");
			this._panelModel.setProperty("/PLANT_CLUSTER", "");

			// setTimeout(
			// 	function () {
			// 		if (sap.ui.getCore().getModel("globalM").getProperty("/SalesContractsNavto") === undefined) {
			// 			if (that.getView().byId("contractSID") !== undefined) {
			// 				that.getView().byId("contractSID").removeAllTokens();
			// 				that.getView().byId("contractSID").destroyTokens();
			// 				// if (sap.ui.getCore().getModel("globalM").getProperty("/QuoteManagementNavto") === undefined) {
			// 				that.getView().byId("contractSID").addToken(new sap.m.Token({
			// 					key: "C1",
			// 					text: "open"
			// 				}));
			// 				that._panelModel.setProperty("/CONTRACT_STATUS", "C1");
			// 				// }
			// 			}
			// 		}
			// 	}, 1000);

		},

		onChangeSeason: function (oEvent) {
			if (oEvent.getParameter("newValue") === "" || oEvent.getParameter("newValue") === undefined) {
				this._panelModel.setProperty("/SFULLYEAR", "");
				this._panelModel.setProperty("/EFULLYEAR", "");
				this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/PREV_FROM", "");
				this._panelModel.setProperty("/PREV_TO", "");
				this._panelModel.setProperty("/SEASON", "");
			} else {
				oEvent.getSource().setValue("");
				MessageToast.show("You cannot enter a value manually!");
			}
		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			var argum = sap.ui.getCore().getModel("globalM").getProperty("/SalesContractsNavto");
			if (type === "added" || type === "removed" || type === "removedAll") {
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "WERKS") {

					if (argum === undefined) {
						this._panelModel.setProperty("/EFULLYEAR", "");
						this._panelModel.setProperty("/SFULLYEAR", "");
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
						this._panelModel.setProperty("/PREV_FROM", "");
						this._panelModel.setProperty("/PREV_TO", "");
						this._panelModel.setProperty("/SEASON", "");
					}
					// this._prefillSeason(oEvent); commented by OAP
				}
				if ((oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "PRODUCT_TYPE") && (argum ===
						undefined)) {
					this.getView().byId("FS1").removeAllTokens();
					this.getView().byId("FS2").removeAllTokens();
					this.getView().byId("FS3").removeAllTokens();
				}
			}

		},

		checkSeason: function () {
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_DATA"
				},
				"INPUTPARAMS": [{
					"FRUIT": this._panelModel.getProperty("/FRUIT"),
					"PLANT": this._panelModel.getProperty("/PLANT")
				}]
			};
			return params;
		},

		handleValueHelp: function (oEvent) {
			var params = "";
			var fruit = this._panelModel.getProperty("/FRUIT");
			if (fruit === "") {
				MessageToast.show("Fruit field can not be empty!");
				return;
			} else {
				params = this.checkSeason();

			}

			var oView = this.getView();
			this._sInputId = oEvent.getSource().getId();

			// create value help dialog
			if (!this._pValueHelpDialog) {
				this._pValueHelpDialog = Fragment.load({
					id: oView.getId(),
					name: "com.doehler.Z_GTP.fragments.SeasonSearchHelp",
					controller: this
				}).then(function (oValueHelpDialog) {
					oView.addDependent(oValueHelpDialog);
					return oValueHelpDialog;
				});
			}
			this._prepareSeason(params, this._globalModel, this._pValueHelpDialog, undefined, "X");
		},

		_handleValueHelpClose: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var productInput = this.byId(this._sInputId);
				productInput.setValue(oSelectedItem.getTitle());
			}
			oEvent.getSource().getBinding("items").filter([]);

			var eFormat = "",
				sFormmat = "";
			if (this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath()) === undefined)
				return;

			var line = this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath());
			if (line.efullYear.split(".").length === 3) {
				eFormat = line.efullYear.split(".")[2] + line.efullYear.split(".")[1] + line.efullYear.split(".")[0];
			}
			if (line.sfullYear.split(".").length === 3) {
				sFormmat = line.sfullYear.split(".")[2] + line.sfullYear.split(".")[1] + line.sfullYear.split(".")[0];
			}
			this._panelModel.setProperty("/SFULLYEAR", line.sfullYear);
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
			this._panelModel
				.setProperty("/EFULLYEAR", line.efullYear);
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormat);
			this._panelModel.setProperty(
				"/SEASON_TEXT", line.text);
		},

		_prefillSeason: function (oEvent) {
			var fruit = this.getView().byId("fruitId");
			var params = this.checkSeason();
			if (fruit.getTokens().length === 1)
				this._prepareSeason(params, this._globalModel, null, this._panelModel, "");
		},

		onGo: function () {
			var that = this;
			var seasonInput = this._panelModel.getData()["SEASON"];

			var seasonCheckMsg = this._globalModel.getProperty("/seasonCheckMsg");
			// if (seasonCheckMsg !== undefined && seasonCheckMsg !== "") {
			// 	MessageBox.warning(seasonCheckMsg);
			// 	return;
			// }
			var SFULLYEAR = this._panelModel.getData().SFULLYEAR;
			var EFULLYEAR = this._panelModel.getData().EFULLYEAR;
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				if (seasonInput.split("[").length > 1) {
					if (seasonInput.split("[")[1].split(" - ").length > 1) {
						SFULLYEAR = seasonInput.split("[")[1].split(" - ")[0];
						var sFormmat = SFULLYEAR.split(".")[2] + SFULLYEAR.split(".")[1] + SFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
						EFULLYEAR = seasonInput.split("[")[1].split(" - ")[1];
						var eFormmat = EFULLYEAR.split(".")[2] + EFULLYEAR.split(".")[1] + EFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormmat);
					}
				}
			}
			if (SFULLYEAR !== "" && EFULLYEAR !== "") {
				var dateFrom = SFULLYEAR.split(".")[1] + "." + SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[2];
				dateFrom = new Date(dateFrom);
				var yearFrom = dateFrom.getFullYear() - 1;
				var prevDateFrom = SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[1] + "." + yearFrom.toString();
				var dateTo = new Date(dateFrom - 24 * 60 * 60 * 1000);
				var prevDateTo = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "dd.MM.YYYY"
				}).format(new Date(dateTo));
			}
			this._panelModel.setProperty("/PREV_FROM", prevDateFrom);
			this._panelModel.setProperty("/PREV_TO", prevDateTo);
			if (this.getView().byId("validFromId").getDateValue() !== "") {
				var validFromFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "dd.MM.YYYY"
				}).format(this.getView().byId("validFromId").getDateValue());
				this._panelModel.setProperty("/VALID_FROM_F", validFromFormat);
			}
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SALES_CONTRACTS"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			dbcontext.callServer(params, function (oModel) {
				debugger;
				var data = oModel.getData().RESULTS;
				data.forEach(function (line) {
					if (line["MATERIAL__RMW11ZP16"] === "#") line["MATERIAL__RMW11ZP16"] = "";
					if (line["MATERIAL__DFRTSPC1"] === "#") line["MATERIAL__DFRTSPC1"] = "";
					if (line["MATERIAL__DFRTSPC2"] === "#") line["MATERIAL__DFRTSPC2"] = "";
					if (line["MATERIAL__DFRTSPC3"] === "#") line["MATERIAL__DFRTSPC3"] = "";
					line["MATERIAL__RMW11ZE03___T"] = that.getDetermineOrganic(line["MATERIAL__RMW11ZE03"]);
					line["MATERIAL__RMW71ZE03___T"] = that.getDetermineHalal(line["MATERIAL__RMW71ZE03"]);
					line["MATERIAL__RMW81ZE03___T"] = that.getDetermineKosher(line["MATERIAL__RMW81ZE03"]);
					line["GROSSPUNIT"] = line["GROSSPUNIT"].replace("[", "").replace("]", "");

				});
				that._salesContractsTableM.setProperty("/tableRows", data);
				that.initallData = data;
				that._findTableKeys();

			}, that);
		},

		onPressAggregation: function () {
			this.convertGroup(this.mainTableKeys);
			this.collectData();
		},

		convertGroup: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData, groupCols);
			this.groupData = groupData;
		},

		collectData: function () {
			var newCollectedData = [];
			var that = this;
			$.each(this.groupData, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.MATERIAL__RMW21ZH02 = k.MATERIAL__RMW21ZH02;
					newEntry.MATERIAL__RMW21ZH02___T = k.MATERIAL__RMW21ZH02___T;
					// newEntry.A0MATERIAL__RMW21ZH02 = k.A0MATERIAL__RMW21ZH02;
					newEntry.MATERIAL__RMW11ZP16___T = k.MATERIAL__RMW11ZP16___T;
					newEntry.MATERIAL__RMW11ZP16 = k.MATERIAL__RMW11ZP16;
					newEntry.MAT_SALES__DOEWERK = k.MAT_SALES__DOEWERK;
					newEntry.MAT_SALES__DOEWERK___T = k.MAT_SALES__DOEWERK___T;
					newEntry.PLANT__COUNTRY = k.PLANT__COUNTRY;
					newEntry.PLANT__COUNTRY__T = k.PLANT__COUNTRY__T;
					newEntry.MATERIAL__RMW11ZE03 = k.MATERIAL__RMW11ZE03;
					newEntry.MATERIAL__RMW11ZE03___T = k.MATERIAL__RMW11ZE03___T;
					newEntry.MATERIAL__RMW71ZE03 = k.MATERIAL__RMW71ZE03;
					newEntry.MATERIAL__RMW71ZE03___T = k.MATERIAL__RMW71ZE03___T;
					newEntry.MATERIAL__RMW81ZE03 = k.MATERIAL__RMW81ZE03;
					newEntry.MATERIAL__RMW81ZE03___T = k.MATERIAL__RMW81ZE03___T;
					newEntry.MATERIAL__DFRTSPC1 = k.MATERIAL__DFRTSPC1;
					newEntry.MATERIAL__DFRTSPC1___T = k.MATERIAL__DFRTSPC1___T;
					newEntry.MATERIAL__DFRTSPC2 = k.MATERIAL__DFRTSPC2;
					newEntry.MATERIAL__DFRTSPC2___T = k.MATERIAL__DFRTSPC2___T;
					newEntry.MATERIAL__DFRTSPC3 = k.MATERIAL__DFRTSPC3;
					newEntry.MATERIAL__DFRTSPC3___T = k.MATERIAL__DFRTSPC3___T;
					newEntry.MATERIAL = k.MATERIAL;
					newEntry.MATERIAL___T = k.MATERIAL___T;
					newEntry.CUSTOMER = k.CUSTOMER;
					newEntry.CUSTOMER___T = k.CUSTOMER___T;
					newEntry.CUSTOMER__DOEBKUNDE = k.CUSTOMER__DOEBKUNDE;
					newEntry.CUSTOMER__DOEBKUNDE___T = k.CUSTOMER__DOEBKUNDE___T;
					newEntry.CUSTOMER__0COUNTRY = k.CUSTOMER__0COUNTRY;
					newEntry.CUSTOMER__0COUNTRY___T = k.CUSTOMER__0COUNTRY___T;
					newEntry.CUSTOMER__DSDAREA = k.CUSTOMER__DSDAREA
					newEntry.CUSTOMER__DSDAREA___T = k.CUSTOMER__DSDAREA___T;
					newEntry.CUSTOMER__DSDREGION = k.CUSTOMER__DSDREGION;
					newEntry.CUSTOMER__DSDREGION___T = k.CUSTOMER__DSDREGION___T;
					newEntry.SALESORG = k.SALESORG;
					newEntry.SALESORG___T = k.SALESORG___T;
					newEntry.DOEGBSTK = k.DOEGBSTK;
					newEntry.DOEGBSTK___T = k.DOEGBSTK___T;
					newEntry.DOEKONDNR = k.DOEKONDNR;
					newEntry.DOEKONDPO = k.DOEKONDPO;
					if (newEntry.DOEKONDPO === "000000") newEntry.DOEKONDPO = "";
					newEntry.DOC_DATE = k.DOC_DATE;
					newEntry.QUOT_FROM = k.QUOT_FROM;
					newEntry.QUOT_TO = k.QUOT_TO;
					newEntry.MATERIAL__DZZB2B_L1 = k.MATERIAL__DZZB2B_L1;
					newEntry.MATERIAL__DZZB2B_L1___T = k.MATERIAL__DZZB2B_L1___T;
					newEntry.MATERIAL__DZZB2B_L2 = k.MATERIAL__DZZB2B_L2;
					newEntry.MATERIAL__DZZB2B_L2___T = k.MATERIAL__DZZB2B_L2___T;
					newEntry.MATERIAL__DZZB2B_L3 = k.MATERIAL__DZZB2B_L3;
					newEntry.MATERIAL__DZZB2B_L3___T = k.MATERIAL__DZZB2B_L3___T;
					newEntry.MATERIAL__DZZB2B_L4 = k.MATERIAL__DZZB2B_L4;
					newEntry.MATERIAL__DZZB2B_L4___T = k.MATERIAL__DZZB2B_L4___T;
					newEntry.MATERIAL__DZZB2B_L5 = k.MATERIAL__DZZB2B_L5;
					newEntry.MATERIAL__DZZB2B_L5___T = k.MATERIAL__DZZB2B_L5___T;
					newEntry.CUSTOMER__DCUSTSEG = k.CUSTOMER__DCUSTSEG;
					newEntry.CUSTOMER__DCUSTSEG___T = k.CUSTOMER__DCUSTSEG___T;
					newEntry.GROSSPUNIT = k.GROSSPUNIT;
					newEntry.GROSSALESDCUNIT = k.GROSSALESDCUNIT;
					newEntry.INCOTERMS = k.INCOTERMS;
					newEntry.CUSTOMER__CUST_CLASS = k.CUSTOMER__CUST_CLASS;
					newEntry.CUSTOMER__CUST_CLASS___T = k.CUSTOMER__CUST_CLASS___T;
					newEntry.PLANT = k.PLANT;
					newEntry.PLANT___T = k.PLANT___T;
					newEntry.DOEORSTG = k.DOEORSTG;
					newEntry.DOEORSTG___T = k.DOEORSTG___T;
					newEntry.DOEORSTAT = k.DOEORSTAT;
					newEntry.DOEORSTAT___T = k.DOEORSTAT___T;
					newEntry.DOEITMSTG = k.DOEITMSTG;
					newEntry.DOEITMSTG___T = k.DOEITMSTG___T;
					newEntry.DOESUBSTG = k.DOESUBSTG;
					newEntry.DOESUBSTG___T = k.DOESUBSTG___T;

					//numerics
					newEntry.CONTQUANTITY = newEntry.CONTQUANTITY === undefined ? 0 : newEntry.CONTQUANTITY;
					newEntry.CONTQUANTITY = newEntry.CONTQUANTITY + k.CONTQUANTITY;
					newEntry.OPENQUANTITY = newEntry.OPENQUANTITY === undefined ? 0 : newEntry.OPENQUANTITY;
					newEntry.OPENQUANTITY = newEntry.OPENQUANTITY + k.OPENQUANTITY;
					newEntry.GROSSPRICE = newEntry.GROSSPRICE === undefined ? 0 : newEntry.GROSSPRICE;
					newEntry.GROSSPRICE = newEntry.GROSSPRICE + k.GROSSPRICE;
					newEntry.CANCELLEDQTY = newEntry.CANCELLEDQTY === undefined ? 0 : newEntry.CANCELLEDQTY;
					newEntry.CANCELLEDQTY = newEntry.CANCELLEDQTY + k.CANCELLEDQTY;
					newEntry.GROSSALESDC = newEntry.GROSSALESDC === undefined ? 0 : newEntry.GROSSALESDC;
					newEntry.GROSSALESDC = newEntry.GROSSALESDC + k.GROSSALESDC;
					newEntry.DOC_CURRCY = k.DOC_CURRCY;
					newEntry["CONT_QUAN_GROSS"] = newEntry["CONT_QUAN_GROSS"] === undefined ? 0 : newEntry["CONT_QUAN_GROSS"];
					newEntry["CONT_QUAN_GROSS"] = newEntry["CONT_QUAN_GROSS"] + (k.GROSSPRICE * k.CONTQUANTITY);

					newEntry.DEDUCTIONS_REQQUANTITY = newEntry.DEDUCTIONS_REQQUANTITY === undefined ? 0 : newEntry.DEDUCTIONS_REQQUANTITY;
					newEntry.DEDUCTIONS_REQQUANTITY = newEntry.DEDUCTIONS_REQQUANTITY + k.DEDUCTIONS_REQQUANTITY;

					newEntry.DEDUCTIONS_REQQUANTITY_W = newEntry.DEDUCTIONS_REQQUANTITY_W === undefined ? 0 : newEntry.DEDUCTIONS_REQQUANTITY_W;
					newEntry.DEDUCTIONS_REQQUANTITY_W = newEntry.DEDUCTIONS_REQQUANTITY_W + (k.DEDUCTIONS_REQQUANTITY * k.CONTQUANTITY);

					newEntry.FREIGHTOUT = newEntry.FREIGHTOUT === undefined ? 0 : newEntry.FREIGHTOUT;
					newEntry.FREIGHTOUT = newEntry.FREIGHTOUT + k.FREIGHTOUT;

					newEntry.FREIGHTOUT_W = newEntry.FREIGHTOUT_W === undefined ? 0 : newEntry.FREIGHTOUT_W;
					newEntry.FREIGHTOUT_W = newEntry.FREIGHTOUT_W + (k.FREIGHTOUT * k.CONTQUANTITY);

					newEntry.COMMISSION3RDP = newEntry.COMMISSION3RDP === undefined ? 0 : newEntry.COMMISSION3RDP;
					newEntry.COMMISSION3RDP = newEntry.COMMISSION3RDP + k.COMMISSION3RDP;

					newEntry.COMMISSION3RDP_W = newEntry.COMMISSION3RDP_W === undefined ? 0 : newEntry.COMMISSION3RDP_W;
					newEntry.COMMISSION3RDP_W = newEntry.COMMISSION3RDP_W + (k.COMMISSION3RDP * k.CONTQUANTITY);

					newEntry.NETPRICEEXW = newEntry.NETPRICEEXW === undefined ? 0 : newEntry.NETPRICEEXW;
					newEntry.NETPRICEEXW = newEntry.NETPRICEEXW + k.NETPRICEEXW - newEntry.COMMISSION3RDP;

					newEntry.GROSS_EUR = newEntry.GROSS_EUR === undefined ? 0 : newEntry.GROSS_EUR;
					newEntry.GROSS_EUR = newEntry.GROSS_EUR + k.GROSS_EUR;

					newEntry.GROSS_EUR_W = newEntry.GROSS_EUR_W === undefined ? 0 : newEntry.GROSS_EUR_W;
					newEntry.GROSS_EUR_W = newEntry.GROSS_EUR_W + (k.GROSS_EUR * k.CONTQUANTITY);

					newEntry.NET_EUR = newEntry.NET_EUR === undefined ? 0 : newEntry.NET_EUR;
					newEntry.NET_EUR = newEntry.NET_EUR + k.NET_EUR;
					newEntry.NET_EUR_W = newEntry.NET_EUR_W === undefined ? 0 : newEntry.NET_EUR_W;
					newEntry.NET_EUR_W = newEntry.NET_EUR_W + (k.NET_EUR * k.CONTQUANTITY);

					newEntry.DELIVEREDQTY = newEntry.DELIVEREDQTY === undefined ? 0 : newEntry.DELIVEREDQTY;
					newEntry.DELIVEREDQTY = newEntry.DELIVEREDQTY + k.DELIVEREDQTY;

					newEntry.OPENCALLOFFQTY = newEntry.OPENCALLOFFQTY === undefined ? 0 : newEntry.OPENCALLOFFQTY;
					newEntry.OPENCALLOFFQTY = newEntry.OPENCALLOFFQTY + k.OPENCALLOFFQTY;
				});

				newEntry.GROSS_EUR = newEntry.CONTQUANTITY === 0 ? 0 : newEntry.GROSS_EUR_W / newEntry.CONTQUANTITY;
				newEntry.NET_EUR = newEntry.CONTQUANTITY === 0 ? 0 : newEntry.NET_EUR_W / newEntry.CONTQUANTITY;

				newEntry["NETQUANTITY"] = newEntry.CONTQUANTITY - newEntry.CANCELLEDQTY;
				if (Number.isFinite(newEntry.GROSSALESDC / newEntry.CONTQUANTITY)) {
					newEntry.GROSSPRICE = newEntry.GROSSALESDC / newEntry.CONTQUANTITY;
				}

				newEntry = that._hasComment(newEntry, 1);
				newCollectedData.push(newEntry);
			});
			var totalRow = this.setTotalRow(newCollectedData, "SALES_CONTRACTS", this.columnfilter);
			totalRow["GROSSPRICE"] = totalRow["CONT_QUAN_GROSS"] / totalRow["CONTQUANTITY"];
			totalRow.GROSS_EUR = totalRow.GROSS_EUR_W / totalRow.CONTQUANTITY;
			totalRow.DEDUCTIONS_REQQUANTITY = totalRow.DEDUCTIONS_REQQUANTITY_W / totalRow.CONTQUANTITY;
			totalRow.FREIGHTOUT = totalRow.FREIGHTOUT_W / totalRow.CONTQUANTITY;
			totalRow.COMMISSION3RDP = totalRow.COMMISSION3RDP_W / totalRow.CONTQUANTITY;
			totalRow.NET_EUR = totalRow.NET_EUR_W / totalRow.CONTQUANTITY;
			newCollectedData.unshift(totalRow);
			newCollectedData.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						if (key === "GROSSPRICE" ||
							key === "NETPRICEEXW" ||
							key === "COMMISSION3RDP" ||
							key === "FREIGHTOUT" ||
							key === "GROSS_EUR" ||
							key === "DEDUCTIONS_REQQUANTITY" ||
							key === "NET_EUR" ||
							key === "GROSS_EUR")
							row[key] = parseFloat(row[key].toFixed(2));
						else
							row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this._salesContractsTableM.setProperty("/tableRows", newCollectedData);
			var oBinding = this.getView().byId("SalesContractsTableId").getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true)
				]);
			}, 100);
		},

		_handleValueHelpSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Name",
				FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

		// onSortTable: function (oEvent) {
		// 	var sortOrder = oEvent.getParameter("sortOrder");
		// 	var data = this._salesContractsTableM.getProperty("/tableRows");
		// 	var sortProperty = oEvent.getParameter("column").getSortProperty();
		// 	var sumRow = "",
		// 		notSumRow = "";
		// 	oEvent.getSource().setModel(this._salesContractsTableM);

		// 	sumRow = _.filter(_.cloneDeep(data), function (item) {
		// 		return item.isSum === "X";
		// 	})[0];

		// 	notSumRow = _.filter(_.cloneDeep(data), function (item) {
		// 		return item.isSum !== "X";
		// 	});

		// 	notSumRow =
		// 		_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
		// 	var that = this;
		// 	setTimeout(function () {

		// 		that.getView().byId("SalesContractsTableId").unbindRows();

		// 		notSumRow.unshift(sumRow);
		// 		that._salesContractsTableM.setProperty("/tableRows", notSumRow);
		// 		that.getView().byId("SalesContractsTableId").bindRows("salesContractsTableM>/tableRows");

		// 	}, 100)
		// },

		onFilterTable: function (oEvent) {
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		currTableData: null,
		oTPC: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId;
			var oVM = this.getView().byId("SalesContractsTableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
					// Find Table Key
					thiz._findTableKeys();
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys();
		},

		_findTableKeys: function () {
			var thiz = this;
			var oTable = this._tableId;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys.length)
				thiz.onPressAggregation();
		},

		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		//************************************************table variant codes**************************************

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdSalesContracts");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("salesContractsPanelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isSalesContracts", true);
			sap.ui.getCore().getModel("globalM").setProperty("/salesContractsInit", true);
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("salesContractsPanelM").setData(ovar[ovar.defaultKey]);
			var argum = sap.ui.getCore().getModel("globalM").getProperty("/SalesContractsNavto");
			try {
				var row = JSON.parse(argum.row);
				var viewid = argum.viewid;
				this.createToken(undefined, row, viewid);
			} catch (err) {
				this.createToken();

			}
		},

		/* on select variant */
		onSelectVariantSalesContracts: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("salesContractsPanelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantSalesContracts: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("salesContractsPanelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMSalesContracts: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1, param2, param3) {
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined)
				this._updatePanelData(fromData, sap.ui.getCore().getModel("salesContractsPanelM"), param2, param3);
			var aControls = this.getView().byId("searchGridId").getContent();
			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));
					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {
									if (value != "") {
										if (item.getFieldName() === "FRUIT") {
											const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
											item.addToken(new sap.m.Token({
												key: value,
												text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
											}));
										} else {
											item.addToken(new sap.m.Token({
												key: value,
												text: value
											}));
										}
									}
								});

							} else {
								// var r = sap.ui.getCore().getModel("globalM").getProperty("/SalesContractsNavto");
								// r = JSON.parse(r["row"])["salesHandover"];
								// if (param1 === undefined && item.getFieldName() === "CONTRACT_STATUS") {
								// 	var arr = [];
								// 	arr.push({
								// 		key: "A",
								// 		text: "A"
								// 	});
								// 	arr.push({
								// 		key: "B",
								// 		text: "B"
								// 	});
								// 	arr.forEach(function (value) {
								// 		if (value.key != "") {
								// 			item.addToken(new sap.m.Token({
								// 				key: value.key,
								// 				text: value.text
								// 			}));
								// 		}
								// 	});
								// } else {
								// 	// if (r === "X" && item.getFieldName() === "CONTRACT_STATUS") {
								// 	// 	item.removeAllTokens();
								// 	// 	item.destroyTokens();
								// 	// }
								// }
							}
							// var r = sap.ui.getCore().getModel("globalM").getProperty("/SalesContractsNavto");
							// r = JSON.parse(r["row"])["salesHandover"];
							// if (r === "X" && item.getFieldName() === "CONTRACT_STATUS") {
							// 	item.removeAllTokens();
							// 	item.destroyTokens();
							// }
						}
					});
				}, 1000);
		},

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			this.initVariant();
			this.initSearchVariant();
		},

		onExit: function () {

		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					that._findTableKeys();
					// that._findTableKeys2();
				},
				function (error) { /* code if some error */ }
			);
		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);
		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

	});

});