sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/formatter",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	'sap/ui/core/Fragment',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, formatter, dbcontext, dbcontext2, excel, Fragment, JSONModel, MessageToast,
	MessageBox, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.PurchaseContracts", {
		formatter: formatter,
		onInit: function () {
			this._setButtonsPressed("supplyBtnId");
			this._panelModel = this.getOwnerComponent().getModel("purchaseContractsPanelM");
			this._purchaseContractsTableM = this.getOwnerComponent().getModel("purchaseContractsTableM");
			this._globalModel = this.getOwnerComponent().getModel("globalM");
			this._tableId = this.getView().byId("purchaseContractsTableId");
			this.columnfilter = {};
			this._tableIdText = "purchaseContractsTableId";
			this._purchaseContractsTableM.setProperty("/tableRows", []);
			this._panelModel.setData({});
			this._initializePanelModel();
			sap.ui.getCore().getModel("globalM").setProperty("/isPurchaseContracts", false);
			sap.ui.getCore().getModel("globalM").setProperty("/PurchaseContractsNavto", undefined);
			this.getRouter().getRoute("PurchaseContracts").attachPatternMatched(this._onRouteMatched, this);
		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("supplyBtnId");
			var argum = oEvent.getParameter("arguments");
			
			try {
				var row = JSON.parse(argum.row);
				var viewid = argum.viewid;
				this.createToken(undefined, row, viewid);
				sap.ui.getCore().getModel("globalM").setProperty("/PurchaseContractsNavto", argum);
				this.onGo();
			} catch (err) {
				sap.ui.getCore().getModel("globalM").setProperty("/PurchaseContractsNavto", undefined);
				if (sap.ui.getCore().getModel("globalM").getProperty("/isPurchaseContracts") === true)
					this.createToken();
			}
			

		},

		onSortTable: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._purchaseContractsTableM.getProperty("/tableRows");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this.getView().byId("purchaseContractsTableId").getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);
			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._purchaseContractsTableM);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("purchaseContractsTableId").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._purchaseContractsTableM.setProperty("/tableRows", notSumRow);
			// 	that.getView().byId("purchaseContractsTableId").bindRows("purchaseContractsTableM>/tableRows");

			// }, 100)
		},

		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
			}
		},

		onExport: function () {
			var data = [];
			var tableData = this._purchaseContractsTableM.getData();
			var copiedData = tableData.tableRows;
			var aIndices = this._tableId.getBinding("rows").aIndices;

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			excel._downloadExcel("PURCHASE_CONTRACTS", this._tableId, data);
		},

		onValueHelpInit: function (oEvent) {
			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
		},

		onChangeCreatedOn: function (oEvent) {
			if (oEvent.getParameter("newValue") !== "") {
				this._panelModel.setProperty("/SFULLYEAR", "");
				this._panelModel.setProperty("/EFULLYEAR", "");
				this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/PREV_FROM", "");
				this._panelModel.setProperty("/PREV_TO", "");
				this._panelModel.setProperty("/SEASON", "");
			}
		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/COLLECTIVE_NUMBER", "");
			this._panelModel.setProperty("/SUPPLIER", "");
			this._panelModel.setProperty("/FS1", "");
			this._panelModel.setProperty("/FS2", "");
			this._panelModel.setProperty("/FS3", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/MAT_GRP_3", "");
			this._panelModel.setProperty("/MAT_GRP_4", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/SFULLYEAR", "");
			this._panelModel.setProperty("/EFULLYEAR", "");
			this._panelModel.setProperty("/SUB_GROUP", "");
			this._panelModel.setProperty("/CERTIFICATE", "");
			this._panelModel.setProperty("/CREATED_ON", "");
			this._panelModel.setProperty("/PURCHASE_ORG", "");
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
			this._panelModel.setProperty("/PREV_FROM", "");
			this._panelModel.setProperty("/PREV_TO", "");
			this._panelModel.setProperty("/SEASON", "");
			this._panelModel.setProperty("/CONTRACT_STATUS", "");
			this._panelModel.setProperty("/VALID_FROM", "");
			this._panelModel.setProperty("/VALID_TO", "");
			this._panelModel.setProperty("/DOC_DATE", "");
			this._panelModel.setProperty("/CONTRACT_NUMBER", "");
		},

		onChangeSeason: function (oEvent) {
			if (oEvent.getParameter("newValue") === "" || oEvent.getParameter("newValue") === undefined) {
				this._panelModel.setProperty("/SFULLYEAR", "");
				this._panelModel.setProperty("/EFULLYEAR", "");
				this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
				this._panelModel.setProperty("/PREV_FROM", "");
				this._panelModel.setProperty("/PREV_TO", "");
				this._panelModel.setProperty("/SEASON", "");
			} else {
				oEvent.getSource().setValue("");
				MessageToast.show("You cannot enter a value manually!");
			}
		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			var argum = sap.ui.getCore().getModel("globalM").getProperty("/PurchaseContractsNavto");
			if (type === "added" || type === "removed" || type === "removedAll") {
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "WERKS") {
                  
                  if (argum === undefined) {
					this._panelModel.setProperty("/EFULLYEAR", "");
					this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/PREV_FROM", "");
					this._panelModel.setProperty("/PREV_TO", "");
					this._panelModel.setProperty("/SEASON", "");

					this._prefillSeason(oEvent);
                  }
				}
					if ( ( oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "PRODUCT_TYPE"  ) && (argum === undefined) ) {
					this.getView().byId("FS1").removeAllTokens();
					this.getView().byId("FS2").removeAllTokens();
					this.getView().byId("FS3").removeAllTokens();
				}
			}

		},

		checkSeason: function () {
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_DATA"
				},
				"INPUTPARAMS": [{
					"FRUIT": this._panelModel.getProperty("/FRUIT"),
					"PLANT": this._panelModel.getProperty("/PLANT")
				}]
			};
			return params;
		},

		handleValueHelp: function (oEvent) {
			var params = "";
			var fruit = this._panelModel.getProperty("/FRUIT");
			if (fruit === "") {
				MessageToast.show("Fruit field can not be empty!");
				return;
			} else {
				params = this.checkSeason();

			}

			var oView = this.getView();
			this._sInputId = oEvent.getSource().getId();

			// create value help dialog
			if (!this._pValueHelpDialog) {
				this._pValueHelpDialog = Fragment.load({
					id: oView.getId(),
					name: "com.doehler.Z_GTP.fragments.SeasonSearchHelp",
					controller: this
				}).then(function (oValueHelpDialog) {
					oView.addDependent(oValueHelpDialog);
					return oValueHelpDialog;
				});
			}
			this._prepareSeason(params, this._globalModel, this._pValueHelpDialog, undefined, "X");
		},

		_prefillSeason: function (oEvent) {
			var fruit = this.getView().byId("fruitId");
			var params = this.checkSeason();
			if (fruit.getTokens().length === 1)
				this._prepareSeason(params, this._globalModel, null, this._panelModel, "");
		},

		onGo: function () {
			var that = this;
			var seasonInput = this._panelModel.getData()["SEASON"];

			var seasonCheckMsg = this._globalModel.getProperty("/seasonCheckMsg");
			// if (seasonCheckMsg !== undefined && seasonCheckMsg !== "") {
			// 	MessageBox.warning(seasonCheckMsg);
			// 	return;
			// }
			var SFULLYEAR = this._panelModel.getData().SFULLYEAR;
			var EFULLYEAR = this._panelModel.getData().EFULLYEAR;
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				if (seasonInput.split("[").length > 1) {
					if (seasonInput.split("[")[1].split(" - ").length > 1) {
						SFULLYEAR = seasonInput.split("[")[1].split(" - ")[0];
						var sFormmat = SFULLYEAR.split(".")[2] + SFULLYEAR.split(".")[1] + SFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
						EFULLYEAR = seasonInput.split("[")[1].split(" - ")[1];
						var eFormmat = EFULLYEAR.split(".")[2] + EFULLYEAR.split(".")[1] + EFULLYEAR.split(".")[0];
						this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormmat);
					}
				}
			}
			var dateFrom = SFULLYEAR.split(".")[1] + "." + SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[2];
			dateFrom = new Date(dateFrom);
			var yearFrom = dateFrom.getFullYear() - 1;
			var prevDateFrom = SFULLYEAR.split(".")[0] + "." + SFULLYEAR.split(".")[1] + "." + yearFrom.toString();
			var dateTo = new Date(dateFrom - 24 * 60 * 60 * 1000);
			var prevDateTo = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(new Date(dateTo));
			if (SFULLYEAR === "" && EFULLYEAR === "") {
				prevDateFrom = "";
				prevDateTo = "";
			}

			this._panelModel.setProperty("/PREV_FROM", prevDateFrom);
			this._panelModel.setProperty("/PREV_TO", prevDateTo);
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_PURCHASE_CONTRACTS"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			dbcontext.callServer(params, function (oModel) {
				debugger;
				var data = oModel.getData().RESULTS;
				data.forEach(function (line) {
					line["A0SUPPL_VEND__0COUNTRY_F"] = line["A0SUPPL_VEND__0COUNTRY"] + line["A0SUPPL_VEND__0COUNTRY_T"];
					line["A0VENDOR_F"] = line["A0VENDOR"] + line["A0VENDOR_T"];
					if (line["A0MATERIAL__RMW11ZP16"] === "#") line["A0MATERIAL__RMW11ZP16"] = "";
					if (line["A0MATERIAL__DFRTSPC1"] === "#") line["A0MATERIAL__DFRTSPC1"] = "";
					if (line["A0MATERIAL__DFRTSPC2"] === "#") line["A0MATERIAL__DFRTSPC2"] = "";
					if (line["A0MATERIAL__DFRTSPC3"] === "#") line["A0MATERIAL__DFRTSPC3"] = "";
					line["A0MATERIAL__RMW11ZE03_T"] = that.getDetermineOrganic(line["A0MATERIAL__RMW11ZE03"]);
					line["A0MATERIAL__RMW21ZE03_T"] = that.getDetermineHalal(line["A0MATERIAL__RMW21ZE03"]);
					line["A0MATERIAL__RMW31ZE03_T"] = that.getDetermineKosher(line["A0MATERIAL__RMW31ZE03"]);

				});
				that.initallData = data;
				that._findTableKeys();

			}, that);
		},

		_findTableKeys: function () {
			var thiz = this;
			var oTable = this._tableId;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys.length)
				thiz.onPressAggregation();
		},

		onPressAggregation: function () {
			this.convertGroup(this.mainTableKeys);
			this.collectData();
		},

		convertGroup: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData, groupCols);
			this.groupData = groupData;
		},

		collectData: function () {
			var newCollectedData = [];
			var that = this;
			$.each(this.groupData, function (i, v) {

				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.A0MATERIAL__RMW21ZH02 = k.A0MATERIAL__RMW21ZH02;
					newEntry.A0MATERIAL__RMW21ZH02_T = k.A0MATERIAL__RMW21ZH02_T;
					newEntry.A0MATERIAL__RMW11ZP16 = k.A0MATERIAL__RMW11ZP16;
					newEntry.A0MATERIAL__RMW11ZP16_T = k.A0MATERIAL__RMW11ZP16_T;
					newEntry.A0PLANT = k.A0PLANT;
					newEntry.A0PLANT_T = k.A0PLANT_T;
					newEntry.A0PURCH_ORG = k.A0PURCH_ORG;
					newEntry.A0PURCH_ORG_T = k.A0PURCH_ORG_T;
					newEntry.A0MATERIAL__RMW11ZE03 = k.A0MATERIAL__RMW11ZE03;
					newEntry.A0MATERIAL__RMW11ZE03_T = k.A0MATERIAL__RMW11ZE03_T;
					newEntry.A0MATERIAL__RMW21ZE03 = k.A0MATERIAL__RMW21ZE03;
					newEntry.A0MATERIAL__RMW21ZE03_T = k.A0MATERIAL__RMW21ZE03_T;
					newEntry.A0MATERIAL__RMW31ZE03 = k.A0MATERIAL__RMW31ZE03;
					newEntry.A0MATERIAL__RMW31ZE03_T = k.A0MATERIAL__RMW31ZE03_T;
					newEntry.A0MATERIAL__DFRTSPC1 = k.A0MATERIAL__DFRTSPC1;
					newEntry.A0MATERIAL__DFRTSPC1_T = k.A0MATERIAL__DFRTSPC1_T;
					newEntry.A0MATERIAL__DFRTSPC2 = k.A0MATERIAL__DFRTSPC2;
					newEntry.A0MATERIAL__DFRTSPC2_T = k.A0MATERIAL__DFRTSPC2_T;
					newEntry.A0MATERIAL__DFRTSPC3 = k.A0MATERIAL__DFRTSPC3;
					newEntry.A0MATERIAL__DFRTSPC3_T = k.A0MATERIAL__DFRTSPC3_T;
					newEntry.A0MATERIAL = k.A0MATERIAL;
					newEntry.A0MATERIAL_T = k.A0MATERIAL_T;
					newEntry.A0MATERIAL__DCOLLNR = k.A0MATERIAL__DCOLLNR;
					newEntry.A0MATERIAL__DCOLLNR_T = k.A0MATERIAL__DCOLLNR_T;
					newEntry.A0MATERIAL__DOEMGL3 = k.A0MATERIAL__DOEMGL3;
					newEntry.A0MATERIAL__DOEMGL3_T = k.A0MATERIAL__DOEMGL3_T;
					newEntry.A0MATERIAL__DOEMGL4 = k.A0MATERIAL__DOEMGL4;
					newEntry.A0MATERIAL__DOEMGL4_T = k.A0MATERIAL__DOEMGL4_T;
					newEntry.ADPURGROUP = k.ADPURGROUP;
					newEntry.ADPURGROUP_T = k.ADPURGROUP_T;
					newEntry.A0VENDOR = k.A0VENDOR;
					newEntry.A0VENDOR_F = k.A0VENDOR_F;
					newEntry.A0VENDOR_T = k.A0VENDOR_T;
					newEntry.A0SUPPL_VEND__0COUNTRY = k.A0SUPPL_VEND__0COUNTRY;
					newEntry.A0SUPPL_VEND__0COUNTRY_F = k.A0SUPPL_VEND__0COUNTRY_F;
					newEntry.A0SUPPL_VEND__0COUNTRY_T = k.A0SUPPL_VEND__0COUNTRY_T;
					newEntry.A0CALDAY = k.A0CALDAY;
					//newEntry.A0DOC_DATE = k.A0DOC_DATE;
					newEntry.A0CONTRACT = k.A0CONTRACT;
					newEntry.A0CONTRACT__0VAL_START = k.A0CONTRACT__0VAL_START;
					newEntry.A0CONTRACT__0VAL_END = k.A0CONTRACT__0VAL_END;
					newEntry.A0ORDER_CURR = k.A0ORDER_CURR;
					newEntry.A0DOC_DATE = k.A0DOC_DATE;
					newEntry.A0DOC_DATE_T = k.A0DOC_DATE_T;
					newEntry.A0CONT_ITEM = k.A0CONT_ITEM;

					//numerics
					newEntry.CONTRACT_QUANTITY = newEntry.CONTRACT_QUANTITY === undefined ? 0 : newEntry.CONTRACT_QUANTITY;
					newEntry.CONTRACT_QUANTITY = newEntry.CONTRACT_QUANTITY + k.CONTRACT_QUANTITY;
					newEntry.QUANTITY_DELIVERED = newEntry.QUANTITY_DELIVERED === undefined ? 0 : newEntry.QUANTITY_DELIVERED;
					newEntry.QUANTITY_DELIVERED = newEntry.QUANTITY_DELIVERED + k.QUANTITY_DELIVERED;
					newEntry.OPEN_CALLS = newEntry.OPEN_CALLS === undefined ? 0 : newEntry.OPEN_CALLS;
					newEntry.OPEN_CALLS = newEntry.OPEN_CALLS + k.OPEN_CALLS;
					newEntry.GROSS_PRICE_DC = newEntry.GROSS_PRICE_DC === undefined ? 0 : newEntry.GROSS_PRICE_DC;
					newEntry.GROSS_PRICE_DC = newEntry.GROSS_PRICE_DC + k.GROSS_PRICE_DC;
					newEntry.CONTRACT_VALUE = newEntry.CONTRACT_VALUE === undefined ? 0 : newEntry.CONTRACT_VALUE;
					newEntry.CONTRACT_VALUE = newEntry.CONTRACT_VALUE + k.CONTRACT_VALUE;
					newEntry.CONTRACT_VALUE_EUR = newEntry.CONTRACT_VALUE_EUR === undefined ? 0 : newEntry.CONTRACT_VALUE_EUR;
					newEntry.CONTRACT_VALUE_EUR = newEntry.CONTRACT_VALUE_EUR + k.CONTRACT_VALUE_EUR;
				});
				newEntry = that._hasComment(newEntry, 1);
				newCollectedData.push(newEntry);
			});
			var totalRow = this.setTotalRow(newCollectedData, "", this.columnfilter);
			newCollectedData.unshift(totalRow);
			newCollectedData.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						if (key === "GROSS_PRICE_DC")
							row[key] = parseFloat(row[key].toFixed(2));
						else
							row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this._purchaseContractsTableM.setProperty("/tableRows", newCollectedData);
		},

		_handleValueHelpSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Name",
				FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

		onTableSelect: function (oEvent) {
			var row = this._purchaseContractsTableM.getProperty(oEvent.getParameter("rowContext").getPath());
			if (row.isSum === "X") {
				oEvent.getSource().removeSelectionInterval(oEvent.getParameter("rowIndex"), 0);
			}
		},

		_handleValueHelpClose: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var productInput = this.byId(this._sInputId);
				productInput.setValue(oSelectedItem.getTitle());
			}
			oEvent.getSource().getBinding("items").filter([]);

			var eFormat = "",
				sFormmat = "";
			if (this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath()) === undefined)
				return;

			var line = this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath());
			if (line.efullYear.split(".").length === 3) {
				eFormat = line.efullYear.split(".")[2] + line.efullYear.split(".")[1] + line.efullYear.split(".")[0];
			}
			if (line.sfullYear.split(".").length === 3) {
				sFormmat = line.sfullYear.split(".")[2] + line.sfullYear.split(".")[1] + line.sfullYear.split(".")[0];
			}
			this._panelModel.setProperty("/SFULLYEAR", line.sfullYear);
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
			this._panelModel
				.setProperty("/EFULLYEAR", line.efullYear);
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormat);
			this._panelModel.setProperty(
				"/SEASON_TEXT", line.text);
			this._panelModel.setProperty("/CREATED_ON", "");
		},

		// onSortTable: function (oEvent) {
		// 	var sortOrder = oEvent.getParameter("sortOrder");
		// 	var data = this._purchaseContractsTableM.getProperty("/tableRows");
		// 	var sortProperty = oEvent.getParameter("column").getSortProperty();
		// 	var sumRow = "",
		// 		notSumRow = "";
		// 	oEvent.getSource().setModel(this._purchaseContractsTableM);

		// 	sumRow = _.filter(_.cloneDeep(data), function (item) {
		// 		return item.isSum === "X";
		// 	})[0];

		// 	notSumRow = _.filter(_.cloneDeep(data), function (item) {
		// 		return item.isSum !== "X";
		// 	});

		// 	notSumRow =
		// 		_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
		// 	var that = this;
		// 	setTimeout(function () {

		// 		that.getView().byId("purchaseContractsTableId").unbindRows();

		// 		notSumRow.unshift(sumRow);
		// 		that._purchaseContractsTableM.setProperty("/tableRows", notSumRow);
		// 		that.getView().byId("purchaseContractsTableId").bindRows("purchaseContractsTableM>/tableRows");

		// 	}, 100)
		// },

		onFilterTable: function (oEvent) {
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		currTableData: null,
		oTPC: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId;
			var oVM = this.getView().byId("purchaseContractsTableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
					// Find Table Key
					thiz._findTableKeys();
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys();
		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		//************************************************table variant codes**************************************

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdPurchaseContracts");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("purchaseContractsPanelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isPurchaseContracts", true);
			sap.ui.getCore().getModel("globalM").setProperty("/purchaseContractsInit", true);
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("purchaseContractsPanelM").setData(ovar[ovar.defaultKey]);
			
				var argum = sap.ui.getCore().getModel("globalM").getProperty("/PurchaseContractsNavto");
			try {
				var row = JSON.parse(argum.row);
				var viewid = argum.viewid;
				this.createToken(undefined, row, viewid);
			} catch (err) {
				this.createToken();

			}
			
		},

		/* on select variant */
		onSelectVariantPurchaseContracts: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("purchaseContractsPanelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantPurchaseContracts: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("purchaseContractsPanelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMPurchaseContracts: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1, param2, param3) {
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined)
				this._updatePanelData(fromData, sap.ui.getCore().getModel("purchaseContractsPanelM"),param2, param3);
			var aControls = this.getView().byId("searchGridId").getContent();
			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));
					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {
									if (value != "") {
										if (item.getFieldName() === "FRUIT") {
											const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
											item.addToken(new sap.m.Token({
												key: value,
												text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
											}));
										} else {
											item.addToken(new sap.m.Token({
												key: value,
												text: value
											}));
										}
									}
								});

							} else {
								if (param1 === undefined && item.getFieldName() === "CONTRACT_STATUS") {
									var arr = [];
									arr.push({
										key: "A",
										text: "A"
									});
									arr.push({
										key: "B",
										text: "B"
									});
									arr.forEach(function (value) {
										if (value.key != "") {
											item.addToken(new sap.m.Token({
												key: value.key,
												text: value.text
											}));
										}
									});
								}
							}
						}
					});
				}, 1000);
		},

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			this.initVariant();
			this.initSearchVariant();
		},

		onExit: function () {

		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();

			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					that._findTableKeys();
				},
				function (error) { /* code if some error */ }
			);
		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);
		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

	});

});