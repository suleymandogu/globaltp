sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController"
], function (BaseController) {
	"use strict";
 
	return BaseController.extend("com.doehler.Z_GTP.controller.App", {
		onInit: function () {
			var app = this.getView().byId("app");
			var that = this;
			var Promise1 = new Promise(function (myResolve, myReject) {
				that.getSearchHelpData("FRUIT", myResolve, myReject);

			});
				Promise1.then(values => {
					sap.ui.getCore().getModel("globalM").setProperty("/FRUIT", values);
				});

			app.attachNavigate(function (evt) {
				var fromPage = evt.getParameter("from").getProperty("viewName");
				var toPage = evt.getParameter("to").getProperty("viewName");

				var contents = evt.getParameter("from").byId("searchGridId").getContent();

				var fromModel, toModel;
				switch (fromPage) {
				case "com.doehler.Z_GTP.view.Overview/Main":
					fromModel = sap.ui.getCore().getModel("panelM");
					break;
				case "com.doehler.Z_GTP.view.Demand/Demand":
					fromModel = sap.ui.getCore().getModel("demandPanelM");
					break;
				case "com.doehler.Z_GTP.view.Supply/Supply":
					fromModel = sap.ui.getCore().getModel("supplyPanelM");
					break;
				case "com.doehler.Z_GTP.view.ProductionList/ProductionList":
					fromModel = sap.ui.getCore().getModel("productionListPanelM");
					break;
				case "com.doehler.Z_GTP.view.SeasonalPlanToOffer/SeasonalPlanToOffer":
					fromModel = sap.ui.getCore().getModel("seasonPanelM");
					break;
				case "com.doehler.Z_GTP.view.QuoteManagement/QuoteManagement":
					fromModel = sap.ui.getCore().getModel("quotePanelM");
					break;
				case "com.doehler.Z_GTP.view.SeasonSummary/SeasonSummary":
					fromModel = sap.ui.getCore().getModel("seasonSummaryPanelM");
					break;
				case "com.doehler.Z_GTP.view.PurchasePlanning/PurchasePlanning":
					fromModel = sap.ui.getCore().getModel("purchasePlanPanelM");
					break;
				case "com.doehler.Z_GTP.view.ProductionPlanning/ProductionPlanning":
					fromModel = sap.ui.getCore().getModel("productionPlanPanelM");
					break;
				case "com.doehler.Z_GTP.view.PurchaseContracts/PurchaseContracts":
					fromModel = sap.ui.getCore().getModel("purchaseContractsPanelM");
					break;
				case "com.doehler.Z_GTP.view.SalesContracts/SalesContracts":
					fromModel = sap.ui.getCore().getModel("salesContractsPanelM");
					break;
				case "com.doehler.Z_GTP.view.DataMaintenance/DataMaintenance":
					fromModel = sap.ui.getCore().getModel("dataMaintenancePanelM");
					break;
				default:
				}

				switch (toPage) {
				case "com.doehler.Z_GTP.view.Overview/Main":
					toModel = sap.ui.getCore().getModel("panelM");
					break;
				case "com.doehler.Z_GTP.view.Demand/Demand":
					toModel = sap.ui.getCore().getModel("demandPanelM");
					break;
				case "com.doehler.Z_GTP.view.Supply/Supply":
					toModel = sap.ui.getCore().getModel("supplyPanelM");
					break;
				case "com.doehler.Z_GTP.view.ProductionList/ProductionList":
					toModel = sap.ui.getCore().getModel("productionListPanelM");
					break;
				case "com.doehler.Z_GTP.view.SeasonalPlanToOffer/SeasonalPlanToOffer":
					toModel = sap.ui.getCore().getModel("seasonPanelM");
					break;
				case "com.doehler.Z_GTP.view.QuoteManagement/QuoteManagement":
					toModel = sap.ui.getCore().getModel("quotePanelM");
					break;
				case "com.doehler.Z_GTP.view.SeasonSummary/SeasonSummary":
					toModel = sap.ui.getCore().getModel("seasonSummaryPanelM");
					break;
				case "com.doehler.Z_GTP.view.PurchasePlanning/PurchasePlanning":
					toModel = sap.ui.getCore().getModel("purchasePlanPanelM");
					break;
				case "com.doehler.Z_GTP.view.ProductionPlanning/ProductionPlanning":
					toModel = sap.ui.getCore().getModel("productionPlanPanelM");
					break;
				case "com.doehler.Z_GTP.view.PurchaseContracts/PurchaseContracts":
					toModel = sap.ui.getCore().getModel("purchaseContractsPanelM");
					break;
				case "com.doehler.Z_GTP.view.SalesContracts/SalesContracts":
					toModel = sap.ui.getCore().getModel("salesContractsPanelM");
					break;
				case "com.doehler.Z_GTP.view.DataMaintenance/DataMaintenance":
					toModel = sap.ui.getCore().getModel("dataMaintenancePanelM");
					break;
				default:
				}

				var dataFrom = fromModel.getData();
				var dataFromCopy = $.extend(true, {}, dataFrom);
				var dataTo = toModel.getData();
				var dataToCopy = $.extend(true, {}, dataTo);

				// toModel.setData(dataFromCopy);
				sap.ui.getCore().getModel("globalM").setProperty("/contentsForHandover", contents);
				sap.ui.getCore().getModel("globalM").setProperty("/fromModel", fromModel);
				sap.ui.getCore().getModel("globalM").setProperty("/toModel", toModel);

				sap.ui.getCore().getModel("globalM").setProperty("/fromData", dataFromCopy);
			});

		}
	});
});