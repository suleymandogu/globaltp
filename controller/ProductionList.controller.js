sap.ui.define([
	"com/doehler/Z_GTP/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/doehler/Z_GTP/model/formatter",
	"com/doehler/Z_GTP/model/dbcontext",
	"com/doehler/Z_GTP/model/dbcontext2",
	"com/doehler/Z_GTP/excel/excel",
	'sap/ui/core/Fragment',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"com/doehler/Z_GTP/model/models"
], function (BaseController, Filter, FilterOperator, formatter, dbcontext, dbcontext2, excel, Fragment, JSONModel, MessageToast,
	MessageBox, models) {
	"use strict";

	return BaseController.extend("com.doehler.Z_GTP.controller.ProductionList", {
		formatter: formatter,
		onInit: function () {
			this._setButtonsPressed("supplyBtnId");
			this._tableId = this.getView().byId("ProductionListTableId");
			this._tableIdText = "ProductionListTableId";
			this.bDialog = new sap.m.BusyDialog();
			this.columnfilter = {};
			//panel Model
			this._panelModel = this.getOwnerComponent().getModel("productionListPanelM");
			this._globalModel = this.getOwnerComponent().getModel("globalM");
			this._globalModel.setProperty("/seasonText", "");
			this._productionListTableM = this.getOwnerComponent().getModel("productionListTableM");

			//oDataModel
			this._seasonalODataModel = this.getOwnerComponent().getModel("QUOTE_MANAGEMENT_MODEL");
			sap.ui.getCore().setModel(this._panelModel, "productionListPanelM");
			this._initializePanelModel();
			sap.ui.getCore().getModel("globalM").setProperty("/isProductionList", false);
			this.getRouter().getRoute("ProductionList").attachPatternMatched(this._onRouteMatched, this);

		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed("supplyBtnId");
			if (sap.ui.getCore().getModel("globalM").getProperty("/isProductionList") === true)
				this.createToken();

		},

		onSort: function (oEvent) {
			var sortOrder = oEvent.getParameter("sortOrder");
			var data = this._productionListTableM.getProperty("/tableRows");
			var sortProperty = oEvent.getParameter("column").getSortProperty();

			var oBinding = this.getView().byId("ProductionListTableId").getBinding("rows");
			setTimeout(function () {
				oBinding.sort([
					new sap.ui.model.Sorter("isSum", true),
					new sap.ui.model.Sorter(sortProperty, sortOrder === "Descending")
				]);
			}, 100);
			// var sumRow = "",
			// 	notSumRow = "";
			// oEvent.getSource().setModel(this._productionListTableM);

			// sumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum === "X";
			// })[0];

			// notSumRow = _.filter(_.cloneDeep(data), function (item) {
			// 	return item.isSum !== "X";
			// });

			// notSumRow =
			// 	_.orderBy(notSumRow, sortProperty, sortOrder === "Ascending" ? "asc" : "desc");
			// var that = this;
			// setTimeout(function () {

			// 	that.getView().byId("ProductionListTableId").unbindRows();

			// 	notSumRow.unshift(sumRow);
			// 	that._productionListTableM.setProperty("/tableRows", notSumRow);
			// 	that.getView().byId("ProductionListTableId").bindRows("productionListTableM>/tableRows");

			// }, 100)
		},

		onFilterProd: function (oEvent) {
			this.onFilter(oEvent);
			if (oEvent.getParameter("value") !== "") {
				this.columnfilter.col = oEvent.getParameter("column").getProperty("filterProperty");
				this.columnfilter.val = oEvent.getParameter("value");
			} else {
				this.columnfilter = {};
			}

			// if (value === "") {
			// 	this.columnfilter1 = _.remove(this.columnfilter1, function (n) {
			// 		return n.col === column && n.val === value;
			// 	});
			// } else {
			// 	this.columnfilter1.push({"col":column, "val":value});
			// }

			// var data = this._tableModel.getProperty("/tableRows");
			// this.Quotedfiltered = _.filter(_.cloneDeep(data), function (item) {
			// 	return item[column] === value;
			// });
		},

		onValueHelpInit: function (oEvent) {
			if (oEvent.getSource().getRelFieldModel() === "CreateGTPquoteTableM") {
				var pressed = sap.ui.getCore().getModel("globalM").getData()["pressTableValueControl"];
				var createGTPquoteTableModel = this.CreateGTPquoteTableM;
				if (pressed !== undefined) {
					if (this._checkValidationOfGTPQuote(oEvent.getSource().getFieldName(), pressed.getBindingContext("CreateGTPquoteTableM").getPath(),
							createGTPquoteTableModel)) {
						oEvent.getSource().setController(this);
						oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
					}
				}
			} else if (oEvent.getSource().getRelFieldModel() === "createSAPquoteTableM") {
				pressed = sap.ui.getCore().getModel("globalM").getData()["pressTableValueControl"];
				if (pressed !== undefined) {
					if (this._checkValidationOfSAPQuote(oEvent.getSource().getFieldName(), pressed.getBindingContext("createSAPquoteTableM").getPath())) {
						oEvent.getSource().setController(this);
						oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
					}
				}
			} else {
				oEvent.getSource().setController(this);
				oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
			}

		},

		_initializePanelModel: function () {
			var that = this;
			Object.keys(this._panelModel.getData()).forEach(function (key) {
				that._panelModel.setProperty("/" + key, "");
			});
			this._panelModel.setProperty("/FRUIT", "");
			this._panelModel.setProperty("/VALID_FROM", "");
			this._panelModel.setProperty("/CALENDAR_DAY", "");
			this._panelModel.setProperty("/CALENDAR_YEAR_MONTH", "");
			this._panelModel.setProperty("/FS1", "");
			this._panelModel.setProperty("/FS2", "");
			this._panelModel.setProperty("/FS3", "");
			this._panelModel.setProperty("/PRODUCT_TYPE", "");
			this._panelModel.setProperty("/PLANT", "");
			this._panelModel.setProperty("/CERTIFICATE", "");
			this._panelModel.setProperty("/PLANT_CLUSTER", "");

		},

		onGo: function () {
			var that = this,
				data = [],
				calendarDay = that._panelModel.getData().CALENDAR_DAY,
				calendarYearMonth = that._panelModel.getData().CALENDAR_YEAR_MONTH;
			var validFromInput = this._panelModel.getData()["VALID_FROM"];
			var filterCurrent = this._panelModel.getData();
			if (validFromInput === undefined || validFromInput === "") {
				MessageBox.warning("Valid From input can not be empty");
				return;
			}

			var validFromFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.YYYY"
			}).format(this.getView().byId("validFromId").getDateValue());
			this._panelModel.setProperty("/VALID_FROM_F", validFromFormat);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_PRODUCTION_LIST_SUPPLY"
				},
				"INPUTPARAMS": [this._panelModel.getData()]
			};

			dbcontext.callServer(params, function (oModel) {
				data = oModel.getData().RESULTS.OUTPUT;
				var mcurr = oModel.getData().RESULTS.MCURRR;
				that._globalModel.setProperty("/Mcurr", mcurr);
				if (calendarDay !== "" && calendarDay !== undefined) {
					data = data.filter(function (m) {
						return m.A0CALDAY === calendarDay;
					});
				}
				if (calendarYearMonth !== "" && calendarYearMonth !== undefined) {
					data = data.filter(function (m) {
						return m.A0CALMONTH === calendarYearMonth;
					});
				}
				data.forEach(function (line) {
					if (line["A0MATERIAL__RMW11ZP16"] === "#") line["A0MATERIAL__RMW11ZP16"] = "";
					if (line["A0MATERIAL__DFRTSPC1"] === "#") line["A0MATERIAL__DFRTSPC1"] = "";
					if (line["A0MATERIAL__DFRTSPC2"] === "#") line["A0MATERIAL__DFRTSPC2"] = "";
					if (line["A0MATERIAL__DFRTSPC3"] === "#") line["A0MATERIAL__DFRTSPC3"] = "";
					line["A0MATERIAL__RMW11ZE03_T"] = that.getDetermineOrganic(line["A0MATERIAL__RMW11ZE03"]);
					line["A0MATERIAL__RMW21ZE03_T"] = that.getDetermineHalal(line["A0MATERIAL__RMW21ZE03"]);
					line["A0MATERIAL__RMW31ZE03_T"] = that.getDetermineKosher(line["A0MATERIAL__RMW31ZE03"]);

				});
				that._productionListTableM.setProperty("/tableRows", data);
				that.initallData = data;
				that._findTableKeys();
			}, that);
		},

		onPressAggregation: function () {
			this.convertGroup(this.mainTableKeys);
			this.collectData();
		},

		convertGroup: function (groupCols) {
			var groupBy = function (xs, groupFields) {
				groupFields = [].concat(groupFields);
				return xs.reduce(function (rv, x) {
					let groupKey = groupFields.reduce((keyObject, field) => {
						keyObject[field] = x[field];
						return keyObject;
					}, {});
					(rv[JSON.stringify(groupKey)] = rv[JSON.stringify(groupKey)] || []).push(x);
					return rv;
				}, {});
			};

			var groupData = groupBy(this.initallData, groupCols);
			this.groupData = groupData;
		},

		collectData: function () {
			var newCollectedData = [];
			var that = this;
			$.each(this.groupData, function (i, v) {
				var valueOfPROD_BRIX_QUAN = 0,
					valueOfINPUT_FRUIT = 0,
					valueOfPRODUCTION_VALUE = 0,
					valueOfPRODUKTIONSWERTEUR_TAGESKURS = 0,
					valueOfPROD_ACID_QUAN = 0,
					valueOfYIELD_STANDARD_BRIX = 0;
				var newEntry = {};
				$.each(v, function (j, k) {
					newEntry["isSum"] = "";
					newEntry.A0PLANT = k.A0PLANT;
					newEntry.PLANT_CLUSTER = k.PLANT_CLUSTER;
					newEntry.PLANT_CLUSTER_T = k.PLANT_CLUSTER_T;
					newEntry.WAERS = k.WAERS;
					newEntry.A0PLANT_T = k.A0PLANT_T;
					newEntry.A0MATERIAL__RMW21ZH02 = k.A0MATERIAL__RMW21ZH02;
					newEntry.A0MATERIAL__RMW21ZH02_T = k.A0MATERIAL__RMW21ZH02_T;
					newEntry.A0MATERIAL__RMW11ZP16 = k.A0MATERIAL__RMW11ZP16;
					newEntry.A0MATERIAL__RMW11ZP16_T = k.A0MATERIAL__RMW11ZP16_T;
					newEntry.A0MATERIAL__RMW11ZE03 = k.A0MATERIAL__RMW11ZE03;
					newEntry.A0MATERIAL__RMW11ZE03_T = k.A0MATERIAL__RMW11ZE03_T;
					newEntry.A0CALDAY = k.A0CALDAY;

					newEntry.A0CALMONTH = k.A0CALMONTH;

					newEntry.A0MATERIAL = k.A0MATERIAL;
					newEntry.A0MATERIAL_T = k.A0MATERIAL_T;

					newEntry.A0MATERIAL__RMW21ZE03 = k.A0MATERIAL__RMW21ZE03_
					newEntry.A0MATERIAL__RMW21ZE03_T = k.A0MATERIAL__RMW21ZE03_T;
					newEntry.A0MATERIAL__RMW31ZE03 = k.A0MATERIAL__RMW31ZE03;
					newEntry.A0MATERIAL__RMW31ZE03_T = k.A0MATERIAL__RMW31ZE03_T;
					newEntry.A0MATERIAL__DFRTSPC1 = k.A0MATERIAL__DFRTSPC1;
					newEntry.A0MATERIAL__DFRTSPC1_T = k.A0MATERIAL__DFRTSPC1_T;
					newEntry.A0MATERIAL__DFRTSPC2 = k.A0MATERIAL__DFRTSPC2;
					newEntry.A0MATERIAL__DFRTSPC2_T = k.A0MATERIAL__DFRTSPC2_T;
					newEntry.A0MATERIAL__DFRTSPC3 = k.A0MATERIAL__DFRTSPC3;
					newEntry.A0MATERIAL__DFRTSPC3_T = k.A0MATERIAL__DFRTSPC3_T;

					newEntry.INPUT_FRUIT = newEntry.INPUT_FRUIT === undefined ? 0 : newEntry.INPUT_FRUIT;
					newEntry.INPUT_FRUIT = (k.INPUT_FRUIT) + newEntry.INPUT_FRUIT;

					newEntry.OUTPUT_CONCENTRATE = newEntry.OUTPUT_CONCENTRATE === undefined ? 0 : newEntry.OUTPUT_CONCENTRATE;
					newEntry.OUTPUT_CONCENTRATE = (k.OUTPUT_CONCENTRATE) + newEntry.OUTPUT_CONCENTRATE;

					newEntry.FLAVOUR = newEntry.FLAVOUR === undefined ? 0 : newEntry.FLAVOUR;
					newEntry.FLAVOUR = (k.FLAVOUR) + newEntry.FLAVOUR;

					//formulas
					newEntry.BRIX_OF_FRUIT = newEntry.BRIX_OF_FRUIT === undefined ? 0 : newEntry.BRIX_OF_FRUIT;
					valueOfPROD_BRIX_QUAN += k.PROD_BRIX_QUAN;
					valueOfINPUT_FRUIT += k.INPUT_FRUIT;

					newEntry.PRICE_OF_FRUIT = newEntry.PRICE_OF_FRUIT === undefined ? 0 : newEntry.PRICE_OF_FRUIT;
					valueOfPRODUCTION_VALUE += k.PRODUCTION_VALUE;

					newEntry.PREIS_TAGESKURS = newEntry.PREIS_TAGESKURS === undefined ? 0 : newEntry.PREIS_TAGESKURS;
					valueOfPRODUKTIONSWERTEUR_TAGESKURS += k.PRODUKTIONSWERTEUR_TAGESKURS;

					newEntry.PER_CA = newEntry.PER_CA === undefined ? 0 : newEntry.PER_CA;
					valueOfPROD_ACID_QUAN += k.PROD_ACID_QUAN;

					newEntry.YIELD_REAL_BRIX = newEntry.YIELD_REAL_BRIX === undefined ? 0 : newEntry.YIELD_REAL_BRIX;

					newEntry.YIELD_STANDARD_BRIX = newEntry.YIELD_STANDARD_BRIX === undefined ? 0 : newEntry.YIELD_STANDARD_BRIX;
					valueOfYIELD_STANDARD_BRIX += k.INPUT_FRUIT_STANDARD_BRIX;

					newEntry.COST_CONCENTRATE_EUR_TAGESKURS = newEntry.COST_CONCENTRATE_EUR_TAGESKURS === undefined ? 0 : newEntry.COST_CONCENTRATE_EUR_TAGESKURS;

				});
				if (valueOfINPUT_FRUIT !== 0) {
					newEntry.BRIX_OF_FRUIT = valueOfPROD_BRIX_QUAN / valueOfINPUT_FRUIT;
					newEntry.PRICE_OF_FRUIT = valueOfPRODUCTION_VALUE / valueOfINPUT_FRUIT;
					newEntry.PREIS_TAGESKURS = valueOfPRODUKTIONSWERTEUR_TAGESKURS / valueOfINPUT_FRUIT;
					newEntry.PER_CA = valueOfPROD_ACID_QUAN / valueOfINPUT_FRUIT;
				}

				if (newEntry.OUTPUT_CONCENTRATE !== 0) {
					newEntry.YIELD_REAL_BRIX = valueOfINPUT_FRUIT / newEntry.OUTPUT_CONCENTRATE;
					newEntry.YIELD_STANDARD_BRIX = valueOfYIELD_STANDARD_BRIX / newEntry.OUTPUT_CONCENTRATE;
					newEntry.COST_CONCENTRATE_EUR_TAGESKURS = newEntry.PREIS_TAGESKURS * newEntry.YIELD_REAL_BRIX;
				}
				newEntry = that._hasComment(newEntry, 1);
				newCollectedData.push(newEntry);
			});
			var totalRow = this.setTotalRow(newCollectedData, "PROD", this.columnfilter);

			newCollectedData.unshift(totalRow);
			newCollectedData.forEach(function (row) {
				Object.keys(row).forEach(function (key) {
					if (typeof row[key] === "number") {
						if (key === "BRIX_OF_FRUIT" || key === "PRICE_OF_FRUIT" || key === "PREIS_TAGESKURS" || key === "PER_CA" || key ===
							"YIELD_REAL_BRIX" || key === "YIELD_STANDARD_BRIX" || key === "COST_CONCENTRATE_EUR_TAGESKURS")
							row[key] = parseFloat(row[key].toFixed(2));
						else
							row[key] = parseFloat(row[key].toFixed(0));
					}
				});

			});
			this._productionListTableM.setProperty("/tableRows", newCollectedData);
		},

		clearSelectionFields: function () {
			var controls, i;
			this._initializePanelModel();
			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();

				}
				if (controls[i].getMetadata().getName() === "sap.m.Input") {
					controls[i].setValue("");
				}
			}
		},

		onExport: function () {
			var data = [];
			var tableData = this._productionListTableM.getData();
			var copiedData = tableData.tableRows;
			var aIndices = this._tableId.getBinding("rows").aIndices;

			if (copiedData !== undefined && copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			excel._downloadExcel("PRODUCTION_LIST", this._tableId, data);
		},

		//************************************************ Season search help**************************************

		checkSeason: function () {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_SEASON_DATA"
				},
				"INPUTPARAMS": [{
					"FRUIT": this._panelModel.getProperty("/FRUIT"),
					"PLANT": this._panelModel.getProperty("/PLANT")
				}]
			};

			return params;

		},

		handleValueHelp: function (oEvent) {
			var params = "";
			var fruit = this._panelModel.getProperty("/FRUIT");
			if (fruit === "") {
				MessageToast.show("Fruit field can not be empty!");
				return;
			} else {
				params = this.checkSeason();

			}

			var oView = this.getView();
			this._sInputId = oEvent.getSource().getId();

			// create value help dialog
			if (!this._pValueHelpDialog) {
				this._pValueHelpDialog = Fragment.load({
					id: oView.getId(),
					name: "com.doehler.Z_GTP.fragments.SeasonSearchHelp",
					controller: this
				}).then(function (oValueHelpDialog) {
					oView.addDependent(oValueHelpDialog);
					return oValueHelpDialog;
				});
			}
			this._prepareSeason(params, this._globalModel, this._pValueHelpDialog, undefined, "X");
		},

		_handleValueHelpSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter(
				"Name",
				FilterOperator.Contains, sValue
			);
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},

		_handleValueHelpClose: function (oEvent) {
			var oSelectedItem = oEvent.getParameter("selectedItem");
			if (oSelectedItem) {
				var productInput = this.byId(this._sInputId);
				productInput.setValue(oSelectedItem.getTitle());
			}
			oEvent.getSource().getBinding("items").filter([]);

			var eFormat = "",
				sFormmat = "";
			if (this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath()) === undefined)
				return;

			var line = this._globalModel.getProperty(oEvent.getParameter("selectedItem").getBindingContext("globalM").getPath());
			if (line.efullYear.split(".").length === 3) {
				eFormat = line.efullYear.split(".")[2] + line.efullYear.split(".")[1] + line.efullYear.split(".")[0];
			}
			if (line.sfullYear.split(".").length === 3) {
				sFormmat = line.sfullYear.split(".")[2] + line.sfullYear.split(".")[1] + line.sfullYear.split(".")[0];
			}
			this._panelModel.setProperty("/SFULLYEAR", line.sfullYear);
			this._panelModel.setProperty("/SFULLYEAR_FORMAT", sFormmat);
			this._panelModel
				.setProperty("/EFULLYEAR", line.efullYear);
			this._panelModel.setProperty("/EFULLYEAR_FORMAT", eFormat);
			this._panelModel.setProperty(
				"/SEASON_TEXT", line.text);
		},

		//>>>>>*************************************************table variant codes**************************************

		oCC: null,
		currTableData: null,
		oTPC: null,
		oCC2: null,
		currTableData2: null,
		oTPC2: null,
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.Z_GTP").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz._tableIdText);
				oCC.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var oTable = this._tableId;
			var oVM = this.getView().byId("searchFilterVMIdProdList");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(this._tableIdText, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(thiz._tableIdText);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() !== "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
					// Find Table Key
					thiz._findTableKeys();
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this._tableIdText;
			var oTable = this._tableId1;
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			this._findTableKeys();
		},

		_findTableKeys: function () {
			var thiz = this;
			var oTable = this._tableId;
			thiz.mainTableKeys = [];
			thiz.commentTableKeys = [];
			oTable.getColumns().map(function (columns, i) {
				if (columns.data("key") !== null) {
					if (columns.getVisible() === true) {
						thiz.mainTableKeys.push(columns.data("key"));
						if (columns.data("comment") !== null)
							thiz.commentTableKeys.push(columns.data("comment"));
					}
				}
			});
			if (thiz.mainTableKeys.length)
				thiz.onPressAggregation();
		},

		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this._tableIdText;
			var oTable = this._tableId;
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},

		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this._tableIdText;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},

		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey === item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},

		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		//************************************************table variant codes**************************************

		/*************************************************Start Panel Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdProdList");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("productionListPanelM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//	that.addSearchFilter();
			}, function () {
				//	that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			sap.ui.getCore().getModel("globalM").setProperty("/isProductionList", true);
			sap.ui.getCore().getModel("globalM").setProperty("/prodListInit", true);
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("productionListPanelM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantProdListPanel: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("productionListPanelM").setData(ovar[selKey]);
			this.createToken("S");
		},

		/* on save variant */
		onSaveVariantProdListPanel: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("productionListPanelM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMProdListPanel: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function (param1) {
			var fromData = sap.ui.getCore().getModel("globalM").getProperty("/fromData");
			if (param1 === undefined)
				this._updatePanelData(fromData, sap.ui.getCore().getModel("productionListPanelM"));
			var aControls = this.getView().byId("searchGridId").getContent();
			setTimeout(
				function () {
					aControls = aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") !== "FS1" && c.getProperty("fieldName") !== "FS2" && c.getProperty(
								"fieldName") !== "FS3");
					}).concat(aControls.filter(function (c) {
						return c.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || c.getMetadata().getName() ===
							"sap.m.MultiInput" && (c.getProperty("fieldName") === "FS1" || c.getProperty("fieldName") === "FS2" || c.getProperty(
								"fieldName") === "FS3");
					}));
					aControls.forEach(function (item) {
						if (item.getMetadata().getName() === "com.doehler.pr.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
							"sap.m.MultiInput") {

							if ((item.getMultiSelect() || item.getMaxTokens() === 1) && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								var arr2 = [];
								if (item.getMetadata().getName() === "sap.m.MultiInput")
									item.destroyTokens();
								if (item.getMaxTokens() === 1) {
									arr2.push(arr[0]);
									arr = arr2;
								}
								if (item.getFieldName() === "FRUIT") {
									var data = sap.ui.getCore().getModel("globalM").getProperty("/FRUIT");
								}
								arr.forEach(function (value) {
									if (value != "") {
										if (item.getFieldName() === "FRUIT") {
											const elementsIndex = data.findIndex(element1 => element1.PHRKEY == value);
											item.addToken(new sap.m.Token({
												key: value,
												text: data[elementsIndex].PHRTEXT === undefined ? value : data[elementsIndex].PHRTEXT
											}));
										} else {
											item.addToken(new sap.m.Token({
												key: value,
												text: value
											}));
										}
									}
								});

							}
						}
					});
				}, 1000);

		},

		tokenChange: function (oEvent) {
			var type = oEvent.getParameter("type");
			if (type === "added" || type === "removed" || type === "removedAll") {
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "WERKS") {

					this._panelModel.setProperty("/EFULLYEAR", "");
					this._panelModel.setProperty("/SFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/SFULLYEAR", "");
					this._panelModel.setProperty("/EFULLYEAR_FORMAT", "");
					this._panelModel.setProperty("/PREV_FROM", "");
					this._panelModel.setProperty("/PREV_TO", "");
					this._panelModel.setProperty("/SEASON", "");

					// this._prefillSeason(oEvent);
				}
				if (oEvent.getSource().getFieldName() === "FRUIT" || oEvent.getSource().getFieldName() === "PRODUCT_TYPE") {
					this.getView().byId("FS1").removeAllTokens();
					this.getView().byId("FS2").removeAllTokens();
					this.getView().byId("FS3").removeAllTokens();
				}
			}

		},

		_prefillSeason: function (oEvent) {
			var fruit = this.getView().byId("fruitId");
			var params = this.checkSeason();
			if (fruit.getTokens().length === 1)
				this._prepareSeason(params, this._globalModel, null, this._panelModel, "");
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			//	this.clearSelectionFields();
			this.initVariant();
			this.initSearchVariant();
			// var date = sap.ui.core.format.DateFormat.getDateInstance({
			// 	pattern: "yyyyMMdd"
			// }).format(new Date());
			// this._panelModel.setProperty("/VALID_FROM", date);
		},

		/*****************start*********comment codes********************************/

		onOpenComment: function (oEvent) {
			var that = this,
				oFooter = sap.ui.getCore().byId(this.getView().getId()).getContent()[0].getFooter(),
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._getComment(oEvent, myResolve, myReject, null);
				});
			if (oEvent.getSource().getType() === "Accept")
				oEvent.getSource().setPressed(true);
			else
				oEvent.getSource().setPressed(false);

			promiseGetComment.then(
				function (value) {
					if (!that._oPopoverComment) {
						Fragment.load({
							id: that.getView().getId(),
							name: "com.doehler.Z_GTP.fragments.CommentsTable",
							controller: that
						}).then(function (oPopover) {
							that._oPopoverComment = oPopover;
							that.getView().addDependent(that._oPopoverComment);
							that._oPopoverComment.openBy(oFooter);
						}.bind(that));
					} else {
						that._oPopoverComment.openBy(oFooter);
					}
				},
				function (error) { /* code if some error */ }
			);
		},

		onCloseComment: function (oEvent) {
			this._oPopoverComment.close();

			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					models.getAllComments(myResolve);
				});

			promiseGetComment.then(
				function (value) {
					that._findTableKeys();
				},
				function (error) { /* code if some error */ }
			);
		},

		onSaveComment: function (oEvent) {
			this._saveComment(this._oPopoverComment);
		},

		onAddComment: function (oEvent) {
			this._addComment(oEvent);
		},

		onDeleteComment: function (oEvent) {
			var that = this,
				promiseGetComment = new Promise(function (myResolve, myReject) {
					that._deleteComment(oEvent, myResolve);
				});

			promiseGetComment.then(
				function (value) {
					// that._oPopoverComment.close();
				},
				function (error) { /* code if some error */ }
			);

		},

		handleLoadItems: function (oEvent) {
			this._handleLoadItems(oEvent);
		},

		/***********************end***comment codes********************************/

	});

});