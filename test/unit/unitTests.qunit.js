/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/doehler/Z_GTP/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});