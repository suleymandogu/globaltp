sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"com/doehler/Z_GTP/model/dbcontext"
], function (JSONModel, Device, dbcontext) {
	"use strict";

	return {

		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createJSONModel: function () {
			return new JSONModel();
		},

		getUser: function () {
			var gModel = sap.ui.getCore().getModel("globalM"),
				value;
			var userId = sap.ushell.Container.getUser().getId();
			if (userId === "DEFAULT_USER")
				userId = "EX_DOGUS";

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "USER_DETAIL"
				},
				"INPUTPARAMS": [{
					"BNAME": userId
				}]
			};
			dbcontext.callServer(params, function (oModel) {
				var decimal = oModel.getProperty("/DECIMAL");
				sap.ui.getCore().getModel("globalM").setProperty("/decimal", decimal);
				var admin = oModel.getProperty("/ADMIN");
				sap.ui.getCore().getModel("globalM").setProperty("/admin", admin);
				if (userId === "DEFAULT_USER" || userId === "EX_DOGUS" || userId === "EX_PRIMO" || userId === "MATTHEC" || userId === "MERCADJ" ||
					userId === "FISCHEA"  || userId === "UZUNO1" ) {
					sap.ui.getCore().getModel("globalM").setProperty("/SUBROLE", "X");

				}
			}, this, false, function () {}, false, false);
		},

		getAllComments: function (myResolve) {
			var that = this,
				inputJson = {},
				commentData;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_COMMENTS"
				},
				"INPUTPARAMS": [inputJson]
			};
			dbcontext.callServer(params, function (oModel) {
				commentData = oModel.getData().COMMENTS;
				commentData.forEach(function (cm) {
					cm["ORGANIC"] = cm["ORGANIC"] === "CUST-X.0000000001563" ? "no" : cm["ORGANIC"] === "CUST-X.0000000001563" === "" ? "" : "yes";
					cm["HALAL"] = cm["HALAL"] === "CUST-X.0000000001563" ? "no" : cm["HALAL"] === "CUST-X.0000000001563" === "" ? "" : "yes";
					cm["KOSHER"] = cm["KOSHER"] === "CUST-X.0000000001563" ? "no" : cm["KOSHER"] === "CUST-X.0000000001563" === "" ? "" : "yes";
				});
				sap.ui.getCore().getModel("globalM").setProperty("/allCommentData", commentData);
				try {
					myResolve();
				} catch (err) {}

			});
		},

		getFSTexts: function () {

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_FS_TEXT"
				},
				"INPUTPARAMS": []
			};
			dbcontext.callServer(params, function (oModel) {
				var result = oModel.getProperty("/RESULTS");
				sap.ui.getCore().getModel("globalM").setProperty("/FS_TEXTS", result);

			}, this);

		},

		getBWPorts: function () {
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "REPORTS_GET_PORTAL_HOST"
				},
				"INPUTPARAMS": []
			};
			dbcontext.callServer(params, function (oModel) {
				var hostport = oModel.getProperty("/HOST_PORT");
				var bwport = oModel.getProperty("/PORTAL_HOST");
				sap.ui.getCore().getModel("globalM").setProperty("/HOST_PORT", hostport);
				sap.ui.getCore().getModel("globalM").setProperty("/BW_HOST_PORT", bwport);
				var tcodeLinkHostName = oModel.getData().TCODE_LINK_HOST;
				sap.ui.getCore().getModel("globalM").setProperty("/tcodeHostName", tcodeLinkHostName);

			}, this);
		},

		handoverFilters: function () {
			sap.ui.getCore().getModel("globalM").setProperty("/OverviewHandover", {});
			sap.ui.getCore().getModel("globalM").setProperty("/SupplyHandover", {});
			sap.ui.getCore().getModel("globalM").setProperty("/DemandHandover", {});
			sap.ui.getCore().getModel("globalM").setProperty("/SeasonHandover", {});
		},

		setInitialFields: function () {
			sap.ui.getCore().getModel("globalM").setProperty("/PageTitle", "");
		}

	};
});