sap.ui.define(["sap/ui/core/format/DateFormat"], function (DateFormat) {
	"use strict";

	return {
		//"2021-09-13
		getDetermineOrganic: function (jsonData) {
			if (!jsonData || jsonData === undefined || jsonData === "") {
				return "no";
			} else return "yes";
		},

		getDetermineHalal: function (jsonData) {
			if (!jsonData || jsonData === undefined || jsonData === "") {
				return "no";
			} else return "yes";
		},
		

		getDetermineKosher: function (jsonData) {
			if (!jsonData || jsonData === undefined || jsonData === "") {
				return "no";
			} else return "yes";
		},

		getLastUpdate: function (jsonDate) {
			if (!jsonDate || jsonDate === "00000000") {
				return "";
			}
			return jsonDate.split("-")[2] + "." + jsonDate.split("-")[1] + "." + jsonDate.split("-")[0];
		},

		getCounterStringFormat: function (val) {
			if (val === null || val === undefined || val === "") return "";
			else return parseInt(val).toString();

		},

		getDateStringFormat: function (jsonDate) {
			if (!jsonDate || jsonDate === "00000000") {
				return "";
			}

			return jsonDate.substring(6, 8) + "." + jsonDate.substring(4, 6) + "." + jsonDate.substring(0, 4);
		},

		getDateStringFormatConcat: function (jsonDate1, jsonDate2) {
			if (!jsonDate1 || jsonDate1 === "00000000") {
				jsonDate1 = "00000000";
			}

			if (!jsonDate2 || jsonDate2 === "00000000") {
				jsonDate2 = "00000000";
			}
			var date1 = jsonDate1.substring(6, 8) + "." + jsonDate1.substring(4, 6) + "." + jsonDate1.substring(0, 4);
			var date2 = jsonDate2.substring(6, 8) + "." + jsonDate2.substring(4, 6) + "." + jsonDate2.substring(0, 4);

			return date1 + "-" + date2;
		}
	};

});